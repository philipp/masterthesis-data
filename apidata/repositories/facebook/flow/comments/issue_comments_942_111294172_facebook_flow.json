[
{
  "url": "https://api.github.com/repos/facebook/flow/issues/comments/147889553",
  "html_url": "https://github.com/facebook/flow/issues/942#issuecomment-147889553",
  "issue_url": "https://api.github.com/repos/facebook/flow/issues/942",
  "id": 147889553,
  "user": {
    "login": "samwgoldman",
    "id": 254842,
    "avatar_url": "https://avatars.githubusercontent.com/u/254842?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/samwgoldman",
    "html_url": "https://github.com/samwgoldman",
    "followers_url": "https://api.github.com/users/samwgoldman/followers",
    "following_url": "https://api.github.com/users/samwgoldman/following{/other_user}",
    "gists_url": "https://api.github.com/users/samwgoldman/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/samwgoldman/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/samwgoldman/subscriptions",
    "organizations_url": "https://api.github.com/users/samwgoldman/orgs",
    "repos_url": "https://api.github.com/users/samwgoldman/repos",
    "events_url": "https://api.github.com/users/samwgoldman/events{/privacy}",
    "received_events_url": "https://api.github.com/users/samwgoldman/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-10-14T00:14:47Z",
  "updated_at": "2015-10-14T00:16:14Z",
  "body": "I have actually run into this one before myself. To shed some light here, the issue is that `Class` is actually a builtin type. Given a class, `A`, the type `Class<A>` is the type of classes that create instances of `A`.\r\n\r\nBecause this is a builtin type, when we see `Class` in a type annotation, Flow automatically assumes we mean the `Class<T>` builtin, hence the error message you got.\r\n\r\nI think this is 100% a bug in Flow, but I'm not sure the best way to fix it. Here are a couple options, definitely not a complete list:\r\n\r\n1. Consider the current scope when evaluating type aliases—if a type named Class is in scope, it should shadow the global builtin `Class<T>` type.\r\n2. Rename `Class<T>` to something else. This just moves the problem around, unless we can come up with a naming/scoping mechanism that keeps locally defined types separate from globally defined ones.\r\n3. Prevent people from creating local types that shadow global ones. Really, I'm listing this here more as a strawman. It's too easy to create a conflicting type name via class declaration and I see no reason to go this route."
}
, {
  "url": "https://api.github.com/repos/facebook/flow/issues/comments/148434760",
  "html_url": "https://github.com/facebook/flow/issues/942#issuecomment-148434760",
  "issue_url": "https://api.github.com/repos/facebook/flow/issues/942",
  "id": 148434760,
  "user": {
    "login": "gabelevi",
    "id": 1887264,
    "avatar_url": "https://avatars.githubusercontent.com/u/1887264?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/gabelevi",
    "html_url": "https://github.com/gabelevi",
    "followers_url": "https://api.github.com/users/gabelevi/followers",
    "following_url": "https://api.github.com/users/gabelevi/following{/other_user}",
    "gists_url": "https://api.github.com/users/gabelevi/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/gabelevi/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/gabelevi/subscriptions",
    "organizations_url": "https://api.github.com/users/gabelevi/orgs",
    "repos_url": "https://api.github.com/users/gabelevi/repos",
    "events_url": "https://api.github.com/users/gabelevi/events{/privacy}",
    "received_events_url": "https://api.github.com/users/gabelevi/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-10-15T16:01:40Z",
  "updated_at": "2015-10-15T16:01:40Z",
  "body": "We had a very long debate about this back in May. There were a few issues in contention:\r\n\r\n* We could have made the builtin type operator be called `$Class` instead of `Class`. However thus far the `$` was used for experimental things rather than just for reserving things for Flow.\r\n* How should builtin type operators be invoked? I was of the opinion that they should have a different syntax to differentiate them. For example, we do `typeof MyClass` rather than `Typeof<MyClass>`. We could have done something like `classof T` or `Class(T)` or really anything except reuse the generics syntax.\r\n* Should we let local variables shadow operators? We can pretty easily see that `Class` is defined locally and use that instead of the `Class` operator. Though this doesn't really work if there is a global variable named `Class`\r\n\r\nWe really should have fixed this back in May before people started using `Class<T>`. Le sigh.\r\n\r\nPersonally, I think this is what we should do:\r\n\r\n1. Type functions should be called using parens. With a single argument parens are optional. So things like `var a: typeof b`, `var c: class A`, `var d: $Diff(e, f)`\r\n2. `$` should be continued to be used for experimental-only\r\n3. We either stop supporting `Class<T>` immediately, or continue legacy support, using that if-locally-defined-do-the-right-thing trick. I think I'd rather make the breaking change, though."
}
, {
  "url": "https://api.github.com/repos/facebook/flow/issues/comments/149108149",
  "html_url": "https://github.com/facebook/flow/issues/942#issuecomment-149108149",
  "issue_url": "https://api.github.com/repos/facebook/flow/issues/942",
  "id": 149108149,
  "user": {
    "login": "deian",
    "id": 374012,
    "avatar_url": "https://avatars.githubusercontent.com/u/374012?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/deian",
    "html_url": "https://github.com/deian",
    "followers_url": "https://api.github.com/users/deian/followers",
    "following_url": "https://api.github.com/users/deian/following{/other_user}",
    "gists_url": "https://api.github.com/users/deian/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/deian/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/deian/subscriptions",
    "organizations_url": "https://api.github.com/users/deian/orgs",
    "repos_url": "https://api.github.com/users/deian/repos",
    "events_url": "https://api.github.com/users/deian/events{/privacy}",
    "received_events_url": "https://api.github.com/users/deian/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-10-19T05:47:12Z",
  "updated_at": "2015-10-19T05:47:12Z",
  "body": "+1 I think that is a pretty reasonable approach. I'm personally in favour of the breaking change. Though I like the if-locally-defined-do-the-right-thing trick, I suspect it may lead to some confusion.\r\n\r\nThanks both for looking into this!"
}
]
