[
{
  "url": "https://api.github.com/repos/facebook/immutable-js/issues/comments/67875966",
  "html_url": "https://github.com/facebook/immutable-js/issues/259#issuecomment-67875966",
  "issue_url": "https://api.github.com/repos/facebook/immutable-js/issues/259",
  "id": 67875966,
  "user": {
    "login": "tolmasky",
    "id": 23753,
    "avatar_url": "https://avatars.githubusercontent.com/u/23753?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/tolmasky",
    "html_url": "https://github.com/tolmasky",
    "followers_url": "https://api.github.com/users/tolmasky/followers",
    "following_url": "https://api.github.com/users/tolmasky/following{/other_user}",
    "gists_url": "https://api.github.com/users/tolmasky/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/tolmasky/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/tolmasky/subscriptions",
    "organizations_url": "https://api.github.com/users/tolmasky/orgs",
    "repos_url": "https://api.github.com/users/tolmasky/repos",
    "events_url": "https://api.github.com/users/tolmasky/events{/privacy}",
    "received_events_url": "https://api.github.com/users/tolmasky/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2014-12-22T19:04:51Z",
  "updated_at": "2014-12-22T19:06:08Z",
  "body": "From what I can tell, it seems that other languages solve this with lazy values (see: http://stackoverflow.com/questions/8374010/scala-circular-references-in-immutable-data-types ). immutable could do something like this:\r\n\r\n```javascript\r\nfunction Lazy(aFunction)\r\n{\r\n    this.get = memoize(aFunction);\r\n}\r\n```\r\nThen, in Map/List/etc's .get():\r\n\r\n```javascript\r\nget = function(key)\r\n{\r\n    value = /*current*/get(x);\r\n    if (value.constructor === Lazy)\r\n         return value.get();\r\n    return value;\r\n}\r\n```\r\nNow, we can do the following:\r\n\r\n```javascript\r\nvar recursive = I.Map().set(\"x\", Lazy(() => recursive));\r\nconsole.log(recursive === recursive.get(\"x\")) // prints true\r\n\r\n//We can also use it to make doubly linked lists:\r\n\r\nvar node0 = I.Map({ prev: null, next: Lazy(() => node1) })\r\nvar node1 = I.Map({ prev:  Lazy(() => node0), next: Lazy(() => node2) })\r\nvar node2 = I.Map({ prev: Lazy(() => node1), next: Lazy(() => node3) })\r\nvar node3 = I.Map({ prev: Lazy(() => node2), next: null })\r\n```\r\n"
}
, {
  "url": "https://api.github.com/repos/facebook/immutable-js/issues/comments/67899036",
  "html_url": "https://github.com/facebook/immutable-js/issues/259#issuecomment-67899036",
  "issue_url": "https://api.github.com/repos/facebook/immutable-js/issues/259",
  "id": 67899036,
  "user": {
    "login": "leebyron",
    "id": 50130,
    "avatar_url": "https://avatars.githubusercontent.com/u/50130?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/leebyron",
    "html_url": "https://github.com/leebyron",
    "followers_url": "https://api.github.com/users/leebyron/followers",
    "following_url": "https://api.github.com/users/leebyron/following{/other_user}",
    "gists_url": "https://api.github.com/users/leebyron/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/leebyron/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/leebyron/subscriptions",
    "organizations_url": "https://api.github.com/users/leebyron/orgs",
    "repos_url": "https://api.github.com/users/leebyron/repos",
    "events_url": "https://api.github.com/users/leebyron/events{/privacy}",
    "received_events_url": "https://api.github.com/users/leebyron/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2014-12-22T22:31:30Z",
  "updated_at": "2014-12-22T22:31:30Z",
  "body": "There is no way (intentionally) for immutable persistent data structures to contain circular references. Immutable structures can contain circular references, but in order to be persistent and take advantage of structural sharing, they need to be acyclic. The stack-overflow you link to illustrates this well - showing how a double-linked list needs to make a complete copy in order to perform an append operation.\r\n\r\nThere are some other problems that emerge from circular references in values, such as deep equality checking and hash-value computation.\r\n\r\nLanguages based on immutable structures handle this in different ways:\r\n\r\nScala has `lazy var` which is pretty much what you wrote up here. A deferred memoized function.\r\n\r\nClojure has `atom` which is a bit different (and easier to understand IMHO). Atom is an atomically locked mutable wrapper. Since JavaScript is single threaded, no locks are necessary so it's only a mutable wrapper. In other words: `{ value: \"foobar\" }`.\r\n\r\nFor equality and hashing, Atoms are treated as Objects not as Values. So `{ value: \"A\" } !== { value: \"A\" }`.\r\n\r\nSo you might treat this like:\r\n\r\n```js\r\nvar node0 = I.Map({ prev: null, next: {} })\r\nvar node1 = I.Map({ prev:  {value: node0}, next: {} })\r\nnode0.get('next').value = node1;\r\nvar node2 = I.Map({ prev: {value: node1}, next: {} })\r\nnode1.get('next').value = node2;\r\nvar node3 = I.Map({ prev: {value: node2}, next: null })\r\nnode2.get('next').value = node3;\r\n```\r\n\r\n---\r\n\r\nHowever, both approaches: lazy function or atom object are totally fine! This data structure library has no opinion on either, you just need to be aware of their use so you can treat them appropriately:\r\n\r\n```js\r\n// Lazy function\r\nnode3.get('prev').get();\r\n// Atom object\r\nnode3.get('prev').value;\r\n```"
}
, {
  "url": "https://api.github.com/repos/facebook/immutable-js/issues/comments/67899544",
  "html_url": "https://github.com/facebook/immutable-js/issues/259#issuecomment-67899544",
  "issue_url": "https://api.github.com/repos/facebook/immutable-js/issues/259",
  "id": 67899544,
  "user": {
    "login": "leebyron",
    "id": 50130,
    "avatar_url": "https://avatars.githubusercontent.com/u/50130?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/leebyron",
    "html_url": "https://github.com/leebyron",
    "followers_url": "https://api.github.com/users/leebyron/followers",
    "following_url": "https://api.github.com/users/leebyron/following{/other_user}",
    "gists_url": "https://api.github.com/users/leebyron/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/leebyron/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/leebyron/subscriptions",
    "organizations_url": "https://api.github.com/users/leebyron/orgs",
    "repos_url": "https://api.github.com/users/leebyron/repos",
    "events_url": "https://api.github.com/users/leebyron/events{/privacy}",
    "received_events_url": "https://api.github.com/users/leebyron/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2014-12-22T22:36:42Z",
  "updated_at": "2014-12-22T22:37:30Z",
  "body": "For those looking for an implementation of Lazy evaluated variables, try this:\r\n\r\n```js\r\nfunction lazy(fn) {\r\n  var didRun;\r\n  var result;\r\n  return { get: () => didRun ? result : (didRun = true, result = fn()) };\r\n}\r\n```\r\n\r\nUsage example\r\n\r\n```js\r\nvar lazyFoo = lazy(() => \"foo\");\r\nconsole.log(lazyFoo.get());\r\n```"
}
, {
  "url": "https://api.github.com/repos/facebook/immutable-js/issues/comments/67899904",
  "html_url": "https://github.com/facebook/immutable-js/issues/259#issuecomment-67899904",
  "issue_url": "https://api.github.com/repos/facebook/immutable-js/issues/259",
  "id": 67899904,
  "user": {
    "login": "leebyron",
    "id": 50130,
    "avatar_url": "https://avatars.githubusercontent.com/u/50130?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/leebyron",
    "html_url": "https://github.com/leebyron",
    "followers_url": "https://api.github.com/users/leebyron/followers",
    "following_url": "https://api.github.com/users/leebyron/following{/other_user}",
    "gists_url": "https://api.github.com/users/leebyron/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/leebyron/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/leebyron/subscriptions",
    "organizations_url": "https://api.github.com/users/leebyron/orgs",
    "repos_url": "https://api.github.com/users/leebyron/repos",
    "events_url": "https://api.github.com/users/leebyron/events{/privacy}",
    "received_events_url": "https://api.github.com/users/leebyron/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2014-12-22T22:40:04Z",
  "updated_at": "2014-12-22T22:40:04Z",
  "body": "I'm currently not sure baking this concept into the library, and changing the `get` definition to evaluate the lazy function is the right thing to do, as it would have pretty serious implications and possibly introduce bugs or at the very least confusion on equality checking and hash computation.\r\n\r\nAt this point I think it's reasonable to assume if you're using lazy values to determine for yourself when the lazy function should be evaluated."
}
, {
  "url": "https://api.github.com/repos/facebook/immutable-js/issues/comments/67901167",
  "html_url": "https://github.com/facebook/immutable-js/issues/259#issuecomment-67901167",
  "issue_url": "https://api.github.com/repos/facebook/immutable-js/issues/259",
  "id": 67901167,
  "user": {
    "login": "tolmasky",
    "id": 23753,
    "avatar_url": "https://avatars.githubusercontent.com/u/23753?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/tolmasky",
    "html_url": "https://github.com/tolmasky",
    "followers_url": "https://api.github.com/users/tolmasky/followers",
    "following_url": "https://api.github.com/users/tolmasky/following{/other_user}",
    "gists_url": "https://api.github.com/users/tolmasky/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/tolmasky/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/tolmasky/subscriptions",
    "organizations_url": "https://api.github.com/users/tolmasky/orgs",
    "repos_url": "https://api.github.com/users/tolmasky/repos",
    "events_url": "https://api.github.com/users/tolmasky/events{/privacy}",
    "received_events_url": "https://api.github.com/users/tolmasky/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2014-12-22T22:52:27Z",
  "updated_at": "2014-12-22T22:52:27Z",
  "body": "Yeah I suppose the hope is to have some sort of built in support for\r\neither, since if not it will never work with other people's code (they\r\ncan't know that some internal value is an atom or lazy, whereas with the\r\nlazy example I gave they don't need to know). Similarly in my own code I'd\r\nhave to check for .value/lazy everywhere I make an access, and thus\r\neffectively break getin/etc. For example, if someone had implemented\r\noneMore(object, keypath) { return object.getIn(keyPath) + 1; }, this will\r\nbreak with my circular structure : x= Map { a: 1, b: self }, oneMore(x,\r\n[“b”,”b”,”b”,”b”,”a’]), but would work with built in support for lazies.\r\n\r\nOn Monday, December 22, 2014, Lee Byron <notifications@github.com> wrote:\r\n\r\n> There is no way (intentionally) for immutable persistent data structures\r\n> to contain circular references. Immutable structures can contain circular\r\n> references, but in order to be persistent and take advantage of structural\r\n> sharing, they need to be acyclic. The stack-overflow you link to\r\n> illustrates this well - showing how a double-linked list needs to make a\r\n> complete copy in order to perform an append operation.\r\n>\r\n> There are some other problems that emerge from circular references in\r\n> values, such as deep equality checking and hash-value computation.\r\n>\r\n> Languages based on immutable structures handle this in different ways:\r\n>\r\n> Scala has lazy var which is pretty much what you wrote up here. A\r\n> deferred memoized function.\r\n>\r\n> Clojure has atom which is a bit different (and easier to understand\r\n> IMHO). Atom is an atomically locked mutable wrapper. Since JavaScript is\r\n> single threaded, no locks are necessary so it's only a mutable wrapper. In\r\n> other words: { value: \"foobar\" }.\r\n>\r\n> For equality and hashing, Atoms are treated as Objects not as Values. So {\r\n> value: \"A\" } !== { value: \"A\" }.\r\n>\r\n> So you might treat this like:\r\n>\r\n> var node0 = I.Map({ prev: null, next: {} })var node1 = I.Map({ prev:  {value: node0}, next: {} })\r\n> node0.get('next').value = node1;var node2 = I.Map({ prev: {value: node1}, next: {} })\r\n> node1.get('next').value = node2;var node3 = I.Map({ prev: {value: node2}, next: null })\r\n> node2.get('next').value = node3;\r\n>\r\n> ------------------------------\r\n>\r\n> However, both approaches: lazy function or atom object are totally fine!\r\n> This data structure library has no opinion on either, you just need to be\r\n> aware of their use so you can treat them appropriately:\r\n>\r\n> // Lazy function\r\n> node3.get('prev').get();// Atom object\r\n> node3.get('prev').value;\r\n>\r\n> —\r\n> Reply to this email directly or view it on GitHub\r\n> <https://github.com/facebook/immutable-js/issues/259#issuecomment-67899036>\r\n> .\r\n>"
}
, {
  "url": "https://api.github.com/repos/facebook/immutable-js/issues/comments/67901484",
  "html_url": "https://github.com/facebook/immutable-js/issues/259#issuecomment-67901484",
  "issue_url": "https://api.github.com/repos/facebook/immutable-js/issues/259",
  "id": 67901484,
  "user": {
    "login": "leebyron",
    "id": 50130,
    "avatar_url": "https://avatars.githubusercontent.com/u/50130?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/leebyron",
    "html_url": "https://github.com/leebyron",
    "followers_url": "https://api.github.com/users/leebyron/followers",
    "following_url": "https://api.github.com/users/leebyron/following{/other_user}",
    "gists_url": "https://api.github.com/users/leebyron/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/leebyron/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/leebyron/subscriptions",
    "organizations_url": "https://api.github.com/users/leebyron/orgs",
    "repos_url": "https://api.github.com/users/leebyron/repos",
    "events_url": "https://api.github.com/users/leebyron/events{/privacy}",
    "received_events_url": "https://api.github.com/users/leebyron/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2014-12-22T22:56:12Z",
  "updated_at": "2014-12-22T22:56:12Z",
  "body": "I'll definitely considering adding true support for lazy values in the future, but I would prefer to see such a thing used outside the library first to better understand the implications before baking it in.\r\n\r\nIf you end up patching Immutable to add this functionality, I would be really interested in hearing about the unexpected issues and opportunities you run into."
}
, {
  "url": "https://api.github.com/repos/facebook/immutable-js/issues/comments/72330286",
  "html_url": "https://github.com/facebook/immutable-js/issues/259#issuecomment-72330286",
  "issue_url": "https://api.github.com/repos/facebook/immutable-js/issues/259",
  "id": 72330286,
  "user": {
    "login": "tolmasky",
    "id": 23753,
    "avatar_url": "https://avatars.githubusercontent.com/u/23753?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/tolmasky",
    "html_url": "https://github.com/tolmasky",
    "followers_url": "https://api.github.com/users/tolmasky/followers",
    "following_url": "https://api.github.com/users/tolmasky/following{/other_user}",
    "gists_url": "https://api.github.com/users/tolmasky/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/tolmasky/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/tolmasky/subscriptions",
    "organizations_url": "https://api.github.com/users/tolmasky/orgs",
    "repos_url": "https://api.github.com/users/tolmasky/repos",
    "events_url": "https://api.github.com/users/tolmasky/events{/privacy}",
    "received_events_url": "https://api.github.com/users/tolmasky/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-01-31T18:46:23Z",
  "updated_at": "2015-01-31T18:46:23Z",
  "body": "So, interestingly enough, I was able to accomplish this using withMutations: https://gist.github.com/tolmasky/8a91af5ac1e0a949b423\r\n\r\nNow the only issue is that toString borks and toJS bork (these are much easier to fix). Would you be opposed to having an implementation of toString and toJS that account for circular references (just like log you'd see something like Map { a: [Circular] }). I'm happy to just have them in my code of course, but given that this is \"possible\", I figured you might want it in mainline as well."
}
, {
  "url": "https://api.github.com/repos/facebook/immutable-js/issues/comments/72331182",
  "html_url": "https://github.com/facebook/immutable-js/issues/259#issuecomment-72331182",
  "issue_url": "https://api.github.com/repos/facebook/immutable-js/issues/259",
  "id": 72331182,
  "user": {
    "login": "tolmasky",
    "id": 23753,
    "avatar_url": "https://avatars.githubusercontent.com/u/23753?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/tolmasky",
    "html_url": "https://github.com/tolmasky",
    "followers_url": "https://api.github.com/users/tolmasky/followers",
    "following_url": "https://api.github.com/users/tolmasky/following{/other_user}",
    "gists_url": "https://api.github.com/users/tolmasky/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/tolmasky/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/tolmasky/subscriptions",
    "organizations_url": "https://api.github.com/users/tolmasky/orgs",
    "repos_url": "https://api.github.com/users/tolmasky/repos",
    "events_url": "https://api.github.com/users/tolmasky/events{/privacy}",
    "received_events_url": "https://api.github.com/users/tolmasky/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-01-31T19:07:27Z",
  "updated_at": "2015-01-31T19:07:27Z",
  "body": "I've filed this related issue which deals with the same reference in objects (not necessarily circular): #305 "
}
, {
  "url": "https://api.github.com/repos/facebook/immutable-js/issues/comments/72340771",
  "html_url": "https://github.com/facebook/immutable-js/issues/259#issuecomment-72340771",
  "issue_url": "https://api.github.com/repos/facebook/immutable-js/issues/259",
  "id": 72340771,
  "user": {
    "login": "leebyron",
    "id": 50130,
    "avatar_url": "https://avatars.githubusercontent.com/u/50130?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/leebyron",
    "html_url": "https://github.com/leebyron",
    "followers_url": "https://api.github.com/users/leebyron/followers",
    "following_url": "https://api.github.com/users/leebyron/following{/other_user}",
    "gists_url": "https://api.github.com/users/leebyron/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/leebyron/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/leebyron/subscriptions",
    "organizations_url": "https://api.github.com/users/leebyron/orgs",
    "repos_url": "https://api.github.com/users/leebyron/repos",
    "events_url": "https://api.github.com/users/leebyron/events{/privacy}",
    "received_events_url": "https://api.github.com/users/leebyron/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-01-31T22:45:57Z",
  "updated_at": "2015-01-31T22:45:57Z",
  "body": "Circular references break a lot of promises that immutable data provides, so I don't think I plan on accounting for them. \r\n\r\nYou can read a more lengthy explanation of the issues with circular references via manual mutation written up in the \"transients\" section of the clojure documentation. "
}
, {
  "url": "https://api.github.com/repos/facebook/immutable-js/issues/comments/113251771",
  "html_url": "https://github.com/facebook/immutable-js/issues/259#issuecomment-113251771",
  "issue_url": "https://api.github.com/repos/facebook/immutable-js/issues/259",
  "id": 113251771,
  "user": {
    "login": "leebyron",
    "id": 50130,
    "avatar_url": "https://avatars.githubusercontent.com/u/50130?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/leebyron",
    "html_url": "https://github.com/leebyron",
    "followers_url": "https://api.github.com/users/leebyron/followers",
    "following_url": "https://api.github.com/users/leebyron/following{/other_user}",
    "gists_url": "https://api.github.com/users/leebyron/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/leebyron/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/leebyron/subscriptions",
    "organizations_url": "https://api.github.com/users/leebyron/orgs",
    "repos_url": "https://api.github.com/users/leebyron/repos",
    "events_url": "https://api.github.com/users/leebyron/events{/privacy}",
    "received_events_url": "https://api.github.com/users/leebyron/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-06-18T18:32:34Z",
  "updated_at": "2015-06-18T18:32:34Z",
  "body": "Closing this for now - I think this is going to be really hard to do without language features."
}
]
