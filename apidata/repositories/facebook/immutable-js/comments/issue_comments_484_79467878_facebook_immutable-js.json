[
{
  "url": "https://api.github.com/repos/facebook/immutable-js/issues/comments/106587394",
  "html_url": "https://github.com/facebook/immutable-js/issues/484#issuecomment-106587394",
  "issue_url": "https://api.github.com/repos/facebook/immutable-js/issues/484",
  "id": 106587394,
  "user": {
    "login": "leebyron",
    "id": 50130,
    "avatar_url": "https://avatars.githubusercontent.com/u/50130?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/leebyron",
    "html_url": "https://github.com/leebyron",
    "followers_url": "https://api.github.com/users/leebyron/followers",
    "following_url": "https://api.github.com/users/leebyron/following{/other_user}",
    "gists_url": "https://api.github.com/users/leebyron/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/leebyron/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/leebyron/subscriptions",
    "organizations_url": "https://api.github.com/users/leebyron/orgs",
    "repos_url": "https://api.github.com/users/leebyron/repos",
    "events_url": "https://api.github.com/users/leebyron/events{/privacy}",
    "received_events_url": "https://api.github.com/users/leebyron/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-05-28T20:32:41Z",
  "updated_at": "2015-05-28T20:33:00Z",
  "body": "This is about the constant time speed difference I would expect. Native Array is full of performance optimizations and relies on mutability which will always beat immutable data in raw-perf numbers. On average Immutable.js operations are a constant time 5-15x slower than native operations, depending on the operation. In part due to their being implemented directly in JS rather than in the VM, but also because the operations themselves are more involved.\r\n\r\nThe performance wins (both time and memory) come from opportunities to use structural sharing between operations, which unfortunately `concat` currently does not provide."
}
, {
  "url": "https://api.github.com/repos/facebook/immutable-js/issues/comments/106613216",
  "html_url": "https://github.com/facebook/immutable-js/issues/484#issuecomment-106613216",
  "issue_url": "https://api.github.com/repos/facebook/immutable-js/issues/484",
  "id": 106613216,
  "user": {
    "login": "xixixao",
    "id": 1473433,
    "avatar_url": "https://avatars.githubusercontent.com/u/1473433?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/xixixao",
    "html_url": "https://github.com/xixixao",
    "followers_url": "https://api.github.com/users/xixixao/followers",
    "following_url": "https://api.github.com/users/xixixao/following{/other_user}",
    "gists_url": "https://api.github.com/users/xixixao/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/xixixao/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/xixixao/subscriptions",
    "organizations_url": "https://api.github.com/users/xixixao/orgs",
    "repos_url": "https://api.github.com/users/xixixao/repos",
    "events_url": "https://api.github.com/users/xixixao/events{/privacy}",
    "received_events_url": "https://api.github.com/users/xixixao/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-05-28T22:03:04Z",
  "updated_at": "2015-05-28T22:03:04Z",
  "body": "Thanks for the answer. I imagine that concat would be a good candidate for structural sharing?\r\n\r\nI also wonder whether it would make sense to use native data structures for backing small collections and only switch to tries when the collections hit a certain size. I would also love to know how @swannodette's mori is doing in comparison (again, in general). \r\n\r\nPerhaps the answer for my use case is to switch to laziness, which unfortunately became more difficult with Immutable 3..."
}
, {
  "url": "https://api.github.com/repos/facebook/immutable-js/issues/comments/106641757",
  "html_url": "https://github.com/facebook/immutable-js/issues/484#issuecomment-106641757",
  "issue_url": "https://api.github.com/repos/facebook/immutable-js/issues/484",
  "id": 106641757,
  "user": {
    "login": "leebyron",
    "id": 50130,
    "avatar_url": "https://avatars.githubusercontent.com/u/50130?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/leebyron",
    "html_url": "https://github.com/leebyron",
    "followers_url": "https://api.github.com/users/leebyron/followers",
    "following_url": "https://api.github.com/users/leebyron/following{/other_user}",
    "gists_url": "https://api.github.com/users/leebyron/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/leebyron/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/leebyron/subscriptions",
    "organizations_url": "https://api.github.com/users/leebyron/orgs",
    "repos_url": "https://api.github.com/users/leebyron/repos",
    "events_url": "https://api.github.com/users/leebyron/events{/privacy}",
    "received_events_url": "https://api.github.com/users/leebyron/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-05-29T00:41:12Z",
  "updated_at": "2015-05-29T00:41:55Z",
  "body": "You're correct that `concat` is a good candidate for structural sharing, and in fact there is pretty great research on how to do this - #38 is tracking this enhancement that I'm excited to have in the future.\r\n\r\nThe stock `flatten` implementation does in fact use laziness and that's likely to win back a lot of performance for this scenario.\r\n\r\n```js\r\n// Immutable.JS flatten\r\nxs = Immutable.Range(0, 700).toList();\r\nitem = Immutable.Range(0, 50).toList();\r\nbigList = xs.map(function (x) {return item});\r\nt = +new Date();\r\nbigList.flatten();\r\nconsole.log(new Date() - t);\r\n\r\n// Persistent non-lazy native Array flatten\r\nxs = Array.apply(null, Array(700)).map(function (_, i) {return i;});\r\nitem = Array.apply(null, Array(50)).map(function (_, i) {return i;});\r\nbigList = xs.map(function (x) { return item; });\r\nt = +new Date();\r\nbigList.reduce(function(x, v) { return x.concat(v) }, []);\r\nconsole.log(new Date() - t);\r\n```\r\n\r\nOn my machine, the non-lazy raw Array flatten (your original code) ran in 33ms, but Immutable.js's `flatten` method ran in 20ms due to utilizing laziness under the hood - it avoids building the intermediate data structures.\r\n\r\nmori uses many of the same techniques as Immutable.js (in fact, Immutable.js was inspired by ClojureScript's data structures) and has a similar performance profile, but also has undergone a lot more performance tuning and so that constant time slow down relative to mutable native arrays is more like 4-8x for mori (depending on the operation). ClojureScript (and mori) also do not yet feature RRB trees for enabling structural sharing in the concat case as far as I know."
}
, {
  "url": "https://api.github.com/repos/facebook/immutable-js/issues/comments/107249595",
  "html_url": "https://github.com/facebook/immutable-js/issues/484#issuecomment-107249595",
  "issue_url": "https://api.github.com/repos/facebook/immutable-js/issues/484",
  "id": 107249595,
  "user": {
    "login": "xixixao",
    "id": 1473433,
    "avatar_url": "https://avatars.githubusercontent.com/u/1473433?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/xixixao",
    "html_url": "https://github.com/xixixao",
    "followers_url": "https://api.github.com/users/xixixao/followers",
    "following_url": "https://api.github.com/users/xixixao/following{/other_user}",
    "gists_url": "https://api.github.com/users/xixixao/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/xixixao/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/xixixao/subscriptions",
    "organizations_url": "https://api.github.com/users/xixixao/orgs",
    "repos_url": "https://api.github.com/users/xixixao/repos",
    "events_url": "https://api.github.com/users/xixixao/events{/privacy}",
    "received_events_url": "https://api.github.com/users/xixixao/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-05-31T21:45:44Z",
  "updated_at": "2015-05-31T21:47:05Z",
  "body": "For future reference, I was obviously using `flatten` completely wrong, since what I wanted was Haskell's `concat`, so the fast way to do this is:\r\n```javascript\r\nbigList.first().concat.apply(bigList.first(), bigList.rest().toArray());\r\n```\r\nprovided the outer collection isn't empty (which is incidentally 10 times faster than flatten)."
}
]
