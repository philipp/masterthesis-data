[
{
  "url": "https://api.github.com/repos/facebook/osquery/issues/comments/106988257",
  "html_url": "https://github.com/facebook/osquery/issues/1173#issuecomment-106988257",
  "issue_url": "https://api.github.com/repos/facebook/osquery/issues/1173",
  "id": 106988257,
  "user": {
    "login": "theopolis",
    "id": 981645,
    "avatar_url": "https://avatars.githubusercontent.com/u/981645?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/theopolis",
    "html_url": "https://github.com/theopolis",
    "followers_url": "https://api.github.com/users/theopolis/followers",
    "following_url": "https://api.github.com/users/theopolis/following{/other_user}",
    "gists_url": "https://api.github.com/users/theopolis/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/theopolis/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/theopolis/subscriptions",
    "organizations_url": "https://api.github.com/users/theopolis/orgs",
    "repos_url": "https://api.github.com/users/theopolis/repos",
    "events_url": "https://api.github.com/users/theopolis/events{/privacy}",
    "received_events_url": "https://api.github.com/users/theopolis/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-05-30T04:36:43Z",
  "updated_at": "2015-05-30T04:36:43Z",
  "body": "Hey @skottler, good questions. We don't mind duplicating data in tables, normalization doesn't really mean anything in this case since we're mostly force fitting concepts into tabular form. But optimizing for obvious foreign keys (like pid or interface name) is supported and encouraged. Then we can separate concepts like process metadata and various tabular-esqe process supplemental information like environment variables, open sockets, file descriptors, memory maps, etc. Using that [little bit of optimization](https://github.com/facebook/osquery/blob/master/osquery/tables/system/linux/processes.cpp#L265), `JOIN`s make sense!\r\n\r\nTo represent bonds we could definitely use interface names as a foreign key.\r\n\r\nI'm **pretty** sure there's a 1-1 relationship between bond and interface? I can't put an interface into multiple bonds, right?\r\n\r\nI'd vote for an `interface_bonds` table and the primary key is the bond name and columns are bond stats/metadata. Then we add a `bond` column to `interface_details`, as a foreign key to `interface_bonds`."
}
, {
  "url": "https://api.github.com/repos/facebook/osquery/issues/comments/106991291",
  "html_url": "https://github.com/facebook/osquery/issues/1173#issuecomment-106991291",
  "issue_url": "https://api.github.com/repos/facebook/osquery/issues/1173",
  "id": 106991291,
  "user": {
    "login": "skottler",
    "id": 579876,
    "avatar_url": "https://avatars.githubusercontent.com/u/579876?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/skottler",
    "html_url": "https://github.com/skottler",
    "followers_url": "https://api.github.com/users/skottler/followers",
    "following_url": "https://api.github.com/users/skottler/following{/other_user}",
    "gists_url": "https://api.github.com/users/skottler/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/skottler/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/skottler/subscriptions",
    "organizations_url": "https://api.github.com/users/skottler/orgs",
    "repos_url": "https://api.github.com/users/skottler/repos",
    "events_url": "https://api.github.com/users/skottler/events{/privacy}",
    "received_events_url": "https://api.github.com/users/skottler/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-05-30T05:27:24Z",
  "updated_at": "2015-05-30T05:27:45Z",
  "body": "> I'm pretty sure there's a 1-1 relationship between bond and interface? I can't put an interface into multiple bonds, right?\r\n\r\nIn the case of the `bond-mode` being `active-passive` there is a 1-1 relationship between the `bond-master` and interface, there's only a single master with potentially several slaves. For example (with some addresses redacted):\r\n\r\n```\r\nauto lo\r\niface lo inet loopback\r\n\r\nauto eth0\r\niface eth0 inet manual\r\n        bond-master bond0\r\n        bond-primary eth0\r\n\r\nauto eth1\r\niface eth1 inet manual\r\n        bond-master bond0\r\n\r\nauto bond0\r\niface bond0 inet manual\r\n        bond-mode active-backup\r\n        bond-miimon 100\r\n        bond-slaves none\r\n        address 10.32.1.1\r\n        netmask 255.255.0.0\r\n        gateway 10.32.0.1\r\n```\r\n\r\n`/proc/net/bonding/bond0` then has the following state about the bond:\r\n\r\n```\r\nBonding Mode: fault-tolerance (active-backup)\r\nPrimary Slave: eth0 (primary_reselect always)\r\nCurrently Active Slave: eth0\r\nMII Status: up\r\nMII Polling Interval (ms): 100\r\nUp Delay (ms): 0\r\nDown Delay (ms): 0\r\n\r\nSlave Interface: eth0\r\nMII Status: up\r\nSpeed: 40000 Mbps\r\nDuplex: full\r\nLink Failure Count: 0\r\nPermanent HW addr: 00:00:00:00:00:00\r\nSlave queue ID: 0\r\n\r\nSlave Interface: eth1\r\nMII Status: up\r\nSpeed: 40000 Mbps\r\nDuplex: full\r\nLink Failure Count: 1\r\nPermanent HW addr: 00:00:00:00:00:00\r\nSlave queue ID: 0\r\n```\r\n\r\nOf course the MAC and other details on the interfaces which are part of the bond are already in `interface_details`, but things like `Currently Active Slave`, `MII Status`, or `Link Failure Count` would be useful to exist in the potentially new `interface_bonds` table. This seems to be a reasonable way forward, I'm gonna start working on a pull request for this.\r\n\r\nThanks and have a great weekend!"
}
, {
  "url": "https://api.github.com/repos/facebook/osquery/issues/comments/107008710",
  "html_url": "https://github.com/facebook/osquery/issues/1173#issuecomment-107008710",
  "issue_url": "https://api.github.com/repos/facebook/osquery/issues/1173",
  "id": 107008710,
  "user": {
    "login": "theopolis",
    "id": 981645,
    "avatar_url": "https://avatars.githubusercontent.com/u/981645?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/theopolis",
    "html_url": "https://github.com/theopolis",
    "followers_url": "https://api.github.com/users/theopolis/followers",
    "following_url": "https://api.github.com/users/theopolis/following{/other_user}",
    "gists_url": "https://api.github.com/users/theopolis/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/theopolis/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/theopolis/subscriptions",
    "organizations_url": "https://api.github.com/users/theopolis/orgs",
    "repos_url": "https://api.github.com/users/theopolis/repos",
    "events_url": "https://api.github.com/users/theopolis/events{/privacy}",
    "received_events_url": "https://api.github.com/users/theopolis/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-05-30T08:26:45Z",
  "updated_at": "2015-05-30T08:26:45Z",
  "body": "Perfect! I could see expressing that info in an `interface_bonds` table like:\r\n\r\n| bond | interface | mode | mii_status | speed | duplex | link_failures | queue_id | primary |\r\n|---------|-------------|---------|---------------|----------|-----------|-----------------|--------------|------------|\r\n| bond0 | eth0 | active-backup | up | 40000 | full | 0 | 0 | 1 |\r\n| bond0 | eth1 | active-backup | up | 40000 | full | 1 | 1 | 0 |\r\n\r\n(The `primary` boolean column goes off the right side of the github-comment, you'll need to horizontal scroll.)\r\n\r\nThere a bit of repeated information, but that's no big deal. You can see with a simple schedule of the bond interfaces table (`SELECT * FROM interface_bonds`) whenever interfaces leave/join any bond, and when status/counts change.\r\n\r\nThanks @skottler, you do the same, and definitely looking forward to the PR! :dancers: "
}
, {
  "url": "https://api.github.com/repos/facebook/osquery/issues/comments/113824370",
  "html_url": "https://github.com/facebook/osquery/issues/1173#issuecomment-113824370",
  "issue_url": "https://api.github.com/repos/facebook/osquery/issues/1173",
  "id": 113824370,
  "user": {
    "login": "marpaia",
    "id": 927168,
    "avatar_url": "https://avatars.githubusercontent.com/u/927168?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/marpaia",
    "html_url": "https://github.com/marpaia",
    "followers_url": "https://api.github.com/users/marpaia/followers",
    "following_url": "https://api.github.com/users/marpaia/following{/other_user}",
    "gists_url": "https://api.github.com/users/marpaia/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/marpaia/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/marpaia/subscriptions",
    "organizations_url": "https://api.github.com/users/marpaia/orgs",
    "repos_url": "https://api.github.com/users/marpaia/repos",
    "events_url": "https://api.github.com/users/marpaia/events{/privacy}",
    "received_events_url": "https://api.github.com/users/marpaia/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-06-20T21:40:37Z",
  "updated_at": "2015-06-20T21:40:37Z",
  "body": "I added this to the list in #619 with a reference to this issue. I'm closing this, so that it doesn't show up in the open issues list, but conversation on the topic should still occur here."
}
]
