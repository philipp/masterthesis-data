[
{
  "url": "https://api.github.com/repos/facebook/react/issues/comments/68691853",
  "html_url": "https://github.com/facebook/react/pull/2805#issuecomment-68691853",
  "issue_url": "https://api.github.com/repos/facebook/react/issues/2805",
  "id": 68691853,
  "user": {
    "login": "glenjamin",
    "id": 151272,
    "avatar_url": "https://avatars.githubusercontent.com/u/151272?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/glenjamin",
    "html_url": "https://github.com/glenjamin",
    "followers_url": "https://api.github.com/users/glenjamin/followers",
    "following_url": "https://api.github.com/users/glenjamin/following{/other_user}",
    "gists_url": "https://api.github.com/users/glenjamin/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/glenjamin/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/glenjamin/subscriptions",
    "organizations_url": "https://api.github.com/users/glenjamin/orgs",
    "repos_url": "https://api.github.com/users/glenjamin/repos",
    "events_url": "https://api.github.com/users/glenjamin/events{/privacy}",
    "received_events_url": "https://api.github.com/users/glenjamin/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-01-05T10:38:51Z",
  "updated_at": "2015-01-05T10:38:51Z",
  "body": "With replaceState being deprecated, is it intentional to prohibit state from being something other than a plain JS object?\r\n\r\nI've seen a bunch of examples where an Immutable.Map is used directly, but I don't think that works if only setState exists (and merges)."
}
, {
  "url": "https://api.github.com/repos/facebook/react/issues/comments/68723101",
  "html_url": "https://github.com/facebook/react/pull/2805#issuecomment-68723101",
  "issue_url": "https://api.github.com/repos/facebook/react/issues/2805",
  "id": 68723101,
  "user": {
    "login": "sebmarkbage",
    "id": 63648,
    "avatar_url": "https://avatars.githubusercontent.com/u/63648?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/sebmarkbage",
    "html_url": "https://github.com/sebmarkbage",
    "followers_url": "https://api.github.com/users/sebmarkbage/followers",
    "following_url": "https://api.github.com/users/sebmarkbage/following{/other_user}",
    "gists_url": "https://api.github.com/users/sebmarkbage/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/sebmarkbage/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/sebmarkbage/subscriptions",
    "organizations_url": "https://api.github.com/users/sebmarkbage/orgs",
    "repos_url": "https://api.github.com/users/sebmarkbage/repos",
    "events_url": "https://api.github.com/users/sebmarkbage/events{/privacy}",
    "received_events_url": "https://api.github.com/users/sebmarkbage/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-01-05T15:28:23Z",
  "updated_at": "2015-01-05T15:28:23Z",
  "body": "The idea is to make state a record-like object with fixed keys. E.g. you should specify all keys in the initial state and never add extra keys. It ensures type stability both for type systems and VMs. It also ensures that you explicitly define an initial and descriptive value.\r\n\r\nWe could potentially support arbitrary state objects, but props is also always an object (could also become a record). The idea is that these objects act as named arguments. That way it's always easy to add and remove new arguments. I think it's valuable to keep that consistency through out components.\r\n\r\nYou can always put anything else one level below it. `Immutable.Map` isn't ideal as the primary state object since it's dynamic. Something like `Immutable.Record` is closer to what I had in mind. We could potentially support `Immutable.Record` natively.\r\n\r\nThese changes are not set in stone. The reason I'm not supporting them __yet__ is to make this new API more restrictive to ensure some kind of insurance of future compatibility. Please, feel free to describe your use case with some real world examples.\r\n\r\nWe're playing around with updaters returning state in which case it would probably auto-merge: https://github.com/reactjs/react-future/blob/master/07%20-%20Returning%20State/01%20-%20Stateful%20Functions.js#L17"
}
, {
  "url": "https://api.github.com/repos/facebook/react/issues/comments/69625568",
  "html_url": "https://github.com/facebook/react/pull/2805#issuecomment-69625568",
  "issue_url": "https://api.github.com/repos/facebook/react/issues/2805",
  "id": 69625568,
  "user": {
    "login": "glenjamin",
    "id": 151272,
    "avatar_url": "https://avatars.githubusercontent.com/u/151272?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/glenjamin",
    "html_url": "https://github.com/glenjamin",
    "followers_url": "https://api.github.com/users/glenjamin/followers",
    "following_url": "https://api.github.com/users/glenjamin/following{/other_user}",
    "gists_url": "https://api.github.com/users/glenjamin/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/glenjamin/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/glenjamin/subscriptions",
    "organizations_url": "https://api.github.com/users/glenjamin/orgs",
    "repos_url": "https://api.github.com/users/glenjamin/repos",
    "events_url": "https://api.github.com/users/glenjamin/events{/privacy}",
    "received_events_url": "https://api.github.com/users/glenjamin/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-01-12T19:05:21Z",
  "updated_at": "2015-01-12T19:05:21Z",
  "body": "I think if there's an improved API for accessing _pendingState, or a way to define custom setState merge behaviour, no functionality is lost.\r\n\r\nIf you only have setState, and want to use some custom flavour of collection in a similar way to how setState works (different handlers updating different keys), then it's quite tricky to ensure clean updates.\r\n\r\nI've seen code like this in a few demos:\r\n\r\n```js\r\nthis.replaceState((this._pendingState || this.state).merge({some: changes}));\r\n```\r\n\r\nSupporting Record might be neat, but if going down that route I think some sort of protocol that other libs can implement would be preferrable."
}
, {
  "url": "https://api.github.com/repos/facebook/react/issues/comments/69628277",
  "html_url": "https://github.com/facebook/react/pull/2805#issuecomment-69628277",
  "issue_url": "https://api.github.com/repos/facebook/react/issues/2805",
  "id": 69628277,
  "user": {
    "login": "sebmarkbage",
    "id": 63648,
    "avatar_url": "https://avatars.githubusercontent.com/u/63648?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/sebmarkbage",
    "html_url": "https://github.com/sebmarkbage",
    "followers_url": "https://api.github.com/users/sebmarkbage/followers",
    "following_url": "https://api.github.com/users/sebmarkbage/following{/other_user}",
    "gists_url": "https://api.github.com/users/sebmarkbage/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/sebmarkbage/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/sebmarkbage/subscriptions",
    "organizations_url": "https://api.github.com/users/sebmarkbage/orgs",
    "repos_url": "https://api.github.com/users/sebmarkbage/repos",
    "events_url": "https://api.github.com/users/sebmarkbage/events{/privacy}",
    "received_events_url": "https://api.github.com/users/sebmarkbage/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-01-12T19:20:46Z",
  "updated_at": "2015-01-12T19:20:46Z",
  "body": "Let's move the replaceState discussion to #2843 since it's not directly related to this pull request."
}
]
