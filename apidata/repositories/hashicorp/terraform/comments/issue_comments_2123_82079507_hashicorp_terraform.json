[
{
  "url": "https://api.github.com/repos/hashicorp/terraform/issues/comments/106799913",
  "html_url": "https://github.com/hashicorp/terraform/issues/2123#issuecomment-106799913",
  "issue_url": "https://api.github.com/repos/hashicorp/terraform/issues/2123",
  "id": 106799913,
  "user": {
    "login": "matthewrees",
    "id": 3985321,
    "avatar_url": "https://avatars.githubusercontent.com/u/3985321?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/matthewrees",
    "html_url": "https://github.com/matthewrees",
    "followers_url": "https://api.github.com/users/matthewrees/followers",
    "following_url": "https://api.github.com/users/matthewrees/following{/other_user}",
    "gists_url": "https://api.github.com/users/matthewrees/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/matthewrees/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/matthewrees/subscriptions",
    "organizations_url": "https://api.github.com/users/matthewrees/orgs",
    "repos_url": "https://api.github.com/users/matthewrees/repos",
    "events_url": "https://api.github.com/users/matthewrees/events{/privacy}",
    "received_events_url": "https://api.github.com/users/matthewrees/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-05-29T13:16:13Z",
  "updated_at": "2015-05-29T13:16:13Z",
  "body": "Simply put, this would be amazing.\r\n\r\nI have a quite a few use cases where I could simplify my infrastructure configuration considerably if I was able to reference the instances of an ASG, and there is no way to reference them or their attributes in CloudFormation either.\r\n\r\nMassive +1 from me for this request!"
}
, {
  "url": "https://api.github.com/repos/hashicorp/terraform/issues/comments/106839251",
  "html_url": "https://github.com/hashicorp/terraform/issues/2123#issuecomment-106839251",
  "issue_url": "https://api.github.com/repos/hashicorp/terraform/issues/2123",
  "id": 106839251,
  "user": {
    "login": "phinze",
    "id": 37534,
    "avatar_url": "https://avatars.githubusercontent.com/u/37534?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/phinze",
    "html_url": "https://github.com/phinze",
    "followers_url": "https://api.github.com/users/phinze/followers",
    "following_url": "https://api.github.com/users/phinze/following{/other_user}",
    "gists_url": "https://api.github.com/users/phinze/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/phinze/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/phinze/subscriptions",
    "organizations_url": "https://api.github.com/users/phinze/orgs",
    "repos_url": "https://api.github.com/users/phinze/repos",
    "events_url": "https://api.github.com/users/phinze/events{/privacy}",
    "received_events_url": "https://api.github.com/users/phinze/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-05-29T14:58:48Z",
  "updated_at": "2015-05-29T14:58:48Z",
  "body": "I understand the desire for this, but there's a bit of an impedance mismatch that prevents us from integrating ASG instances into Terraform. The problem is: the set of instances in an ASG can change at any time.\r\n\r\nSo picture this hypothetical feature:\r\n\r\n```\r\nvariable \"asg_size\" { }\r\nresource \"aws_autoscaling_group\" \"foo\" {\r\n  // ...\r\n  desired_capacity = \"${var.asg_size}\"\r\n}\r\nresource \"aws_route53_record\" {\r\n  type = \"A\"\r\n  count = \"${var.asg_size}\"\r\n  name = \"asg-instance-${count.index}.myapp.com\"\r\n  value = \"${element(aws_autoscaling_group.foo.instances.*.public_ip, count.index)}\"\r\n}\r\n```\r\n\r\nThat might theoretically work for the first set of instances launched into the ASG, but what should happen when the set changes - when an instance is replaced or the group scales up or down? Terraform's not currently designed to deal with scenarios like that, so exposing that information in the config directly could lead users into building configs that exhibit confusing behaviors.\r\n\r\nAs to adding the information into `terraform show` as a convenience, unfortunately that command does not make any provider API calls - it exists purely to show the state as Terraform sees it, so we don't really have room to add additional external information there.\r\n\r\nI'd recommend continuing to do what you're doing to list ASG instances - dropping down to the AWS CLI - that will always be able to give you the most current set of instances in the group."
}
, {
  "url": "https://api.github.com/repos/hashicorp/terraform/issues/comments/106894442",
  "html_url": "https://github.com/hashicorp/terraform/issues/2123#issuecomment-106894442",
  "issue_url": "https://api.github.com/repos/hashicorp/terraform/issues/2123",
  "id": 106894442,
  "user": {
    "login": "shaiguitar",
    "id": 3294,
    "avatar_url": "https://avatars.githubusercontent.com/u/3294?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/shaiguitar",
    "html_url": "https://github.com/shaiguitar",
    "followers_url": "https://api.github.com/users/shaiguitar/followers",
    "following_url": "https://api.github.com/users/shaiguitar/following{/other_user}",
    "gists_url": "https://api.github.com/users/shaiguitar/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/shaiguitar/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/shaiguitar/subscriptions",
    "organizations_url": "https://api.github.com/users/shaiguitar/orgs",
    "repos_url": "https://api.github.com/users/shaiguitar/repos",
    "events_url": "https://api.github.com/users/shaiguitar/events{/privacy}",
    "received_events_url": "https://api.github.com/users/shaiguitar/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-05-29T18:16:41Z",
  "updated_at": "2015-05-29T18:16:41Z",
  "body": "What if we had a different state file that wasn't just the resources listed in the terraform file but the full graph of dependencies (ie, for autoscaler/launch configs, the actual aws instances) and one could do a `terraform update` that would maintain a different state file with the \"full\" graph of backend infrastructure data?  "
}
]
