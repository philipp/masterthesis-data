[
{
  "url": "https://api.github.com/repos/hashicorp/terraform/issues/comments/76346309",
  "html_url": "https://github.com/hashicorp/terraform/issues/1071#issuecomment-76346309",
  "issue_url": "https://api.github.com/repos/hashicorp/terraform/issues/1071",
  "id": 76346309,
  "user": {
    "login": "knuckolls",
    "id": 91188,
    "avatar_url": "https://avatars.githubusercontent.com/u/91188?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/knuckolls",
    "html_url": "https://github.com/knuckolls",
    "followers_url": "https://api.github.com/users/knuckolls/followers",
    "following_url": "https://api.github.com/users/knuckolls/following{/other_user}",
    "gists_url": "https://api.github.com/users/knuckolls/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/knuckolls/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/knuckolls/subscriptions",
    "organizations_url": "https://api.github.com/users/knuckolls/orgs",
    "repos_url": "https://api.github.com/users/knuckolls/repos",
    "events_url": "https://api.github.com/users/knuckolls/events{/privacy}",
    "received_events_url": "https://api.github.com/users/knuckolls/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-02-27T06:32:43Z",
  "updated_at": "2015-02-27T06:32:43Z",
  "body": "Here are my thoughts on the subject. Having a rigid configuration syntax is important because Terraform is declarative, like SQL. The \"query planner\" effectively ensures that the correct things are occurring, in the correct order, and corrects drift as it arises. When it comes to maintaining a sufficiently large infrastructure the primary concern is maintainability and reasonability. The HCL format, with it's overrides, supply a way to keep the configuration human readable and visualizeable.\r\n\r\nYour concern of course, is valid. There will always be the need for custom logic. The remote-exec and local-exec provisioners allow you to hang code onto the correct places in the execution of the dependency chain. Furthermore, the entire configuration language itself is extendable with plugins written in go. Custom needs can be expressed in the resource CRUD framework, which is actually quite nice. Then you can supply nodes of custom logic to the correct places in your dependency graph.\r\n\r\nBy limiting the expressiveness of the configuration to just \"what should execute and when\", we ensure that hardened software under the covers solves that problem and solves it well. We are thereby freed up to introduce imperative logic and the bugs that come along with such at the right parts in the execution graph. Thus it becomes a game of hardening a tier of infrastructure at a time, writing logic for doing post-deploy verification, and composing those tiers together one by one. \r\n\r\nI consider Terraform's declarative nature to be it's defining trait. One that subsequent tooling will likely emulate to optimize for maintainability. "
}
, {
  "url": "https://api.github.com/repos/hashicorp/terraform/issues/comments/76350105",
  "html_url": "https://github.com/hashicorp/terraform/issues/1071#issuecomment-76350105",
  "issue_url": "https://api.github.com/repos/hashicorp/terraform/issues/1071",
  "id": 76350105,
  "user": {
    "login": "coen-hyde",
    "id": 123503,
    "avatar_url": "https://avatars.githubusercontent.com/u/123503?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/coen-hyde",
    "html_url": "https://github.com/coen-hyde",
    "followers_url": "https://api.github.com/users/coen-hyde/followers",
    "following_url": "https://api.github.com/users/coen-hyde/following{/other_user}",
    "gists_url": "https://api.github.com/users/coen-hyde/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/coen-hyde/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/coen-hyde/subscriptions",
    "organizations_url": "https://api.github.com/users/coen-hyde/orgs",
    "repos_url": "https://api.github.com/users/coen-hyde/repos",
    "events_url": "https://api.github.com/users/coen-hyde/events{/privacy}",
    "received_events_url": "https://api.github.com/users/coen-hyde/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-02-27T07:26:02Z",
  "updated_at": "2015-02-27T07:26:02Z",
  "body": "Thanks for the in-depth explanation @knuckolls, much appreciated. This makes sense, I understand why Terraform chose declarative DSL now. Closing."
}
, {
  "url": "https://api.github.com/repos/hashicorp/terraform/issues/comments/76351883",
  "html_url": "https://github.com/hashicorp/terraform/issues/1071#issuecomment-76351883",
  "issue_url": "https://api.github.com/repos/hashicorp/terraform/issues/1071",
  "id": 76351883,
  "user": {
    "login": "mitchellh",
    "id": 1299,
    "avatar_url": "https://avatars.githubusercontent.com/u/1299?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/mitchellh",
    "html_url": "https://github.com/mitchellh",
    "followers_url": "https://api.github.com/users/mitchellh/followers",
    "following_url": "https://api.github.com/users/mitchellh/following{/other_user}",
    "gists_url": "https://api.github.com/users/mitchellh/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/mitchellh/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/mitchellh/subscriptions",
    "organizations_url": "https://api.github.com/users/mitchellh/orgs",
    "repos_url": "https://api.github.com/users/mitchellh/repos",
    "events_url": "https://api.github.com/users/mitchellh/events{/privacy}",
    "received_events_url": "https://api.github.com/users/mitchellh/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-02-27T07:49:10Z",
  "updated_at": "2015-02-27T07:49:10Z",
  "body": "@coen-hyde I want to just add one small detail to what @knuckolls said (which is all correct and very well written!): HCL is also fully JSON compatible, which we also test in the unit tests (so its not a 2nd class citizen, it was designed specifically to be JSON compatible). While this isn't a true \"scripting language\", this also avoids the issue of HCL simply being some custom bespoke format: it can be thought of as human-readable/editable syntactic sugar on top of JSON. "
}
]
