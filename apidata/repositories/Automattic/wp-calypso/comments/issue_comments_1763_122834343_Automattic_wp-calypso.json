[
{
  "url": "https://api.github.com/repos/Automattic/wp-calypso/issues/comments/165597792",
  "html_url": "https://github.com/Automattic/wp-calypso/issues/1763#issuecomment-165597792",
  "issue_url": "https://api.github.com/repos/Automattic/wp-calypso/issues/1763",
  "id": 165597792,
  "user": {
    "login": "designsimply",
    "id": 1119271,
    "avatar_url": "https://avatars.githubusercontent.com/u/1119271?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/designsimply",
    "html_url": "https://github.com/designsimply",
    "followers_url": "https://api.github.com/users/designsimply/followers",
    "following_url": "https://api.github.com/users/designsimply/following{/other_user}",
    "gists_url": "https://api.github.com/users/designsimply/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/designsimply/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/designsimply/subscriptions",
    "organizations_url": "https://api.github.com/users/designsimply/orgs",
    "repos_url": "https://api.github.com/users/designsimply/repos",
    "events_url": "https://api.github.com/users/designsimply/events{/privacy}",
    "received_events_url": "https://api.github.com/users/designsimply/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-12-17T22:17:01Z",
  "updated_at": "2015-12-17T22:17:01Z",
  "body": "Copying over a comment from @folletto \r\n\r\nExcellent analysis. After reading it my thought was \"The two field should be independent and separate\", or in other terms:\r\n\r\n* The top bit should always refer to the saved date.\r\n* The bottom date should always be the publish / scheduled date.\r\n\r\nBut, I also notice that the top part is very effective alone in mixing well both status (\"Published\") and save status, so the semantics overlap a fair bit. It's also fairly counter-intuitive that once scheduled you have no idea anymore when the last save happened... but maybe it's not super relevant?\r\n\r\nThe pairing of the full date inside the drawer with the revisions makes me also think that the date showed there could always be the latest revision date, so always save date. Do we have it? Or it just overrides it with the scheduled one?\r\n\r\nSidenote: we can't go on the same line with long text afaik: \"DRAFT SAVED -- A FEW MINUTES AGO\" won't fit considering also i18n and the \"Save\" / \"Saving\" badge on the right. That's why we went on two rows there.\r\n\r\nI'm afraid I just added even more trouble... I don't have a clear solution. However, I entirely agree on the issue and the problem you raise.\r\n\r\nIterating on your proposal:\r\n\r\n> DRAFT SAVED \r\n> A FEW MINUTES AGO\r\n\r\n> PUBLISHED\r\n> 2 DAYS AGO AT 4:15pm\r\n\r\n> PUBLISHED\r\n> OCT 28, 2015 4:15pm\r\n\r\n> SCHEDULED\r\n> FOR OCT 31, 2015 4:15pm\r\n\r\n* The \"delta\" date shouldn't be used imho for scheduled and times beyond just a few days, make it harder to position in time imho.\r\n* The date below the folded panel then would always be the \"Last Save\" date."
}
, {
  "url": "https://api.github.com/repos/Automattic/wp-calypso/issues/comments/165598172",
  "html_url": "https://github.com/Automattic/wp-calypso/issues/1763#issuecomment-165598172",
  "issue_url": "https://api.github.com/repos/Automattic/wp-calypso/issues/1763",
  "id": 165598172,
  "user": {
    "login": "designsimply",
    "id": 1119271,
    "avatar_url": "https://avatars.githubusercontent.com/u/1119271?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/designsimply",
    "html_url": "https://github.com/designsimply",
    "followers_url": "https://api.github.com/users/designsimply/followers",
    "following_url": "https://api.github.com/users/designsimply/following{/other_user}",
    "gists_url": "https://api.github.com/users/designsimply/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/designsimply/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/designsimply/subscriptions",
    "organizations_url": "https://api.github.com/users/designsimply/orgs",
    "repos_url": "https://api.github.com/users/designsimply/repos",
    "events_url": "https://api.github.com/users/designsimply/events{/privacy}",
    "received_events_url": "https://api.github.com/users/designsimply/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-12-17T22:18:02Z",
  "updated_at": "2015-12-17T22:18:02Z",
  "body": "Copying over a clarification from @rralian \r\n\r\nMaybe this screenshot can better clarify the issue I'm trying to address. I scheduled the post to publish in the future. And then I opened the calendar and selected a date in the past.\r\n\r\n![confusing state](https://cloudup.com/c25-tc-jc9Y+)\r\n\r\nNotice that we updated the date in the sidebar, but we didn't change anything about the status (still showing \"scheduled\" and \"in a day\"). So right there the UX is contradicting itself. And the date is in the past, but we have the badge \"future\" on it. That doesn't make any sense.\r\n\r\nI can now close the calendar without saving it just by clicking on the calendar icon, and this is how it will remain in the page.\r\n\r\n![another weird state](https://cloudup.com/cN5wkR706n4+)\r\n\r\nIt seems like I've made a change here but I have not. It's just reflecting some weird contradicting state. If I reload the page, this is what I see instead because nothing has been saved.\r\n\r\n![what is actually saved](https://cloudup.com/c3-SNrYs2jo+)\r\n\r\nWe can avoid this confusion by treating the calendar as a two-step process. Click a day (which is _only_ reflected in the calendar UX), and then click \"schedule/update\" to confirm that new date (and then see it reflected in the status/date in the sidebar). In fact we are already forcing this two-step process anyway... because if you click a day and then do not click \"schedule/update\", then the app breaks in interesting ways.\r\n\r\nReply from @folletto \r\n\r\n> Agreed. The update of the date without an actual \"save\" is misleading. It would be still a bit \"cryptic\" that you actually have to click Update after clicking the date but would be better already."
}
]
