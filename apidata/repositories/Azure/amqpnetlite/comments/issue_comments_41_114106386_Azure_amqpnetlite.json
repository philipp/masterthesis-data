[
{
  "url": "https://api.github.com/repos/Azure/amqpnetlite/issues/comments/152279435",
  "html_url": "https://github.com/Azure/amqpnetlite/issues/41#issuecomment-152279435",
  "issue_url": "https://api.github.com/repos/Azure/amqpnetlite/issues/41",
  "id": 152279435,
  "user": {
    "login": "ChugR",
    "id": 3505055,
    "avatar_url": "https://avatars.githubusercontent.com/u/3505055?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/ChugR",
    "html_url": "https://github.com/ChugR",
    "followers_url": "https://api.github.com/users/ChugR/followers",
    "following_url": "https://api.github.com/users/ChugR/following{/other_user}",
    "gists_url": "https://api.github.com/users/ChugR/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/ChugR/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/ChugR/subscriptions",
    "organizations_url": "https://api.github.com/users/ChugR/orgs",
    "repos_url": "https://api.github.com/users/ChugR/repos",
    "events_url": "https://api.github.com/users/ChugR/events{/privacy}",
    "received_events_url": "https://api.github.com/users/ChugR/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-10-29T18:36:05Z",
  "updated_at": "2015-10-29T18:36:05Z",
  "body": "You are trying to assert some kind of flow control from you server end when the clients might overwhelm you. A mechanism built in to AMQP to help with this is the Flow performative's 'drain' flag.\r\n\r\nSuppose you have 100 clients and you only want 20 incoming messages at a time. \r\n* Start them all off with zero credits: **receiver.SetCredit(0);**\r\nNo client sends messages.\r\n* For the first 20 clients send a Flow with **credit=1, drain=true**.\r\nEach clients is supposed to send enough messages to consume its outstanding credit, one message in this case, or if it has no messages to send then it updates its flow state with the delivery count advanced to consume all available credit. This lets each client send one message or come right back with a Link OnFlow event saying that it has no messages. Either way if the client has more messages in its queue or generates its next message right away it will not send any more until you issue some credit.\r\n* As messages and OnFlow events arrive you send Flow with credit=1, drain=true to the next client in turn.\r\n\r\n"
}
, {
  "url": "https://api.github.com/repos/Azure/amqpnetlite/issues/comments/153159501",
  "html_url": "https://github.com/Azure/amqpnetlite/issues/41#issuecomment-153159501",
  "issue_url": "https://api.github.com/repos/Azure/amqpnetlite/issues/41",
  "id": 153159501,
  "user": {
    "login": "xinchen10",
    "id": 13647453,
    "avatar_url": "https://avatars.githubusercontent.com/u/13647453?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/xinchen10",
    "html_url": "https://github.com/xinchen10",
    "followers_url": "https://api.github.com/users/xinchen10/followers",
    "following_url": "https://api.github.com/users/xinchen10/following{/other_user}",
    "gists_url": "https://api.github.com/users/xinchen10/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/xinchen10/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/xinchen10/subscriptions",
    "organizations_url": "https://api.github.com/users/xinchen10/orgs",
    "repos_url": "https://api.github.com/users/xinchen10/repos",
    "events_url": "https://api.github.com/users/xinchen10/events{/privacy}",
    "received_events_url": "https://api.github.com/users/xinchen10/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-11-02T21:19:28Z",
  "updated_at": "2015-11-02T21:20:42Z",
  "body": "It is not an easy problem to solve. As ChugR mentioned, the server can use the drain mechanism to distribute limited credits to clients. However, without client's cooperation, the server now needs to \"drain\" clients constantly in case some of them have messages to send. It may be fine if clients always have messages to send, but if there are 1000 links and most of them are idle, the server will end up sending and receiving flows most of the time. If you have control of the clients, you can send flow with available field set on the client to request credits from the server. However, the library does not give you control at such low level right now.\r\n\r\nThe message processor API is designed such that credits are not sent to client until MessageContext.Complete is called. By controlling how fast you complete the message context objects, you are actually controlling how fast clients can send messages. For example, if you want to have a max 20 messages being processed at the server, you can delay completing the message context until the concurrent request count is below 20. If you also want to control the initial messages after link is attached, you would need to implement ILinkProcessor on the listener side and add AttachContext processing to the flow controlling logic.\r\n\r\nThe listener API is designed to give different levels of flexibility/control to the user.\r\n1. IMessageProcessor: the easiest way to process messages but no control of links.\r\n2. ILinkProcessor: user needs to manage attach request and link events (transfer, flow and disposition) which gives more control to the protocol.\r\n3. IContainer: user has full control of the protocol but needs to manage everything.\r\n\r\nIt should not be difficult to use a Semaphore to control the max allowed concurrent requests (AttachContext and MessageContext).\r\n"
}
, {
  "url": "https://api.github.com/repos/Azure/amqpnetlite/issues/comments/153819590",
  "html_url": "https://github.com/Azure/amqpnetlite/issues/41#issuecomment-153819590",
  "issue_url": "https://api.github.com/repos/Azure/amqpnetlite/issues/41",
  "id": 153819590,
  "user": {
    "login": "robreeves",
    "id": 5604993,
    "avatar_url": "https://avatars.githubusercontent.com/u/5604993?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/robreeves",
    "html_url": "https://github.com/robreeves",
    "followers_url": "https://api.github.com/users/robreeves/followers",
    "following_url": "https://api.github.com/users/robreeves/following{/other_user}",
    "gists_url": "https://api.github.com/users/robreeves/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/robreeves/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/robreeves/subscriptions",
    "organizations_url": "https://api.github.com/users/robreeves/orgs",
    "repos_url": "https://api.github.com/users/robreeves/repos",
    "events_url": "https://api.github.com/users/robreeves/events{/privacy}",
    "received_events_url": "https://api.github.com/users/robreeves/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-11-04T18:24:42Z",
  "updated_at": "2015-11-04T18:24:42Z",
  "body": "@xinchen10 Thanks, Xin. The flow control feature and example look like they should give us the control we need. I'll play around with it, but I think those changes and suggestions are sufficient to close the issue. I can open a new issue if I have specific questions.\r\n\r\n@ChugR Thanks for the suggestions. I have not had time to fully prototype them. It taught me something new about AMQP. I will likely think about variations that do not require as much back and forth communication when the client has no messages to send. "
}
]
