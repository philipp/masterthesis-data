[
{
  "url": "https://api.github.com/repos/mapbox/mapbox-ios-sdk-legacy/issues/comments/31240976",
  "html_url": "https://github.com/mapbox/mapbox-ios-sdk-legacy/issues/66#issuecomment-31240976",
  "issue_url": "https://api.github.com/repos/mapbox/mapbox-ios-sdk-legacy/issues/66",
  "id": 31240976,
  "user": {
    "login": "incanus",
    "id": 17722,
    "avatar_url": "https://avatars.githubusercontent.com/u/17722?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/incanus",
    "html_url": "https://github.com/incanus",
    "followers_url": "https://api.github.com/users/incanus/followers",
    "following_url": "https://api.github.com/users/incanus/following{/other_user}",
    "gists_url": "https://api.github.com/users/incanus/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/incanus/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/incanus/subscriptions",
    "organizations_url": "https://api.github.com/users/incanus/orgs",
    "repos_url": "https://api.github.com/users/incanus/repos",
    "events_url": "https://api.github.com/users/incanus/events{/privacy}",
    "received_events_url": "https://api.github.com/users/incanus/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-12-27T00:02:25Z",
  "updated_at": "2013-12-27T00:02:25Z",
  "body": "In my mind there are two approaches that could be taken here: \r\n\r\n * Reactively as part of the cache touch pipeline. \r\n\r\n   Currently when tiles are needed for screen: \r\n\r\n   * Cache is checked for tile\r\n   * Network is hit if tile doesn't exist in cache\r\n   * Tile is cached for future use\r\n   * All tiles are subject to garbage collection (probability-based) purge during this process *(does not involve network activity)*\r\n\r\n   Possibly firing off `GET` requests for tiles other than the one in question seems dicey here unless some sort of background queue were setup in which to place them. But that essentially means any map loads could possibly trigger GC, which could possibly trigger a bunch of background `GET` activity for tiles. \r\n\r\n   Firing off a `GET` request for *only* the tile in question is also not great because it puts us in the situation of relatively frequently firing off network requests for tiles that we already have locally, e.g.: \r\n\r\n   * When we cached the tile, headers said check back in two weeks\r\n   * Post-two weeks when tile is requested & found in cache, we fire off a `GET` first\r\n   * When the conditional `GET` comes back negative (nothing new), we update cached header time for tile & serve existing cache\r\n   * Two weeks later, we repeat\r\n   * We have now introduced relatively unnecessary `GET` requests into a local cache pipeline, which holds up rendering\r\n\r\n  Weak point to argument: I could use better numbers about average lifetimes of network tiles. \r\n\r\n * Proactively in the background based on cache age. This also seems like bad territory to venture into as no mobile developer wants random stuff running in the background relatively unpredictably. Combine this with the need for background API use since odds are the app is not the one running most of the time and it's a tough sell. \r\n\r\nFor the first point, I think the frequency of occurrence of the extra `GET` requests is not worth the tradeoff given that we're not hearing about people not having fresh enough maps (though: maybe they don't know?)\r\n\r\nThe existing API of blowing away the whole cache or cache for a particular map layer is probably sufficient. App devs can set a defaults key e.g. `clearedCache1.3` in order to synchronize global cache clears with app releases in order to freshen things up if they so desire. \r\n\r\n/cc @miccolis since we were talking about this recently. "
}
, {
  "url": "https://api.github.com/repos/mapbox/mapbox-ios-sdk-legacy/issues/comments/31241616",
  "html_url": "https://github.com/mapbox/mapbox-ios-sdk-legacy/issues/66#issuecomment-31241616",
  "issue_url": "https://api.github.com/repos/mapbox/mapbox-ios-sdk-legacy/issues/66",
  "id": 31241616,
  "user": {
    "login": "incanus",
    "id": 17722,
    "avatar_url": "https://avatars.githubusercontent.com/u/17722?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/incanus",
    "html_url": "https://github.com/incanus",
    "followers_url": "https://api.github.com/users/incanus/followers",
    "following_url": "https://api.github.com/users/incanus/following{/other_user}",
    "gists_url": "https://api.github.com/users/incanus/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/incanus/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/incanus/subscriptions",
    "organizations_url": "https://api.github.com/users/incanus/orgs",
    "repos_url": "https://api.github.com/users/incanus/repos",
    "events_url": "https://api.github.com/users/incanus/events{/privacy}",
    "received_events_url": "https://api.github.com/users/incanus/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-12-27T00:28:04Z",
  "updated_at": "2013-12-27T00:28:04Z",
  "body": "Related: https://github.com/mapbox/mapbox-ios-sdk/pull/274"
}
, {
  "url": "https://api.github.com/repos/mapbox/mapbox-ios-sdk-legacy/issues/comments/53513630",
  "html_url": "https://github.com/mapbox/mapbox-ios-sdk-legacy/issues/66#issuecomment-53513630",
  "issue_url": "https://api.github.com/repos/mapbox/mapbox-ios-sdk-legacy/issues/66",
  "id": 53513630,
  "user": {
    "login": "incanus",
    "id": 17722,
    "avatar_url": "https://avatars.githubusercontent.com/u/17722?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/incanus",
    "html_url": "https://github.com/incanus",
    "followers_url": "https://api.github.com/users/incanus/followers",
    "following_url": "https://api.github.com/users/incanus/following{/other_user}",
    "gists_url": "https://api.github.com/users/incanus/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/incanus/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/incanus/subscriptions",
    "organizations_url": "https://api.github.com/users/incanus/orgs",
    "repos_url": "https://api.github.com/users/incanus/repos",
    "events_url": "https://api.github.com/users/incanus/events{/privacy}",
    "received_events_url": "https://api.github.com/users/incanus/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2014-08-27T00:48:39Z",
  "updated_at": "2014-08-27T00:48:39Z",
  "body": "We are ok here as tiles aren't downloaded unless they are missing from local cache anyway. "
}
]
