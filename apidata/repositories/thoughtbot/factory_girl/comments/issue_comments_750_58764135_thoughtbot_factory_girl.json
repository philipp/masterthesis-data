[
{
  "url": "https://api.github.com/repos/thoughtbot/factory_girl/issues/comments/75793687",
  "html_url": "https://github.com/thoughtbot/factory_girl/issues/750#issuecomment-75793687",
  "issue_url": "https://api.github.com/repos/thoughtbot/factory_girl/issues/750",
  "id": 75793687,
  "user": {
    "login": "theirishpenguin",
    "id": 39653,
    "avatar_url": "https://avatars.githubusercontent.com/u/39653?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/theirishpenguin",
    "html_url": "https://github.com/theirishpenguin",
    "followers_url": "https://api.github.com/users/theirishpenguin/followers",
    "following_url": "https://api.github.com/users/theirishpenguin/following{/other_user}",
    "gists_url": "https://api.github.com/users/theirishpenguin/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/theirishpenguin/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/theirishpenguin/subscriptions",
    "organizations_url": "https://api.github.com/users/theirishpenguin/orgs",
    "repos_url": "https://api.github.com/users/theirishpenguin/repos",
    "events_url": "https://api.github.com/users/theirishpenguin/events{/privacy}",
    "received_events_url": "https://api.github.com/users/theirishpenguin/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-02-24T16:46:42Z",
  "updated_at": "2015-02-24T16:46:42Z",
  "body": "Workaround: Call `FactoryGirl.reload ` after calling `FactoryGirl.lint` in your spec/test setup code.\r\n\r\nFeel free to close this issue. I will leave it open in case you think a cleaner solution is warranted.\r\n\r\nBTW, I know that relying implicit sequence numbers in a spec is an anti-pattern. But having the linter \"disturb\" the generated sequence just seems wrong. ie. turning on and off linting could break the test suite, which seems wrong."
}
, {
  "url": "https://api.github.com/repos/thoughtbot/factory_girl/issues/comments/75834994",
  "html_url": "https://github.com/thoughtbot/factory_girl/issues/750#issuecomment-75834994",
  "issue_url": "https://api.github.com/repos/thoughtbot/factory_girl/issues/750",
  "id": 75834994,
  "user": {
    "login": "joshuaclayton",
    "id": 1574,
    "avatar_url": "https://avatars.githubusercontent.com/u/1574?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/joshuaclayton",
    "html_url": "https://github.com/joshuaclayton",
    "followers_url": "https://api.github.com/users/joshuaclayton/followers",
    "following_url": "https://api.github.com/users/joshuaclayton/following{/other_user}",
    "gists_url": "https://api.github.com/users/joshuaclayton/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/joshuaclayton/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/joshuaclayton/subscriptions",
    "organizations_url": "https://api.github.com/users/joshuaclayton/orgs",
    "repos_url": "https://api.github.com/users/joshuaclayton/repos",
    "events_url": "https://api.github.com/users/joshuaclayton/events{/privacy}",
    "received_events_url": "https://api.github.com/users/joshuaclayton/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-02-24T20:07:19Z",
  "updated_at": "2015-02-24T20:07:19Z",
  "body": "@theirishpenguin yes, reloading should fix the issue.\r\n\r\nMuch to your point, yes, tests asserting against sequential values in a hard-coded fashion in general should be avoided, instead focusing on the restraints/requirements of the data model (e.g. email has to be unique, value doesn't matter).\r\n\r\nWhat you're referring to here at a broader level is considered a [mystery guest](http://xunitpatterns.com/Obscure%20Test.html#Mystery%20Guest), wherein you assert against values that are generated outside of the test itself.\r\n\r\n```ruby\r\nFactoryGirl.define do\r\n  factory :user do\r\n    sequence(:email) { |n| \"person#{n}@example.com\" }\r\n  end\r\nend\r\n\r\nRSpec.describe User do\r\n  it \"generates an email as a mystery guest\" do\r\n    user = build(:user)\r\n\r\n    expect(user.email).to eq \"person1@example.com\" # email address generation occurs somewhere else, unclear where this happens\r\n  end\r\n\r\n  it \"generates an email explicitly\" do\r\n    user = build(:user, email: \"example@example.com\")\r\n\r\n    expect(user.email).to eq \"example@example.com\" # explicit assignment, clear where email address is coming from\r\n  end\r\nend\r\n```\r\n\r\nMystery guests cause indirection in a test suite and frequently see false failures. Another example of how relying on sequence data would be if the actual sequence values were changed, e.g. moving from `sequence(:email) { |n| \"person#{n}@example.com\" }` to `sequence(:email) { |n| \"person--#{n}@example.com\" }`. In both cases, asserting against these values assuming a specific index or interpolation is not ideal."
}
, {
  "url": "https://api.github.com/repos/thoughtbot/factory_girl/issues/comments/75851538",
  "html_url": "https://github.com/thoughtbot/factory_girl/issues/750#issuecomment-75851538",
  "issue_url": "https://api.github.com/repos/thoughtbot/factory_girl/issues/750",
  "id": 75851538,
  "user": {
    "login": "theirishpenguin",
    "id": 39653,
    "avatar_url": "https://avatars.githubusercontent.com/u/39653?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/theirishpenguin",
    "html_url": "https://github.com/theirishpenguin",
    "followers_url": "https://api.github.com/users/theirishpenguin/followers",
    "following_url": "https://api.github.com/users/theirishpenguin/following{/other_user}",
    "gists_url": "https://api.github.com/users/theirishpenguin/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/theirishpenguin/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/theirishpenguin/subscriptions",
    "organizations_url": "https://api.github.com/users/theirishpenguin/orgs",
    "repos_url": "https://api.github.com/users/theirishpenguin/repos",
    "events_url": "https://api.github.com/users/theirishpenguin/events{/privacy}",
    "received_events_url": "https://api.github.com/users/theirishpenguin/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-02-24T21:36:33Z",
  "updated_at": "2015-02-24T21:36:33Z",
  "body": ":+1: "
}
]
