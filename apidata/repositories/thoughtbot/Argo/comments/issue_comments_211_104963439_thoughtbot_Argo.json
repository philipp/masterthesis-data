[
{
  "url": "https://api.github.com/repos/thoughtbot/Argo/issues/comments/140928883",
  "html_url": "https://github.com/thoughtbot/Argo/issues/211#issuecomment-140928883",
  "issue_url": "https://api.github.com/repos/thoughtbot/Argo/issues/211",
  "id": 140928883,
  "user": {
    "login": "gfontenot",
    "id": 124499,
    "avatar_url": "https://avatars.githubusercontent.com/u/124499?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/gfontenot",
    "html_url": "https://github.com/gfontenot",
    "followers_url": "https://api.github.com/users/gfontenot/followers",
    "following_url": "https://api.github.com/users/gfontenot/following{/other_user}",
    "gists_url": "https://api.github.com/users/gfontenot/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/gfontenot/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/gfontenot/subscriptions",
    "organizations_url": "https://api.github.com/users/gfontenot/orgs",
    "repos_url": "https://api.github.com/users/gfontenot/repos",
    "events_url": "https://api.github.com/users/gfontenot/events{/privacy}",
    "received_events_url": "https://api.github.com/users/gfontenot/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-09-16T23:59:10Z",
  "updated_at": "2015-09-16T23:59:10Z",
  "body": "So this seems easy/reasonable enough. I don't have any real problem with it. My only question is whether it should be a \"fatal\" error or not?\r\n\r\nBasically, we need to consider whether a failed decoding with a custom type should be considered a failure for an optional property. Right now, this is what we do:\r\n\r\n```swift\r\npublic extension Decoded {\r\n  static func optional<T>(x: Decoded<T>) -> Decoded<T?> {\r\n    switch x {\r\n    case let .Success(value): return .Success(.Some(value))\r\n    case .Failure(.MissingKey): return .Success(.None)\r\n    case let .Failure(.TypeMismatch(x)): return .Failure(.TypeMismatch(x))\r\n    }\r\n  }\r\n```\r\n\r\nBasically, we say that a missing key is actually fine, and that a type mismatch is a real failure. My gut says that custom failures should work like type mismatch failures, and so should be treated as \"fatal\" for optional values."
}
, {
  "url": "https://api.github.com/repos/thoughtbot/Argo/issues/comments/140929815",
  "html_url": "https://github.com/thoughtbot/Argo/issues/211#issuecomment-140929815",
  "issue_url": "https://api.github.com/repos/thoughtbot/Argo/issues/211",
  "id": 140929815,
  "user": {
    "login": "jshier",
    "id": 51020,
    "avatar_url": "https://avatars.githubusercontent.com/u/51020?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/jshier",
    "html_url": "https://github.com/jshier",
    "followers_url": "https://api.github.com/users/jshier/followers",
    "following_url": "https://api.github.com/users/jshier/following{/other_user}",
    "gists_url": "https://api.github.com/users/jshier/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/jshier/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/jshier/subscriptions",
    "organizations_url": "https://api.github.com/users/jshier/orgs",
    "repos_url": "https://api.github.com/users/jshier/repos",
    "events_url": "https://api.github.com/users/jshier/events{/privacy}",
    "received_events_url": "https://api.github.com/users/jshier/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-09-17T00:06:38Z",
  "updated_at": "2015-09-17T00:06:38Z",
  "body": "I agree, which is why I used `.TypeMismatch` as my equivalent for a custom error, as it seemed closest in intent, even if it didn't really describe the issues I could see in custom transformers.\r\n\r\nCustomization of which errors are fatal is yet another, small, issue I have with Argo. It would be nice to, say, decode an array and not have the decode fail because a single element failed to decode. But I'll raise that issue if I ever run into that requirement while using Argo. "
}
, {
  "url": "https://api.github.com/repos/thoughtbot/Argo/issues/comments/140930465",
  "html_url": "https://github.com/thoughtbot/Argo/issues/211#issuecomment-140930465",
  "issue_url": "https://api.github.com/repos/thoughtbot/Argo/issues/211",
  "id": 140930465,
  "user": {
    "login": "gfontenot",
    "id": 124499,
    "avatar_url": "https://avatars.githubusercontent.com/u/124499?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/gfontenot",
    "html_url": "https://github.com/gfontenot",
    "followers_url": "https://api.github.com/users/gfontenot/followers",
    "following_url": "https://api.github.com/users/gfontenot/following{/other_user}",
    "gists_url": "https://api.github.com/users/gfontenot/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/gfontenot/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/gfontenot/subscriptions",
    "organizations_url": "https://api.github.com/users/gfontenot/orgs",
    "repos_url": "https://api.github.com/users/gfontenot/repos",
    "events_url": "https://api.github.com/users/gfontenot/events{/privacy}",
    "received_events_url": "https://api.github.com/users/gfontenot/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-09-17T00:12:11Z",
  "updated_at": "2015-09-17T00:12:11Z",
  "body": ">  It would be nice to, say, decode an array and not have the decode fail because a single element failed to decode.\r\n\r\nDifferent issue, but fwiw that can be achieved on a case by case basis by using a different decoder.\r\n\r\nInstead of using our decoder, which is basically `sequence(A.decode <^> a)` where `a` is the array, you could just do `catFailures(A.decode <^> a)`. That's a function that we could probably support along with our implementation of `sequence`, as well."
}
, {
  "url": "https://api.github.com/repos/thoughtbot/Argo/issues/comments/140930722",
  "html_url": "https://github.com/thoughtbot/Argo/issues/211#issuecomment-140930722",
  "issue_url": "https://api.github.com/repos/thoughtbot/Argo/issues/211",
  "id": 140930722,
  "user": {
    "login": "paulyoung",
    "id": 84700,
    "avatar_url": "https://avatars.githubusercontent.com/u/84700?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/paulyoung",
    "html_url": "https://github.com/paulyoung",
    "followers_url": "https://api.github.com/users/paulyoung/followers",
    "following_url": "https://api.github.com/users/paulyoung/following{/other_user}",
    "gists_url": "https://api.github.com/users/paulyoung/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/paulyoung/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/paulyoung/subscriptions",
    "organizations_url": "https://api.github.com/users/paulyoung/orgs",
    "repos_url": "https://api.github.com/users/paulyoung/repos",
    "events_url": "https://api.github.com/users/paulyoung/events{/privacy}",
    "received_events_url": "https://api.github.com/users/paulyoung/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-09-17T00:14:29Z",
  "updated_at": "2015-09-17T00:14:29Z",
  "body": "FWIW: https://github.com/thoughtbot/Argo/issues/109#issuecomment-93572717"
}
]
