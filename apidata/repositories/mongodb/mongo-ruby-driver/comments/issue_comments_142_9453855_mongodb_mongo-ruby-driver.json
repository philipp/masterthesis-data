[
{
  "url": "https://api.github.com/repos/mongodb/mongo-ruby-driver/issues/comments/11602076",
  "html_url": "https://github.com/mongodb/mongo-ruby-driver/pull/142#issuecomment-11602076",
  "issue_url": "https://api.github.com/repos/mongodb/mongo-ruby-driver/issues/142",
  "id": 11602076,
  "user": {
    "login": "TylerBrock",
    "id": 280807,
    "avatar_url": "https://avatars.githubusercontent.com/u/280807?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/TylerBrock",
    "html_url": "https://github.com/TylerBrock",
    "followers_url": "https://api.github.com/users/TylerBrock/followers",
    "following_url": "https://api.github.com/users/TylerBrock/following{/other_user}",
    "gists_url": "https://api.github.com/users/TylerBrock/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/TylerBrock/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/TylerBrock/subscriptions",
    "organizations_url": "https://api.github.com/users/TylerBrock/orgs",
    "repos_url": "https://api.github.com/users/TylerBrock/repos",
    "events_url": "https://api.github.com/users/TylerBrock/events{/privacy}",
    "received_events_url": "https://api.github.com/users/TylerBrock/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2012-12-21T03:57:50Z",
  "updated_at": "2012-12-21T03:57:50Z",
  "body": "This is great. Thank you!\r\n\r\nYou could be getting segfaults because one needs to run rake clean / clobber when switching from 1.9.3 to 1.8.7 and back. I ran it locally under all currently supported versions and it seemed fine.\r\n\r\nI'd prefer to not to pollute Thread.current but seeing as this solution actually cleans up threads properly and uses a mongo specific name I think this will work just fine. I was torn on doing that or using a finalizer to clean up the hash. Would love to hear what you think regarding that approach."
}
, {
  "url": "https://api.github.com/repos/mongodb/mongo-ruby-driver/issues/comments/11603404",
  "html_url": "https://github.com/mongodb/mongo-ruby-driver/pull/142#issuecomment-11603404",
  "issue_url": "https://api.github.com/repos/mongodb/mongo-ruby-driver/issues/142",
  "id": 11603404,
  "user": {
    "login": "cheald",
    "id": 9830,
    "avatar_url": "https://avatars.githubusercontent.com/u/9830?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/cheald",
    "html_url": "https://github.com/cheald",
    "followers_url": "https://api.github.com/users/cheald/followers",
    "following_url": "https://api.github.com/users/cheald/following{/other_user}",
    "gists_url": "https://api.github.com/users/cheald/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/cheald/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/cheald/subscriptions",
    "organizations_url": "https://api.github.com/users/cheald/orgs",
    "repos_url": "https://api.github.com/users/cheald/repos",
    "events_url": "https://api.github.com/users/cheald/events{/privacy}",
    "received_events_url": "https://api.github.com/users/cheald/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2012-12-21T05:46:04Z",
  "updated_at": "2012-12-21T05:46:04Z",
  "body": "I totally agree about not polluting Thread.current. What if we had a single hash that we could then keep values in? That way, the damage is at least contained to one \"global\". Junking up Thread.current is icky, but it's semantically correct - these are values that are only valid for the context of the thread they were set in, so keeping them there makes sense and is hopefully fairly easy to follow. I do dislike the object_id as part of the hash key, though, and wonder if that might be a signal to revert to the ivar with a finalizer.\r\n\r\nI don't really like the finalizer solution, just out of personal preference. Finalizers feel \"upside down\" to me, where cleanup has to happen explicitly rather than just as a natural effect of objects going out of scope, and using objects as hash keys is something of a code smell, as this bug specifically demonstrates."
}
, {
  "url": "https://api.github.com/repos/mongodb/mongo-ruby-driver/issues/comments/11604351",
  "html_url": "https://github.com/mongodb/mongo-ruby-driver/pull/142#issuecomment-11604351",
  "issue_url": "https://api.github.com/repos/mongodb/mongo-ruby-driver/issues/142",
  "id": 11604351,
  "user": {
    "login": "cheald",
    "id": 9830,
    "avatar_url": "https://avatars.githubusercontent.com/u/9830?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/cheald",
    "html_url": "https://github.com/cheald",
    "followers_url": "https://api.github.com/users/cheald/followers",
    "following_url": "https://api.github.com/users/cheald/following{/other_user}",
    "gists_url": "https://api.github.com/users/cheald/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/cheald/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/cheald/subscriptions",
    "organizations_url": "https://api.github.com/users/cheald/orgs",
    "repos_url": "https://api.github.com/users/cheald/repos",
    "events_url": "https://api.github.com/users/cheald/events{/privacy}",
    "received_events_url": "https://api.github.com/users/cheald/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2012-12-21T06:57:25Z",
  "updated_at": "2012-12-21T06:57:25Z",
  "body": "I did a quick POC refactor to get thread locals all using a common interface. Right now, it's just a hash on the local thread. However, by using a common interface, we could switch the implementation (Hash on Thread.current, or an instance variable that is deleted with a finalizer) and the code should theoretically stay the same in the places that it's consumed.\r\n\r\nhttps://github.com/cheald/mongo-ruby-driver/commit/258c88b6e90ff3f5ece1c50a80c892a6cd67ac3f\r\n\r\nTests pass in 1.9.3, haven't tried the others yet. If you like it, I'll open a PR on it."
}
, {
  "url": "https://api.github.com/repos/mongodb/mongo-ruby-driver/issues/comments/11615282",
  "html_url": "https://github.com/mongodb/mongo-ruby-driver/pull/142#issuecomment-11615282",
  "issue_url": "https://api.github.com/repos/mongodb/mongo-ruby-driver/issues/142",
  "id": 11615282,
  "user": {
    "login": "TylerBrock",
    "id": 280807,
    "avatar_url": "https://avatars.githubusercontent.com/u/280807?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/TylerBrock",
    "html_url": "https://github.com/TylerBrock",
    "followers_url": "https://api.github.com/users/TylerBrock/followers",
    "following_url": "https://api.github.com/users/TylerBrock/following{/other_user}",
    "gists_url": "https://api.github.com/users/TylerBrock/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/TylerBrock/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/TylerBrock/subscriptions",
    "organizations_url": "https://api.github.com/users/TylerBrock/orgs",
    "repos_url": "https://api.github.com/users/TylerBrock/repos",
    "events_url": "https://api.github.com/users/TylerBrock/events{/privacy}",
    "received_events_url": "https://api.github.com/users/TylerBrock/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2012-12-21T14:40:37Z",
  "updated_at": "2012-12-21T14:40:37Z",
  "body": "Nailed it! This seems much better to me."
}
]
