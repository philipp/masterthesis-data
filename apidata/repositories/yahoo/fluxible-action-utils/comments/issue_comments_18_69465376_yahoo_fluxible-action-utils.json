[
{
  "url": "https://api.github.com/repos/yahoo/fluxible-action-utils/issues/comments/94331803",
  "html_url": "https://github.com/yahoo/fluxible-action-utils/issues/18#issuecomment-94331803",
  "issue_url": "https://api.github.com/repos/yahoo/fluxible-action-utils/issues/18",
  "id": 94331803,
  "user": {
    "login": "koulmomo",
    "id": 983861,
    "avatar_url": "https://avatars.githubusercontent.com/u/983861?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/koulmomo",
    "html_url": "https://github.com/koulmomo",
    "followers_url": "https://api.github.com/users/koulmomo/followers",
    "following_url": "https://api.github.com/users/koulmomo/following{/other_user}",
    "gists_url": "https://api.github.com/users/koulmomo/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/koulmomo/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/koulmomo/subscriptions",
    "organizations_url": "https://api.github.com/users/koulmomo/orgs",
    "repos_url": "https://api.github.com/users/koulmomo/repos",
    "events_url": "https://api.github.com/users/koulmomo/events{/privacy}",
    "received_events_url": "https://api.github.com/users/koulmomo/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-04-20T01:16:00Z",
  "updated_at": "2015-04-20T01:16:00Z",
  "body": "@gpbl (all examples are arbitrary and are meant to be taken with a grain of salt)\r\n\r\nThe major use case for the library is compositing multiple actions that end up initializing the state of your application so that you can server side render. \r\n\r\n\r\nTake the following layout for an article page as an example\r\n![screen shot 2015-04-19 at 5 53 31 pm](https://cloud.githubusercontent.com/assets/983861/7222178/13954500-e6bd-11e4-92c3-4826a5c7dd94.png)\r\n\r\nYou have the search bar header on top, a gallery module up top, and modules to the right and left of the page.\r\n\r\nYou might break down the rendering of that page to the following action(s)\r\n\r\n```js\r\n'use strict';\r\n\r\nvar executeMultiple = require('fluxible-action-utils/async/executeMultiple');\r\nvar actions = require('./../load');\r\n\r\nmodule.export = function initializeArticlePage (context, params, done) {\r\n    executeMultiple(context, {\r\n        loadHeader: actions.loadHeader,\r\n        loadArticle: actions.loadArticle,\r\n        loadSidebar: actions.loadSidebar,    // this is the left column on the page\r\n        loadRightModules: actions.loadRightModules  // this is the right column on the page\r\n    }, function (err, results) {\r\n        // cleanup...\r\n        done();\r\n    });\r\n};\r\n```\r\n\r\nBy default, none of the actions we are composing are currently being considered as **critical**, so we will wait for all of the actions `loadHeader`, `loadArticle`, `loadSidebar`, and `loadRightModules` to finish before the final `callback` is called (the one that calls `done();`). Even if one of the actions fails/errors (this occurs when the individual action calls `done(NON_FALSY_ERROR_VALUE)`), we will still wait for the other actions to call `done`.\r\n\r\n**critical** actions then, are actions that allow you to fail early. \r\n\r\nYou can say, that if the `loadArticle` action fails, then we shouldn't even bother waiting for the other actions to complete and should redirect the user to an `Error Reporting` page, or 404 page (if the error was a 404). That's what marking an action as critical does. Regardless of the state of the other actions being executed in the same block as your critical action, you immediately call the final `callback`.\r\n\r\n```js\r\n'use strict';\r\n\r\nvar executeMultiple = require('fluxible-action-utils/async/executeMultiple');\r\nvar actions = require('./../load');\r\n\r\nmodule.export = function initializeArticlePage (context, params, done) {\r\n    executeMultiple(context, {\r\n        loadHeader: actions.loadHeader,\r\n        loadArticle: {\r\n             isCritical: true,\r\n             action: actions.loadArticle\r\n        },\r\n        loadSidebar: actions.loadSidebar,    // this is the left column on the page\r\n        loadRightModules: actions.loadRightModules  // this is the right column on the page\r\n    }, function (err, results) {\r\n        // All actions completed OR loadArticle action ended in failure\r\n\r\n        if (!err) {\r\n           done();\r\n        }\r\n        \r\n        if (err.loadArticle) {\r\n             switch (err.loadArticle.statusCode) {\r\n                 case 404:\r\n                     context.dispatch('ARTICLE_NOT_FOUND_PAGE');\r\n                     return done();\r\n                 default: \r\n                     context.dispatch('REDIRECT_ERROR_PAGE', err.loadArticle.statusCode);\r\n                     return done();\r\n             }\r\n         }\r\n\r\n        // Minor errors/more cleanup\r\n    });\r\n};\r\n```\r\n\r\ndoes that make sense?"
}
, {
  "url": "https://api.github.com/repos/yahoo/fluxible-action-utils/issues/comments/94389538",
  "html_url": "https://github.com/yahoo/fluxible-action-utils/issues/18#issuecomment-94389538",
  "issue_url": "https://api.github.com/repos/yahoo/fluxible-action-utils/issues/18",
  "id": 94389538,
  "user": {
    "login": "gpbl",
    "id": 120693,
    "avatar_url": "https://avatars.githubusercontent.com/u/120693?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/gpbl",
    "html_url": "https://github.com/gpbl",
    "followers_url": "https://api.github.com/users/gpbl/followers",
    "following_url": "https://api.github.com/users/gpbl/following{/other_user}",
    "gists_url": "https://api.github.com/users/gpbl/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/gpbl/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/gpbl/subscriptions",
    "organizations_url": "https://api.github.com/users/gpbl/orgs",
    "repos_url": "https://api.github.com/users/gpbl/repos",
    "events_url": "https://api.github.com/users/gpbl/events{/privacy}",
    "received_events_url": "https://api.github.com/users/gpbl/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-04-20T08:17:05Z",
  "updated_at": "2015-04-20T08:17:05Z",
  "body": "Yes! thank you for the detailed explanation."
}
]
