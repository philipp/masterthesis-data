[
{
  "url": "https://api.github.com/repos/yahoo/fluxible/issues/comments/142154633",
  "html_url": "https://github.com/yahoo/fluxible/issues/265#issuecomment-142154633",
  "issue_url": "https://api.github.com/repos/yahoo/fluxible/issues/265",
  "id": 142154633,
  "user": {
    "login": "mridgway",
    "id": 191142,
    "avatar_url": "https://avatars.githubusercontent.com/u/191142?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/mridgway",
    "html_url": "https://github.com/mridgway",
    "followers_url": "https://api.github.com/users/mridgway/followers",
    "following_url": "https://api.github.com/users/mridgway/following{/other_user}",
    "gists_url": "https://api.github.com/users/mridgway/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/mridgway/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/mridgway/subscriptions",
    "organizations_url": "https://api.github.com/users/mridgway/orgs",
    "repos_url": "https://api.github.com/users/mridgway/repos",
    "events_url": "https://api.github.com/users/mridgway/events{/privacy}",
    "received_events_url": "https://api.github.com/users/mridgway/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-09-22T01:21:18Z",
  "updated_at": "2015-09-22T01:39:23Z",
  "body": "Redux is able to handle this because there is only one store and any changes that are synchronous are batched into a single `change` event. The [`batchedUpdatePlugin`](https://github.com/yahoo/fluxible-addons-react/blob/master/docs/api/batchedUpdatePlugin.md) should solve this in Fluxible for any `emitChange`s in the same dispatch, but not multiple dispatches. It would be difficult to do this across dispatches since they would typically be done in multiple action creators which are asynchronous. I don't think Redux solves the use case of asynchronous action creators that dispatch multiple actions (for instance `NAVIGATE_START` followed later by `NAVIGATE_SUCCESS`).\r\n\r\nWe've been playing around with using React's batched update strategy to buffer all `setState` calls between two dispatches, but it feels a little hacky.\r\n\r\nI've been thinking about proxying the store listeners through some central hub that would be able to create transactions between two different dispatch events. I think you could even use `connectToStores` as the abstraction that could manage subscriptions using context. It would be interesting to play around with but I'm not sure that I have the time right now."
}
, {
  "url": "https://api.github.com/repos/yahoo/fluxible/issues/comments/142159634",
  "html_url": "https://github.com/yahoo/fluxible/issues/265#issuecomment-142159634",
  "issue_url": "https://api.github.com/repos/yahoo/fluxible/issues/265",
  "id": 142159634,
  "user": {
    "login": "geekyme",
    "id": 977460,
    "avatar_url": "https://avatars.githubusercontent.com/u/977460?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/geekyme",
    "html_url": "https://github.com/geekyme",
    "followers_url": "https://api.github.com/users/geekyme/followers",
    "following_url": "https://api.github.com/users/geekyme/following{/other_user}",
    "gists_url": "https://api.github.com/users/geekyme/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/geekyme/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/geekyme/subscriptions",
    "organizations_url": "https://api.github.com/users/geekyme/orgs",
    "repos_url": "https://api.github.com/users/geekyme/repos",
    "events_url": "https://api.github.com/users/geekyme/events{/privacy}",
    "received_events_url": "https://api.github.com/users/geekyme/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-09-22T02:09:07Z",
  "updated_at": "2015-09-22T02:09:07Z",
  "body": "Thanks for the clarification @mridgway! In that case what's the benefit of having multiple stores vs a single store with multiple reducers ?"
}
, {
  "url": "https://api.github.com/repos/yahoo/fluxible/issues/comments/142416819",
  "html_url": "https://github.com/yahoo/fluxible/issues/265#issuecomment-142416819",
  "issue_url": "https://api.github.com/repos/yahoo/fluxible/issues/265",
  "id": 142416819,
  "user": {
    "login": "mridgway",
    "id": 191142,
    "avatar_url": "https://avatars.githubusercontent.com/u/191142?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/mridgway",
    "html_url": "https://github.com/mridgway",
    "followers_url": "https://api.github.com/users/mridgway/followers",
    "following_url": "https://api.github.com/users/mridgway/following{/other_user}",
    "gists_url": "https://api.github.com/users/mridgway/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/mridgway/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/mridgway/subscriptions",
    "organizations_url": "https://api.github.com/users/mridgway/orgs",
    "repos_url": "https://api.github.com/users/mridgway/repos",
    "events_url": "https://api.github.com/users/mridgway/events{/privacy}",
    "received_events_url": "https://api.github.com/users/mridgway/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-09-22T20:49:48Z",
  "updated_at": "2015-09-22T20:49:48Z",
  "body": "That's a question I've been asking myself recently. I think in a lot of our use cases we have sections of the page that require data that is completely unrelated to the rest of the page and requires a separate domain of knowledge. In these cases, it's useful to be able to completely separate them rather than have a root reducer that has to deal with calling the correct reducer for different knowledge domains. \r\n\r\nBy having this separation, we've been able to create a plug-and-play system that allows adding components and stores via npm without having to update a root store/reducer to make sure it receives the correct data. Instead we just need to execute the new action and the store will be populated.\r\n\r\nIn a larger application, I could also see the single root reducer model breaking down as well since it would continuously get larger and larger without a way to code split. With multiple stores, we're able to load and register stores on the client on demand as they are needed. \r\n\r\nI've been thinking about whether Redux's model works only with a single store. It seems like it shouldn't be dependent on that, but the way middleware and dispatching works, you are forced into have only one. \r\n\r\nI think there are benefits to both ways but I'd love to hear how others have fared with single vs. multiple stores.\r\n"
}
, {
  "url": "https://api.github.com/repos/yahoo/fluxible/issues/comments/143546703",
  "html_url": "https://github.com/yahoo/fluxible/issues/265#issuecomment-143546703",
  "issue_url": "https://api.github.com/repos/yahoo/fluxible/issues/265",
  "id": 143546703,
  "user": {
    "login": "geekyme",
    "id": 977460,
    "avatar_url": "https://avatars.githubusercontent.com/u/977460?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/geekyme",
    "html_url": "https://github.com/geekyme",
    "followers_url": "https://api.github.com/users/geekyme/followers",
    "following_url": "https://api.github.com/users/geekyme/following{/other_user}",
    "gists_url": "https://api.github.com/users/geekyme/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/geekyme/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/geekyme/subscriptions",
    "organizations_url": "https://api.github.com/users/geekyme/orgs",
    "repos_url": "https://api.github.com/users/geekyme/repos",
    "events_url": "https://api.github.com/users/geekyme/events{/privacy}",
    "received_events_url": "https://api.github.com/users/geekyme/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-09-27T12:30:26Z",
  "updated_at": "2015-09-27T12:30:26Z",
  "body": "Thanks for your thoughts Michael. Those are real concerns. \r\n\r\nThis article http://www.code-experience.com/problems-with-flux/ discusses a better way to structure reducers and store. \r\n\r\n> On 23 Sep 2015, at 4:49 AM, Michael Ridgway <notifications@github.com> wrote:\r\n> \r\n> That's a question I've been asking myself recently. I think in a lot of our use cases we have sections of the page that require data that is completely unrelated to the rest of the page and requires a separate domain of knowledge. In these cases, it's useful to be able to completely separate them rather than have a root reducer that has to deal with calling the correct reducer for different knowledge domains.\r\n> \r\n> By having this separation, we've been able to create a plug-and-play system that allows adding components and stores via npm without having to update a root store/reducer to make sure it receives the correct data. Instead we just need to execute the new action and the store will be populated.\r\n> \r\n> In a larger application, I could also see the single root reducer model breaking down as well since it would continuously get larger and larger without a way to code split. With multiple stores, we're able to load and register stores on the client on demand as they are needed.\r\n> \r\n> I've been thinking about whether Redux's model works only with a single store. It seems like it shouldn't be dependent on that, but the way middleware and dispatching works, you are forced into have only one.\r\n> \r\n> I think there are benefits to both ways but I'd love to hear how others have fared with single vs. multiple stores.\r\n> \r\n> —\r\n> Reply to this email directly or view it on GitHub.\r\n> \r\n"
}
, {
  "url": "https://api.github.com/repos/yahoo/fluxible/issues/comments/165620066",
  "html_url": "https://github.com/yahoo/fluxible/issues/265#issuecomment-165620066",
  "issue_url": "https://api.github.com/repos/yahoo/fluxible/issues/265",
  "id": 165620066,
  "user": {
    "login": "mridgway",
    "id": 191142,
    "avatar_url": "https://avatars.githubusercontent.com/u/191142?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/mridgway",
    "html_url": "https://github.com/mridgway",
    "followers_url": "https://api.github.com/users/mridgway/followers",
    "following_url": "https://api.github.com/users/mridgway/following{/other_user}",
    "gists_url": "https://api.github.com/users/mridgway/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/mridgway/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/mridgway/subscriptions",
    "organizations_url": "https://api.github.com/users/mridgway/orgs",
    "repos_url": "https://api.github.com/users/mridgway/repos",
    "events_url": "https://api.github.com/users/mridgway/events{/privacy}",
    "received_events_url": "https://api.github.com/users/mridgway/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-12-18T00:03:20Z",
  "updated_at": "2015-12-18T00:03:20Z",
  "body": "I think https://github.com/yahoo/fluxible/pull/324 could be relevant here."
}
]
