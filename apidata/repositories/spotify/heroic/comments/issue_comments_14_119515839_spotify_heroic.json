[
{
  "url": "https://api.github.com/repos/spotify/heroic/issues/comments/160756713",
  "html_url": "https://github.com/spotify/heroic/issues/14#issuecomment-160756713",
  "issue_url": "https://api.github.com/repos/spotify/heroic/issues/14",
  "id": 160756713,
  "user": {
    "login": "udoprog",
    "id": 111092,
    "avatar_url": "https://avatars.githubusercontent.com/u/111092?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/udoprog",
    "html_url": "https://github.com/udoprog",
    "followers_url": "https://api.github.com/users/udoprog/followers",
    "following_url": "https://api.github.com/users/udoprog/following{/other_user}",
    "gists_url": "https://api.github.com/users/udoprog/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/udoprog/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/udoprog/subscriptions",
    "organizations_url": "https://api.github.com/users/udoprog/orgs",
    "repos_url": "https://api.github.com/users/udoprog/repos",
    "events_url": "https://api.github.com/users/udoprog/events{/privacy}",
    "received_events_url": "https://api.github.com/users/udoprog/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-11-30T20:46:47Z",
  "updated_at": "2015-11-30T20:46:47Z",
  "body": "Hey,\r\n\r\nThe setup I'd recommend is building metadata straight from the pipeline, sort of suggestion number three.\r\n\r\nAssuming you are using a Kafka setup:\r\n\r\n* Setup consumer in each DC that writes to Elasticsearch this will cause a bit of extra cross-DC traffic. A lot of this traffic could be avoided by extending something like FFWD to write to a separate metadata topic with a much lower rate. This would be specifically designed to only advertise available time series over.\r\n* Configure each shard with Cross-DC tags include API nodes in the cluster from all available DCs.\r\n* Since you already have a data consumer, you could just as well ingest data as well, but this will be less consistent (e.g. non-identical rows) than cross DC Cassandra replication. I don't see a need to use MirrorMaker since there will be the same number of consumers in the receiving DC and less moving parts. But if you find this to be necessary, it is certainly an option.\r\n\r\nWith the above, Cross-DC metadata is not guaranteed to be consistent at all times. Effectively, no two snapshots ever will be. But I consider it 'close enough'. Another downside is that with the current naive round-robin federation pattern you might hit undesirable nodes. The system won't prefer closer nodes with lower latency if they are available. At the very least I'd like to see this in a future patch. Either through latency measurement, or extra metadata like `dc_tags` in the cluster configuration.\r\n\r\nWe rate-limit metadata ingestion to around 50k/s, with this, it takes us about 30 minutes to rebuild everything, or build a new index once it rotates. This limit is in place to reduce the overhead of ingestion to Elasticsearch. Without it we quickly kill the the cluster. I also expect that this rate will scale with the size of the cluster, so total time to ingest should not be adversely affected as we grow.\r\n\r\nRebuilding Elasticsearch from Cassandra is slow, and gets slower with the number of rows. We have clusters with hundreds of millions of row keys where iterating over them takes hours (can be faster if parallelized over the token space). I've considered writing a slow, rotating indexes that performs a number of slow, parallel traversals to index everything for the ones who need it. We don't mind that a time series disappears from Elasticsearch when we rotate two times (two weeks), and no data for it has been emitted. It's a feature we enjoy.\r\n\r\nAs a general principle I prefer to keep everything *hot* even when not in use to avoid complex failover procedures.\r\n\r\nI hope this was helpful!"
}
, {
  "url": "https://api.github.com/repos/spotify/heroic/issues/comments/160909557",
  "html_url": "https://github.com/spotify/heroic/issues/14#issuecomment-160909557",
  "issue_url": "https://api.github.com/repos/spotify/heroic/issues/14",
  "id": 160909557,
  "user": {
    "login": "gmucha",
    "id": 10435930,
    "avatar_url": "https://avatars.githubusercontent.com/u/10435930?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/gmucha",
    "html_url": "https://github.com/gmucha",
    "followers_url": "https://api.github.com/users/gmucha/followers",
    "following_url": "https://api.github.com/users/gmucha/following{/other_user}",
    "gists_url": "https://api.github.com/users/gmucha/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/gmucha/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/gmucha/subscriptions",
    "organizations_url": "https://api.github.com/users/gmucha/orgs",
    "repos_url": "https://api.github.com/users/gmucha/repos",
    "events_url": "https://api.github.com/users/gmucha/events{/privacy}",
    "received_events_url": "https://api.github.com/users/gmucha/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-12-01T09:36:24Z",
  "updated_at": "2015-12-01T09:36:24Z",
  "body": "Thanks for the write up- very helpful indeed. As far as I understand new rows get written into metadata storage only when new series appear (or, presumably, when a series spills into a new row in Cassandra). - but Heroic tries a metadata write every time a point is added, right? Theoretically, I could set up multiple MetadataBackends and write directly into them, but cross-DC latency would make this too costly for each item written (also, this would mean adding support for lost writes, using this for write and not read only). I guess doing this using Kafka would be nicer (just wondering how to effectively reduce # of metadata messages being written).\r\n\r\nThe \"close enough\" metadata is good enough for me, there will definitely exist some disparity between backend and metadata due to different rates at which data replicate.\r\n\r\nBTW - as if on cue, Elasticsearch provided a writeup on scaling across datacenters using Kafka. (https://www.elastic.co/blogs/scaling_elasticsearch_across_data_centers_with_kafka)\r\n"
}
, {
  "url": "https://api.github.com/repos/spotify/heroic/issues/comments/160986694",
  "html_url": "https://github.com/spotify/heroic/issues/14#issuecomment-160986694",
  "issue_url": "https://api.github.com/repos/spotify/heroic/issues/14",
  "id": 160986694,
  "user": {
    "login": "udoprog",
    "id": 111092,
    "avatar_url": "https://avatars.githubusercontent.com/u/111092?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/udoprog",
    "html_url": "https://github.com/udoprog",
    "followers_url": "https://api.github.com/users/udoprog/followers",
    "following_url": "https://api.github.com/users/udoprog/following{/other_user}",
    "gists_url": "https://api.github.com/users/udoprog/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/udoprog/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/udoprog/subscriptions",
    "organizations_url": "https://api.github.com/users/udoprog/orgs",
    "repos_url": "https://api.github.com/users/udoprog/repos",
    "events_url": "https://api.github.com/users/udoprog/events{/privacy}",
    "received_events_url": "https://api.github.com/users/udoprog/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-12-01T14:42:24Z",
  "updated_at": "2015-12-01T14:42:24Z",
  "body": "Thanks!\r\n\r\nThe metadata writes are rate-limited, uses a timed write-if-absent cache for each consumer, and uses [op_type=create](https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-index_.html#operation-type) to reduce overhead on duplicates.\r\n\r\nI'll mark this as an enhancement for now.\r\n\r\nI can see this turning into 1) a tutorial on https://spotify.github.io/heroic/#!/tutorial/index and 2) some improvements to the clustering logic."
}
]
