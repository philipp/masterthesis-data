[
{
  "url": "https://api.github.com/repos/Shopify/identity_cache/issues/comments/18374818",
  "html_url": "https://github.com/Shopify/identity_cache/issues/43#issuecomment-18374818",
  "issue_url": "https://api.github.com/repos/Shopify/identity_cache/issues/43",
  "id": 18374818,
  "user": {
    "login": "airhorns",
    "id": 158950,
    "avatar_url": "https://avatars.githubusercontent.com/u/158950?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/airhorns",
    "html_url": "https://github.com/airhorns",
    "followers_url": "https://api.github.com/users/airhorns/followers",
    "following_url": "https://api.github.com/users/airhorns/following{/other_user}",
    "gists_url": "https://api.github.com/users/airhorns/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/airhorns/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/airhorns/subscriptions",
    "organizations_url": "https://api.github.com/users/airhorns/orgs",
    "repos_url": "https://api.github.com/users/airhorns/repos",
    "events_url": "https://api.github.com/users/airhorns/events{/privacy}",
    "received_events_url": "https://api.github.com/users/airhorns/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-05-23T22:03:35Z",
  "updated_at": "2013-05-23T22:03:56Z",
  "body": "I totally see the use case but I think I'd prefer to leave `IdentityCache` as is. The `scope` option seems ripe for abuse for other less correct ideas, and we also would have a hard time calling the method on an object we haven't found yet (during `fetch_by_email`). Ideally you can find a way to let IDC do `fetch_by_email_and_country`, and otherwise you should be able to override the cache key generation to change the string however you want. The methods in question would be in https://github.com/Shopify/identity_cache/blob/master/lib/identity_cache/cache_key_generation.rb, and should be mixed in to your model. \r\n\r\nAlso, the approach we are taking for multi-tenancy at Shopify uses globally unique IDs as generated by `noeqd`. Kind of annoying to build but we think its a good choice for debugging sanity and so that we don't have to change the assumptions made everywhere else, like in `IdentityCache`.\r\n\r\n\r\nGood luck!"
}
, {
  "url": "https://api.github.com/repos/Shopify/identity_cache/issues/comments/18670382",
  "html_url": "https://github.com/Shopify/identity_cache/issues/43#issuecomment-18670382",
  "issue_url": "https://api.github.com/repos/Shopify/identity_cache/issues/43",
  "id": 18670382,
  "user": {
    "login": "NielsKSchjoedt",
    "id": 570755,
    "avatar_url": "https://avatars.githubusercontent.com/u/570755?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/NielsKSchjoedt",
    "html_url": "https://github.com/NielsKSchjoedt",
    "followers_url": "https://api.github.com/users/NielsKSchjoedt/followers",
    "following_url": "https://api.github.com/users/NielsKSchjoedt/following{/other_user}",
    "gists_url": "https://api.github.com/users/NielsKSchjoedt/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/NielsKSchjoedt/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/NielsKSchjoedt/subscriptions",
    "organizations_url": "https://api.github.com/users/NielsKSchjoedt/orgs",
    "repos_url": "https://api.github.com/users/NielsKSchjoedt/repos",
    "events_url": "https://api.github.com/users/NielsKSchjoedt/events{/privacy}",
    "received_events_url": "https://api.github.com/users/NielsKSchjoedt/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-05-30T09:33:18Z",
  "updated_at": "2013-05-30T09:33:18Z",
  "body": "Okay well, I see your point, event though it would be useful in my case.\r\n\r\nI forked the gem, and tried to add my method call (which is actually a class method) called `CountryManager.country`. I did it like so, just to test it out:\r\n\r\n```ruby\r\nmodule IdentityCache\r\n  module CacheKeyGeneration\r\n    extend ActiveSupport::Concern\r\n\r\n    module ClassMethods\r\n      def rails_cache_key(id)\r\n        rails_cache_key_prefix + id.to_s\r\n      end\r\n\r\n      def rails_cache_key_prefix\r\n        @rails_cache_key_prefix ||= begin\r\n          \"IDC:blob:#{base_class.name}:#{CountryManager.country}:#{IdentityCache.memcache_hash(IdentityCache.schema_to_string(columns))}:\"\r\n        end\r\n      end\r\n\r\n      def rails_cache_index_key_for_fields_and_values(fields, values)\r\n        \"IDC:index:#{base_class.name}:#{CountryManager.country}:#{rails_cache_string_for_fields_and_values(fields, values)}\"\r\n      end\r\n\r\n      def rails_cache_key_for_attribute_and_fields_and_values(attribute, fields, values)\r\n        \"IDC:attribute:#{base_class.name}:#{CountryManager.country}:#{attribute}:#{rails_cache_string_for_fields_and_values(fields, values)}\"\r\n      end\r\n\r\n      def rails_cache_string_for_fields_and_values(fields, values)\r\n        \"#{CountryManager.country}:#{fields.join('/')}:#{IdentityCache.memcache_hash(values.join('/'))}\"\r\n      end\r\n    end\r\n```\r\n\r\nHowever it doesn't seem to work. If I set the `CountryManager.country=:da`, and then fetch one of the cached AR objects, then it correctly performs (according to the log): `Cache read: IDC:blob:Advert:da:8995784370078824810:1959380` However if I then change the country to `CountryManager.country=:en`, and try to fetch the AR object again, then it still performs: `Cache read: IDC:blob:Advert:da:8995784370078824810:1959380` instead of `Cache read: IDC:blob:Advert:en:8995784370078824810:1959380`\r\n\r\nWhat am I doing wrong?\r\n"
}
, {
  "url": "https://api.github.com/repos/Shopify/identity_cache/issues/comments/22966198",
  "html_url": "https://github.com/Shopify/identity_cache/issues/43#issuecomment-22966198",
  "issue_url": "https://api.github.com/repos/Shopify/identity_cache/issues/43",
  "id": 22966198,
  "user": {
    "login": "arthurnn",
    "id": 833383,
    "avatar_url": "https://avatars.githubusercontent.com/u/833383?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/arthurnn",
    "html_url": "https://github.com/arthurnn",
    "followers_url": "https://api.github.com/users/arthurnn/followers",
    "following_url": "https://api.github.com/users/arthurnn/following{/other_user}",
    "gists_url": "https://api.github.com/users/arthurnn/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/arthurnn/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/arthurnn/subscriptions",
    "organizations_url": "https://api.github.com/users/arthurnn/orgs",
    "repos_url": "https://api.github.com/users/arthurnn/repos",
    "events_url": "https://api.github.com/users/arthurnn/events{/privacy}",
    "received_events_url": "https://api.github.com/users/arthurnn/received_events",
    "type": "User",
    "site_admin": true
  },
  "created_at": "2013-08-20T18:22:49Z",
  "updated_at": "2013-08-20T18:22:49Z",
  "body": "On the lastest version of IDC, there is a new method called [.rails_cache_key_namespace](https://github.com/Shopify/identity_cache/blob/master/lib/identity_cache/cache_key_generation.rb#L47). You can just overwrite that method on the class level, and you should be able to namespace the key depending in or DB.\r\n\r\nThanks for the feature request, but as @hornairs said, we are not adding this overhead to the core implementation. fell free to fork the project, however I still think you can only use the new `.rails_cache_key_namespace` method."
}
]
