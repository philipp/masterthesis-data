[
{
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/162135447",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/5930#issuecomment-162135447",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/5930",
  "id": 162135447,
  "user": {
    "login": "RyanCavanaugh",
    "id": 6685088,
    "avatar_url": "https://avatars.githubusercontent.com/u/6685088?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/RyanCavanaugh",
    "html_url": "https://github.com/RyanCavanaugh",
    "followers_url": "https://api.github.com/users/RyanCavanaugh/followers",
    "following_url": "https://api.github.com/users/RyanCavanaugh/following{/other_user}",
    "gists_url": "https://api.github.com/users/RyanCavanaugh/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/RyanCavanaugh/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/RyanCavanaugh/subscriptions",
    "organizations_url": "https://api.github.com/users/RyanCavanaugh/orgs",
    "repos_url": "https://api.github.com/users/RyanCavanaugh/repos",
    "events_url": "https://api.github.com/users/RyanCavanaugh/events{/privacy}",
    "received_events_url": "https://api.github.com/users/RyanCavanaugh/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-12-05T03:20:07Z",
  "updated_at": "2015-12-05T03:20:21Z",
  "body": "Type guards do not narrow `any`. The reasoning for this is that we want type guards to allow you to do *more*, not *less*. `any` can already do everything, so it doesn't \"help\" you in that regard.\r\n\r\nMore specifically, we actually did try it the other way. The initial version of type guards *did* narrow on `any`; what we found is that a lot people had code like this:\r\n\r\n```ts\r\n// By contract, this function accepts only a Dog or a House\r\nfunction fn(x: any) {\r\n  // For whatever reason, user is checking against a base class rather\r\n  // than the most-specific class. Happens a lot with e.g. HTMLElement\r\n  if (x instanceof Animal) {\r\n    x.woof(); // Disallowed if x: Animal\r\n  } else {\r\n    // handle House case\r\n  }\r\n}\r\n```"
}
, {
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/162139755",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/5930#issuecomment-162139755",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/5930",
  "id": 162139755,
  "user": {
    "login": "DanielRosenwasser",
    "id": 972891,
    "avatar_url": "https://avatars.githubusercontent.com/u/972891?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/DanielRosenwasser",
    "html_url": "https://github.com/DanielRosenwasser",
    "followers_url": "https://api.github.com/users/DanielRosenwasser/followers",
    "following_url": "https://api.github.com/users/DanielRosenwasser/following{/other_user}",
    "gists_url": "https://api.github.com/users/DanielRosenwasser/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/DanielRosenwasser/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/DanielRosenwasser/subscriptions",
    "organizations_url": "https://api.github.com/users/DanielRosenwasser/orgs",
    "repos_url": "https://api.github.com/users/DanielRosenwasser/repos",
    "events_url": "https://api.github.com/users/DanielRosenwasser/events{/privacy}",
    "received_events_url": "https://api.github.com/users/DanielRosenwasser/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-12-05T04:51:44Z",
  "updated_at": "2015-12-05T04:51:44Z",
  "body": "At the expense of being pedantic, this isn't entirely true. `instanceof` and user-defined type guards do not perform narrowing. `typeof` type guards *do* because we know *exactly* what type a value has. \r\n\r\n```ts\r\nlet x: any;\r\n\r\nif (typeof x === \"number\") {\r\n    // 'x' has type 'number' here.\r\n    let y = x * x;\r\n}\r\n```"
}
, {
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/162243600",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/5930#issuecomment-162243600",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/5930",
  "id": 162243600,
  "user": {
    "login": "aleksey-bykov",
    "id": 937933,
    "avatar_url": "https://avatars.githubusercontent.com/u/937933?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/aleksey-bykov",
    "html_url": "https://github.com/aleksey-bykov",
    "followers_url": "https://api.github.com/users/aleksey-bykov/followers",
    "following_url": "https://api.github.com/users/aleksey-bykov/following{/other_user}",
    "gists_url": "https://api.github.com/users/aleksey-bykov/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/aleksey-bykov/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/aleksey-bykov/subscriptions",
    "organizations_url": "https://api.github.com/users/aleksey-bykov/orgs",
    "repos_url": "https://api.github.com/users/aleksey-bykov/repos",
    "events_url": "https://api.github.com/users/aleksey-bykov/events{/privacy}",
    "received_events_url": "https://api.github.com/users/aleksey-bykov/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-12-05T20:30:14Z",
  "updated_at": "2015-12-05T20:33:53Z",
  "body": "@RyanCavanaugh what bothers me is that the design team is up for encouraging bad habits, this is wrong, please stop, you cannot follow the flow even if everyone flows with it, you should make what's right, not what's everyone is accustomed to be doing for their lives, i mean an average member of javascript community doesn't have a PhD in computer science, why the hell we choose to follow his bad habits?\r\n\r\ntype guard should narrow things, this is what they do, narrowing `any` makes perfect sense"
}
, {
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/163782825",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/5930#issuecomment-163782825",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/5930",
  "id": 163782825,
  "user": {
    "login": "fongandrew",
    "id": 179327,
    "avatar_url": "https://avatars.githubusercontent.com/u/179327?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/fongandrew",
    "html_url": "https://github.com/fongandrew",
    "followers_url": "https://api.github.com/users/fongandrew/followers",
    "following_url": "https://api.github.com/users/fongandrew/following{/other_user}",
    "gists_url": "https://api.github.com/users/fongandrew/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/fongandrew/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/fongandrew/subscriptions",
    "organizations_url": "https://api.github.com/users/fongandrew/orgs",
    "repos_url": "https://api.github.com/users/fongandrew/repos",
    "events_url": "https://api.github.com/users/fongandrew/events{/privacy}",
    "received_events_url": "https://api.github.com/users/fongandrew/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-12-10T23:13:11Z",
  "updated_at": "2015-12-10T23:13:11Z",
  "body": "+1 on changing the behavior to narrowing the type.\r\n\r\nWith respect to prior user behavior, I don't think it's something to be encouraged, but if it is widespread, it's because `instanceof` existed prior to typeguards being introduced. For user-defined typeguards however, that's not the case. If I define a function with the signature `(arg: any) => arg is A`, there's zero ambiguity that the user intends for `any` type to be narrowed to `A`.\r\n\r\nAlternatively, maybe there could be some sort of syntax where the user can explicitly indicate intent to narrow? E.g. `isA(arg: any) => arg isOnly A`?\r\n\r\n\r\n"
}
, {
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/163817104",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/5930#issuecomment-163817104",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/5930",
  "id": 163817104,
  "user": {
    "login": "RyanCavanaugh",
    "id": 6685088,
    "avatar_url": "https://avatars.githubusercontent.com/u/6685088?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/RyanCavanaugh",
    "html_url": "https://github.com/RyanCavanaugh",
    "followers_url": "https://api.github.com/users/RyanCavanaugh/followers",
    "following_url": "https://api.github.com/users/RyanCavanaugh/following{/other_user}",
    "gists_url": "https://api.github.com/users/RyanCavanaugh/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/RyanCavanaugh/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/RyanCavanaugh/subscriptions",
    "organizations_url": "https://api.github.com/users/RyanCavanaugh/orgs",
    "repos_url": "https://api.github.com/users/RyanCavanaugh/repos",
    "events_url": "https://api.github.com/users/RyanCavanaugh/events{/privacy}",
    "received_events_url": "https://api.github.com/users/RyanCavanaugh/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-12-11T02:29:24Z",
  "updated_at": "2015-12-11T02:29:24Z",
  "body": "I'm serious about the number of breakages we found in real-world code. It was very large, and feedback from people trying early builds said they didn't want this.\r\n\r\nIt's so far been basically impossible to get a type error on a value of type `any` and there would need to be a stronger justification other than \"You should be stricter on people\" when the entire point of the `any` type is to be as unstrict as possible.\r\n\r\nOn the one hand, listen to users, and on the other hand, listen to users..."
}
, {
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/163818434",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/5930#issuecomment-163818434",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/5930",
  "id": 163818434,
  "user": {
    "login": "aleksey-bykov",
    "id": 937933,
    "avatar_url": "https://avatars.githubusercontent.com/u/937933?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/aleksey-bykov",
    "html_url": "https://github.com/aleksey-bykov",
    "followers_url": "https://api.github.com/users/aleksey-bykov/followers",
    "following_url": "https://api.github.com/users/aleksey-bykov/following{/other_user}",
    "gists_url": "https://api.github.com/users/aleksey-bykov/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/aleksey-bykov/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/aleksey-bykov/subscriptions",
    "organizations_url": "https://api.github.com/users/aleksey-bykov/orgs",
    "repos_url": "https://api.github.com/users/aleksey-bykov/repos",
    "events_url": "https://api.github.com/users/aleksey-bykov/events{/privacy}",
    "received_events_url": "https://api.github.com/users/aleksey-bykov/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-12-11T02:36:09Z",
  "updated_at": "2015-12-11T02:36:09Z",
  "body": "Don't see a contradiction. Let `any` be the most unrestricted thing in the world. No problem. But when someone does `if (anything is Potato) { eatSteak(anything); }` they expect a compiler error, `any` won't give it to them, so they don't want `any`, they want a shaped value. So the strictness is welcome here, it's a good thing, why would anyone be pissed by it."
}
, {
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/163818998",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/5930#issuecomment-163818998",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/5930",
  "id": 163818998,
  "user": {
    "login": "RyanCavanaugh",
    "id": 6685088,
    "avatar_url": "https://avatars.githubusercontent.com/u/6685088?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/RyanCavanaugh",
    "html_url": "https://github.com/RyanCavanaugh",
    "followers_url": "https://api.github.com/users/RyanCavanaugh/followers",
    "following_url": "https://api.github.com/users/RyanCavanaugh/following{/other_user}",
    "gists_url": "https://api.github.com/users/RyanCavanaugh/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/RyanCavanaugh/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/RyanCavanaugh/subscriptions",
    "organizations_url": "https://api.github.com/users/RyanCavanaugh/orgs",
    "repos_url": "https://api.github.com/users/RyanCavanaugh/repos",
    "events_url": "https://api.github.com/users/RyanCavanaugh/events{/privacy}",
    "received_events_url": "https://api.github.com/users/RyanCavanaugh/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-12-11T02:41:11Z",
  "updated_at": "2015-12-11T02:41:11Z",
  "body": "What you've written there is runtime-invalid code. The point is that there's lots of runtime-*valid* code that also issues a type error if we narrow `any`"
}
, {
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/163821334",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/5930#issuecomment-163821334",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/5930",
  "id": 163821334,
  "user": {
    "login": "aleksey-bykov",
    "id": 937933,
    "avatar_url": "https://avatars.githubusercontent.com/u/937933?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/aleksey-bykov",
    "html_url": "https://github.com/aleksey-bykov",
    "followers_url": "https://api.github.com/users/aleksey-bykov/followers",
    "following_url": "https://api.github.com/users/aleksey-bykov/following{/other_user}",
    "gists_url": "https://api.github.com/users/aleksey-bykov/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/aleksey-bykov/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/aleksey-bykov/subscriptions",
    "organizations_url": "https://api.github.com/users/aleksey-bykov/orgs",
    "repos_url": "https://api.github.com/users/aleksey-bykov/repos",
    "events_url": "https://api.github.com/users/aleksey-bykov/events{/privacy}",
    "received_events_url": "https://api.github.com/users/aleksey-bykov/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-12-11T02:55:57Z",
  "updated_at": "2015-12-11T03:17:17Z",
  "body": "Let's not use word strictness or narrowing for a second. Type guard is a predicate that asserts at runtime that a given value is of certain type. Ultimately one can put anything in it and it would say yes or no. Once it said yes you get a guarantee (up to the implementation of the type guard) that a value is of that type. Am I right so far? If so, then your example with the `woof` method must break. We asserted that a value is of type `Animal`. Animals don't woof, dogs do. It's not a runtime valid code, because if I put a cat in that function it will crash regardless of what the author thought the contract is. There is no such thing as a contract in JavaScript, there are only our best assumptions and hopefully a test that verifies them. So I am not sure why we are into giving the developers an illusion that they know what they are doing. I see your point that there are a lot of naive people who already written tons of crappy code, well this is TypeScript for goodness sake, time to grow up."
}
, {
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/163824913",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/5930#issuecomment-163824913",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/5930",
  "id": 163824913,
  "user": {
    "login": "aleksey-bykov",
    "id": 937933,
    "avatar_url": "https://avatars.githubusercontent.com/u/937933?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/aleksey-bykov",
    "html_url": "https://github.com/aleksey-bykov",
    "followers_url": "https://api.github.com/users/aleksey-bykov/followers",
    "following_url": "https://api.github.com/users/aleksey-bykov/following{/other_user}",
    "gists_url": "https://api.github.com/users/aleksey-bykov/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/aleksey-bykov/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/aleksey-bykov/subscriptions",
    "organizations_url": "https://api.github.com/users/aleksey-bykov/orgs",
    "repos_url": "https://api.github.com/users/aleksey-bykov/repos",
    "events_url": "https://api.github.com/users/aleksey-bykov/events{/privacy}",
    "received_events_url": "https://api.github.com/users/aleksey-bykov/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-12-11T03:13:27Z",
  "updated_at": "2015-12-11T03:16:15Z",
  "body": "I am not trying to convince you. Just ranting. I get it that the design goal is to make a mainstream language that fits the least competent developer. It is indeed very frustrating that correctness isn't the goal. I get that too. There is already quite a number of features that I wish didn't exist at all, we one more one less doesn't make a difference. Hopefully the niche for a correct/sound counterpart of TypeScript will be filled soon. As soon as we have it we will get a natural partitioning between the skilled developers and beginners."
}
]
