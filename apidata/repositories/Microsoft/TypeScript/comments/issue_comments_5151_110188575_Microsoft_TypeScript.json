[
{
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/146266151",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/5151#issuecomment-146266151",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/5151",
  "id": 146266151,
  "user": {
    "login": "mhegazy",
    "id": 8000722,
    "avatar_url": "https://avatars.githubusercontent.com/u/8000722?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/mhegazy",
    "html_url": "https://github.com/mhegazy",
    "followers_url": "https://api.github.com/users/mhegazy/followers",
    "following_url": "https://api.github.com/users/mhegazy/following{/other_user}",
    "gists_url": "https://api.github.com/users/mhegazy/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/mhegazy/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/mhegazy/subscriptions",
    "organizations_url": "https://api.github.com/users/mhegazy/orgs",
    "repos_url": "https://api.github.com/users/mhegazy/repos",
    "events_url": "https://api.github.com/users/mhegazy/events{/privacy}",
    "received_events_url": "https://api.github.com/users/mhegazy/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-10-07T17:15:31Z",
  "updated_at": "2015-10-07T17:15:31Z",
  "body": "This is an interesting topic, but I do not have any ideas here. the main problem i see, is that the html template and the JS code that populates its context are detached, and the linkage happens dynamically. JSX on the other hand does not have this issue, as the JSX elements are evaluated in the context of their declaration. \r\n\r\nWould love to discuss ideas here on how to we can get the type checking in the html templates.. \r\n\r\nOne option is to get the template compilers to generate `.ts` code instead of `.js` code and then the compiler can typecheck it, we could also use something akin of sourcemaps to make sure that errors are reported in your html template instead of in the generated .ts code. \r\nthis leaves us with the tooling, like suggestions while you are writing your html template, and i think the IDE's can use naming conventions to identify the `context` object you are using and then provide completions, goto definition, etc.."
}
, {
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/146333200",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/5151#issuecomment-146333200",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/5151",
  "id": 146333200,
  "user": {
    "login": "ducin",
    "id": 375027,
    "avatar_url": "https://avatars.githubusercontent.com/u/375027?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/ducin",
    "html_url": "https://github.com/ducin",
    "followers_url": "https://api.github.com/users/ducin/followers",
    "following_url": "https://api.github.com/users/ducin/following{/other_user}",
    "gists_url": "https://api.github.com/users/ducin/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/ducin/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/ducin/subscriptions",
    "organizations_url": "https://api.github.com/users/ducin/orgs",
    "repos_url": "https://api.github.com/users/ducin/repos",
    "events_url": "https://api.github.com/users/ducin/events{/privacy}",
    "received_events_url": "https://api.github.com/users/ducin/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-10-07T21:20:50Z",
  "updated_at": "2015-10-07T21:21:46Z",
  "body": "Considering the easiest example, [underscore's _.template](http://underscorejs.org/#template) function:\r\n\r\n    var compiled = _.template(\"hello: <%= name %>\");\r\n    compiled({name: 'moe'});\r\n    => \"hello: moe\"\r\n\r\nwe should be able to decorate/extend the base function, so its contract remains the same (behaves the same way from the outside).\r\n\r\n**What we want here is to make typescript compiler throw errors, when non-existent variable is accessed in the template**, right? In order to do so, one option mentioned is to generate typescript code from template files. This generated code would have to rely on strict types (other than `any`) to run typechecking.\r\n\r\nI did a quick check, that **typescript's typecheck doesn't rely on inheritance, but on field names**:\r\n```\r\nclass A {\r\n\tfield1: string;\r\n}\r\n\r\nclass B {\r\n\tfield1: string;\r\n}\r\n\r\nclass C {\r\n\tfield2: string;\r\n}\r\n\r\nfunction hello(a: A) {\r\n\treturn a.field1;\r\n}\r\n\r\nvar a = new A();\r\nvar b = new B();\r\nvar c = new C();\r\n\r\nconsole.log( hello(a) ); // passes, obvious\r\nconsole.log( hello(b) ); // passes, although B and A are not related (!), no \"extends\", no \"implements\"\r\nconsole.log( hello(c) ); // fails, because of missing property, obvious\r\n```\r\nThis means that we could parse a template, analyse what variables/structures are used there, generate disposable interfaces with optional properties (one for each template) and use them to typecheck parameters of the compiled template. For example, having following underscore template:\r\n```\r\nvar compiled = _.template(\"hello: <%= name %>, we've got <%= weather %> weather today!\");\r\n```\r\nwe could generate the disposable interface:\r\n```\r\ninterface template_Gl0oerMwXZ {\r\n\tname?: string;\r\n\tweather?: string;\r\n}\r\n```\r\nand use it as the type of the only input parameter of the function:\r\n```\r\nvar compiled = function(templateVariables: template_Gl0oerMwXZ) {\r\n\t// do something\r\n}\r\n```\r\nThen we decorate the template with real data. The correct invocation:\r\n```\r\ncompiled({name: \"moe\", weather: \"beautiful\"});\r\n```\r\npasses, while the typo one (note weath**a**r instead of weath**e**r):\r\n```\r\ncompiled({name: \"moe\", weathar: \"beautiful\"});\r\n```\r\nfails with following error: `Argument of type '{ name: string; weathar: string; }' is not assignable to parameter of type 'template_Gl0oerMwXZ'. Object literal may only specify known properties, and 'weathar' does not exist in type 'template_Gl0oerMwXZ'.` The error tells exactly what is wrong, but it might also tell which file does the template come from.\r\n\r\nI think that's what we need, in general. This idea has some unsolved problems, such as how to re-use external template compiling functions (underscore, handlebars, etc) beneath, but above proof of concept works as expected in the typescript console.\r\n\r\n----\r\n\r\nI didn't get the sourcemaps idea in that context (however I do know source maps in general). Does this solution require to extend typescript core (e.g. interpreting those source maps)?\r\n\r\n----\r\n\r\nHope we can figure it out somehow, this would be a killer feature!"
}
, {
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/146340887",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/5151#issuecomment-146340887",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/5151",
  "id": 146340887,
  "user": {
    "login": "ducin",
    "id": 375027,
    "avatar_url": "https://avatars.githubusercontent.com/u/375027?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/ducin",
    "html_url": "https://github.com/ducin",
    "followers_url": "https://api.github.com/users/ducin/followers",
    "following_url": "https://api.github.com/users/ducin/following{/other_user}",
    "gists_url": "https://api.github.com/users/ducin/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/ducin/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/ducin/subscriptions",
    "organizations_url": "https://api.github.com/users/ducin/orgs",
    "repos_url": "https://api.github.com/users/ducin/repos",
    "events_url": "https://api.github.com/users/ducin/events{/privacy}",
    "received_events_url": "https://api.github.com/users/ducin/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-10-07T21:39:14Z",
  "updated_at": "2015-10-07T22:01:57Z",
  "body": "How to wrap the original `_.template` function with typechecking? I came up with following idea of a *preprocessor*... Each time typescript compiler encounters a template (be it a file or just inline template string), it scans it and produces following (or similar) **typescript additional code**:\r\n\r\n```\r\ninterface template_Gl0oerMwXZ {\r\n    name?: string;\r\n    weather?: string;\r\n}\r\n\r\nfunction compiled_Gl0oerMwXZ(templateCode: string) {\r\n    var compiled = _.template(templateCode);\r\n    return function decorator_Gl0oerMwXZ(templateVariables: template_Gl0oerMwXZ) {\r\n        return compiled(templateVariables);\r\n    }\r\n}\r\n```\r\nThis code is linked to the original TS code. And for each occurence of `_.template` in original ts code, such as a [backbone view](http://cdnjs.com/libraries/backbone.js/tutorials/what-is-a-view) (link code slightly modified):\r\n```\r\n  var SearchView = Backbone.View.extend({\r\n    initialize: function(){\r\n      this.render();\r\n    },\r\n    render: function(){\r\n      // Compile the template using underscore\r\n      var template = _.template( $(\"#search_template\").html() );\r\n      // Load the compiled HTML into the Backbone \"el\"\r\n      this.$el.html( template( {abc: \"def\"}) );\r\n    }\r\n```\r\nthe *preprocessor* would replace:\r\n```\r\nvar template = _.template( $(\"#search_template\").html() );\r\n```\r\nwith\r\n```\r\nvar template = compiled_Gl0oerMwXZ( $(\"#search_template\").html() );\r\n```\r\nThe code would look like this:\r\n```\r\n  var SearchView = Backbone.View.extend({\r\n    initialize: function(){\r\n      this.render();\r\n    },\r\n    render: function(){\r\n      // Compile the template using underscore\r\n      var template = compiled_Gl0oerMwXZ( $(\"#search_template\").html() );\r\n// NOTE: template is decorator_Gl0oerMwXZ with template code enclosed in the closure\r\n      // Load the compiled HTML into the Backbone \"el\"\r\n      this.$el.html( template( {abc: \"def\"}) );\r\n    }\r\n```\r\nNote that the closure function `decorator_Gl0oerMwXZ` would work out of the box.\r\n\r\nSo, for above example code, the *preprocessor* would generate:\r\n```\r\ninterface template_Gl0oerMwXZ {\r\n    name?: string;\r\n    weather?: string;\r\n}\r\n\r\nfunction compiled_Gl0oerMwXZ(templateCode: string) {\r\n    var compiled = _.template(templateCode);\r\n    return function decorator_Gl0oerMwXZ(templateVariables: template_Gl0oerMwXZ) {\r\n        return compiled(templateVariables);\r\n    }\r\n}\r\n\r\nvar SearchView = Backbone.View.extend({\r\n    initialize: function () {\r\n        this.render();\r\n    },\r\n    render: function () {\r\n        // Compile the template using underscore\r\n        var template = compiled_Gl0oerMwXZ( $(\"#search_template\").html() );\r\n        // Load the compiled HTML into the Backbone \"el\"\r\n        this.$el.html(template( {abc: \"def\"} ));\r\n    } });\r\n```\r\nand this would be now executed under standard typescript compiler, which would catch all errors.\r\n\r\nAnyway, I'm trying to find an easier way..."
}
, {
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/146348018",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/5151#issuecomment-146348018",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/5151",
  "id": 146348018,
  "user": {
    "login": "ducin",
    "id": 375027,
    "avatar_url": "https://avatars.githubusercontent.com/u/375027?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/ducin",
    "html_url": "https://github.com/ducin",
    "followers_url": "https://api.github.com/users/ducin/followers",
    "following_url": "https://api.github.com/users/ducin/following{/other_user}",
    "gists_url": "https://api.github.com/users/ducin/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/ducin/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/ducin/subscriptions",
    "organizations_url": "https://api.github.com/users/ducin/orgs",
    "repos_url": "https://api.github.com/users/ducin/repos",
    "events_url": "https://api.github.com/users/ducin/events{/privacy}",
    "received_events_url": "https://api.github.com/users/ducin/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-10-07T22:14:46Z",
  "updated_at": "2015-10-07T22:32:20Z",
  "body": "For [Handlebars](http://jsfiddle.net/aybalasubramanian/N2b5M/1/):\r\n```\r\nvar data = {\r\n    users: [ { \r\n        person: {\r\n            firstName: \"Garry\", \r\n            lastName: \"Finch\"\r\n        },\r\n        jobTitle: \"Front End Technical Lead\",\r\n        twitter: \"gazraa\" \r\n    }, {\r\n        person: {\r\n            firstName: \"Garry\", \r\n            lastName: \"Finch\"\r\n        }, \r\n        jobTitle: \"Photographer\",\r\n        twitter: \"photobasics\"\r\n    }, {\r\n        person: {\r\n            firstName: \"Garry\", \r\n            lastName: \"Finch\"\r\n        }, \r\n        jobTitle: \"LEGO Geek\",\r\n        twitter: \"minifigures\"\r\n    } ]\r\n}; \r\n\r\nHandlebars.registerHelper('fullName', function(person) {\r\n  return person.firstName + \" \" + person.lastName;\r\n});\r\n\r\n$('body').append(template(data));\r\n```\r\nit could work exactly the same way (just with more advanced data structures, but it all seems parsable):\r\n```\r\ninterface person_Gl0oerMwXZ {\r\n    firstName?: string;\r\n    lastName?: string;\r\n}\r\n\r\ninterface usersElement_Gl0oerMwXZ {\r\n    person?: person_Gl0oerMwXZ;\r\n    jobTitle?: string;\r\n    twitter?: string;\r\n}\r\n\r\ninterface users_Gl0oerMwXZ {\r\n    [i : number] : usersElement_Gl0oerMwXZ;\r\n}\r\n\r\ninterface template_Gl0oerMwXZ {\r\n    users?: users_Gl0oerMwXZ;\r\n}\r\n\r\nfunction compiled_Gl0oerMwXZ(templateCode: string) {\r\n    var compiled = Handlebars.compile(templateCode);\r\n    return function decorator_Gl0oerMwXZ(templateVariables: template_Gl0oerMwXZ) {\r\n        return compiled(templateVariables);\r\n    }\r\n}\r\n\r\nvar source = $(\"#some-template\").html(); \r\nvar template = compiled_Gl0oerMwXZ(source); \r\nvar data = {...};\r\n$('body').append(template(data));\r\n```\r\nin above example this:\r\n```\r\nvar template = Handlebars.compile(source);\r\n```\r\nhas been replaced with:\r\n```\r\nvar template = compiled_Gl0oerMwXZ(source);\r\n```\r\n\r\n## Different templating engines\r\n\r\n...just the preprocessor would need a different analyser for underscore and handlebars syntax. AngularJS templates seem more complicated, though...\r\n\r\n## Strict template properties\r\n\r\n[Handlebars' `if` syntax](http://handlebarsjs.com/builtin_helpers.html) could determine whether auto-generated interfaces do contain `?` for optional properties or not:\r\n```\r\n<div class=\"entry\">\r\n  {{#if author}}\r\n    <h1>{{firstName}} {{lastName}}</h1>\r\n  {{/if}}\r\n</div>\r\n```\r\nin above case, this would generate optional `author?: string;` property. Without the `if` clause, `author` could be a required property. Or, for compatibility reasons, there could be a *strict template mode* or some other flags that would determine the `?` presence in interfaces.\r\n\r\nAbove is handlebars's `if` syntax. Other syntax constructions could work the same way, just as `each`:\r\n```\r\n<ul class=\"people_list\">\r\n  {{#each people}}\r\n    <li>{{this}}</li>\r\n  {{/each}}\r\n</ul>\r\n```\r\nthat would determine, that a structure is a collection (just as `users_Gl0oerMwXZ ` and `usersElement_Gl0oerMwXZ` above).\r\n\r\n## Template base type: String and/or Number\r\n\r\nAll above examples rely on strings. But numbers are valid atomic types of templating engines and should be supported by ts-based templates. And typescript compiler differentiates strings and numbers. *Union types* is a possible solution. Below code compiles with no errors:\r\n\r\n```\r\ninterface person_Gl0oerMwXZ {\r\n    firstName?: string|number;\r\n    lastName?: string|number;\r\n}\r\n\r\nvar p1: person_Gl0oerMwXZ = {\r\n\tfirstName: \"Tomasz\",\r\n\tlastName: 123\r\n} \r\n```\r\nSince JavaScript doesn't check types, the original underscore/handlebars/whatever template code doesn't contain information about the type. So, probably, we'd need to state `string|number;` everywhere."
}
, {
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/146632720",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/5151#issuecomment-146632720",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/5151",
  "id": 146632720,
  "user": {
    "login": "mhegazy",
    "id": 8000722,
    "avatar_url": "https://avatars.githubusercontent.com/u/8000722?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/mhegazy",
    "html_url": "https://github.com/mhegazy",
    "followers_url": "https://api.github.com/users/mhegazy/followers",
    "following_url": "https://api.github.com/users/mhegazy/following{/other_user}",
    "gists_url": "https://api.github.com/users/mhegazy/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/mhegazy/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/mhegazy/subscriptions",
    "organizations_url": "https://api.github.com/users/mhegazy/orgs",
    "repos_url": "https://api.github.com/users/mhegazy/repos",
    "events_url": "https://api.github.com/users/mhegazy/events{/privacy}",
    "received_events_url": "https://api.github.com/users/mhegazy/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-10-08T17:35:45Z",
  "updated_at": "2015-10-08T17:35:45Z",
  "body": "i think the inlined templates are different from template files. i would say the inlined templates resolution can be solved by #3136.\r\n\r\nthe standalone templates are different though. i do not think you want to infer types from them. you want to report errors there. e.g.\r\n```html\r\n<div class=\"entry\">\r\n    <h1>{{lastname}}</h1>\r\n</div>\r\n```\r\n\r\ni want to get an error that `Class Entry does not have a property \"lastname\"`  cause i misspelled it, and it should have been \"lastName\" with a capital 'N'. \r\n\r\nI think this is more useful than saying that this template expects a type with `lastname` and you passed it a type with `lastName` when the user instantiates it.\r\n\r\none other thing to consider, is you want tooling as well. in your VSCode/VS/Sublime/Eclipse/Webstorm/etc... you want to hit \"goto def\" on `lastName` and it takes you to your `Entry` class definition, or when you ask for \"all references\" to find this one as well, this way you can rename safely.\r\n\r\nThat is why i was saying the template compiler generates .ts code, with type annotations, that is \"linked\" to the template code some how,  and then the TS compiler/Langage Service understands it as part of your program."
}
, {
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/146919148",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/5151#issuecomment-146919148",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/5151",
  "id": 146919148,
  "user": {
    "login": "ducin",
    "id": 375027,
    "avatar_url": "https://avatars.githubusercontent.com/u/375027?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/ducin",
    "html_url": "https://github.com/ducin",
    "followers_url": "https://api.github.com/users/ducin/followers",
    "following_url": "https://api.github.com/users/ducin/following{/other_user}",
    "gists_url": "https://api.github.com/users/ducin/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/ducin/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/ducin/subscriptions",
    "organizations_url": "https://api.github.com/users/ducin/orgs",
    "repos_url": "https://api.github.com/users/ducin/repos",
    "events_url": "https://api.github.com/users/ducin/events{/privacy}",
    "received_events_url": "https://api.github.com/users/ducin/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-10-09T16:24:13Z",
  "updated_at": "2015-10-09T16:24:13Z",
  "body": "Regarding the `Class Entry does not have a property \"lastname\"` and the example code you pasted:\r\n\r\n```html\r\n<div class=\"entry\">\r\n    <h1>{{lastname}}</h1>\r\n</div>\r\n```\r\nI'm not sure if you want to enforce developers to use CSS class property that way? At least I can see no other place where `Entry` class is mentioned.\r\n\r\n> That is why i was saying the template compiler generates .ts code, with type annotations, that is \"linked\" to the template code some how, and then the TS compiler/Langage Service understands it as part of your program.\r\n\r\nI was thinking about generating .ts code as well, basing on template code, that would specify the types. I'm afraid I know too little about TS compiler internals.\r\n\r\nLet me know if I can help you further."
}
, {
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/147115439",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/5151#issuecomment-147115439",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/5151",
  "id": 147115439,
  "user": {
    "login": "mhegazy",
    "id": 8000722,
    "avatar_url": "https://avatars.githubusercontent.com/u/8000722?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/mhegazy",
    "html_url": "https://github.com/mhegazy",
    "followers_url": "https://api.github.com/users/mhegazy/followers",
    "following_url": "https://api.github.com/users/mhegazy/following{/other_user}",
    "gists_url": "https://api.github.com/users/mhegazy/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/mhegazy/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/mhegazy/subscriptions",
    "organizations_url": "https://api.github.com/users/mhegazy/orgs",
    "repos_url": "https://api.github.com/users/mhegazy/repos",
    "events_url": "https://api.github.com/users/mhegazy/events{/privacy}",
    "received_events_url": "https://api.github.com/users/mhegazy/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-10-10T18:42:31Z",
  "updated_at": "2015-10-10T18:42:31Z",
  "body": "Sorry my bad. I did not mean the css class, that was just a copy/past from your example. I meant whatever \"contex\" object this template will be bound to at runtime, the one expected to have a property lastName.\r\nOne issue is to know what template file binds to at runtime, and I do not have much knowledge here..."
}
, {
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/147116712",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/5151#issuecomment-147116712",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/5151",
  "id": 147116712,
  "user": {
    "login": "ducin",
    "id": 375027,
    "avatar_url": "https://avatars.githubusercontent.com/u/375027?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/ducin",
    "html_url": "https://github.com/ducin",
    "followers_url": "https://api.github.com/users/ducin/followers",
    "following_url": "https://api.github.com/users/ducin/following{/other_user}",
    "gists_url": "https://api.github.com/users/ducin/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/ducin/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/ducin/subscriptions",
    "organizations_url": "https://api.github.com/users/ducin/orgs",
    "repos_url": "https://api.github.com/users/ducin/repos",
    "events_url": "https://api.github.com/users/ducin/events{/privacy}",
    "received_events_url": "https://api.github.com/users/ducin/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-10-10T18:52:39Z",
  "updated_at": "2015-10-10T18:53:13Z",
  "body": "Do you mean that, in order to suggest a solution, we need to divide all frontend templating engines into two groups:\r\n1. compiled in compile-time\r\n2. compiled in runtime\r\n? If not, please, clarify, how can I help."
}
, {
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/147117477",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/5151#issuecomment-147117477",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/5151",
  "id": 147117477,
  "user": {
    "login": "mhegazy",
    "id": 8000722,
    "avatar_url": "https://avatars.githubusercontent.com/u/8000722?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/mhegazy",
    "html_url": "https://github.com/mhegazy",
    "followers_url": "https://api.github.com/users/mhegazy/followers",
    "following_url": "https://api.github.com/users/mhegazy/following{/other_user}",
    "gists_url": "https://api.github.com/users/mhegazy/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/mhegazy/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/mhegazy/subscriptions",
    "organizations_url": "https://api.github.com/users/mhegazy/orgs",
    "repos_url": "https://api.github.com/users/mhegazy/repos",
    "events_url": "https://api.github.com/users/mhegazy/events{/privacy}",
    "received_events_url": "https://api.github.com/users/mhegazy/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-10-10T19:05:27Z",
  "updated_at": "2015-10-10T19:05:27Z",
  "body": "I do not think I have a solution :) I was just brain storming. I do agree that there is a need for better experience here. If you would like to champion this issue and drive a proposal I would be willing to help.\r\n\r\nI think there is error reporting, either in the template as I mentioned or at the use side as you suggested; and the tooling support."
}
, {
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/156867168",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/5151#issuecomment-156867168",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/5151",
  "id": 156867168,
  "user": {
    "login": "styfle",
    "id": 229881,
    "avatar_url": "https://avatars.githubusercontent.com/u/229881?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/styfle",
    "html_url": "https://github.com/styfle",
    "followers_url": "https://api.github.com/users/styfle/followers",
    "following_url": "https://api.github.com/users/styfle/following{/other_user}",
    "gists_url": "https://api.github.com/users/styfle/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/styfle/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/styfle/subscriptions",
    "organizations_url": "https://api.github.com/users/styfle/orgs",
    "repos_url": "https://api.github.com/users/styfle/repos",
    "events_url": "https://api.github.com/users/styfle/events{/privacy}",
    "received_events_url": "https://api.github.com/users/styfle/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-11-15T22:57:24Z",
  "updated_at": "2015-11-15T22:57:24Z",
  "body": "@ducin @mhegazy \r\nI have been thinking about this problem since our team started getting bugs when refactoring and forgetting to change the handlebars templates. It's the one place in our code that is not type safe.\r\n\r\nI implemented my own solution: [typed-tmpl](https://github.com/styfle/typed-tmpl). Let me know what you think!"
}
, {
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/182151481",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/5151#issuecomment-182151481",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/5151",
  "id": 182151481,
  "user": {
    "login": "thorn0",
    "id": 94334,
    "avatar_url": "https://avatars.githubusercontent.com/u/94334?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/thorn0",
    "html_url": "https://github.com/thorn0",
    "followers_url": "https://api.github.com/users/thorn0/followers",
    "following_url": "https://api.github.com/users/thorn0/following{/other_user}",
    "gists_url": "https://api.github.com/users/thorn0/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/thorn0/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/thorn0/subscriptions",
    "organizations_url": "https://api.github.com/users/thorn0/orgs",
    "repos_url": "https://api.github.com/users/thorn0/repos",
    "events_url": "https://api.github.com/users/thorn0/events{/privacy}",
    "received_events_url": "https://api.github.com/users/thorn0/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2016-02-10T00:54:07Z",
  "updated_at": "2016-02-10T16:04:24Z",
  "body": "I've tried coming up with a solution for Angular 1.x templates based on JSX. Please have a look at [my experiments](https://github.com/thorn0/babel-plugin-transform-ng1-jsx).\r\n\r\nIt looked really promising in the beginning, but unfortunately, even though the type-checking for JSX in TypeScript is really flexible, it turned out to be not flexible enough for Angular. \r\n\r\nSee the first entry in [the Issues section of README](https://github.com/thorn0/babel-plugin-transform-ng1-jsx#issues--tbd). If we have a look at an Angular component, we'll see that the attributes are bound directly to its properties, not to a separate `props` object. At first, I thought it worked great when I wrote an empty declaration for the `ElementAttributesProperty` interface:\r\n\r\n```\r\ninterface ElementAttributesProperty {} \r\n```\r\n\r\nYes, TypeScript started to understand that the attributes correspond to the properties of the component class. But then I discovered that the compiler wants to see all the class members as attributes, even methods, private properties, etc. If not all the members are represented as attributes, it generates an error. And there is no way to make the compiler require that only some properties be represented as attributes, not all.\r\n\r\nA demo: \r\n![demo](https://cloud.githubusercontent.com/assets/94334/12949758/443942da-d012-11e5-8951-a49b4deaa4e6.gif)"
}
]
