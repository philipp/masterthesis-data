[
{
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/69339159",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/1624#issuecomment-69339159",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/1624",
  "id": 69339159,
  "user": {
    "login": "ahejlsberg",
    "id": 4226954,
    "avatar_url": "https://avatars.githubusercontent.com/u/4226954?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/ahejlsberg",
    "html_url": "https://github.com/ahejlsberg",
    "followers_url": "https://api.github.com/users/ahejlsberg/followers",
    "following_url": "https://api.github.com/users/ahejlsberg/following{/other_user}",
    "gists_url": "https://api.github.com/users/ahejlsberg/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/ahejlsberg/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/ahejlsberg/subscriptions",
    "organizations_url": "https://api.github.com/users/ahejlsberg/orgs",
    "repos_url": "https://api.github.com/users/ahejlsberg/repos",
    "events_url": "https://api.github.com/users/ahejlsberg/events{/privacy}",
    "received_events_url": "https://api.github.com/users/ahejlsberg/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-01-09T14:18:53Z",
  "updated_at": "2015-01-09T14:18:53Z",
  "body": "This is by design and an effect of TypeScript's structural type system. See #468.\r\n\r\nIn your example you declare the `Bar` as an empty class that never references its type parameter:\r\n```typescript\r\nclass Bar<T> { }\r\n```\r\nWhen a type parameter is never referenced, it doesn't matter what type you pass as a type argument--all generic instantiations of the class are *structurally* equivalent. If you change your declaration to\r\n```typescript\r\nclass Bar<T> {\r\n    x: T;\r\n}\r\n```\r\nthe instantiations now differ by structure and you will see the expected errors."
}
, {
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/69357907",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/1624#issuecomment-69357907",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/1624",
  "id": 69357907,
  "user": {
    "login": "vujevits",
    "id": 2270661,
    "avatar_url": "https://avatars.githubusercontent.com/u/2270661?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/vujevits",
    "html_url": "https://github.com/vujevits",
    "followers_url": "https://api.github.com/users/vujevits/followers",
    "following_url": "https://api.github.com/users/vujevits/following{/other_user}",
    "gists_url": "https://api.github.com/users/vujevits/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/vujevits/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/vujevits/subscriptions",
    "organizations_url": "https://api.github.com/users/vujevits/orgs",
    "repos_url": "https://api.github.com/users/vujevits/repos",
    "events_url": "https://api.github.com/users/vujevits/events{/privacy}",
    "received_events_url": "https://api.github.com/users/vujevits/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-01-09T16:25:54Z",
  "updated_at": "2015-01-09T16:25:54Z",
  "body": "and this is true for arrays as well, \r\n\r\n```ts\r\nfunction foo():Array<string> {\r\n\tvar arr = [];\r\n\tarr.push(5);\r\n\treturn  arr;\r\n}\r\n```\r\nis there a best practice, apart from being explicit when instantiating a variable?"
}
, {
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/69361084",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/1624#issuecomment-69361084",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/1624",
  "id": 69361084,
  "user": {
    "login": "ahejlsberg",
    "id": 4226954,
    "avatar_url": "https://avatars.githubusercontent.com/u/4226954?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/ahejlsberg",
    "html_url": "https://github.com/ahejlsberg",
    "followers_url": "https://api.github.com/users/ahejlsberg/followers",
    "following_url": "https://api.github.com/users/ahejlsberg/following{/other_user}",
    "gists_url": "https://api.github.com/users/ahejlsberg/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/ahejlsberg/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/ahejlsberg/subscriptions",
    "organizations_url": "https://api.github.com/users/ahejlsberg/orgs",
    "repos_url": "https://api.github.com/users/ahejlsberg/repos",
    "events_url": "https://api.github.com/users/ahejlsberg/events{/privacy}",
    "received_events_url": "https://api.github.com/users/ahejlsberg/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-01-09T16:45:23Z",
  "updated_at": "2015-01-09T16:45:23Z",
  "body": "The array example is different. The type of `arr` is inferred as `any[]` because there is no type annotation on the declaration and  no elements in the array literal from which to infer an element type. And since an `any[]` is assignment compatible with any array type, including `string[]`, there is no error.\r\n\r\nIf you compile with the `-noImplicitAny` flag (which disallows inferences of type `any`, requiring explicit type annotations in those cases) you will get an error on the declaration of `arr` because `any[]` was implicitly inferred.\r\n\r\nThe best way to write this particular example would be:\r\n```typescript\r\nfunction foo() {\r\n    var arr: string[] = [];\r\n    arr.push(5);  // Error here\r\n    return arr;\r\n}\r\n```\r\nWith or without `-noImplicitAny` this will give you an error."
}
, {
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/69363628",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/1624#issuecomment-69363628",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/1624",
  "id": 69363628,
  "user": {
    "login": "vujevits",
    "id": 2270661,
    "avatar_url": "https://avatars.githubusercontent.com/u/2270661?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/vujevits",
    "html_url": "https://github.com/vujevits",
    "followers_url": "https://api.github.com/users/vujevits/followers",
    "following_url": "https://api.github.com/users/vujevits/following{/other_user}",
    "gists_url": "https://api.github.com/users/vujevits/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/vujevits/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/vujevits/subscriptions",
    "organizations_url": "https://api.github.com/users/vujevits/orgs",
    "repos_url": "https://api.github.com/users/vujevits/repos",
    "events_url": "https://api.github.com/users/vujevits/events{/privacy}",
    "received_events_url": "https://api.github.com/users/vujevits/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-01-09T17:00:36Z",
  "updated_at": "2015-01-09T17:00:36Z",
  "body": "Thanks."
}
]
