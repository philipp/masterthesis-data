[
{
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/123503504",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/3964#issuecomment-123503504",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/3964",
  "id": 123503504,
  "user": {
    "login": "mhegazy",
    "id": 8000722,
    "avatar_url": "https://avatars.githubusercontent.com/u/8000722?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/mhegazy",
    "html_url": "https://github.com/mhegazy",
    "followers_url": "https://api.github.com/users/mhegazy/followers",
    "following_url": "https://api.github.com/users/mhegazy/following{/other_user}",
    "gists_url": "https://api.github.com/users/mhegazy/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/mhegazy/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/mhegazy/subscriptions",
    "organizations_url": "https://api.github.com/users/mhegazy/orgs",
    "repos_url": "https://api.github.com/users/mhegazy/repos",
    "events_url": "https://api.github.com/users/mhegazy/events{/privacy}",
    "received_events_url": "https://api.github.com/users/mhegazy/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-07-21T23:00:49Z",
  "updated_at": "2015-07-21T23:00:49Z",
  "body": "How does it get emitted in a .d.ts for instance? what if the const value is dependent on non-const values or has side effects, e.g. \r\n\r\n```ts\r\ndeclare function foo():number;\r\n\r\nconst x = foo();\r\n\r\nconsole.log(\"here is x: \" + x);\r\nconsole.log(\"again: \" + x);\r\n```\r\n\r\nthe const enums are restricted to only constant expressions which is different from the const semantics; so we can not copy the implementation, we need to come up with a different proposal."
}
, {
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/123964493",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/3964#issuecomment-123964493",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/3964",
  "id": 123964493,
  "user": {
    "login": "isiahmeadows",
    "id": 4483844,
    "avatar_url": "https://avatars.githubusercontent.com/u/4483844?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/isiahmeadows",
    "html_url": "https://github.com/isiahmeadows",
    "followers_url": "https://api.github.com/users/isiahmeadows/followers",
    "following_url": "https://api.github.com/users/isiahmeadows/following{/other_user}",
    "gists_url": "https://api.github.com/users/isiahmeadows/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/isiahmeadows/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/isiahmeadows/subscriptions",
    "organizations_url": "https://api.github.com/users/isiahmeadows/orgs",
    "repos_url": "https://api.github.com/users/isiahmeadows/repos",
    "events_url": "https://api.github.com/users/isiahmeadows/events{/privacy}",
    "received_events_url": "https://api.github.com/users/isiahmeadows/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-07-23T04:57:31Z",
  "updated_at": "2015-07-23T04:57:31Z",
  "body": "Suggestion: limit that scope to what enums can handle. And FWIW, in my [enum proposal](https://gist.github.com/impinball/1462ff07309111810799), I already created an algorithm to figure out most of what's safe to replace. It's intentionally limited to booleans, numbers, strings, `null`, and `undefined`, and function calls do not count. It's towards the end of [this section](https://gist.github.com/impinball/1462ff07309111810799#enum-members).\r\n\r\nAs for functions themselves, there's a lot of bikeshedding to do to figure out how, if possible, to judge and/or enforce if a function is truly immutable or not.\r\n\r\nIf functions are explicitly designated as immutable, it's not hard to verify those functions as immutable. And for languages that are immutable by default, it's even easier. Inferring if a function is mutable is easy - it's a matter of \"Is there any way this can cause side effects?\". Merely an assignment to a reference the function didn't create is enough to say yes (variable outside a closure, assignment to member, etc.).\r\n\r\nAs for inferring immutability, that's probably impossible to do completely. Even in C/C++, as advanced as their compilers are, they still can only partially make that judgement, even with the static types and explicit reference vs value, and even without lexical closures (in the case of C). This is still a CS research topic in trying to figure how far they can. As for why it's likely impossible to fully infer this, try to factor in branch checking or recursion (which requires branch checking) into the process. It would require partially running the program to figure some parts out. You'd have to build a Turing-complete scope checker to figure that out."
}
, {
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/123987257",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/3964#issuecomment-123987257",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/3964",
  "id": 123987257,
  "user": {
    "login": "mhegazy",
    "id": 8000722,
    "avatar_url": "https://avatars.githubusercontent.com/u/8000722?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/mhegazy",
    "html_url": "https://github.com/mhegazy",
    "followers_url": "https://api.github.com/users/mhegazy/followers",
    "following_url": "https://api.github.com/users/mhegazy/following{/other_user}",
    "gists_url": "https://api.github.com/users/mhegazy/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/mhegazy/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/mhegazy/subscriptions",
    "organizations_url": "https://api.github.com/users/mhegazy/orgs",
    "repos_url": "https://api.github.com/users/mhegazy/repos",
    "events_url": "https://api.github.com/users/mhegazy/events{/privacy}",
    "received_events_url": "https://api.github.com/users/mhegazy/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-07-23T06:06:06Z",
  "updated_at": "2015-07-23T06:06:06Z",
  "body": "This is then just constant propagation. it is not strictly tied to `const` declarations or enums. we will have to allow constant expressions to emitted in .d.ts files (which today they do not) to enable this feature."
}
, {
  "url": "https://api.github.com/repos/Microsoft/TypeScript/issues/comments/123996628",
  "html_url": "https://github.com/Microsoft/TypeScript/issues/3964#issuecomment-123996628",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript/issues/3964",
  "id": 123996628,
  "user": {
    "login": "isiahmeadows",
    "id": 4483844,
    "avatar_url": "https://avatars.githubusercontent.com/u/4483844?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/isiahmeadows",
    "html_url": "https://github.com/isiahmeadows",
    "followers_url": "https://api.github.com/users/isiahmeadows/followers",
    "following_url": "https://api.github.com/users/isiahmeadows/following{/other_user}",
    "gists_url": "https://api.github.com/users/isiahmeadows/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/isiahmeadows/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/isiahmeadows/subscriptions",
    "organizations_url": "https://api.github.com/users/isiahmeadows/orgs",
    "repos_url": "https://api.github.com/users/isiahmeadows/repos",
    "events_url": "https://api.github.com/users/isiahmeadows/events{/privacy}",
    "received_events_url": "https://api.github.com/users/isiahmeadows/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-07-23T06:41:43Z",
  "updated_at": "2015-07-23T06:41:53Z",
  "body": "@mhegazy I'm aware of that. That's basically what the suggestion would entail, just as the enum situation currently does. I am fairly neutral on this, although IMHO it would be better for the compiler to allow some sort of pluggable transform like what Babel does, or a similar framework to allow that. The API exists to do the manipulations, but there isn't much of an ecosystem to go with it."
}
]
