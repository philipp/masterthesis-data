[
{
  "url": "https://api.github.com/repos/Microsoft/TypeScript-Handbook/issues/comments/130571695",
  "html_url": "https://github.com/Microsoft/TypeScript-Handbook/issues/15#issuecomment-130571695",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript-Handbook/issues/15",
  "id": 130571695,
  "user": {
    "login": "DanielRosenwasser",
    "id": 972891,
    "avatar_url": "https://avatars.githubusercontent.com/u/972891?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/DanielRosenwasser",
    "html_url": "https://github.com/DanielRosenwasser",
    "followers_url": "https://api.github.com/users/DanielRosenwasser/followers",
    "following_url": "https://api.github.com/users/DanielRosenwasser/following{/other_user}",
    "gists_url": "https://api.github.com/users/DanielRosenwasser/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/DanielRosenwasser/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/DanielRosenwasser/subscriptions",
    "organizations_url": "https://api.github.com/users/DanielRosenwasser/orgs",
    "repos_url": "https://api.github.com/users/DanielRosenwasser/repos",
    "events_url": "https://api.github.com/users/DanielRosenwasser/events{/privacy}",
    "received_events_url": "https://api.github.com/users/DanielRosenwasser/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-08-13T08:16:55Z",
  "updated_at": "2015-08-13T08:16:55Z",
  "body": "This could probably use union types, and potentially local types."
}
, {
  "url": "https://api.github.com/repos/Microsoft/TypeScript-Handbook/issues/comments/144154906",
  "html_url": "https://github.com/Microsoft/TypeScript-Handbook/issues/15#issuecomment-144154906",
  "issue_url": "https://api.github.com/repos/Microsoft/TypeScript-Handbook/issues/15",
  "id": 144154906,
  "user": {
    "login": "DustinWehr",
    "id": 1623534,
    "avatar_url": "https://avatars.githubusercontent.com/u/1623534?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/DustinWehr",
    "html_url": "https://github.com/DustinWehr",
    "followers_url": "https://api.github.com/users/DustinWehr/followers",
    "following_url": "https://api.github.com/users/DustinWehr/following{/other_user}",
    "gists_url": "https://api.github.com/users/DustinWehr/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/DustinWehr/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/DustinWehr/subscriptions",
    "organizations_url": "https://api.github.com/users/DustinWehr/orgs",
    "repos_url": "https://api.github.com/users/DustinWehr/repos",
    "events_url": "https://api.github.com/users/DustinWehr/events{/privacy}",
    "received_events_url": "https://api.github.com/users/DustinWehr/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-09-29T18:53:04Z",
  "updated_at": "2015-09-29T18:53:04Z",
  "body": "I took some time (too much) to refine the Mixin pattern I've been using, with the intention of offering to write a new version of http://www.typescriptlang.org/Handbook#mixins if you guys like it. I think it's a good use of user-defined type guard functions.\r\nI'll give a short summary, then some items of differences between the new pattern and the Handbook's, and then the example from the Handbook rewritten in terms of the new pattern. I have no opinion about naming conventions, so please do suggest alternatives.\r\n\r\nFor the summary I'll use the example as a reference.  The new pattern produces a namespace ActivatableMixin with three visible members:\r\n-An interface, `ActivatableInterface`\r\n-A function `usedBy`, which tests if an object's class uses the mixin. This is the main improvement over the Handbook pattern.\r\n-A function `mixInto`, such that `ActivatableMixin.mixInto(C)` does everything that the Handbook's `applyMixins(C, [Activatable])` does, and additionally stores the class `C`, so we can later use `(x instanceof C)` in `ActivatableMixin.usedBy`.\r\n\r\nIn addition to the usedBy function, I'm suggesting two small alterations to prevent some surprising behavior of the current Handbook pattern that bit me.\r\n\r\n1. Added an option to exclude \"constructor\" from the mixed-in property names. In the code below (see makeMixinApplicator) it defaults to true. When it's excluded, you have `smartObject.constructor === SmartObject` instead of `smartObject.constructor === <constructor of last-supplied mixin>`, which I think is less surprising, at least for someone like me who didn't know the JS prototype system well when first learning TS.\r\n\r\n2. The implementation of the mixin is hidden. This is so that a new TS user doesn't mistakenly think they can use instanceof to check if an object implements a mixin. It's an easy mistake to make since `smartObject instanceof Activatable` compiles fine.\r\n\r\n\r\n```typescript\r\nnamespace DisposableMixin {\r\n  var classesUsingThis : Function[] = [];\r\n\r\n  export interface DisposableInterface {\r\n    isDisposed: boolean;\r\n    dispose() : void;\r\n  }\r\n\r\n  class DisposableImplementation implements DisposableInterface {\r\n    isDisposed:boolean;\r\n    dispose():void {\r\n      this.isDisposed = true;\r\n    }\r\n  }\r\n\r\n  export var mixInto = makeMixinApplicator(DisposableImplementation, classesUsingThis);\r\n  export var usedBy = makeMixinInstanceChecker<DisposableInterface>(classesUsingThis);\r\n}\r\n\r\n\r\nnamespace ActivatableMixin {\r\n  var classesUsingThis : Function[] = [];\r\n\r\n  export interface ActivatableInterface {\r\n    isActive: boolean\r\n    activate() : void\r\n    deactivate() : void\r\n  }\r\n\r\n  class ActivatableImplementation implements ActivatableInterface {\r\n    isActive: boolean;\r\n    activate() {\r\n      this.isActive = true;\r\n    }\r\n    deactivate() {\r\n      this.isActive = false;\r\n    }\r\n  }\r\n\r\n  export var mixInto = makeMixinApplicator(ActivatableImplementation, classesUsingThis);\r\n  export var isActivatable = makeMixinInstanceChecker<ActivatableInterface>(classesUsingThis);\r\n}\r\n\r\nimport DisposableInterface = DisposableMixin.DisposableInterface;\r\nimport ActivatableInterface = ActivatableMixin.ActivatableInterface;\r\n\r\nclass SmartObject implements DisposableInterface, ActivatableInterface {\r\n  constructor() {\r\n    setInterval(() => console.log(\"activated: \" + this.isActive + \" | disposed: \" + this.isDisposed), 500);\r\n  }\r\n\r\n  interact() {\r\n    this.activate();\r\n  }\r\n\r\n  // Disposable\r\n  isDisposed: boolean = false;\r\n  dispose: () => void;\r\n\r\n  // Activatable\r\n  isActive: boolean = false;\r\n  activate: () => void;\r\n  deactivate: () => void;\r\n}\r\nDisposableMixin.mixInto(SmartObject);\r\nActivatableMixin.mixInto(SmartObject);\r\n\r\nvar smartObj = new SmartObject();\r\nsetTimeout(() => smartObj.interact(), 1000);\r\n\r\n\r\n////////////////////////////////////////\r\n// In your runtime library somewhere\r\n////////////////////////////////////////\r\n\r\nfunction makeMixinApplicator(mixinImplementation:any, classesUsingMixin:any[], exclude_constructor=true) : (c:any) => void {\r\n  return (c:any) => {\r\n    classesUsingMixin.push(c);\r\n    Object.getOwnPropertyNames(mixinImplementation.prototype).forEach(name => {\r\n      if(!exclude_constructor || name !== \"constructor\") {\r\n        (<any>c.prototype)[name] = (<any>mixinImplementation.prototype)[name];\r\n      }\r\n    })\r\n  };\r\n}\r\n\r\nfunction makeMixinInstanceChecker<T>(classesUsingMixin:Function[]) : ( (x:any) => x is T ) {\r\n  return (x:any) : x is T => {\r\n    for(let i=0; i<classesUsingMixin.length; i++) {\r\n      if(x instanceof classesUsingMixin[i])\r\n        return true;\r\n    }\r\n    return false;\r\n  }\r\n}\r\n```"
}
]
