[
{
  "url": "https://api.github.com/repos/Microsoft/clrmd/issues/comments/156302254",
  "html_url": "https://github.com/Microsoft/clrmd/issues/32#issuecomment-156302254",
  "issue_url": "https://api.github.com/repos/Microsoft/clrmd/issues/32",
  "id": 156302254,
  "user": {
    "login": "leculver",
    "id": 8496639,
    "avatar_url": "https://avatars.githubusercontent.com/u/8496639?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/leculver",
    "html_url": "https://github.com/leculver",
    "followers_url": "https://api.github.com/users/leculver/followers",
    "following_url": "https://api.github.com/users/leculver/following{/other_user}",
    "gists_url": "https://api.github.com/users/leculver/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/leculver/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/leculver/subscriptions",
    "organizations_url": "https://api.github.com/users/leculver/orgs",
    "repos_url": "https://api.github.com/users/leculver/repos",
    "events_url": "https://api.github.com/users/leculver/events{/privacy}",
    "received_events_url": "https://api.github.com/users/leculver/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-11-13T02:28:16Z",
  "updated_at": "2015-11-13T02:29:01Z",
  "body": "Apologies for the delay in responding, it's been a busy week.  I suppose this should be better documented, but CreateFromDebuggerInterface expects that you have set up IDebugClient in a way such that it is ready to be used.  The HRESULT is the giveaway:  0x8000fff is the \"catastrophic\" error code that dbgeng hands out when it's not ready for the APIs that are called.  The goal with this API is to allow you to construct a `DataTarget` from your own DbgEng debugger session.\r\n\r\nHere is a minimal example of it working properly:\r\n\r\n    using Microsoft.Diagnostics.Runtime;\r\n    using Microsoft.Diagnostics.Runtime.Interop;\r\n    using System;\r\n    using System.Diagnostics;\r\n    using System.Runtime.InteropServices;\r\n\r\n    class Program\r\n    {\r\n        static void Main(string[] args)\r\n        {\r\n            Guid debugClientGuid = typeof(IDebugClient).GUID;\r\n            object debugClientIUnknown;\r\n            int hr = DebugCreate(ref debugClientGuid, out debugClientIUnknown);\r\n            Debug.Assert(hr == 0);\r\n\r\n            IDebugClient client = (IDebugClient)debugClientIUnknown;\r\n            IDebugControl control = (IDebugControl)debugClientIUnknown;\r\n            int hr = client.OpenDumpFile(@\"crash.dmp\");\r\n            Debug.Assert(hr == 0);\r\n            hr = control.WaitForEvent(DEBUG_WAIT.DEFAULT, 5000);\r\n            Debug.Assert(hr == 0);\r\n\r\n            using (DataTarget target = DataTarget.CreateFromDebuggerInterface(client))\r\n            {\r\n                // TODO\r\n            }\r\n        }\r\n\r\n        [DllImport(\"dbgeng.dll\")]\r\n        public static extern int DebugCreate(ref Guid InterfaceId, [MarshalAs(UnmanagedType.IUnknown)] out object Interface);\r\n    }\r\n\r\nSimilarly, you can look in the ClrMD source's test infrastructure to see more examples of using DbgEng (specifically https://github.com/Microsoft/clrmd/blob/master/src/Microsoft.Diagnostics.Runtime.Tests/Debugger.cs and things that call it).  However, DbgEng itself (IDebugClient and related interfaces) is a Microsoft library independent from ClrMD, so you should consult its documentation on how to use it...\r\n\r\nDoes this address your concern?"
}
, {
  "url": "https://api.github.com/repos/Microsoft/clrmd/issues/comments/156453543",
  "html_url": "https://github.com/Microsoft/clrmd/issues/32#issuecomment-156453543",
  "issue_url": "https://api.github.com/repos/Microsoft/clrmd/issues/32",
  "id": 156453543,
  "user": {
    "login": "keithfink",
    "id": 15802710,
    "avatar_url": "https://avatars.githubusercontent.com/u/15802710?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/keithfink",
    "html_url": "https://github.com/keithfink",
    "followers_url": "https://api.github.com/users/keithfink/followers",
    "following_url": "https://api.github.com/users/keithfink/following{/other_user}",
    "gists_url": "https://api.github.com/users/keithfink/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/keithfink/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/keithfink/subscriptions",
    "organizations_url": "https://api.github.com/users/keithfink/orgs",
    "repos_url": "https://api.github.com/users/keithfink/repos",
    "events_url": "https://api.github.com/users/keithfink/events{/privacy}",
    "received_events_url": "https://api.github.com/users/keithfink/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-11-13T14:56:45Z",
  "updated_at": "2015-11-13T14:56:45Z",
  "body": "Hi Lee,\nThank you for the response! Yes, this is certainly helpful. I actually figured this out yesterday morning and the key was the call to WaitForEvent. Once I added that, it started working.\nThe reason I used this approach is because in addition to all the great things ClrMd offer, I would also like to be able to execute debugger commands via IDebugControl::Execute / ExecuteWide. I didn't see an API exposed in ClrMd for actually executing commands through the engine, so that's why I went with this approach. Though I suppose I could just cast the DataTarget.DebuggerInterface to an IDebugControl. Hmm... I wonder I didn't do that to start with.\nThanks for all the great work here.  I am writing some diagnostic tooling for our company and ClrMd is a fantastic help.  Before this, I worked for Microsoft Developer Support for many years so am very familiar with sos, psscor, windbg, etc. (though this is my first attempt at using the dbgeng API). \n\n\nThanks,\n\n\nKeith\nDate: Thu, 12 Nov 2015 18:28:25 -0800\nFrom: notifications@github.com\nTo: clrmd@noreply.github.com\nCC: keithfink@hotmail.com\nSubject: Re: [clrmd] DataTarget.CreateFromDebuggerInterface Throws Exception (#32)\n\nApologies for the delay in responding, it's been a busy week.  I suppose this should be better documented, but CreateFromDebuggerInterface expects that you have set up IDebugClient in a way such that it is ready to be used.  The HRESULT is the giveaway:  0x8000fff is the \"catastrophic\" error code that dbgeng hands out when it's not ready for the APIs that are called.  The goal with this API is to allow you to construct a DataTarget from a DbgEng debugger session.\n\n\nHere is a minimal example of it working properly:\n\n\nusing Microsoft.Diagnostics.Runtime;\nusing Microsoft.Diagnostics.Runtime.Interop;\nusing System;\nusing System.Diagnostics;\nusing System.Runtime.InteropServices;\n\nclass Program\n{\n    static void Main(string[] args)\n    {\n        Guid debugClientGuid = typeof(IDebugClient).GUID;\n        object debugClientIUnknown;\n        int hr = DebugCreate(ref debugClientGuid, out debugClientIUnknown);\n        Debug.Assert(hr == 0);\n\n        IDebugClient client = (IDebugClient)debugClientIUnknown;\n        IDebugControl control = (IDebugControl)debugClientIUnknown;\n        int hr = client.OpenDumpFile(@\"crash.dmp\");\n        Debug.Assert(hr == 0);\n        control.WaitForEvent(DEBUG_WAIT.DEFAULT, 5000);\n        Debug.Assert(hr == 0);\n\n        using (DataTarget target = DataTarget.CreateFromDebuggerInterface(client))\n        {\n            // TODO\n        }\n    }\n\n    [DllImport(\"dbgeng.dll\")]\n    public static extern int DebugCreate(ref Guid InterfaceId, [MarshalAs(UnmanagedType.IUnknown)] out object Interface);\n}\n\n\nSimilarly, you can look in the ClrMD source's test infrastructure to see more examples of using DbgEng (specifically https://github.com/Microsoft/clrmd/blob/master/src/Microsoft.Diagnostics.Runtime.Tests/Debugger.cs and things that call it).  However, DbgEng itself (IDebugClient and related interfaces) is a Microsoft library independent from ClrMD, so you should consult its documentation on how to use it...\n\n\nDoes this address your concern?\n\n\n—\nReply to this email directly or view it on GitHub.\n\n\n  \n  \n\n\n \t\t \t   \t\t  "
}
]
