[
{
  "url": "https://api.github.com/repos/aws/aws-sdk-ruby/issues/comments/14790874",
  "html_url": "https://github.com/aws/aws-sdk-ruby/issues/193#issuecomment-14790874",
  "issue_url": "https://api.github.com/repos/aws/aws-sdk-ruby/issues/193",
  "id": 14790874,
  "user": {
    "login": "trevorrowe",
    "id": 27863,
    "avatar_url": "https://avatars.githubusercontent.com/u/27863?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/trevorrowe",
    "html_url": "https://github.com/trevorrowe",
    "followers_url": "https://api.github.com/users/trevorrowe/followers",
    "following_url": "https://api.github.com/users/trevorrowe/following{/other_user}",
    "gists_url": "https://api.github.com/users/trevorrowe/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/trevorrowe/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/trevorrowe/subscriptions",
    "organizations_url": "https://api.github.com/users/trevorrowe/orgs",
    "repos_url": "https://api.github.com/users/trevorrowe/repos",
    "events_url": "https://api.github.com/users/trevorrowe/events{/privacy}",
    "received_events_url": "https://api.github.com/users/trevorrowe/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-03-12T17:42:43Z",
  "updated_at": "2013-03-12T17:42:43Z",
  "body": "Each service behaves differently.  Some will block until the resource is in a state that you can describe it (the state may be pending or creating for example) and other will return straightway and you may have to guard against returned errors about the resource not existing for a period of time.  \r\n\r\nMost of our resources have `#exists?` methods you can call.  These generally make a describe call and then rescue s any missing resource exceptions that might get raised.\r\n\r\nI recommend using code like the following to wait for a particular state:\r\n\r\n```ruby\r\ndef wait_for options = {}, &block\r\n  require 'timeout'\r\n  Timeout::timeout(options[:max_seconds] || 60) do\r\n    until block.call\r\n      sleep(options[:delay] || 10)\r\n    end\r\n  end\r\nend\r\n\r\nwait_for { instance.exists? }\r\n```\r\n\r\nThis helper method ensures you don't block indefinitely.  We do have plans to add additional helpers to the SDK that make it easy to wait for resources to hit particular states."
}
, {
  "url": "https://api.github.com/repos/aws/aws-sdk-ruby/issues/comments/14831725",
  "html_url": "https://github.com/aws/aws-sdk-ruby/issues/193#issuecomment-14831725",
  "issue_url": "https://api.github.com/repos/aws/aws-sdk-ruby/issues/193",
  "id": 14831725,
  "user": {
    "login": "jan",
    "id": 1519,
    "avatar_url": "https://avatars.githubusercontent.com/u/1519?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/jan",
    "html_url": "https://github.com/jan",
    "followers_url": "https://api.github.com/users/jan/followers",
    "following_url": "https://api.github.com/users/jan/following{/other_user}",
    "gists_url": "https://api.github.com/users/jan/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/jan/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/jan/subscriptions",
    "organizations_url": "https://api.github.com/users/jan/orgs",
    "repos_url": "https://api.github.com/users/jan/repos",
    "events_url": "https://api.github.com/users/jan/events{/privacy}",
    "received_events_url": "https://api.github.com/users/jan/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-03-13T09:30:47Z",
  "updated_at": "2013-03-13T09:30:47Z",
  "body": "Thanks for the quick and helpful response, @trevorrowe. \r\n\r\nFrom a user's perspective to the SDK, it is a a strange concept that after a create, the instance might not exist. Does the API return \"okay\" before even creating the instance description? This would mean, that every user of the SDK would have to code this timeout behavior after an instance create. And that really means, that it should be done on the SDK level in the create instance call, and meanwhile be mentioned in the documentation with blinking alert signs attached to it. Don't you think? \r\n\r\nI see that there might be a use case for cranking out instances and not using the returned instance object afterwards, but I would think that the majority of users does access the instance after creating it.\r\n\r\nOf course you cannot change the default behavior anymore. Maybe an option or optional second parameter `block_until_exists`? Or a helper method on instance itself with that name?\r\n\r\n```ruby\r\ninstance = region.instances.create({image_id: 'ami-myami'}).block_until_exists\r\ninstance.add_tag(\"Name\", value: \"trevorrowe\")\r\n```\r\n\r\nAnyway, you'll figure it out and I know to solve it for my case. Thank you again."
}
, {
  "url": "https://api.github.com/repos/aws/aws-sdk-ruby/issues/comments/14843091",
  "html_url": "https://github.com/aws/aws-sdk-ruby/issues/193#issuecomment-14843091",
  "issue_url": "https://api.github.com/repos/aws/aws-sdk-ruby/issues/193",
  "id": 14843091,
  "user": {
    "login": "lsegal",
    "id": 686,
    "avatar_url": "https://avatars.githubusercontent.com/u/686?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/lsegal",
    "html_url": "https://github.com/lsegal",
    "followers_url": "https://api.github.com/users/lsegal/followers",
    "following_url": "https://api.github.com/users/lsegal/following{/other_user}",
    "gists_url": "https://api.github.com/users/lsegal/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/lsegal/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/lsegal/subscriptions",
    "organizations_url": "https://api.github.com/users/lsegal/orgs",
    "repos_url": "https://api.github.com/users/lsegal/repos",
    "events_url": "https://api.github.com/users/lsegal/events{/privacy}",
    "received_events_url": "https://api.github.com/users/lsegal/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-03-13T14:10:55Z",
  "updated_at": "2013-03-13T14:11:57Z",
  "body": "Hey @jan,\r\n\r\nYes, the API returns from the call before it \"creates\" the instance. This is due to the fact that instance creation is an operation that can only be considered *eventually consistent*. Some services optimize for eventual consistency over atomicity for performance reasons, and occasionally some API operations *require* eventual consistency because they take a relatively long time to process and would otherwise end up tying up a socket connection instead of serving other requests. \r\n\r\nAs @trevorrowe pointed out, this differs per the needs of each service, though we do want to have better support for waiting on specific resource states for these scenarios. That said, you'll probably see an API closer to the one proposed by Trevor above, or perhaps something like `wait_for(&:exists?)` / `wait_for(:created)`, instead of a separate `block_until_exists` method. This is because some resources have many different states (created, deleted, pending, etc.), so we would want an API that could express all of these different states in a consistent way."
}
]
