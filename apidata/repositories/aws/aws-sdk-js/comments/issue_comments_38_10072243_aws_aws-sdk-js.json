[
{
  "url": "https://api.github.com/repos/aws/aws-sdk-js/issues/comments/12390115",
  "html_url": "https://github.com/aws/aws-sdk-js/pull/38#issuecomment-12390115",
  "issue_url": "https://api.github.com/repos/aws/aws-sdk-js/issues/38",
  "id": 12390115,
  "user": {
    "login": "lsegal",
    "id": 686,
    "avatar_url": "https://avatars.githubusercontent.com/u/686?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/lsegal",
    "html_url": "https://github.com/lsegal",
    "followers_url": "https://api.github.com/users/lsegal/followers",
    "following_url": "https://api.github.com/users/lsegal/following{/other_user}",
    "gists_url": "https://api.github.com/users/lsegal/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/lsegal/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/lsegal/subscriptions",
    "organizations_url": "https://api.github.com/users/lsegal/orgs",
    "repos_url": "https://api.github.com/users/lsegal/repos",
    "events_url": "https://api.github.com/users/lsegal/events{/privacy}",
    "received_events_url": "https://api.github.com/users/lsegal/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-01-17T20:30:49Z",
  "updated_at": "2013-01-17T20:30:49Z",
  "body": "This patch actually causes our tests to fail (not related to the false positive from Travis CI's failed test above) because `arrayEach` is not explicitly meant *just* for arrays, but rather to iterate over properties with the property value as the first param of the callback (rather than the standard each, which passes the property name first). It's named `arrayEach` because it's more useful for array iteration (where you don't care about the property name, aka the index), but we actually use this in [event_emitter.js](https://github.com/aws/aws-sdk-js/blob/d8f3ca82b6920e4ca11ddd4b8ea50049d4b287e1/lib/event_emitter.js#L28) for objects that *can* be arrays *or* standard objects. The related failing tests are failing when a non-array is passed into the above function.\r\n\r\nAll that said, there are two ways to solve this:\r\n\r\n1. We take a hard stance that `arrayEach` *only* allows actual *Array* objects as parameters. This would mean changing the above failing code to check Array vs non-array objects and choosing the appropriate looping function, or,\r\n2. We add the `if (obj.hasOwnProperty(prop))` check that `each` uses to still handle both object types at a performance cost. \r\n\r\nI'm inclined to go with option 2, since I think a better policy for array-primitive iteration is to inline the for loops directly, ie., if we know we're always dealing with arrays, we should not be using the util function."
}
, {
  "url": "https://api.github.com/repos/aws/aws-sdk-js/issues/comments/12390162",
  "html_url": "https://github.com/aws/aws-sdk-js/pull/38#issuecomment-12390162",
  "issue_url": "https://api.github.com/repos/aws/aws-sdk-js/issues/38",
  "id": 12390162,
  "user": {
    "login": "lsegal",
    "id": 686,
    "avatar_url": "https://avatars.githubusercontent.com/u/686?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/lsegal",
    "html_url": "https://github.com/lsegal",
    "followers_url": "https://api.github.com/users/lsegal/followers",
    "following_url": "https://api.github.com/users/lsegal/following{/other_user}",
    "gists_url": "https://api.github.com/users/lsegal/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/lsegal/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/lsegal/subscriptions",
    "organizations_url": "https://api.github.com/users/lsegal/orgs",
    "repos_url": "https://api.github.com/users/lsegal/repos",
    "events_url": "https://api.github.com/users/lsegal/events{/privacy}",
    "received_events_url": "https://api.github.com/users/lsegal/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-01-17T20:31:40Z",
  "updated_at": "2013-01-17T20:31:40Z",
  "body": "sidenote: along with option 2, we might want to rename the function to cause less confusion. Although the function is in our private API, I agree that the intent can be confusing."
}
, {
  "url": "https://api.github.com/repos/aws/aws-sdk-js/issues/comments/12393205",
  "html_url": "https://github.com/aws/aws-sdk-js/pull/38#issuecomment-12393205",
  "issue_url": "https://api.github.com/repos/aws/aws-sdk-js/issues/38",
  "id": 12393205,
  "user": {
    "login": "corymsmith",
    "id": 7315,
    "avatar_url": "https://avatars.githubusercontent.com/u/7315?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/corymsmith",
    "html_url": "https://github.com/corymsmith",
    "followers_url": "https://api.github.com/users/corymsmith/followers",
    "following_url": "https://api.github.com/users/corymsmith/following{/other_user}",
    "gists_url": "https://api.github.com/users/corymsmith/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/corymsmith/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/corymsmith/subscriptions",
    "organizations_url": "https://api.github.com/users/corymsmith/orgs",
    "repos_url": "https://api.github.com/users/corymsmith/repos",
    "events_url": "https://api.github.com/users/corymsmith/events{/privacy}",
    "received_events_url": "https://api.github.com/users/corymsmith/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-01-17T21:41:41Z",
  "updated_at": "2013-01-17T21:41:41Z",
  "body": "My bad, I had run the tests initially and they failed (but hadn't realized I fat fingered an extra character in there) so it seemed like those tests always failed. I can revert and add the obj.hasOwnProperty in there and resubmit the pull request if you'd like. After ensuring all tests pass this time :)"
}
, {
  "url": "https://api.github.com/repos/aws/aws-sdk-js/issues/comments/12393378",
  "html_url": "https://github.com/aws/aws-sdk-js/pull/38#issuecomment-12393378",
  "issue_url": "https://api.github.com/repos/aws/aws-sdk-js/issues/38",
  "id": 12393378,
  "user": {
    "login": "lsegal",
    "id": 686,
    "avatar_url": "https://avatars.githubusercontent.com/u/686?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/lsegal",
    "html_url": "https://github.com/lsegal",
    "followers_url": "https://api.github.com/users/lsegal/followers",
    "following_url": "https://api.github.com/users/lsegal/following{/other_user}",
    "gists_url": "https://api.github.com/users/lsegal/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/lsegal/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/lsegal/subscriptions",
    "organizations_url": "https://api.github.com/users/lsegal/orgs",
    "repos_url": "https://api.github.com/users/lsegal/repos",
    "events_url": "https://api.github.com/users/lsegal/events{/privacy}",
    "received_events_url": "https://api.github.com/users/lsegal/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-01-17T21:45:45Z",
  "updated_at": "2013-01-17T21:45:45Z",
  "body": "That would be great!"
}
, {
  "url": "https://api.github.com/repos/aws/aws-sdk-js/issues/comments/12393417",
  "html_url": "https://github.com/aws/aws-sdk-js/pull/38#issuecomment-12393417",
  "issue_url": "https://api.github.com/repos/aws/aws-sdk-js/issues/38",
  "id": 12393417,
  "user": {
    "login": "lsegal",
    "id": 686,
    "avatar_url": "https://avatars.githubusercontent.com/u/686?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/lsegal",
    "html_url": "https://github.com/lsegal",
    "followers_url": "https://api.github.com/users/lsegal/followers",
    "following_url": "https://api.github.com/users/lsegal/following{/other_user}",
    "gists_url": "https://api.github.com/users/lsegal/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/lsegal/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/lsegal/subscriptions",
    "organizations_url": "https://api.github.com/users/lsegal/orgs",
    "repos_url": "https://api.github.com/users/lsegal/repos",
    "events_url": "https://api.github.com/users/lsegal/events{/privacy}",
    "received_events_url": "https://api.github.com/users/lsegal/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-01-17T21:46:37Z",
  "updated_at": "2013-01-17T21:53:47Z",
  "body": "Just push to your master branch with the fix. GitHub should pick it up."
}
, {
  "url": "https://api.github.com/repos/aws/aws-sdk-js/issues/comments/12394006",
  "html_url": "https://github.com/aws/aws-sdk-js/pull/38#issuecomment-12394006",
  "issue_url": "https://api.github.com/repos/aws/aws-sdk-js/issues/38",
  "id": 12394006,
  "user": {
    "login": "lsegal",
    "id": 686,
    "avatar_url": "https://avatars.githubusercontent.com/u/686?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/lsegal",
    "html_url": "https://github.com/lsegal",
    "followers_url": "https://api.github.com/users/lsegal/followers",
    "following_url": "https://api.github.com/users/lsegal/following{/other_user}",
    "gists_url": "https://api.github.com/users/lsegal/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/lsegal/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/lsegal/subscriptions",
    "organizations_url": "https://api.github.com/users/lsegal/orgs",
    "repos_url": "https://api.github.com/users/lsegal/repos",
    "events_url": "https://api.github.com/users/lsegal/events{/privacy}",
    "received_events_url": "https://api.github.com/users/lsegal/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-01-17T21:59:08Z",
  "updated_at": "2013-01-17T21:59:08Z",
  "body": "Thanks @corymsmith!"
}
]
