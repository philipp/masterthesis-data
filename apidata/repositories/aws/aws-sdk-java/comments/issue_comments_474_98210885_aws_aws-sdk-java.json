[
{
  "url": "https://api.github.com/repos/aws/aws-sdk-java/issues/comments/126413580",
  "html_url": "https://github.com/aws/aws-sdk-java/issues/474#issuecomment-126413580",
  "issue_url": "https://api.github.com/repos/aws/aws-sdk-java/issues/474",
  "id": 126413580,
  "user": {
    "login": "david-at-aws",
    "id": 5216241,
    "avatar_url": "https://avatars.githubusercontent.com/u/5216241?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/david-at-aws",
    "html_url": "https://github.com/david-at-aws",
    "followers_url": "https://api.github.com/users/david-at-aws/followers",
    "following_url": "https://api.github.com/users/david-at-aws/following{/other_user}",
    "gists_url": "https://api.github.com/users/david-at-aws/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/david-at-aws/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/david-at-aws/subscriptions",
    "organizations_url": "https://api.github.com/users/david-at-aws/orgs",
    "repos_url": "https://api.github.com/users/david-at-aws/repos",
    "events_url": "https://api.github.com/users/david-at-aws/events{/privacy}",
    "received_events_url": "https://api.github.com/users/david-at-aws/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-07-30T17:39:15Z",
  "updated_at": "2015-07-30T17:39:15Z",
  "body": "This is the expected behavior if you don't provide a content-length, at least for the moment. If you do know the content length, providing it up front will allow the TransferManager to be significantly more efficient.\r\n\r\nS3 requires a content-length header to be specified on each putObject/uploadPart request - if the length isn't known up front (and the stream doesn't support mark/reset), the SDK attempts to buffer the stream into memory to find out how large it is. This obviously only works for relatively small streams - for anything large enough that it can't be buffered in memory, you need to provide a content-length up front. The TransferManager assumes that if you haven't provided a content-length, the stream is small enough that doing a single-part upload will be more efficient than a multi-part upload.\r\n\r\nTheoretically we could add a configuration option to force a multi-part upload even when the content-length is unknown, using the configured [minimum part size](https://github.com/aws/aws-sdk-java/blob/master/aws-java-sdk-s3/src/main/java/com/amazonaws/services/s3/transfer/TransferManagerConfiguration.java#L80) and hoping for the best? You'd still have to buffer 5MB at a time to calculate the content-length for each individual part (more if you want to go over ~50GB total, since S3 allows a maximum of 10K parts per MPU), but at least you wouldn't have to buffer the entire thing if you legitimately don't know how large it's going to be.\r\n\r\nI'll add this to our backlog (please chime in here if this would be useful to you so we can prioritize appropriately), or I'd be happy to take a look at a pull request."
}
, {
  "url": "https://api.github.com/repos/aws/aws-sdk-java/issues/comments/126756824",
  "html_url": "https://github.com/aws/aws-sdk-java/issues/474#issuecomment-126756824",
  "issue_url": "https://api.github.com/repos/aws/aws-sdk-java/issues/474",
  "id": 126756824,
  "user": {
    "login": "selaamobee",
    "id": 13403249,
    "avatar_url": "https://avatars.githubusercontent.com/u/13403249?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/selaamobee",
    "html_url": "https://github.com/selaamobee",
    "followers_url": "https://api.github.com/users/selaamobee/followers",
    "following_url": "https://api.github.com/users/selaamobee/following{/other_user}",
    "gists_url": "https://api.github.com/users/selaamobee/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/selaamobee/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/selaamobee/subscriptions",
    "organizations_url": "https://api.github.com/users/selaamobee/orgs",
    "repos_url": "https://api.github.com/users/selaamobee/repos",
    "events_url": "https://api.github.com/users/selaamobee/events{/privacy}",
    "received_events_url": "https://api.github.com/users/selaamobee/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-07-31T17:20:02Z",
  "updated_at": "2015-07-31T17:20:02Z",
  "body": "Hello David and thank you for the quick and detailed response.\r\n\r\nI am trying to write to S3 from a `ZipInputStream` and the `ZipEntry.getSize()` may return -1.\r\n\r\nBuffering the file in memory is not an option since it is quite large and I get an `OutOfMemoryException`.\r\n\r\nWriting it to disk is probably what I do but I would have preferred not to do that.\r\n\r\nSeems to me like a valid and common use case."
}
, {
  "url": "https://api.github.com/repos/aws/aws-sdk-java/issues/comments/126766976",
  "html_url": "https://github.com/aws/aws-sdk-java/issues/474#issuecomment-126766976",
  "issue_url": "https://api.github.com/repos/aws/aws-sdk-java/issues/474",
  "id": 126766976,
  "user": {
    "login": "david-at-aws",
    "id": 5216241,
    "avatar_url": "https://avatars.githubusercontent.com/u/5216241?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/david-at-aws",
    "html_url": "https://github.com/david-at-aws",
    "followers_url": "https://api.github.com/users/david-at-aws/followers",
    "following_url": "https://api.github.com/users/david-at-aws/following{/other_user}",
    "gists_url": "https://api.github.com/users/david-at-aws/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/david-at-aws/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/david-at-aws/subscriptions",
    "organizations_url": "https://api.github.com/users/david-at-aws/orgs",
    "repos_url": "https://api.github.com/users/david-at-aws/repos",
    "events_url": "https://api.github.com/users/david-at-aws/events{/privacy}",
    "received_events_url": "https://api.github.com/users/david-at-aws/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-07-31T17:41:53Z",
  "updated_at": "2015-07-31T17:41:53Z",
  "body": "Yeah, buffering to disk is the best option in the short term. On the plus side, the TransferManager can upload multiple parts in parallel when you give it a file, as opposed to having to read/upload from the `ZipInputStream` serially - may even end up being faster."
}
, {
  "url": "https://api.github.com/repos/aws/aws-sdk-java/issues/comments/150238691",
  "html_url": "https://github.com/aws/aws-sdk-java/issues/474#issuecomment-150238691",
  "issue_url": "https://api.github.com/repos/aws/aws-sdk-java/issues/474",
  "id": 150238691,
  "user": {
    "login": "alexmojaki",
    "id": 3627481,
    "avatar_url": "https://avatars.githubusercontent.com/u/3627481?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/alexmojaki",
    "html_url": "https://github.com/alexmojaki",
    "followers_url": "https://api.github.com/users/alexmojaki/followers",
    "following_url": "https://api.github.com/users/alexmojaki/following{/other_user}",
    "gists_url": "https://api.github.com/users/alexmojaki/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/alexmojaki/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/alexmojaki/subscriptions",
    "organizations_url": "https://api.github.com/users/alexmojaki/orgs",
    "repos_url": "https://api.github.com/users/alexmojaki/repos",
    "events_url": "https://api.github.com/users/alexmojaki/events{/privacy}",
    "received_events_url": "https://api.github.com/users/alexmojaki/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-10-22T14:19:03Z",
  "updated_at": "2015-10-22T14:19:03Z",
  "body": "I've written a library that automatically chunks data from a stream into parts so that you can avoid both running out of memory and writing to disk: https://github.com/alexmojaki/s3-stream-upload"
}
, {
  "url": "https://api.github.com/repos/aws/aws-sdk-java/issues/comments/150253334",
  "html_url": "https://github.com/aws/aws-sdk-java/issues/474#issuecomment-150253334",
  "issue_url": "https://api.github.com/repos/aws/aws-sdk-java/issues/474",
  "id": 150253334,
  "user": {
    "login": "selaamobee",
    "id": 13403249,
    "avatar_url": "https://avatars.githubusercontent.com/u/13403249?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/selaamobee",
    "html_url": "https://github.com/selaamobee",
    "followers_url": "https://api.github.com/users/selaamobee/followers",
    "following_url": "https://api.github.com/users/selaamobee/following{/other_user}",
    "gists_url": "https://api.github.com/users/selaamobee/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/selaamobee/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/selaamobee/subscriptions",
    "organizations_url": "https://api.github.com/users/selaamobee/orgs",
    "repos_url": "https://api.github.com/users/selaamobee/repos",
    "events_url": "https://api.github.com/users/selaamobee/events{/privacy}",
    "received_events_url": "https://api.github.com/users/selaamobee/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-10-22T14:59:16Z",
  "updated_at": "2015-10-22T14:59:16Z",
  "body": "Thank you Alex, looks nice. My company probably won't give me time to return to this feature now and fix it, but if it will happen I'll try and use your library."
}
]
