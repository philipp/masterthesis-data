[
{
  "url": "https://api.github.com/repos/Netflix/falcor/issues/comments/134592779",
  "html_url": "https://github.com/Netflix/falcor/issues/479#issuecomment-134592779",
  "issue_url": "https://api.github.com/repos/Netflix/falcor/issues/479",
  "id": 134592779,
  "user": {
    "login": "dallonf",
    "id": 346300,
    "avatar_url": "https://avatars.githubusercontent.com/u/346300?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/dallonf",
    "html_url": "https://github.com/dallonf",
    "followers_url": "https://api.github.com/users/dallonf/followers",
    "following_url": "https://api.github.com/users/dallonf/following{/other_user}",
    "gists_url": "https://api.github.com/users/dallonf/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/dallonf/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/dallonf/subscriptions",
    "organizations_url": "https://api.github.com/users/dallonf/orgs",
    "repos_url": "https://api.github.com/users/dallonf/repos",
    "events_url": "https://api.github.com/users/dallonf/events{/privacy}",
    "received_events_url": "https://api.github.com/users/dallonf/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-08-25T13:52:15Z",
  "updated_at": "2015-08-25T13:52:15Z",
  "body": "I usually make a request for the first _n_ items in the list (Like `titles[0..19]` to grab the first 20 - this won't throw an error, even if there are less than 20 items in the list) and also grab the length at the same time, which allows me to render pagination controls.\r\n\r\nI do agree it would be nice to be able to make length-based requests, though. It's wasteful to send back 19 `$atom`s if there's only one item in the list. And sometimes, I just happen to know there's not a huge number of items in the list and I do in fact want to grab them all."
}
, {
  "url": "https://api.github.com/repos/Netflix/falcor/issues/comments/134628578",
  "html_url": "https://github.com/Netflix/falcor/issues/479#issuecomment-134628578",
  "issue_url": "https://api.github.com/repos/Netflix/falcor/issues/479",
  "id": 134628578,
  "user": {
    "login": "ewinslow",
    "id": 356564,
    "avatar_url": "https://avatars.githubusercontent.com/u/356564?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/ewinslow",
    "html_url": "https://github.com/ewinslow",
    "followers_url": "https://api.github.com/users/ewinslow/followers",
    "following_url": "https://api.github.com/users/ewinslow/following{/other_user}",
    "gists_url": "https://api.github.com/users/ewinslow/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/ewinslow/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/ewinslow/subscriptions",
    "organizations_url": "https://api.github.com/users/ewinslow/orgs",
    "repos_url": "https://api.github.com/users/ewinslow/repos",
    "events_url": "https://api.github.com/users/ewinslow/events{/privacy}",
    "received_events_url": "https://api.github.com/users/ewinslow/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-08-25T15:32:24Z",
  "updated_at": "2015-08-25T15:46:10Z",
  "body": "So I think I left out some info that would be helpful to explain the problem further...\r\n\r\nSuppose I have a \"posts\" component that looks like this (angular 2 example):\r\n\r\n```\r\n<li *ng-for=\"#post of ??? | async\">\r\n  <post [data]=\"post\">\r\n</li>\r\n```\r\n\r\nHere I don't know statically what data is needed by the post component (or what page of the feed I'm on. Perhaps the user jumped straight to the second page via URL). If I wait for the list of items to come back before rendering, then if have to incur a second round trip in order to fetch all their properties. And perhaps what they render is yet another list of comments or something, so the problem just compounds.\r\n\r\nBasically, I'm thinking I need to be able to \"speculatively\" unravel that loop somehow to discover what data the post is requesting, so that those requests can then be batched and sent to the server. E.g. perhaps `<post>` looks like:\r\n\r\n```\r\n<h1>{{data.getValue('title')}}</h1>\r\n<div [inner-html]=\"data.getvalue('content')\"></div>\r\n```\r\n\r\nBut the feed item should not need to know this statically, to maintain maximum decoupling.\r\n\r\nIs there any way to do this yet? I'm thinking a method that returns an `Observable<Array<Model>>` where the first published Array is as long as the maximum number of items requested but then a new Array truncated to the correct length in published when the results are back from the server."
}
, {
  "url": "https://api.github.com/repos/Netflix/falcor/issues/comments/134641674",
  "html_url": "https://github.com/Netflix/falcor/issues/479#issuecomment-134641674",
  "issue_url": "https://api.github.com/repos/Netflix/falcor/issues/479",
  "id": 134641674,
  "user": {
    "login": "ewinslow",
    "id": 356564,
    "avatar_url": "https://avatars.githubusercontent.com/u/356564?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/ewinslow",
    "html_url": "https://github.com/ewinslow",
    "followers_url": "https://api.github.com/users/ewinslow/followers",
    "following_url": "https://api.github.com/users/ewinslow/following{/other_user}",
    "gists_url": "https://api.github.com/users/ewinslow/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/ewinslow/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/ewinslow/subscriptions",
    "organizations_url": "https://api.github.com/users/ewinslow/orgs",
    "repos_url": "https://api.github.com/users/ewinslow/repos",
    "events_url": "https://api.github.com/users/ewinslow/events{/privacy}",
    "received_events_url": "https://api.github.com/users/ewinslow/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-08-25T15:51:33Z",
  "updated_at": "2015-08-25T15:51:33Z",
  "body": "Perhaps most succinctly: what should I put in the question marks?"
}
]
