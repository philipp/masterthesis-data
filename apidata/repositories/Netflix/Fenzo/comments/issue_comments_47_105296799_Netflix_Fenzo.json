[
{
  "url": "https://api.github.com/repos/Netflix/Fenzo/issues/comments/138435618",
  "html_url": "https://github.com/Netflix/Fenzo/issues/47#issuecomment-138435618",
  "issue_url": "https://api.github.com/repos/Netflix/Fenzo/issues/47",
  "id": 138435618,
  "user": {
    "login": "spodila",
    "id": 6413812,
    "avatar_url": "https://avatars.githubusercontent.com/u/6413812?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/spodila",
    "html_url": "https://github.com/spodila",
    "followers_url": "https://api.github.com/users/spodila/followers",
    "following_url": "https://api.github.com/users/spodila/following{/other_user}",
    "gists_url": "https://api.github.com/users/spodila/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/spodila/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/spodila/subscriptions",
    "organizations_url": "https://api.github.com/users/spodila/orgs",
    "repos_url": "https://api.github.com/users/spodila/repos",
    "events_url": "https://api.github.com/users/spodila/events{/privacy}",
    "received_events_url": "https://api.github.com/users/spodila/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-09-08T05:04:38Z",
  "updated_at": "2015-09-08T05:04:38Z",
  "body": "You are right in that a TaskScheduler is stateful and that there would be a single instance in a cluster. The size of state information in Fenzo would be proportional to the number of VMs (aka agent/host) and the number of tasks assigned. Other state information, such as related to autoscaling and groups, are too small to be concerned about. \r\nAlthough I have no specific data to quote, I used this [test](https://github.com/Netflix/Fenzo/blob/master/fenzo-core/src/test/java/com/netflix/fenzo/TestLotsOfTasks.java) program to create 10,000 VMs (agents/hosts) each with 16 cores, filling the 160K cores with 45K tasks (1-, 8, and 12-copu tasks). I noticed the resident set size to be about 750MB. \r\nWhile this is not meant to be a reference to figuring out memory for a given scale, the quick hack shows you a way to test your possible scale and measure the anticipated memory size as well as the performance to expect. Fenzo makes it easy to also test new plugins for constraints and fitness calculators. LeaseProvider and TaskRequestProvider classes in the test package are useful for this, instead of requiring actual agent hosts."
}
, {
  "url": "https://api.github.com/repos/Netflix/Fenzo/issues/comments/138437946",
  "html_url": "https://github.com/Netflix/Fenzo/issues/47#issuecomment-138437946",
  "issue_url": "https://api.github.com/repos/Netflix/Fenzo/issues/47",
  "id": 138437946,
  "user": {
    "login": "huntc",
    "id": 694893,
    "avatar_url": "https://avatars.githubusercontent.com/u/694893?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/huntc",
    "html_url": "https://github.com/huntc",
    "followers_url": "https://api.github.com/users/huntc/followers",
    "following_url": "https://api.github.com/users/huntc/following{/other_user}",
    "gists_url": "https://api.github.com/users/huntc/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/huntc/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/huntc/subscriptions",
    "organizations_url": "https://api.github.com/users/huntc/orgs",
    "repos_url": "https://api.github.com/users/huntc/repos",
    "events_url": "https://api.github.com/users/huntc/events{/privacy}",
    "received_events_url": "https://api.github.com/users/huntc/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-09-08T05:31:33Z",
  "updated_at": "2015-09-08T05:31:33Z",
  "body": "Thanks for the reply @spodila.\r\n\r\nWhat are your thoughts toward resiliency? For example, if your process containing the `TaskScheduler` dies then what action do you take?\r\n\r\nThanks again for the dialogue.\r\n\r\n(closing as you've addressed my primary question)"
}
, {
  "url": "https://api.github.com/repos/Netflix/Fenzo/issues/comments/139354017",
  "html_url": "https://github.com/Netflix/Fenzo/issues/47#issuecomment-139354017",
  "issue_url": "https://api.github.com/repos/Netflix/Fenzo/issues/47",
  "id": 139354017,
  "user": {
    "login": "spodila",
    "id": 6413812,
    "avatar_url": "https://avatars.githubusercontent.com/u/6413812?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/spodila",
    "html_url": "https://github.com/spodila",
    "followers_url": "https://api.github.com/users/spodila/followers",
    "following_url": "https://api.github.com/users/spodila/following{/other_user}",
    "gists_url": "https://api.github.com/users/spodila/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/spodila/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/spodila/subscriptions",
    "organizations_url": "https://api.github.com/users/spodila/orgs",
    "repos_url": "https://api.github.com/users/spodila/repos",
    "events_url": "https://api.github.com/users/spodila/events{/privacy}",
    "received_events_url": "https://api.github.com/users/spodila/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-09-10T19:28:24Z",
  "updated_at": "2015-09-10T19:28:24Z",
  "body": "Upon start of the process containing the TaskScheduler, we initialize Fenzo with the entire state by calling TaskScheduler.getTaskAssigner().call(...) method for each task that is already known to be running. Specifically, since we run multiple instances of our framework with ZooKeeper based leader election, we perform this initialization upon being elected as the leader.\r\n\r\nThis does bring up a concern on latency at startup with large number of running tasks. However, we haven't come to the point where that is the next big problem to solve. If that does concern you, I'd love to hear your thoughts on it and/or exchange ideas on solving it."
}
, {
  "url": "https://api.github.com/repos/Netflix/Fenzo/issues/comments/139395874",
  "html_url": "https://github.com/Netflix/Fenzo/issues/47#issuecomment-139395874",
  "issue_url": "https://api.github.com/repos/Netflix/Fenzo/issues/47",
  "id": 139395874,
  "user": {
    "login": "huntc",
    "id": 694893,
    "avatar_url": "https://avatars.githubusercontent.com/u/694893?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/huntc",
    "html_url": "https://github.com/huntc",
    "followers_url": "https://api.github.com/users/huntc/followers",
    "following_url": "https://api.github.com/users/huntc/following{/other_user}",
    "gists_url": "https://api.github.com/users/huntc/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/huntc/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/huntc/subscriptions",
    "organizations_url": "https://api.github.com/users/huntc/orgs",
    "repos_url": "https://api.github.com/users/huntc/repos",
    "events_url": "https://api.github.com/users/huntc/events{/privacy}",
    "received_events_url": "https://api.github.com/users/huntc/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-09-10T22:05:53Z",
  "updated_at": "2015-09-10T22:05:53Z",
  "body": "Perhaps a plugable mechanism that a) requests state from other scheduler instances, returning a CompletionStage aka Scala's Future, and b) a callback so that the scheduler can be notified of new state asynchronously. \r\n\r\nThis then permits multiple schedulers to work together. We could then back the request and the callback with CRDTs (for example).\r\n\r\nI think you should also consider reentrancy in your API to support such a mechanism and concurrency in general. "
}
]
