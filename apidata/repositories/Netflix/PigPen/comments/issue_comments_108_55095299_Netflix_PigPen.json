[
{
  "url": "https://api.github.com/repos/Netflix/PigPen/issues/comments/71089799",
  "html_url": "https://github.com/Netflix/PigPen/issues/108#issuecomment-71089799",
  "issue_url": "https://api.github.com/repos/Netflix/PigPen/issues/108",
  "id": 71089799,
  "user": {
    "login": "mbossenbroek",
    "id": 6173807,
    "avatar_url": "https://avatars.githubusercontent.com/u/6173807?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/mbossenbroek",
    "html_url": "https://github.com/mbossenbroek",
    "followers_url": "https://api.github.com/users/mbossenbroek/followers",
    "following_url": "https://api.github.com/users/mbossenbroek/following{/other_user}",
    "gists_url": "https://api.github.com/users/mbossenbroek/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/mbossenbroek/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/mbossenbroek/subscriptions",
    "organizations_url": "https://api.github.com/users/mbossenbroek/orgs",
    "repos_url": "https://api.github.com/users/mbossenbroek/repos",
    "events_url": "https://api.github.com/users/mbossenbroek/events{/privacy}",
    "received_events_url": "https://api.github.com/users/mbossenbroek/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-01-22T20:12:08Z",
  "updated_at": "2015-01-22T20:23:19Z",
  "body": "That has to do with how folds are composed, which is admittedly a little wonky. A little background...\r\n\r\nThere are 4 phases to a fold operation: pre-processing, reducing, combining, and post-processing. Pre-processing and post-processing compose in a fairly regular manner, but the reducer/combiner parts go in pairs and don't compose nicely.\r\n\r\nLet's look at the combinef/reducef for both of count and distinct.\r\n\r\n| fn         | seed  | reducef | combinef     |\r\n| ---        | ---   | ---     | ---          |\r\n| count    | `0`   | `count` | `(reduce +)` |\r\n| distinct | `#{}` | `conj`  | `set/union`  |\r\n\r\nFor your example, we would compute the distinct elements for each mapper, and add them to a set. In the combiner, we would then merge those intermediate sets. We can't count the items in the set in the mappers because then we lose what items are in the set and can't properly combine the outputs of the mappers.\r\n\r\nI still wanted to have fold operations sort of compose, so that you could do stuff like this:\r\n\r\n(->> (fold/map f) (fold/filter g) (fold/distinct) (fold/first))\r\n\r\nWhich ends up doing something like this:\r\n\r\n|         | map | filter | distinct | first   | result       |\r\n| ---     | --- | ---    | ---      | ---     | ---          |\r\n| pre     | `f` | `g`    |          |         | `(comp g f)` |\r\n| reduce  |     |        | `conj`   |         | `conj`       |\r\n| combine |     |        | `union`  |         | `union`      |\r\n| post    |     |        |          | `first` | `first`      |\r\n\r\n\r\nBut it's not clear which parts apply to which phases. I thought that I would throw an exception if you tried to do what you did, but clearly I do not. What you tried looks something like this because it's using the distributed count:\r\n\r\n\r\n|         | distinct | count   | result  |\r\n| ---     | ---      | ---     | ---     |\r\n| pre     |          |         |         |\r\n| reduce  | `conj`   | `count` | `count` |\r\n| combine | `union`  | `+`     | `+`     |\r\n| post    |          |         |         |\r\n\r\nWhere I'm just taking the last reducer/combiner combo that you supply. What you really want is something like this:\r\n\r\n\r\n|         | distinct | count   | result  |\r\n| ---     | ---      | ---     | ---     |\r\n| pre     |          |         |         |\r\n| reduce  | `conj`   |         | `conj`  |\r\n| combine | `union`  |         | `union` |\r\n| post    |          | `count` | `count` |\r\n\r\nWhich does the count as a post-processing operation. So now there are two versions of `fold/count` and that's confusing too...\r\n\r\n\r\nSo, what can you do? The solution today is to define a custom fold-fn that specifies which part is which:\r\n\r\n```\r\n(->> (pig/return records)                                                                                                                                                                                                                                                                                                            \r\n (pig/fold (fold/fold-fn clojure.set/union conj count))\r\n (pig/dump))\r\n```\r\n\r\n... or if you want to reuse it ...\r\n\r\n```\r\n(defn distinct-count\r\n  []\r\n  (fold/fold-fn clojure.set/union conj count))\r\n\r\n(->> (pig/return records)                                                                                                                                                                                                                                                                                                            \r\n (pig/fold (distinct-count))\r\n (pig/dump))\r\n```\r\n\r\nIt's not perfect and it could be better. If you have any ideas for making these compose better, I'm all ears. Take a look at the `comp-*` fns in `pigpen.fold` if you're curious. If not, `pigpen.fold/fold-fn` is probably your friend :)\r\n\r\n"
}
, {
  "url": "https://api.github.com/repos/Netflix/PigPen/issues/comments/71123952",
  "html_url": "https://github.com/Netflix/PigPen/issues/108#issuecomment-71123952",
  "issue_url": "https://api.github.com/repos/Netflix/PigPen/issues/108",
  "id": 71123952,
  "user": {
    "login": "theJohnnyBrown",
    "id": 165906,
    "avatar_url": "https://avatars.githubusercontent.com/u/165906?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/theJohnnyBrown",
    "html_url": "https://github.com/theJohnnyBrown",
    "followers_url": "https://api.github.com/users/theJohnnyBrown/followers",
    "following_url": "https://api.github.com/users/theJohnnyBrown/following{/other_user}",
    "gists_url": "https://api.github.com/users/theJohnnyBrown/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/theJohnnyBrown/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/theJohnnyBrown/subscriptions",
    "organizations_url": "https://api.github.com/users/theJohnnyBrown/orgs",
    "repos_url": "https://api.github.com/users/theJohnnyBrown/repos",
    "events_url": "https://api.github.com/users/theJohnnyBrown/events{/privacy}",
    "received_events_url": "https://api.github.com/users/theJohnnyBrown/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-01-22T23:54:35Z",
  "updated_at": "2015-01-22T23:54:35Z",
  "body": "Thanks, this is very helpful. I was wondering whether fold-fn would be necessary. Looks like it's time for a little brain-stretching :)"
}
]
