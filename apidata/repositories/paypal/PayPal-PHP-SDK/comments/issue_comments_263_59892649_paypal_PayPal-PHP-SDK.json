[
{
  "url": "https://api.github.com/repos/paypal/PayPal-PHP-SDK/issues/comments/77294702",
  "html_url": "https://github.com/paypal/PayPal-PHP-SDK/issues/263#issuecomment-77294702",
  "issue_url": "https://api.github.com/repos/paypal/PayPal-PHP-SDK/issues/263",
  "id": 77294702,
  "user": {
    "login": "jaypatel512",
    "id": 1309895,
    "avatar_url": "https://avatars.githubusercontent.com/u/1309895?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/jaypatel512",
    "html_url": "https://github.com/jaypatel512",
    "followers_url": "https://api.github.com/users/jaypatel512/followers",
    "following_url": "https://api.github.com/users/jaypatel512/following{/other_user}",
    "gists_url": "https://api.github.com/users/jaypatel512/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/jaypatel512/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/jaypatel512/subscriptions",
    "organizations_url": "https://api.github.com/users/jaypatel512/orgs",
    "repos_url": "https://api.github.com/users/jaypatel512/repos",
    "events_url": "https://api.github.com/users/jaypatel512/events{/privacy}",
    "received_events_url": "https://api.github.com/users/jaypatel512/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-03-05T02:27:39Z",
  "updated_at": "2015-03-05T02:27:39Z",
  "body": "Hey @rgeddes !\r\n\r\nThe way you are presenting makes perfect sense for objects like `Item`. However, complicated objects that have multiple objects, and almost 15-20 attributes to set, it starts looking pretty bad, and hard to debug, as in your case, you are using the index of the array to determine the value corresponding to the field. If for some reason, a new field is added in the middle, this would break compatibility very easily.\r\n\r\nHowever, good news is there is a better way than :\r\n```php\r\n$item1 = new Item();\r\n$item1->setName('Ground Coffee 40 oz')\r\n    ->setDescription('Ground Coffee 40 oz')\r\n    ->setCurrency('USD')\r\n    ->setQuantity(1)\r\n    ->setTax(0.3)\r\n    ->setPrice(7.50);\r\n\r\nitem2 ...\r\n```\r\n\r\nThere are actually [2 more ways](https://github.com/paypal/PayPal-PHP-SDK/wiki/Passing-Data) you could pass data to constructor. Now, this may not serve your need exactly, but I think it could take you one step further. Let me know your thoughts on this.\r\n\r\nI will still keep your suggestion in mind, and see if it would make sense in future. \r\n\r\n## JSON String\r\n\r\nYou could directly pass the JSON string as PayPalModel constructor parameter, and PayPal SDK will fill the information accordingly. You could use `get*` methods to retrieve specific information about the object immediately after that. Here is the example.\r\n\r\n```php\r\n$creditCard = new \\PayPal\\Api\\CreditCard(\r\n    '{\r\n    \"type\": \"visa\",\r\n    \"number\": \"4417119669820331\",\r\n    \"expire_month\": \"11\",\r\n    \"expire_year\": \"2019\",\r\n    \"cvv2\": \"012\",\r\n    \"first_name\": \"Joe\",\r\n    \"last_name\": \"Shopper\"\r\n    }'\r\n);\r\n// This will print the number\r\necho $creditCard->getNumber();\r\n\r\n```\r\n\r\n## Array Object\r\nSimilarly, array could be passed too as a constructor parameter to PayPalModel Object, as shown below:\r\n\r\n```php\r\n$creditCard = new \\PayPal\\Api\\CreditCard(\r\n    array(\r\n        \"type\" => \"visa\",\r\n        \"number\"=> \"4417119669820331\",\r\n        \"expire_month\"=> \"11\",\r\n        \"expire_year\"=> \"2019\",\r\n        \"cvv2\"=> \"012\",\r\n        \"first_name\"=> \"Joe\",\r\n        \"last_name\"=> \"Shopper\"\r\n    )\r\n);\r\n// This will print the number\r\necho $creditCard->getNumber();\r\n```\r\n\r\nThis ability to accept multiple data formats allow developers to create instance of PayPalModels with minimal interference."
}
, {
  "url": "https://api.github.com/repos/paypal/PayPal-PHP-SDK/issues/comments/83787103",
  "html_url": "https://github.com/paypal/PayPal-PHP-SDK/issues/263#issuecomment-83787103",
  "issue_url": "https://api.github.com/repos/paypal/PayPal-PHP-SDK/issues/263",
  "id": 83787103,
  "user": {
    "login": "rgeddes",
    "id": 1351774,
    "avatar_url": "https://avatars.githubusercontent.com/u/1351774?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/rgeddes",
    "html_url": "https://github.com/rgeddes",
    "followers_url": "https://api.github.com/users/rgeddes/followers",
    "following_url": "https://api.github.com/users/rgeddes/following{/other_user}",
    "gists_url": "https://api.github.com/users/rgeddes/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/rgeddes/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/rgeddes/subscriptions",
    "organizations_url": "https://api.github.com/users/rgeddes/orgs",
    "repos_url": "https://api.github.com/users/rgeddes/repos",
    "events_url": "https://api.github.com/users/rgeddes/events{/privacy}",
    "received_events_url": "https://api.github.com/users/rgeddes/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-03-19T22:31:09Z",
  "updated_at": "2015-03-19T22:31:09Z",
  "body": "Thanks that makes sense.\r\n\r\nI was an early adopter of the PHP REST API back in 12/2014 with version 0.3x and had to recently upgrade my code when some variable names were recently changed in the API.... I think it was from names like \"payment_total\" to camel case (paymentTotal).\r\n\r\nAnyway, I understand there's a new account interface that makes more sense when using the REST API.  My client is still using the old account interface for viewing the their transactions, and that Paypal clients are being randomly allowed to try the new account interface.\r\n\r\nIs there any way to get the Paypal clients, already using the REST API, to get the option to use the new account interface?"
}
]
