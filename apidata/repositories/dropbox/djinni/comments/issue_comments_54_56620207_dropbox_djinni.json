[
{
  "url": "https://api.github.com/repos/dropbox/djinni/issues/comments/74486120",
  "html_url": "https://github.com/dropbox/djinni/issues/54#issuecomment-74486120",
  "issue_url": "https://api.github.com/repos/dropbox/djinni/issues/54",
  "id": 74486120,
  "user": {
    "login": "pwais",
    "id": 218145,
    "avatar_url": "https://avatars.githubusercontent.com/u/218145?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/pwais",
    "html_url": "https://github.com/pwais",
    "followers_url": "https://api.github.com/users/pwais/followers",
    "following_url": "https://api.github.com/users/pwais/following{/other_user}",
    "gists_url": "https://api.github.com/users/pwais/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/pwais/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/pwais/subscriptions",
    "organizations_url": "https://api.github.com/users/pwais/orgs",
    "repos_url": "https://api.github.com/users/pwais/repos",
    "events_url": "https://api.github.com/users/pwais/events{/privacy}",
    "received_events_url": "https://api.github.com/users/pwais/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-02-16T10:16:34Z",
  "updated_at": "2015-02-16T10:16:34Z",
  "body": "For C++ -> Java: \r\nIf the Java method returns an ```nio.ByteBuffer``` via ```NewDirectByteBuffer()```, the JVM won't free the underlying C++-allocated buffer when the ```nio.ByteBuffer``` gets GC'ed.  Djinni would probably need to provide a subclass of ByteBuffer or some sort of custom wrapper that calls ``free()`` or a C++ deleter on ```finalize()```.\r\n\r\nFor Java -> C++:\r\nNote sure if ```array_view``` will get ratified... I would recommend a small custom structure similar to a capnproto / kj array: https://github.com/sandstorm-io/capnproto/blob/master/c%2B%2B/src/kj/array.h#L128  .  Something like ```kj::Array``` would be small, concise, and largely compatible with existing ```libc++``` utilities.  \r\n\r\nFor C++->Objective-C:\r\nWith respect to the issue of mutability, this looks like a case for an special djinni ObjC class (as in the C++ -> Java case where we need a ByteBuffer that will dispose on GC).  If the special ObjC class is simply a pointer, size, and function pointer to a ```void disposer()```, then it should be largely interoperable with Core Foundation.\r\n\r\n\r\nThe underlying problem here is that ownership of the byte buffer must be transferred across the language border; a shared byte buffer is inherently a (pointer, size, disposer) tuple and not just (pointer, size).  It might make more sense for djinni to include its own simple ByteBuffer for each language to achieve the necessary ownership transfer.  While this additional data structure would increase complexity of djinni's UI, the existing ```binary``` type is a solid solution for the majority of use cases where performance demands are relatively flexible.  \r\n\r\n\r\n\r\nOne last thought for the interim: the user can pass pointers across the language boundary via the ```i64``` type.  For Java <-> C++, I know at least JNA has facilities for mapping pointer addresses.  "
}
, {
  "url": "https://api.github.com/repos/dropbox/djinni/issues/comments/74490750",
  "html_url": "https://github.com/dropbox/djinni/issues/54#issuecomment-74490750",
  "issue_url": "https://api.github.com/repos/dropbox/djinni/issues/54",
  "id": 74490750,
  "user": {
    "login": "mknejp",
    "id": 5156768,
    "avatar_url": "https://avatars.githubusercontent.com/u/5156768?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/mknejp",
    "html_url": "https://github.com/mknejp",
    "followers_url": "https://api.github.com/users/mknejp/followers",
    "following_url": "https://api.github.com/users/mknejp/following{/other_user}",
    "gists_url": "https://api.github.com/users/mknejp/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/mknejp/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/mknejp/subscriptions",
    "organizations_url": "https://api.github.com/users/mknejp/orgs",
    "repos_url": "https://api.github.com/users/mknejp/repos",
    "events_url": "https://api.github.com/users/mknejp/events{/privacy}",
    "received_events_url": "https://api.github.com/users/mknejp/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-02-16T10:54:52Z",
  "updated_at": "2015-02-16T10:54:52Z",
  "body": "Whether it's `array_view` or something else doesn't matter if it can be configured just as the current setting for `optional`, as long as it can be constructed from pointer/size arguments and takes a single template parameter.\r\n\r\nI am not trying to make ownership or lifetime implicit. The user has to decide who owns the buffer. The user has to know that changing the size/capacity of the buffer by anyone may make the memory region invalid for all involved parties and has to be respecified. This is an attempt at providing either\r\n- An argument to a function that is sufficiently large that copying it twice takes significant time and is redundant. The contract of the function should specify that the memory region is only valid for the duration of the call. This use case might be eliminated for if we could get rid of the temporary `std::vector` in the thunks.\r\n- A persistent region of memory that is available for read/write access from both parties. Here lifetime and invalidation has to be managed by the user explicitly. It is the user's responsibility to not access a `java.nio.ByteBuffer` or `NSData` if the corresponding `std::vector` no longer exists or was reallocated.\r\n\r\nI am not opposed to a solution with implicit lifetime management if it can be done properly and safely.\r\n\r\nI guess a custom ObjC type derived rom NSData is acceptable since NSMutableData always copies the content in its initializers."
}
, {
  "url": "https://api.github.com/repos/dropbox/djinni/issues/comments/75740942",
  "html_url": "https://github.com/dropbox/djinni/issues/54#issuecomment-75740942",
  "issue_url": "https://api.github.com/repos/dropbox/djinni/issues/54",
  "id": 75740942,
  "user": {
    "login": "pwais",
    "id": 218145,
    "avatar_url": "https://avatars.githubusercontent.com/u/218145?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/pwais",
    "html_url": "https://github.com/pwais",
    "followers_url": "https://api.github.com/users/pwais/followers",
    "following_url": "https://api.github.com/users/pwais/following{/other_user}",
    "gists_url": "https://api.github.com/users/pwais/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/pwais/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/pwais/subscriptions",
    "organizations_url": "https://api.github.com/users/pwais/orgs",
    "repos_url": "https://api.github.com/users/pwais/repos",
    "events_url": "https://api.github.com/users/pwais/events{/privacy}",
    "received_events_url": "https://api.github.com/users/pwais/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-02-24T11:33:20Z",
  "updated_at": "2015-02-24T11:33:20Z",
  "body": "Another issue not yet discussed for Java <-> C++ is that Java ByteBuffers are by default Big Endian and ObjC/C++ users typically expects Little Endian data.  This is mostly a user-facing issue and should be rare (and the buffer probably has an endian-aware r/w protocol [1](https://github.com/google/protobuf/blob/2cb2358cba7eb296d6243912758b9e9c78d1502e/javanano/src/main/java/com/google/protobuf/nano/CodedInputByteBufferNano.java#L148) [2](https://github.com/dwrensha/capnproto-java/blob/master/runtime/src/main/java/org/capnproto/Serialize.java#L33)).  It might make sense to force native order, where necessary, as [JNA does](https://github.com/twall/jna/blob/master/src/com/sun/jna/Pointer.java#L666).\r\n\r\nI also just noticed that djinni interface functions can accept *interfaces* as parameter types.  (This feature demonstrated in the example code but not the root README example).  While it would indeed be nice to have a djinni `ByteBuffer` datatype, perhaps one simply needs a byte buffer *interface* (that might also include methods to address ownership transfer, if any)?\r\n\r\nFor example:\r\n```\r\nmy_native_byte_buffer = interface +j +o {\r\n  allocate(size_bytes: i64);  # Allocate space for this many bytes\r\n  begin(): i64;               # Return address of first byte\r\n  size(): i64;                # Return size of the buffer\r\n  disown();                   # Release the buffer, but don't delete it;\r\n                              # assume the user now owns the memory at\r\n                              # begin() of size size()\r\n}\r\n\r\nmy_buffer_writer = interface +j {\r\n  create(): my_native_byte_buffer;\r\n}\r\n\r\nmy_buffer_reader = interface +c {\r\n  read(buffer: my_native_byte_buffer);\r\n}\r\n```\r\n\r\nI note that unfortunately djinni won't compile the IDL if `my_native_byte_buffer` is marked as having a `+c` implementation (an assertion error triggers; not sure why).\r\n\r\nIn ObjC, getting a pointer address for `begin()` shouldn't be too hard.  In Java, one probably needs to call into JNI (derp!).  For direct byte buffers, there's `GetDirectBufferAddress`, and for byte arrays, there's `GetByteArrayElements`, but that call might do a copy.  FWIW JNA has [a simple facility](https://github.com/twall/jna/blob/master/src/com/sun/jna/Native.java#L303) to get direct buffer addresses but not one for non-direct byte buffers (e.g. byte[]).  My guess is JNA doesn't handle non-direct byte buffers because 1) the user has to release the `jbyte*` so that the GC is free to e.g. move the `byte[]` upon a compaction 2) the `GetByteArrayElements()` might trigger a copy anyways, so the pointer address doesn't have much value.\r\n\r\nWhile there are problems with this approach, it might be best for most users since it forces them to define how ownership works and to define what creates the pointer (e.g. mmapped file buffer? network buffer? non-direct buffers probably can't be shared even tho they can be `ByteBuffer`-wrapped).  Furthermore, use of large Java direct byte buffers might require special JVM args (e.g. `-XX:MaxDirectMemorySize`) and special tuning so that the JVM leaves space for native heap."
}
, {
  "url": "https://api.github.com/repos/dropbox/djinni/issues/comments/77108881",
  "html_url": "https://github.com/dropbox/djinni/issues/54#issuecomment-77108881",
  "issue_url": "https://api.github.com/repos/dropbox/djinni/issues/54",
  "id": 77108881,
  "user": {
    "login": "pwais",
    "id": 218145,
    "avatar_url": "https://avatars.githubusercontent.com/u/218145?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/pwais",
    "html_url": "https://github.com/pwais",
    "followers_url": "https://api.github.com/users/pwais/followers",
    "following_url": "https://api.github.com/users/pwais/following{/other_user}",
    "gists_url": "https://api.github.com/users/pwais/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/pwais/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/pwais/subscriptions",
    "organizations_url": "https://api.github.com/users/pwais/orgs",
    "repos_url": "https://api.github.com/users/pwais/repos",
    "events_url": "https://api.github.com/users/pwais/events{/privacy}",
    "received_events_url": "https://api.github.com/users/pwais/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-03-04T07:29:52Z",
  "updated_at": "2015-03-04T21:38:14Z",
  "body": "Thinking on this point a bit more, there are a handful of tricky issues here:\r\n * Java -> C++: `GetPrimitiveArrayCritical()` requires no JNI calls until a `ReleasePrimitiveArrayCritical()`, so the user must code intelligently to use this special feature.  Otherwise buffers get deep-copied (so no better than current behavior of `binary`).\r\n * C++ -> Java: Assuming the `ByteBuffer` is direct, a separate call into C++ must happen to free the C++-allocated buffer.  Either the user must put this call in their djinni interface (very undesirable) or Java must get a `ByteBuffer` subclass that calls a C++ deleter on `finalize()` (do-able).\r\n * C++ -> ObjC :  `NSMutableData` responds to `initWithBytesNoCopy` but will just deep-copy the buffer and delete it immediately.  If `NSData` owns a buffer, *it must be allocated using malloc*.\r\n\r\ndjinni mainly offers two features:\r\n * `records` are pass-by-value, always deep-copied, stateless, and marshaled between languages\r\n * `interfaces` are pass-by-reference, stateful, and are never marshaled between languages\r\n\r\nWhat we really need is a union of these two features:\r\n * custom translation/marshaling of ownership semantics\r\n * stateful (if we assume the underlying buffer gets ref-counted)\r\n\r\nPerhaps a solution here is a feature for *(expert) user-defined primitives*:\r\n * User can declare a symbol for their primitive for use in IDL files (and thus code generation)\r\n * User specifies language-native class for each language that supports the primitive.  The native class can be user-provided, i.e. does not need to be part of a standard library\r\n * User must implement {to,from}{languages} marshaling code (similar to the existing {to,from}Java JNI code in `support-lib`).  \r\n * User benefits from all of djinni's existing support code (e.g. exception handling stuff).\r\n\r\nUsing this feature, we could do as much as provide our own ByteBuffer and as little as give C++ a (potentially) zero-copy view of a `nio.ByteBuffer`.\r\n\r\nAnybody put much thought into user-defined primitives before?\r\n"
}
, {
  "url": "https://api.github.com/repos/dropbox/djinni/issues/comments/77203452",
  "html_url": "https://github.com/dropbox/djinni/issues/54#issuecomment-77203452",
  "issue_url": "https://api.github.com/repos/dropbox/djinni/issues/54",
  "id": 77203452,
  "user": {
    "login": "mknejp",
    "id": 5156768,
    "avatar_url": "https://avatars.githubusercontent.com/u/5156768?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/mknejp",
    "html_url": "https://github.com/mknejp",
    "followers_url": "https://api.github.com/users/mknejp/followers",
    "following_url": "https://api.github.com/users/mknejp/following{/other_user}",
    "gists_url": "https://api.github.com/users/mknejp/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/mknejp/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/mknejp/subscriptions",
    "organizations_url": "https://api.github.com/users/mknejp/orgs",
    "repos_url": "https://api.github.com/users/mknejp/repos",
    "events_url": "https://api.github.com/users/mknejp/events{/privacy}",
    "received_events_url": "https://api.github.com/users/mknejp/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-03-04T17:26:28Z",
  "updated_at": "2015-03-04T17:26:28Z",
  "body": "> Anybody put much thought into user-defined primitives before?\r\n\r\nIt's something I'm working on to enable #52 but it needs some not-so-subtle changes to Djinni to be properly supported. It may also be helpful for #45 at some point.\r\n\r\nRegarding the the type discussed here: Maybe you are trying to solve too many problems at once. What I envision (for starters) is a way to exchange a persistent area of memory to which both sides have read/write access. Someone has to create it and someone needs to be responsible for destroying it. I think the lifetime of such a heavyweight objet should be managed explicitly and depend on the use case."
}
, {
  "url": "https://api.github.com/repos/dropbox/djinni/issues/comments/77253431",
  "html_url": "https://github.com/dropbox/djinni/issues/54#issuecomment-77253431",
  "issue_url": "https://api.github.com/repos/dropbox/djinni/issues/54",
  "id": 77253431,
  "user": {
    "login": "pwais",
    "id": 218145,
    "avatar_url": "https://avatars.githubusercontent.com/u/218145?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/pwais",
    "html_url": "https://github.com/pwais",
    "followers_url": "https://api.github.com/users/pwais/followers",
    "following_url": "https://api.github.com/users/pwais/following{/other_user}",
    "gists_url": "https://api.github.com/users/pwais/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/pwais/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/pwais/subscriptions",
    "organizations_url": "https://api.github.com/users/pwais/orgs",
    "repos_url": "https://api.github.com/users/pwais/repos",
    "events_url": "https://api.github.com/users/pwais/events{/privacy}",
    "received_events_url": "https://api.github.com/users/pwais/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-03-04T21:31:07Z",
  "updated_at": "2015-03-04T21:31:07Z",
  "body": "> What I envision (for starters) is a way to exchange a persistent area of memory to which both sides have read/write access.\r\n\r\nFor Java <-> C++, one would need to use a `DirectByteBuffer` (since the JVM can copy heap buffers as it sees fit, and once it does a copy you might as well just use djinni's existing solution).  The use case I have in mind is to give C++ direct r/w access to heap buffers (_or_ direct buffers), and it appears some sort of (admittedly non-trivial) adapter class is necessary.  I agree this latter use case is more complicated, but a lot of libraries have solved these JNI-related problems and it would be nice to distill those solutions into a djinni feature.\r\n\r\n+1 to #52 as a solution to this issue!\r\n"
}
, {
  "url": "https://api.github.com/repos/dropbox/djinni/issues/comments/125359991",
  "html_url": "https://github.com/dropbox/djinni/issues/54#issuecomment-125359991",
  "issue_url": "https://api.github.com/repos/dropbox/djinni/issues/54",
  "id": 125359991,
  "user": {
    "login": "pwais",
    "id": 218145,
    "avatar_url": "https://avatars.githubusercontent.com/u/218145?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/pwais",
    "html_url": "https://github.com/pwais",
    "followers_url": "https://api.github.com/users/pwais/followers",
    "following_url": "https://api.github.com/users/pwais/following{/other_user}",
    "gists_url": "https://api.github.com/users/pwais/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/pwais/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/pwais/subscriptions",
    "organizations_url": "https://api.github.com/users/pwais/orgs",
    "repos_url": "https://api.github.com/users/pwais/repos",
    "events_url": "https://api.github.com/users/pwais/events{/privacy}",
    "received_events_url": "https://api.github.com/users/pwais/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-07-27T22:07:16Z",
  "updated_at": "2015-07-27T22:07:16Z",
  "body": "Now that https://github.com/dropbox/djinni/pull/95 hit master, is this issue closed?  Thanks @mknejp !!!"
}
, {
  "url": "https://api.github.com/repos/dropbox/djinni/issues/comments/125361478",
  "html_url": "https://github.com/dropbox/djinni/issues/54#issuecomment-125361478",
  "issue_url": "https://api.github.com/repos/dropbox/djinni/issues/54",
  "id": 125361478,
  "user": {
    "login": "mknejp",
    "id": 5156768,
    "avatar_url": "https://avatars.githubusercontent.com/u/5156768?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/mknejp",
    "html_url": "https://github.com/mknejp",
    "followers_url": "https://api.github.com/users/mknejp/followers",
    "following_url": "https://api.github.com/users/mknejp/following{/other_user}",
    "gists_url": "https://api.github.com/users/mknejp/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/mknejp/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/mknejp/subscriptions",
    "organizations_url": "https://api.github.com/users/mknejp/orgs",
    "repos_url": "https://api.github.com/users/mknejp/repos",
    "events_url": "https://api.github.com/users/mknejp/events{/privacy}",
    "received_events_url": "https://api.github.com/users/mknejp/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-07-27T22:10:43Z",
  "updated_at": "2015-07-27T22:10:43Z",
  "body": "I suppose, unless such a type should be provided as part of Djinni's \"standard library\""
}
, {
  "url": "https://api.github.com/repos/dropbox/djinni/issues/comments/125362492",
  "html_url": "https://github.com/dropbox/djinni/issues/54#issuecomment-125362492",
  "issue_url": "https://api.github.com/repos/dropbox/djinni/issues/54",
  "id": 125362492,
  "user": {
    "login": "j4cbo",
    "id": 537158,
    "avatar_url": "https://avatars.githubusercontent.com/u/537158?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/j4cbo",
    "html_url": "https://github.com/j4cbo",
    "followers_url": "https://api.github.com/users/j4cbo/followers",
    "following_url": "https://api.github.com/users/j4cbo/following{/other_user}",
    "gists_url": "https://api.github.com/users/j4cbo/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/j4cbo/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/j4cbo/subscriptions",
    "organizations_url": "https://api.github.com/users/j4cbo/orgs",
    "repos_url": "https://api.github.com/users/j4cbo/repos",
    "events_url": "https://api.github.com/users/j4cbo/events{/privacy}",
    "received_events_url": "https://api.github.com/users/j4cbo/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-07-27T22:13:33Z",
  "updated_at": "2015-07-27T22:13:33Z",
  "body": "I do think it should be provided by Djinni (either fully built-in or by way of #95's mechanism, not sure yet), so let's leave this open for now."
}
, {
  "url": "https://api.github.com/repos/dropbox/djinni/issues/comments/133671638",
  "html_url": "https://github.com/dropbox/djinni/issues/54#issuecomment-133671638",
  "issue_url": "https://api.github.com/repos/dropbox/djinni/issues/54",
  "id": 133671638,
  "user": {
    "login": "pwais",
    "id": 218145,
    "avatar_url": "https://avatars.githubusercontent.com/u/218145?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/pwais",
    "html_url": "https://github.com/pwais",
    "followers_url": "https://api.github.com/users/pwais/followers",
    "following_url": "https://api.github.com/users/pwais/following{/other_user}",
    "gists_url": "https://api.github.com/users/pwais/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/pwais/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/pwais/subscriptions",
    "organizations_url": "https://api.github.com/users/pwais/orgs",
    "repos_url": "https://api.github.com/users/pwais/repos",
    "events_url": "https://api.github.com/users/pwais/events{/privacy}",
    "received_events_url": "https://api.github.com/users/pwais/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-08-22T09:37:30Z",
  "updated_at": "2015-08-22T09:37:30Z",
  "body": "My vote would be to have #95 fulfill the ByteBuffer issue; at least my intention is to use that mechanism.  `java.nio.ByteBuffer` isn't necessarily the best solution-- `ByteBuffer`s still have a garbage collected component.  A user could realistically want to leverage `sun.misc.Unsafe` instead to minimize GC pauses.\r\n"
}
, {
  "url": "https://api.github.com/repos/dropbox/djinni/issues/comments/140243314",
  "html_url": "https://github.com/dropbox/djinni/issues/54#issuecomment-140243314",
  "issue_url": "https://api.github.com/repos/dropbox/djinni/issues/54",
  "id": 140243314,
  "user": {
    "login": "pwais",
    "id": 218145,
    "avatar_url": "https://avatars.githubusercontent.com/u/218145?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/pwais",
    "html_url": "https://github.com/pwais",
    "followers_url": "https://api.github.com/users/pwais/followers",
    "following_url": "https://api.github.com/users/pwais/following{/other_user}",
    "gists_url": "https://api.github.com/users/pwais/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/pwais/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/pwais/subscriptions",
    "organizations_url": "https://api.github.com/users/pwais/orgs",
    "repos_url": "https://api.github.com/users/pwais/repos",
    "events_url": "https://api.github.com/users/pwais/events{/privacy}",
    "received_events_url": "https://api.github.com/users/pwais/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-09-15T00:52:15Z",
  "updated_at": "2015-09-15T00:52:15Z",
  "body": "@mknejp did you ever poke much farther on this?  I'm curious if you ended up implementing anything for Java <=> C++.\r\n\r\nI dug into this a bit further with the presumption that a buffer is a `(pointer, size, deleter)` tuple.  The `deleter` addition specifically handles ownership-related issues for arena-allocated memory, mmap-ed files, and other buffers that need special cleanup.  I believe this model covers all possible use cases.  \r\n\r\nBased upon the discussion below, I think a single zero-copy buffer record type would not fulfill all needs; I think most use cases actually call for an *interface*.  Nevertheless, there appear to be some proper practices that could work there way into Djinni support code (if not as a part of an IDL primitive).  \r\n\r\n\r\n### GC-Unmanaged Access\r\n\r\nNote that if the user simply wants to share r/w access to a buffer and does *not* intend to move or share ownership, then a buffer can simply be a `(pointer, size)` tuple.  **Unmanaged** pointers are almost completely portable; if the JVM is 32-bit and the host is 64-bit, one needs to worry about [sign extension](http://hg.openjdk.java.net/jdk7/jdk7/hotspot/file/9b0ca45cd756/src/share/vm/prims/unsafe.cpp#l74).  NB: `java.nio.ByteBuffer` capacities are `int`-sized, but `sun.misc.Unsafe` allows allocating blocks of memory larger than `Integer.MAX_VALUE`; thus each of `(pointer, size)` should be 64-bit.  Therefore, the simplest way to achieve a buffer type would be to define a `record` with members `i64 address` and `i64 size`.  If the user means to invoke an interface upon the buffer frequently (e.g. in a loop), it would be more performant (but slightly uglier) to omit the record and pass `(i64 address, i64 size)` as parameters.\r\n\r\n\r\n### GC-Managed Access\r\n\r\nFor exposing **managed** memory (e.g. `byte[]`s) to native code, JNI's `GetPrimitiveArrayCritical()` might work but can block GC ([as it does in Hotspot](https://github.com/openjdk-mirror/jdk7u-hotspot/blob/50bdefc3afe944ca74c3093e7448d6b889cd20d1/src/share/vm/prims/jni.cpp#L4235)).  It seems that this API is really meant for immediately copying data to/from a device, e.g. as [Android does in these results](https://github.com/android/platform_frameworks_base/search?p=3&q=GetPrimitiveArrayCritical&utf8=%E2%9C%93).  Due to these restrictions, a (potentially) zero-copy *managed* buffer might warrant a completely unique object (record or interface) in user code.  \r\n\r\n\r\n### Moving and Sharing ownership to GC-Unmanaged Memory\r\n\r\nIn the case that the user *does* want to move and/or share ownership, a `deleter` is necessary and significantly complicates the problem.  Without loss of generality, we can assume a `deleter` is either a Java `Runnable` or a C++ callable (`std::function<void()>`) that holds the buffer address (and/or other data/references) as state.  \r\n\r\n`DirectByteBuffer`s offer some direction as to how have the JVM handle foreign deleters properly.  Some JVMs leverage a somewhat peculiar [`Cleaner` utility](http://grepcode.com/file/repository.grepcode.com/java/root/jdk/openjdk/6-b14/java/nio/DirectByteBuffer.java#129) to wrap a native `free()`-calling thunk; the GC interops with cleaners directly.  However, the Android JVM is a bit different and uses [`MemoryBlock#finalize()`](https://android.googlesource.com/platform/libcore/+/android-4.3_r1/luni/src/main/java/java/nio/MemoryBlock.java) to free native memory (i.e. there is no thunk).  It's important to note that `DirectByteBuffer` does **not** have a field for a deleter on all JVMs.\r\n\r\nThus for **shared** buffers, Djinni would need to internally maintain a list of deleters (or perhaps just the buffers themselves) and free memory only once buffers become unreferenced from both sides.  (I believe there's similar existing code to deal with interface instances).  For buffers that only **move** one way, the issue is still complicated (see below).  It looks like a JNI call upon buffer termination is a necessity; moving buffers in a loop would have *high overhead*.\r\n\r\n#### Java => C++\r\n\r\nFor `DirectByteBuffer`s, Djinni needs to maintain a reference to the `DirectByteBuffer` instance.  Once the C++ proxy dies, Djinni can drop the `DirectByteBuffer` reference and the GC will reclaim the memory normally.\r\n\r\nFor user-managed off-heap memory (e.g. memory allocated via `sun.misc.Unsafe`), Djinni needs to invoke a user `Runnable` once the C++ proxy dies.  The user `Runnable` might invoke `Unsafe.freeMemory()` or might have other behavior (e.g. if the buffer resides within a larger arena).  NB: `Unsafe.freeMemory()` [invokes a JVM-dependent `os::free()` method](http://hg.openjdk.java.net/jdk7/jdk7/hotspot/file/9b0ca45cd756/src/share/vm/prims/unsafe.cpp#l615), so user C++ code cannot safely just call `free()` on `Unsafe`-allocated memory.\r\n\r\n#### C++ => Java\r\n\r\nDjinni would need to call a user `std::function<void()>` cleanup method once the proxy object `finalize()` is called.  "
}
, {
  "url": "https://api.github.com/repos/dropbox/djinni/issues/comments/149844005",
  "html_url": "https://github.com/dropbox/djinni/issues/54#issuecomment-149844005",
  "issue_url": "https://api.github.com/repos/dropbox/djinni/issues/54",
  "id": 149844005,
  "user": {
    "login": "pwais",
    "id": 218145,
    "avatar_url": "https://avatars.githubusercontent.com/u/218145?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/pwais",
    "html_url": "https://github.com/pwais",
    "followers_url": "https://api.github.com/users/pwais/followers",
    "following_url": "https://api.github.com/users/pwais/following{/other_user}",
    "gists_url": "https://api.github.com/users/pwais/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/pwais/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/pwais/subscriptions",
    "organizations_url": "https://api.github.com/users/pwais/orgs",
    "repos_url": "https://api.github.com/users/pwais/repos",
    "events_url": "https://api.github.com/users/pwais/events{/privacy}",
    "received_events_url": "https://api.github.com/users/pwais/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-10-21T10:02:50Z",
  "updated_at": "2015-10-21T10:02:50Z",
  "body": "FWIW, am hacking on the C++ <=> Java part of this in https://github.com/pwais/djinni/tree/pwais_perf2 (NOT in preview state yet) with the goal of supporting at least {byte[], (direct) ByteBuffer, Unsafe} <=> C++ using user-defined types.  I think it would make sense to exist in `/extension-libs` if included in djinni at all.  The goal is not to add a core djinni buffer type (as I don't see a great solution in that approach) but to alleviate the user of having to worry about JNI through a few custom array types."
}
, {
  "url": "https://api.github.com/repos/dropbox/djinni/issues/comments/168765252",
  "html_url": "https://github.com/dropbox/djinni/issues/54#issuecomment-168765252",
  "issue_url": "https://api.github.com/repos/dropbox/djinni/issues/54",
  "id": 168765252,
  "user": {
    "login": "jcampbell05",
    "id": 1484989,
    "avatar_url": "https://avatars.githubusercontent.com/u/1484989?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/jcampbell05",
    "html_url": "https://github.com/jcampbell05",
    "followers_url": "https://api.github.com/users/jcampbell05/followers",
    "following_url": "https://api.github.com/users/jcampbell05/following{/other_user}",
    "gists_url": "https://api.github.com/users/jcampbell05/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/jcampbell05/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/jcampbell05/subscriptions",
    "organizations_url": "https://api.github.com/users/jcampbell05/orgs",
    "repos_url": "https://api.github.com/users/jcampbell05/repos",
    "events_url": "https://api.github.com/users/jcampbell05/events{/privacy}",
    "received_events_url": "https://api.github.com/users/jcampbell05/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2016-01-04T18:48:15Z",
  "updated_at": "2016-01-04T18:48:15Z",
  "body": "Any progress ?"
}
, {
  "url": "https://api.github.com/repos/dropbox/djinni/issues/comments/168936780",
  "html_url": "https://github.com/dropbox/djinni/issues/54#issuecomment-168936780",
  "issue_url": "https://api.github.com/repos/dropbox/djinni/issues/54",
  "id": 168936780,
  "user": {
    "login": "pwais",
    "id": 218145,
    "avatar_url": "https://avatars.githubusercontent.com/u/218145?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/pwais",
    "html_url": "https://github.com/pwais",
    "followers_url": "https://api.github.com/users/pwais/followers",
    "following_url": "https://api.github.com/users/pwais/following{/other_user}",
    "gists_url": "https://api.github.com/users/pwais/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/pwais/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/pwais/subscriptions",
    "organizations_url": "https://api.github.com/users/pwais/orgs",
    "repos_url": "https://api.github.com/users/pwais/repos",
    "events_url": "https://api.github.com/users/pwais/events{/privacy}",
    "received_events_url": "https://api.github.com/users/pwais/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2016-01-05T08:51:13Z",
  "updated_at": "2016-01-05T08:51:49Z",
  "body": "Really want to finish the C++ <-> Java work I had started earlier (linked above), but have been inundated with other things.  In that change, `JHeapArrayHandle-inl.hpp` has all the bits for `byte[]` <-> void *, `ByteArrayHandle-inl.hpp` for ByteBuffer <->  void *, and `JUnsafeArrayHandle-inl.hpp` for `sun.misc.Unsafe` <-> void *.  Some of the code is mid-refactor, so please pardon all the dust if you go digging, but I believe I had all the JNI calls correct.\r\n\r\nIf you just want direct ByteBuffer <-> (`void *`, `size_t`), here's what I'd recommend:\r\n  * ByteBuffer -> (`void *`, `size_t`): Just use the `GetDirectBufferAddress()` JNI API\r\n  * (`void *`, `size_t`) -> ByteBuffer: Note that `ByteBuffer.allocateDirect()` is (AFAICT) the only portable way to create a *Java-owned* direct byte buffer; `NewDirectByteBufer()` creates a *native-owned fascade*.  I recommend using JNI to invoke `ByteBuffer.allocateDirect()` (see `JDirectArray::allocateDirectBB()` in the branch).  Have the native code write into the ByteBuffer if at all possible.  Otherwise, you'll need to pass a deleter from native to Java somehow.  \r\n\r\n(FWIW, I saw that Android and OpenJDK implement direct ByteBuffers a bit differently, hence my note about portability.  Since Google appears to be adopting OpenJDK for Android, this may be less of an issue going forward).\r\n\r\n"
}
]
