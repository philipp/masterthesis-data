[
{
  "url": "https://api.github.com/repos/dropbox/pyston/issues/comments/135284364",
  "html_url": "https://github.com/dropbox/pyston/issues/869#issuecomment-135284364",
  "issue_url": "https://api.github.com/repos/dropbox/pyston/issues/869",
  "id": 135284364,
  "user": {
    "login": "kmod",
    "id": 271916,
    "avatar_url": "https://avatars.githubusercontent.com/u/271916?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/kmod",
    "html_url": "https://github.com/kmod",
    "followers_url": "https://api.github.com/users/kmod/followers",
    "following_url": "https://api.github.com/users/kmod/following{/other_user}",
    "gists_url": "https://api.github.com/users/kmod/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/kmod/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/kmod/subscriptions",
    "organizations_url": "https://api.github.com/users/kmod/orgs",
    "repos_url": "https://api.github.com/users/kmod/repos",
    "events_url": "https://api.github.com/users/kmod/events{/privacy}",
    "received_events_url": "https://api.github.com/users/kmod/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-08-27T03:55:38Z",
  "updated_at": "2015-08-27T03:55:38Z",
  "body": "It looks like one thing making reading this quite tricky is our heavy use of patchpoints -- [there's a good description of them in the llvm docs](http://llvm.org/docs/StackMaps.html).  But it makes the IR a bit harder to read due to a bunch of extra arguments and the fact that the actual function being called is buried in the middle of the line.\r\n\r\nWe have some options that you can toggle in src/core/options.cpp; I turned off ENABLE_ICS and ENABLE_FRAME_INTROSPECTION and got this IR:\r\n\r\n```llvm\r\ndefine %\"class.pyston::Box\"* @\"<module>_e2_0\"() {\r\nblock0:\r\n  call void @allowGLReadPreemption()\r\n  %0 = call %\"class.pyston::Box\"* @boxCLFunction(%\"class.pyston::CLFunction\"* @c0, %\"class.pyston::BoxedClosure\"* null, %\"class.pyston::Box\"* null, %\"class.pyston::Box\"** null, i64 0), !dbg !7\r\n  call void @setattr(%\"class.pyston::Box\"* @cParentModule, %\"class.pyston::BoxedString\"* @c2, %\"class.pyston::Box\"* %0), !dbg !7\r\n  %1 = call %\"class.pyston::Box\"* @getGlobal(%\"class.pyston::Box\"* @cParentModule, %\"class.pyston::BoxedString\"* @c3), !dbg !8\r\n  %2 = call %\"class.pyston::Box\"* @runtimeCall(%\"class.pyston::Box\"* %1, i32 131072, %\"class.pyston::Box\"* @c4, %\"class.pyston::Box\"* @c5), !dbg !8\r\n  call void @setattr(%\"class.pyston::Box\"* @cParentModule, %\"class.pyston::BoxedString\"* @c6, %\"class.pyston::Box\"* %2), !dbg !8\r\n  %3 = call %\"class.pyston::Box\"* @getGlobal(%\"class.pyston::Box\"* @cParentModule, %\"class.pyston::BoxedString\"* @c7), !dbg !9\r\n  %4 = call %\"class.pyston::Box\"* @getSysStdout(), !dbg !9\r\n  %5 = call i1 @softspace(%\"class.pyston::Box\"* %4, i1 false), !dbg !9\r\n  br i1 %5, label %softspace, label %print, !dbg !9\r\n\r\nsoftspace:                                        ; preds = %block0\r\n  %6 = call %\"class.pyston::Box\"* @callattr(%\"class.pyston::Box\"* %4, %\"class.pyston::BoxedString\"* @c8, i64 281474976710656, %\"class.pyston::Box\"* @c9), !dbg !9\r\n  br label %print, !dbg !9\r\n\r\nprint:                                            ; preds = %softspace, %block0\r\n  %7 = call %\"class.pyston::Box\"* @strOrUnicode(%\"class.pyston::Box\"* %3), !dbg !9\r\n  %8 = call %\"class.pyston::Box\"* @callattr(%\"class.pyston::Box\"* %4, %\"class.pyston::BoxedString\"* @c10, i64 281474976710656, %\"class.pyston::Box\"* %7), !dbg !9\r\n  %9 = call %\"class.pyston::Box\"* @callattr(%\"class.pyston::Box\"* %4, %\"class.pyston::BoxedString\"* @c11, i64 281474976710656, %\"class.pyston::Box\"* @c12), !dbg !9\r\n  ret %\"class.pyston::Box\"* @cNone, !dbg !9\r\n}\r\n```\r\n\r\nQuite a bit better :)  It's still a bit hard to read since the string values don't make it in, but you can still make out what's going on:\r\n- release the GIL if necessary (`allowGLReadPreemption`)\r\n- create python function object from an ast (`boxCLFunction`)\r\n- store that function object into the current global module (`setattr`)\r\n- get the function back from the global module (`getGlobal`)\r\n- call it (`runtimeCall`)\r\n- store the result in the global module (`setattr`)\r\n- get the result from the global module (`getGlobal`)\r\n- the rest of the function is the handling of the print statement\r\n\r\nHere's the IR for foo():\r\n```llvm\r\ndefine %\"class.pyston::Box\"* @foo_e2_1(%\"class.pyston::Box\"*, %\"class.pyston::Box\"*) {\r\npre_entry:\r\n  %2 = alloca %\"class.pyston::Box\"*, i64 2\r\n  %frame_info = alloca %\"struct.pyston::FrameInfo\"\r\n  %3 = load i64* @c0\r\n  %4 = add i64 %3, 1\r\n  store i64 %4, i64* @c0\r\n  %5 = icmp sgt i64 %4, 10000\r\n  %6 = getelementptr inbounds %\"struct.pyston::FrameInfo\"* %frame_info, i32 0, i32 0\r\n  %7 = getelementptr inbounds %\"struct.pyston::ExcInfo\"* %6, i32 0, i32 0\r\n  store %\"class.pyston::Box\"* null, %\"class.pyston::Box\"** %7\r\n  %8 = getelementptr inbounds %\"struct.pyston::FrameInfo\"* %frame_info, i32 0, i32 1\r\n  store %\"class.pyston::Box\"* null, %\"class.pyston::Box\"** %8\r\n  %9 = getelementptr inbounds %\"struct.pyston::FrameInfo\"* %frame_info, i32 0, i32 2\r\n  store %\"class.pyston::BoxedFrame\"* null, %\"class.pyston::BoxedFrame\"** %9\r\n  br i1 %5, label %reopt, label %block0, !prof !7\r\n\r\nblock0:                                           ; preds = %pre_entry\r\n  call void @allowGLReadPreemption()\r\n  %10 = call %\"class.pyston::Box\"* @binop(%\"class.pyston::Box\"* %0, %\"class.pyston::Box\"* %1, i32 81), !dbg !8\r\n  %11 = call %\"class.pyston::Box\"* @compare(%\"class.pyston::Box\"* %10, %\"class.pyston::Box\"* @c2, i32 63), !dbg !9\r\n  %12 = call i1 @nonzero(%\"class.pyston::Box\"* %11), !dbg !9\r\n  br i1 %12, label %block1, label %block2, !dbg !9\r\n\r\nblock1:                                           ; preds = %block0\r\n  ret %\"class.pyston::Box\"* @c3, !dbg !10\r\n\r\nblock2:                                           ; preds = %block0\r\n  br label %block3, !dbg !11\r\n\r\nblock3:                                           ; preds = %block5, %block2\r\n  %a = phi %\"class.pyston::Box\"* [ %0, %block2 ], [ %a, %block5 ]\r\n  %b = phi %\"class.pyston::Box\"* [ %1, %block2 ], [ %b, %block5 ]\r\n  %c = phi %\"class.pyston::Box\"* [ %10, %block2 ], [ %14, %block5 ]\r\n  %count = phi %\"class.pyston::Box\"* [ @c4, %block2 ], [ %17, %block5 ]\r\n  call void @allowGLReadPreemption()\r\n  br i1 true, label %block4, label %block8, !dbg !12\r\n\r\nblock4:                                           ; preds = %block3\r\n  %13 = call %\"class.pyston::Box\"* @binop(%\"class.pyston::Box\"* %c, %\"class.pyston::Box\"* @c5, i32 74), !dbg !13\r\n  %14 = call %\"class.pyston::Box\"* @binop(%\"class.pyston::Box\"* %c, %\"class.pyston::Box\"* %13, i32 79), !dbg !13\r\n  %15 = call %\"class.pyston::Box\"* @compare(%\"class.pyston::Box\"* %14, %\"class.pyston::Box\"* @c6, i32 67), !dbg !14\r\n  %16 = call i1 @nonzero(%\"class.pyston::Box\"* %15), !dbg !14\r\n  br i1 %16, label %block5, label %block6, !dbg !14\r\n\r\nblock5:                                           ; preds = %block4\r\n  %17 = call %\"class.pyston::Box\"* @binop(%\"class.pyston::Box\"* %count, %\"class.pyston::Box\"* @c8, i32 69), !dbg !15\r\n  %18 = load i64* @edgecount, !dbg !11\r\n  %19 = add i64 %18, 1, !dbg !11\r\n  store i64 %19, i64* @edgecount, !dbg !11\r\n  %20 = icmp sgt i64 %19, 10000, !dbg !11\r\n  br i1 %20, label %onramp, label %block3, !dbg !11, !prof !7\r\n\r\nblock6:                                           ; preds = %block4\r\n  ret %\"class.pyston::Box\"* %count, !dbg !16\r\n\r\nblock8:                                           ; preds = %block3\r\n  ret %\"class.pyston::Box\"* @cNone\r\n\r\nreopt:                                            ; preds = %pre_entry\r\n  %21 = call i8* @reoptCompiledFunc(i8* @c1)\r\n  %22 = bitcast i8* %21 to %\"class.pyston::Box\"* (%\"class.pyston::Box\"*, %\"class.pyston::Box\"*)*\r\n  %23 = tail call %\"class.pyston::Box\"* %22(%\"class.pyston::Box\"* %0, %\"class.pyston::Box\"* %1)\r\n  ret %\"class.pyston::Box\"* %23\r\n\r\nonramp:                                           ; preds = %block5\r\n  %24 = call i8* @\"pyston::compilePartialFunc(pyston::OSRExit*)\"(i8* @c9), !dbg !11\r\n  %25 = getelementptr %\"class.pyston::Box\"** %2, i32 0, !dbg !11\r\n  store %\"class.pyston::Box\"* %14, %\"class.pyston::Box\"** %25, !dbg !11\r\n  %26 = getelementptr %\"class.pyston::Box\"** %2, i32 1, !dbg !11\r\n  store %\"class.pyston::Box\"* %17, %\"class.pyston::Box\"** %26, !dbg !11\r\n  %27 = bitcast i8* %24 to %\"class.pyston::Box\"* (%\"struct.pyston::FrameInfo\"*, %\"class.pyston::Box\"*, %\"class.pyston::Box\"*, %\"class.pyston::Box\"**)*, !dbg !11\r\n  %28 = call %\"class.pyston::Box\"* %27(%\"struct.pyston::FrameInfo\"* %frame_info, %\"class.pyston::Box\"* %a, %\"class.pyston::Box\"* %b, %\"class.pyston::Box\"** %2), !dbg !11\r\n  ret %\"class.pyston::Box\"* %28, !dbg !11\r\n}\r\n```"
}
, {
  "url": "https://api.github.com/repos/dropbox/pyston/issues/comments/135316070",
  "html_url": "https://github.com/dropbox/pyston/issues/869#issuecomment-135316070",
  "issue_url": "https://api.github.com/repos/dropbox/pyston/issues/869",
  "id": 135316070,
  "user": {
    "login": "lanyuesl",
    "id": 13494185,
    "avatar_url": "https://avatars.githubusercontent.com/u/13494185?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/lanyuesl",
    "html_url": "https://github.com/lanyuesl",
    "followers_url": "https://api.github.com/users/lanyuesl/followers",
    "following_url": "https://api.github.com/users/lanyuesl/following{/other_user}",
    "gists_url": "https://api.github.com/users/lanyuesl/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/lanyuesl/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/lanyuesl/subscriptions",
    "organizations_url": "https://api.github.com/users/lanyuesl/orgs",
    "repos_url": "https://api.github.com/users/lanyuesl/repos",
    "events_url": "https://api.github.com/users/lanyuesl/events{/privacy}",
    "received_events_url": "https://api.github.com/users/lanyuesl/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-08-27T07:01:20Z",
  "updated_at": "2015-08-27T07:11:36Z",
  "body": "So why my source code translate to IR with some like \"boxCLFunction\" and \"allowGLReadPreemption\", witch, I think,  can only be understood by pyston developer?\r\nIs it possible that pyston make it more friendly to its users? I mean, make it easy to undetstand without knowing what the function meaning of \"boxCLFunction\" and \"allowGLReadPreemption\".\r\nAs you can see, the CFG block is much more friendly. So Why not LLVM IR?"
}
]
