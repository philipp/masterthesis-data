[
{
  "url": "https://api.github.com/repos/github/markup/issues/comments/115787455",
  "html_url": "https://github.com/github/markup/pull/516#issuecomment-115787455",
  "issue_url": "https://api.github.com/repos/github/markup/issues/516",
  "id": 115787455,
  "user": {
    "login": "gjtorikian",
    "id": 64050,
    "avatar_url": "https://avatars.githubusercontent.com/u/64050?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/gjtorikian",
    "html_url": "https://github.com/gjtorikian",
    "followers_url": "https://api.github.com/users/gjtorikian/followers",
    "following_url": "https://api.github.com/users/gjtorikian/following{/other_user}",
    "gists_url": "https://api.github.com/users/gjtorikian/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/gjtorikian/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/gjtorikian/subscriptions",
    "organizations_url": "https://api.github.com/users/gjtorikian/orgs",
    "repos_url": "https://api.github.com/users/gjtorikian/repos",
    "events_url": "https://api.github.com/users/gjtorikian/events{/privacy}",
    "received_events_url": "https://api.github.com/users/gjtorikian/received_events",
    "type": "User",
    "site_admin": true
  },
  "created_at": "2015-06-26T17:24:37Z",
  "updated_at": "2015-06-26T17:24:37Z",
  "body": "![](http://i.imgur.com/iYjs3A1.gif)\r\n\r\nThanks for this! It'll take some time to go through and review thoroughly. One thing that strikes my eye: doesn't the section listed as `YOU SHOULD NOT SEE THIS!` partially defeat the point? I would think that every unrenderable text should still be shown (as an error). \r\n\r\n> Additional possible enhancements include figuring out how to\r\n> gray out this text\r\n\r\nIf we can emit a class name--say, `github-markup-rst-error`--we can pass it through and style it appropriately on GitHub.com. "
}
, {
  "url": "https://api.github.com/repos/github/markup/issues/comments/115821362",
  "html_url": "https://github.com/github/markup/pull/516#issuecomment-115821362",
  "issue_url": "https://api.github.com/repos/github/markup/issues/516",
  "id": 115821362,
  "user": {
    "login": "pmaupin",
    "id": 2379062,
    "avatar_url": "https://avatars.githubusercontent.com/u/2379062?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/pmaupin",
    "html_url": "https://github.com/pmaupin",
    "followers_url": "https://api.github.com/users/pmaupin/followers",
    "following_url": "https://api.github.com/users/pmaupin/following{/other_user}",
    "gists_url": "https://api.github.com/users/pmaupin/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/pmaupin/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/pmaupin/subscriptions",
    "organizations_url": "https://api.github.com/users/pmaupin/orgs",
    "repos_url": "https://api.github.com/users/pmaupin/repos",
    "events_url": "https://api.github.com/users/pmaupin/events{/privacy}",
    "received_events_url": "https://api.github.com/users/pmaupin/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-06-26T18:10:42Z",
  "updated_at": "2015-06-26T18:10:42Z",
  "body": "> doesn't the section listed as YOU SHOULD NOT SEE THIS! partially defeat the point?\r\n\r\nNo, that's in a comment.  By default and by design, comments in restructuredText are ignored.  I wanted to be able to override this for the special case of being able to create comments that display only at github (and not, for example, at readthedocs).  The primary use case is pointing out that the display at github is not necessarily the optimum one for the particular document that the comment appears in (because, for example, the source text uses restructuredText extensions supported by readthedocs but not github).\r\n\r\nAlso, as you pointed out in issue #514, sometimes people get dismayed/confused/annoyed by showing errors, so I didn't really want to go there.  I think I agree with the way the error level now works, where all errors are ignored.  (Although, if your documentation on the use of this doesn't talk about this, it might be good to add a section that describes how to convert and view the documentation locally before pushing it.)\r\n\r\n> If we can emit a class name--say, github-markup-rst-error--we can pass it through and style it appropriately on GitHub.com.\r\n\r\nWell, again, these aren't errors...   Other than that, I think it's a good idea, and I am absolutely sure that is possible, but don't know the best way to go about it.  I'm also wondering if the stylesheet github currently uses for restructuredText is optimal, because it doesn't distinguish code as well as some others.\r\n\r\nI might dig in and look at that a bit more.  Meanwhile, when I think about your question about errors, I think that it might be useful to articulate principles for processing markup so that we can have a more fruitful discussion and hopefully build consensus.\r\n\r\nBased on the current behavior and history of issues and pull requests, I would say that the current principles are:\r\n\r\n0) Handle markup securely (no include files, etc.)\r\n1) Let people use docutils to style their documents as nicely as they can for github display.\r\n2) Quietly discard error messages and information related to broken parts of files, because those detract significantly from github display.  (People can run the code on their own machines with different logging levels to do debugging -- github should prioritize good display over exposing debugging information.)\r\n\r\nAnd I'm proposing extending these principles to support documents that are stored at github, but that are primarily designed to be converted and displayed by extended tooling elsewhere.  Because rst file parsing is so extensible, there will always be some valid rst files that github cannot fully parse, so:\r\n\r\n3) Github should, by default, show (as relatively plain text) the *valid* parts of the restructuredText file that it does not know how to mark up.\r\n4) But github should allow the document author to turn off this display, if desired (if only because not having it display was the previous behavior).\r\n5) Github should allow a document to include text that *only* appears on github, for example, to allow an explanation that (for that particular document) the github display is not the optimal one, and that a better looking rendering of the document can be found elsewhere.\r\n"
}
, {
  "url": "https://api.github.com/repos/github/markup/issues/comments/115852409",
  "html_url": "https://github.com/github/markup/pull/516#issuecomment-115852409",
  "issue_url": "https://api.github.com/repos/github/markup/issues/516",
  "id": 115852409,
  "user": {
    "login": "pmaupin",
    "id": 2379062,
    "avatar_url": "https://avatars.githubusercontent.com/u/2379062?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/pmaupin",
    "html_url": "https://github.com/pmaupin",
    "followers_url": "https://api.github.com/users/pmaupin/followers",
    "following_url": "https://api.github.com/users/pmaupin/following{/other_user}",
    "gists_url": "https://api.github.com/users/pmaupin/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/pmaupin/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/pmaupin/subscriptions",
    "organizations_url": "https://api.github.com/users/pmaupin/orgs",
    "repos_url": "https://api.github.com/users/pmaupin/repos",
    "events_url": "https://api.github.com/users/pmaupin/events{/privacy}",
    "received_events_url": "https://api.github.com/users/pmaupin/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-06-26T19:47:51Z",
  "updated_at": "2015-06-26T19:47:51Z",
  "body": "I have pushed code to generate classes for the comments and directives.  I guess the ruby tests scrub those, because they aren't part of the compare.  Anyway, on my machine, when I run the script, I can see the class elements in the HTML.  YMMV."
}
, {
  "url": "https://api.github.com/repos/github/markup/issues/comments/116821542",
  "html_url": "https://github.com/github/markup/pull/516#issuecomment-116821542",
  "issue_url": "https://api.github.com/repos/github/markup/issues/516",
  "id": 116821542,
  "user": {
    "login": "pmaupin",
    "id": 2379062,
    "avatar_url": "https://avatars.githubusercontent.com/u/2379062?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/pmaupin",
    "html_url": "https://github.com/pmaupin",
    "followers_url": "https://api.github.com/users/pmaupin/followers",
    "following_url": "https://api.github.com/users/pmaupin/following{/other_user}",
    "gists_url": "https://api.github.com/users/pmaupin/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/pmaupin/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/pmaupin/subscriptions",
    "organizations_url": "https://api.github.com/users/pmaupin/orgs",
    "repos_url": "https://api.github.com/users/pmaupin/repos",
    "events_url": "https://api.github.com/users/pmaupin/events{/privacy}",
    "received_events_url": "https://api.github.com/users/pmaupin/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-06-29T20:03:32Z",
  "updated_at": "2015-06-29T20:03:32Z",
  "body": "@gjtorikian \r\n\r\nAre you waiting for me?  If so, what else do I need to do to facilitate this?\r\n\r\nThanks,\r\nPat"
}
, {
  "url": "https://api.github.com/repos/github/markup/issues/comments/116831345",
  "html_url": "https://github.com/github/markup/pull/516#issuecomment-116831345",
  "issue_url": "https://api.github.com/repos/github/markup/issues/516",
  "id": 116831345,
  "user": {
    "login": "gjtorikian",
    "id": 64050,
    "avatar_url": "https://avatars.githubusercontent.com/u/64050?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/gjtorikian",
    "html_url": "https://github.com/gjtorikian",
    "followers_url": "https://api.github.com/users/gjtorikian/followers",
    "following_url": "https://api.github.com/users/gjtorikian/following{/other_user}",
    "gists_url": "https://api.github.com/users/gjtorikian/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/gjtorikian/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/gjtorikian/subscriptions",
    "organizations_url": "https://api.github.com/users/gjtorikian/orgs",
    "repos_url": "https://api.github.com/users/gjtorikian/repos",
    "events_url": "https://api.github.com/users/gjtorikian/events{/privacy}",
    "received_events_url": "https://api.github.com/users/gjtorikian/received_events",
    "type": "User",
    "site_admin": true
  },
  "created_at": "2015-06-29T20:24:04Z",
  "updated_at": "2015-06-29T20:24:04Z",
  "body": "I haven't had a chance to review your changes as of yet, sorry. I'll let you know if I have other questions. "
}
, {
  "url": "https://api.github.com/repos/github/markup/issues/comments/116913789",
  "html_url": "https://github.com/github/markup/pull/516#issuecomment-116913789",
  "issue_url": "https://api.github.com/repos/github/markup/issues/516",
  "id": 116913789,
  "user": {
    "login": "pmaupin",
    "id": 2379062,
    "avatar_url": "https://avatars.githubusercontent.com/u/2379062?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/pmaupin",
    "html_url": "https://github.com/pmaupin",
    "followers_url": "https://api.github.com/users/pmaupin/followers",
    "following_url": "https://api.github.com/users/pmaupin/following{/other_user}",
    "gists_url": "https://api.github.com/users/pmaupin/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/pmaupin/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/pmaupin/subscriptions",
    "organizations_url": "https://api.github.com/users/pmaupin/orgs",
    "repos_url": "https://api.github.com/users/pmaupin/repos",
    "events_url": "https://api.github.com/users/pmaupin/events{/privacy}",
    "received_events_url": "https://api.github.com/users/pmaupin/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-06-30T02:15:21Z",
  "updated_at": "2015-06-30T02:15:21Z",
  "body": "Great!  Thanks for looking at it."
}
]
