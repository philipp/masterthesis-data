[
{
  "url": "https://api.github.com/repos/github/brubeck/issues/comments/112875501",
  "html_url": "https://github.com/github/brubeck/pull/12#issuecomment-112875501",
  "issue_url": "https://api.github.com/repos/github/brubeck/issues/12",
  "id": 112875501,
  "user": {
    "login": "haneefmubarak",
    "id": 1464144,
    "avatar_url": "https://avatars.githubusercontent.com/u/1464144?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/haneefmubarak",
    "html_url": "https://github.com/haneefmubarak",
    "followers_url": "https://api.github.com/users/haneefmubarak/followers",
    "following_url": "https://api.github.com/users/haneefmubarak/following{/other_user}",
    "gists_url": "https://api.github.com/users/haneefmubarak/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/haneefmubarak/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/haneefmubarak/subscriptions",
    "organizations_url": "https://api.github.com/users/haneefmubarak/orgs",
    "repos_url": "https://api.github.com/users/haneefmubarak/repos",
    "events_url": "https://api.github.com/users/haneefmubarak/events{/privacy}",
    "received_events_url": "https://api.github.com/users/haneefmubarak/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-06-17T16:54:27Z",
  "updated_at": "2015-06-17T16:54:27Z",
  "body": "Good point. I think I'll have the thread yield in case of contention then. Also, while we're at it, [you should use `-pthread` instead of `-lpthread` for compiler options](http://stackoverflow.com/a/23251828/2334407)."
}
, {
  "url": "https://api.github.com/repos/github/brubeck/issues/comments/112883277",
  "html_url": "https://github.com/github/brubeck/pull/12#issuecomment-112883277",
  "issue_url": "https://api.github.com/repos/github/brubeck/issues/12",
  "id": 112883277,
  "user": {
    "login": "haneefmubarak",
    "id": 1464144,
    "avatar_url": "https://avatars.githubusercontent.com/u/1464144?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/haneefmubarak",
    "html_url": "https://github.com/haneefmubarak",
    "followers_url": "https://api.github.com/users/haneefmubarak/followers",
    "following_url": "https://api.github.com/users/haneefmubarak/following{/other_user}",
    "gists_url": "https://api.github.com/users/haneefmubarak/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/haneefmubarak/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/haneefmubarak/subscriptions",
    "organizations_url": "https://api.github.com/users/haneefmubarak/orgs",
    "repos_url": "https://api.github.com/users/haneefmubarak/repos",
    "events_url": "https://api.github.com/users/haneefmubarak/events{/privacy}",
    "received_events_url": "https://api.github.com/users/haneefmubarak/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-06-17T17:22:33Z",
  "updated_at": "2015-06-17T17:22:33Z",
  "body": "Okay so on any platform where explicit bus yielding support is unavailable, we call `pthread_yield()`, which yields the remainder of the thread's timeslice to the scheduler, who may then switch to another thread or resume the calling thread. Either way, this should hopefully be long enough for contention to clear (and it's non-deterministic).\r\n\r\nThe only minus to this is that in cases of contention this is effectively going to be at least two context-switches long, if not longer.\r\n\r\n---\r\n\r\nOne possible solution might be to just switch to using pthread locks entirely, especially considering that on normal, reasonably updated linux systems, glibc (the default libc) is already pretty ingenious with locks:\r\n\r\nSimplified Pseudocode:\r\n\r\n```c\r\nsome_lock () {\r\n\r\n\t...\r\n\r\n\tint x = 0;\r\n\twhile (CANNOT_LOCK) {\r\n\t\tif (x < a_few_times) {\r\n\t\t\tbus_yield ();\r\n\t\t\tspin ();\r\n\t\t} else {\r\n\t\t\tyield_to_kernel ();\r\n\t\t}\r\n\t}\r\n\r\n\tset (lock);\r\n\treturn;\r\n}\r\n```\r\n\r\nThis makes it decently fast for most purposes and allows the kernel to time switch some threads if a lock is held for a while so that other threads can make progress."
}
, {
  "url": "https://api.github.com/repos/github/brubeck/issues/comments/112883519",
  "html_url": "https://github.com/github/brubeck/pull/12#issuecomment-112883519",
  "issue_url": "https://api.github.com/repos/github/brubeck/issues/12",
  "id": 112883519,
  "user": {
    "login": "haneefmubarak",
    "id": 1464144,
    "avatar_url": "https://avatars.githubusercontent.com/u/1464144?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/haneefmubarak",
    "html_url": "https://github.com/haneefmubarak",
    "followers_url": "https://api.github.com/users/haneefmubarak/followers",
    "following_url": "https://api.github.com/users/haneefmubarak/following{/other_user}",
    "gists_url": "https://api.github.com/users/haneefmubarak/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/haneefmubarak/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/haneefmubarak/subscriptions",
    "organizations_url": "https://api.github.com/users/haneefmubarak/orgs",
    "repos_url": "https://api.github.com/users/haneefmubarak/repos",
    "events_url": "https://api.github.com/users/haneefmubarak/events{/privacy}",
    "received_events_url": "https://api.github.com/users/haneefmubarak/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-06-17T17:23:44Z",
  "updated_at": "2015-06-17T17:23:44Z",
  "body": "@vmg anyways, that's more of a long term thing that needs benchmarking and such; for right now, I think it's ready to merge."
}
, {
  "url": "https://api.github.com/repos/github/brubeck/issues/comments/113090119",
  "html_url": "https://github.com/github/brubeck/pull/12#issuecomment-113090119",
  "issue_url": "https://api.github.com/repos/github/brubeck/issues/12",
  "id": 113090119,
  "user": {
    "login": "vmg",
    "id": 42793,
    "avatar_url": "https://avatars.githubusercontent.com/u/42793?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/vmg",
    "html_url": "https://github.com/vmg",
    "followers_url": "https://api.github.com/users/vmg/followers",
    "following_url": "https://api.github.com/users/vmg/following{/other_user}",
    "gists_url": "https://api.github.com/users/vmg/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/vmg/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/vmg/subscriptions",
    "organizations_url": "https://api.github.com/users/vmg/orgs",
    "repos_url": "https://api.github.com/users/vmg/repos",
    "events_url": "https://api.github.com/users/vmg/events{/privacy}",
    "received_events_url": "https://api.github.com/users/vmg/received_events",
    "type": "User",
    "site_admin": true
  },
  "created_at": "2015-06-18T09:33:25Z",
  "updated_at": "2015-06-18T09:33:25Z",
  "body": "Thanks for the PR. :)"
}
]
