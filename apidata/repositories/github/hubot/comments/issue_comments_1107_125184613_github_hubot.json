[
{
  "url": "https://api.github.com/repos/github/hubot/issues/comments/170130711",
  "html_url": "https://github.com/github/hubot/issues/1107#issuecomment-170130711",
  "issue_url": "https://api.github.com/repos/github/hubot/issues/1107",
  "id": 170130711,
  "user": {
    "login": "technicalpickles",
    "id": 159,
    "avatar_url": "https://avatars.githubusercontent.com/u/159?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/technicalpickles",
    "html_url": "https://github.com/technicalpickles",
    "followers_url": "https://api.github.com/users/technicalpickles/followers",
    "following_url": "https://api.github.com/users/technicalpickles/following{/other_user}",
    "gists_url": "https://api.github.com/users/technicalpickles/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/technicalpickles/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/technicalpickles/subscriptions",
    "organizations_url": "https://api.github.com/users/technicalpickles/orgs",
    "repos_url": "https://api.github.com/users/technicalpickles/repos",
    "events_url": "https://api.github.com/users/technicalpickles/events{/privacy}",
    "received_events_url": "https://api.github.com/users/technicalpickles/received_events",
    "type": "User",
    "site_admin": true
  },
  "created_at": "2016-01-08T21:28:07Z",
  "updated_at": "2016-01-08T21:28:07Z",
  "body": "I can definitely appreciate wanting to do this. I'm not sure about using environment variables to do this though. Environment variables and `hubot-scripts.json` and `external-scripts.json` are a bit opaque.\r\n\r\nI started working in https://github.com/github/hubot/pull/1110 to make it possible to programatically build a hubot. I'm imagining having more control of what is being loaded there, rather than making a ton of different environment variables.\r\n\r\nSome other ideas:\r\n\r\n* add support to an option hubot-s3-brain to be a no-op\r\n* iterate on `external-scripts` format to make it possible to customize when those scripts are loaded"
}
, {
  "url": "https://api.github.com/repos/github/hubot/issues/comments/170147125",
  "html_url": "https://github.com/github/hubot/issues/1107#issuecomment-170147125",
  "issue_url": "https://api.github.com/repos/github/hubot/issues/1107",
  "id": 170147125,
  "user": {
    "login": "radeksimko",
    "id": 287584,
    "avatar_url": "https://avatars.githubusercontent.com/u/287584?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/radeksimko",
    "html_url": "https://github.com/radeksimko",
    "followers_url": "https://api.github.com/users/radeksimko/followers",
    "following_url": "https://api.github.com/users/radeksimko/following{/other_user}",
    "gists_url": "https://api.github.com/users/radeksimko/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/radeksimko/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/radeksimko/subscriptions",
    "organizations_url": "https://api.github.com/users/radeksimko/orgs",
    "repos_url": "https://api.github.com/users/radeksimko/repos",
    "events_url": "https://api.github.com/users/radeksimko/events{/privacy}",
    "received_events_url": "https://api.github.com/users/radeksimko/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2016-01-08T22:37:46Z",
  "updated_at": "2016-01-08T22:37:46Z",
  "body": "> add support to an option hubot-s3-brain to be a no-op\r\n\r\nThat's also something that came up on my mind before submitting this issue, but I realised that such concept isn't very well scalable. We can't expect each brain or another module to add this to the codebase. This approach seems a bit hacky to me.\r\n\r\n> iterate on external-scripts format to make it possible to customize when those scripts are loaded\r\n\r\nI kind of like the current simple format (list of strings) of that file. Also using ENV variables seems to be the preferred approach to control environment differences in docker deployments & [12-Factor apps](http://12factor.net/config) generally.\r\n\r\nI understand the first suggestion may not fit well into the current concept, but what do you think of the alternative solutions - i.e. `HUBOT_EXTERNAL_SCRIPTS_PATH` or `HUBOT_EXCLUDE_EXTERNAL_SCRIPTS_PATH`?\r\n\r\nThe idea was to have something like `dev-exclude-ext-scripts.json` and point `HUBOT_EXCLUDE_EXTERNAL_SCRIPTS_PATH` to that filename:\r\n```json\r\n[ \"hubot-s3-brain\" ]\r\n```\r\n\r\nand then let hubot to load both `external-scripts.json` & `dev-exclude-ext-scripts.json`, do simple matching one-by-one and only load the scripts that are actually allowed.\r\n\r\nI personally like the last option as user doesn't have to repeat themselves per each environment, but it also means some extra logic in hubot itself, the first option `HUBOT_EXTERNAL_SCRIPTS_PATH` would be probably just 3 lines of code."
}
, {
  "url": "https://api.github.com/repos/github/hubot/issues/comments/170155481",
  "html_url": "https://github.com/github/hubot/issues/1107#issuecomment-170155481",
  "issue_url": "https://api.github.com/repos/github/hubot/issues/1107",
  "id": 170155481,
  "user": {
    "login": "technicalpickles",
    "id": 159,
    "avatar_url": "https://avatars.githubusercontent.com/u/159?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/technicalpickles",
    "html_url": "https://github.com/technicalpickles",
    "followers_url": "https://api.github.com/users/technicalpickles/followers",
    "following_url": "https://api.github.com/users/technicalpickles/following{/other_user}",
    "gists_url": "https://api.github.com/users/technicalpickles/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/technicalpickles/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/technicalpickles/subscriptions",
    "organizations_url": "https://api.github.com/users/technicalpickles/orgs",
    "repos_url": "https://api.github.com/users/technicalpickles/repos",
    "events_url": "https://api.github.com/users/technicalpickles/events{/privacy}",
    "received_events_url": "https://api.github.com/users/technicalpickles/received_events",
    "type": "User",
    "site_admin": true
  },
  "created_at": "2016-01-08T23:26:36Z",
  "updated_at": "2016-01-08T23:26:36Z",
  "body": "I'm really leaning towards doing this  as I'm talking about in https://github.com/github/hubot/pull/1110, that is:\r\n\r\n```\r\nif process.env.HUBOT_PERSISTENCE is 's3'\r\n  robot.loadScriptPackage require('hubot-help')\r\n```\r\n\r\nThat would be in your own hubot, so you can turn things on and off with environment variable, but you have a lot more control over the end product.\r\n\r\n> I personally like the last option as user doesn't have to repeat themselves per each environment, but it also means some extra logic in hubot itself, the first option HUBOT_EXTERNAL_SCRIPTS_PATH would be probably just 3 lines of code.\r\n\r\nThe lines of code aren't what I'm worried about even if it's 'only' 3. Right now there are just a ton of different ways to configure hubots (hubot-scripts.json, external-scripts.json, your own `scripts`, and tons of a ways to configure, and they aren't always very discover."
}
]
