[
{
  "url": "https://api.github.com/repos/google/auto/issues/comments/118889206",
  "html_url": "https://github.com/google/auto/issues/256#issuecomment-118889206",
  "issue_url": "https://api.github.com/repos/google/auto/issues/256",
  "id": 118889206,
  "user": {
    "login": "sherter",
    "id": 4977136,
    "avatar_url": "https://avatars.githubusercontent.com/u/4977136?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/sherter",
    "html_url": "https://github.com/sherter",
    "followers_url": "https://api.github.com/users/sherter/followers",
    "following_url": "https://api.github.com/users/sherter/following{/other_user}",
    "gists_url": "https://api.github.com/users/sherter/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/sherter/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/sherter/subscriptions",
    "organizations_url": "https://api.github.com/users/sherter/orgs",
    "repos_url": "https://api.github.com/users/sherter/repos",
    "events_url": "https://api.github.com/users/sherter/events{/privacy}",
    "received_events_url": "https://api.github.com/users/sherter/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-07-06T15:20:59Z",
  "updated_at": "2015-07-06T15:37:26Z",
  "body": "> However, it's generally a good idea to have the methods that accept lists and other collections take List<? extends String>.\r\n\r\nThat's not necessarily a good idea. In fact it's a very bad idea if you want to add more elements to the collection (read [Effective Java, Item 28](https://books.google.de/books?id=ka2VUBqHiWkC&pg=PA134#v=onepage&q&f=false)).\r\n\r\nYour cast in `list2()` is not safe! If you really need the flexibility, you have to do something like this:\r\n\r\n```java\r\npublic final Builder list2(List<? extends String> list) {\r\n  List<String> copy = new ArrayList<>();\r\n  copy.addAll(list);\r\n  return list(copy);\r\n}\r\n```\r\n\r\nThat's not something the library can (or should) do for you, because we cannot know which list type is appropriate for your use case."
}
, {
  "url": "https://api.github.com/repos/google/auto/issues/comments/118890456",
  "html_url": "https://github.com/google/auto/issues/256#issuecomment-118890456",
  "issue_url": "https://api.github.com/repos/google/auto/issues/256",
  "id": 118890456,
  "user": {
    "login": "kenzierocks",
    "id": 2093023,
    "avatar_url": "https://avatars.githubusercontent.com/u/2093023?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/kenzierocks",
    "html_url": "https://github.com/kenzierocks",
    "followers_url": "https://api.github.com/users/kenzierocks/followers",
    "following_url": "https://api.github.com/users/kenzierocks/following{/other_user}",
    "gists_url": "https://api.github.com/users/kenzierocks/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/kenzierocks/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/kenzierocks/subscriptions",
    "organizations_url": "https://api.github.com/users/kenzierocks/orgs",
    "repos_url": "https://api.github.com/users/kenzierocks/repos",
    "events_url": "https://api.github.com/users/kenzierocks/events{/privacy}",
    "received_events_url": "https://api.github.com/users/kenzierocks/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-07-06T15:26:20Z",
  "updated_at": "2015-07-06T15:26:20Z",
  "body": "I think I didn't get my point across correctly. I want the library to accept the `super`/`extends` versions as valid versions of the property, in addition to supporting the immediate generic."
}
, {
  "url": "https://api.github.com/repos/google/auto/issues/comments/118912522",
  "html_url": "https://github.com/google/auto/issues/256#issuecomment-118912522",
  "issue_url": "https://api.github.com/repos/google/auto/issues/256",
  "id": 118912522,
  "user": {
    "login": "eamonnmcmanus",
    "id": 5246810,
    "avatar_url": "https://avatars.githubusercontent.com/u/5246810?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/eamonnmcmanus",
    "html_url": "https://github.com/eamonnmcmanus",
    "followers_url": "https://api.github.com/users/eamonnmcmanus/followers",
    "following_url": "https://api.github.com/users/eamonnmcmanus/following{/other_user}",
    "gists_url": "https://api.github.com/users/eamonnmcmanus/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/eamonnmcmanus/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/eamonnmcmanus/subscriptions",
    "organizations_url": "https://api.github.com/users/eamonnmcmanus/orgs",
    "repos_url": "https://api.github.com/users/eamonnmcmanus/repos",
    "events_url": "https://api.github.com/users/eamonnmcmanus/events{/privacy}",
    "received_events_url": "https://api.github.com/users/eamonnmcmanus/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-07-06T16:13:48Z",
  "updated_at": "2015-07-06T16:13:48Z",
  "body": "Since AutoValue classes are supposed to be immutable, AutoValue properties that are collections should be immutable collections. So we have some special treatment for Guava's immutable types such as [ImmutableList](http://docs.guava-libraries.googlecode.com/git-history/release/javadoc/com/google/common/collect/ImmutableList.html). In particular, if you have a property of type `ImmutableList<Foo>` then its builder can indeed have a setter that takes `ImmutableList<? extends Foo>`. The generated builder will copy the argument value using [`ImmutableList.copyOf`](http://docs.guava-libraries.googlecode.com/git-history/release/javadoc/com/google/common/collect/ImmutableList.html#copyOf(java.lang.Iterable)), which does nothing in this case since the argument is already an `ImmutableList`.\r\n\r\nWe could extend this treatment to arbitrary `List` etc, using code similar to what @sherter wrote above, but then in cases like this we would need to decide that the new `List` is an `ArrayList` rather than any other implementation of `List`, etc, and that is much less obviously the right thing to do.\r\n\r\n@kenzierocks touches on another concern, though, which is that sometimes you would like a setter method in the public API to be able to call the generated setter method for the same property, like `list2` calling `list` in the example. There _is_ currently a way to achieve this, but it is ugly. Builders can use methods that look like either `setFoo(int x)` or `foo(int x)`, as long as all the abstract setter methods follow the same one of these two conventions. So you can have package-private abstract setter methods following the convention you _don't_ want in your public API, and public concrete setter methods that call those. The example would have looked something like this in that case:\r\n```java\r\n@AutoValue\r\npublic abstract class Foo {\r\n  @AutoValue.Builder\r\n  public abstract static class Builder {\r\n    public final Builder list(List<? extends String> list) {\r\n      return setList((List<String>) list);\r\n    }\r\n    abstract Builder setList(List<String> list);\r\n    public abstract Foo build();\r\n  }\r\n  public abstract List<String> list();\r\n}\r\n```\r\nOf course you then need to define both `bar` and `setBar` for every property, even the ones where `bar` just calls `setBar`. We don't really think this is a satisfactory solution, but we're not sure either what a better solution should look like or whether it is worth complicating the spec still further to allow it."
}
, {
  "url": "https://api.github.com/repos/google/auto/issues/comments/118916147",
  "html_url": "https://github.com/google/auto/issues/256#issuecomment-118916147",
  "issue_url": "https://api.github.com/repos/google/auto/issues/256",
  "id": 118916147,
  "user": {
    "login": "kenzierocks",
    "id": 2093023,
    "avatar_url": "https://avatars.githubusercontent.com/u/2093023?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/kenzierocks",
    "html_url": "https://github.com/kenzierocks",
    "followers_url": "https://api.github.com/users/kenzierocks/followers",
    "following_url": "https://api.github.com/users/kenzierocks/following{/other_user}",
    "gists_url": "https://api.github.com/users/kenzierocks/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/kenzierocks/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/kenzierocks/subscriptions",
    "organizations_url": "https://api.github.com/users/kenzierocks/orgs",
    "repos_url": "https://api.github.com/users/kenzierocks/repos",
    "events_url": "https://api.github.com/users/kenzierocks/events{/privacy}",
    "received_events_url": "https://api.github.com/users/kenzierocks/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-07-06T16:22:44Z",
  "updated_at": "2015-07-06T16:22:44Z",
  "body": "The ImmutableList may cover my use case, I didn't realize it would work with that."
}
]
