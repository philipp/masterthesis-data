[
{
  "url": "https://api.github.com/repos/google/vimdoc/issues/comments/68433406",
  "html_url": "https://github.com/google/vimdoc/issues/81#issuecomment-68433406",
  "issue_url": "https://api.github.com/repos/google/vimdoc/issues/81",
  "id": 68433406,
  "user": {
    "login": "malcolmr",
    "id": 7025783,
    "avatar_url": "https://avatars.githubusercontent.com/u/7025783?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/malcolmr",
    "html_url": "https://github.com/malcolmr",
    "followers_url": "https://api.github.com/users/malcolmr/followers",
    "following_url": "https://api.github.com/users/malcolmr/following{/other_user}",
    "gists_url": "https://api.github.com/users/malcolmr/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/malcolmr/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/malcolmr/subscriptions",
    "organizations_url": "https://api.github.com/users/malcolmr/orgs",
    "repos_url": "https://api.github.com/users/malcolmr/repos",
    "events_url": "https://api.github.com/users/malcolmr/events{/privacy}",
    "received_events_url": "https://api.github.com/users/malcolmr/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2014-12-31T10:05:49Z",
  "updated_at": "2014-12-31T10:05:49Z",
  "body": "I actually wrote a very simple version of this (`throws` propagation with call-graph analysis) for google/vim-maktaba#110 (see corp CL 83011828 for generators and output). Some examples of the problems I ran into:\r\n\r\n- `maktaba#error#InvalidArguments()` calls `maktaba#error#Exception()`, which will throw `WrongType` or `BadValue` if the exception name is invalid. However, `maktaba#error#InvalidArguments()` uses a constant string literal, and so those are impossible.\r\n- `autoload/plugin.vim`'s `s:ApplySettings()` calls `maktaba#error#Split()`, which will throw `BadValue` if the exception is of the wrong type. However, `s:ApplySettings()` only catches certain types of exception in the first place, so this is impossible.\r\n- Similarly, `maktaba#command#GetOutput()` calls `maktaba#ensure#IsTrue()` with something that's known to be a boolean value, making the `WrongType` exception impossible. More interestingly, though, is that `maktaba#ensure#IsTrue()` will raise `Failure` if the condition is not met, and yet in this case it _doesn't_ make sense for `maktaba#command#GetOutput()` to propagate `Failure` to the callers, because, while it's possible that it'll be thrown, it's being used to check an assertion about Vim's behaviour itself, and so callers shouldn't try to catch it (or even really document it).\r\n- Finally, `maktaba#flags#Callback()` calls `maktaba#function#Apply()`, which can throw `BadValue` and `WrongType` if the argument isn't a funcdict; however, the collection that `maktaba#flags#Callback()` iterates was populated by `maktaba#flags#AddCallback()`, which has already performed this validation.\r\n\r\nIn my implementation, I avoided this by manually annotating callgraph edges to remove exceptions that I verified were handled or impossible (i.e. \"The call edge from `maktaba#error#InvalidArguments()` to `maktaba#error#Exception()` should not propagate `WrongType` or `BadValue`\".) This seemed to be good enough for that use case.\r\n\r\nAnother thing to note on the original callgraph production is that Vimscript is hard to do static analysis on: things like funcdict calls are tricky to resolve, of course (and I didn't even attempt to do so), but also constructs like `call map(l:items, 'maktaba#list#RemoveAll(l:Target, v:val)')`, where the function name is in a string.\r\n\r\nI avoided this by adopting a very simple approach: I extracted all the function names (the strings that appear after `function!` — and I was lucky that in Maktaba's case the script-local function names are also unique), and then simply looked for occurrences of those strings (as \"`\\b<FUNCTION NAME>\\b`\") in the non-comment text of a calling function. However, this did cause some problems, so I did have to do some manual editing to the resulting graph: for example, `maktaba#flags#Create()` doesn't actually call any methods, it just sets up a funcdict with references to them.\r\n\r\nFor vimdoc, being able to warn (and also suppress warnings for) _direct_ undocumented `throws` statements certainly sounds reasonable, but given the above, I'd suggest that building a general callgraph might be too much work (for both vimdoc and for the user running it) just for this feature."
}
, {
  "url": "https://api.github.com/repos/google/vimdoc/issues/comments/68493481",
  "html_url": "https://github.com/google/vimdoc/issues/81#issuecomment-68493481",
  "issue_url": "https://api.github.com/repos/google/vimdoc/issues/81",
  "id": 68493481,
  "user": {
    "login": "dbarnett",
    "id": 65244,
    "avatar_url": "https://avatars.githubusercontent.com/u/65244?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/dbarnett",
    "html_url": "https://github.com/dbarnett",
    "followers_url": "https://api.github.com/users/dbarnett/followers",
    "following_url": "https://api.github.com/users/dbarnett/following{/other_user}",
    "gists_url": "https://api.github.com/users/dbarnett/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/dbarnett/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/dbarnett/subscriptions",
    "organizations_url": "https://api.github.com/users/dbarnett/orgs",
    "repos_url": "https://api.github.com/users/dbarnett/repos",
    "events_url": "https://api.github.com/users/dbarnett/events{/privacy}",
    "received_events_url": "https://api.github.com/users/dbarnett/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-01-01T18:11:40Z",
  "updated_at": "2015-01-01T18:11:40Z",
  "body": "Wow, I knew it would be tricky, but hadn't considered some of these bright red gotchas.\r\n\r\nParticularly the cases where we already know an arg value is safe, to always avoid false positives we'd need a theorem prover and a complete programmatic model of the entire vimscript implementation (e.g., to verify that a literal string value passes a certain regex used in the implementation).\r\n\r\nAnother possibility is to have a standalone vimdoc lint command to detect possible issues. But probably nobody would use it besides (occasionally) us.\r\n\r\nAt any rate, could you attach your script here for the curious?"
}
]
