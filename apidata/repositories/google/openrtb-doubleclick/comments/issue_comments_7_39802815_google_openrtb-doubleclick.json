[
{
  "url": "https://api.github.com/repos/google/openrtb-doubleclick/issues/comments/51650579",
  "html_url": "https://github.com/google/openrtb-doubleclick/issues/7#issuecomment-51650579",
  "issue_url": "https://api.github.com/repos/google/openrtb-doubleclick/issues/7",
  "id": 51650579,
  "user": {
    "login": "opinali",
    "id": 3017776,
    "avatar_url": "https://avatars.githubusercontent.com/u/3017776?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/opinali",
    "html_url": "https://github.com/opinali",
    "followers_url": "https://api.github.com/users/opinali/followers",
    "following_url": "https://api.github.com/users/opinali/following{/other_user}",
    "gists_url": "https://api.github.com/users/opinali/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/opinali/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/opinali/subscriptions",
    "organizations_url": "https://api.github.com/users/opinali/orgs",
    "repos_url": "https://api.github.com/users/opinali/repos",
    "events_url": "https://api.github.com/users/opinali/events{/privacy}",
    "received_events_url": "https://api.github.com/users/opinali/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2014-08-08T20:02:27Z",
  "updated_at": "2014-09-30T15:36:23Z",
  "body": "I have investigated this, some findings. The region names are available; for this specific example, you will find the following hierarchy of records:\r\n\r\n1011969,\"Moscow\",\"Moscow,Moscow,Russia\",\"20950,2643\",\"\",\"RU\",\"City\"\r\n20950,\"Moscow\",\"Moscow,Russia\",\"2643\",\"RU-MOW\",\"RU\",\"Region\"\r\n2643,\"Russia\",\"Russia\",\"\",\"\",\"RU\",\"Country\"\r\n\r\nThis seems correct: you have Moscow > Moscow (RU-MOW) > Russia (RU). I suppose the confusion is the fact that all records have the region code field, but this field is empty for cities, it's only populated for countries and regions (and other region-like divisions like territories, districts, etc.).  If you want to target by the RU-MOW, you need to build an index of all these records and follow the child->parent relationships until you find the region. Notice that according to DoubleClick's docs, the list of Parent Criteria IDs is not a reliable way to do that (it's deprecated and may have errors and ambiguities); what you need to do is parse the Canonical Name column, in this case the city's canonical name is \"Moscow,Moscow,Russia\", so you need to discard the prefix \"Moscow,\" that matches the Name field, and use the tailing string \"Moscow,Russia\" to locate the parent record. (You cannot CSV-split the string because many names have internal commas.)\r\n\r\nIf this looks boring, the `DoubleClickMetadata` class in the doubleclick-core library does all that work already and makes lookup easy and instantaneous, so you're lucky at least if your bidder is Java (if not, the code is a good practical documentation about how to handle all these DoubleClick datasets).\r\n\r\nOn the city name, you are correct that the official romanization and preferred name is \"Moskva\", but \"Moscow\" is also listed in the UN/LOCODE table as an alias; here's the records in their CSV distribution: \r\n\r\n\"=\",\"RU\",\"\",\"Moscow = Moskva\",\"Moscow = Moskva\",\"\",,\"\",,\"\",\"\",\"\"\r\n\r\nThese records with '=' in the first field are \"reference entries\", they allow defining alternative names for the same location. I'm not familiar with this standard but looking at other examples (e.g. Lisbon = Lisboa, Bucharest = Bucuresti, etc.) it seems this is used to provide the \"preferred English name\" for locations which native/roman name is not the most used internationally. DoubleClick's dataset always uses these preferred English names, so I think the easiest way to handle this would be having some code that loads the full UN/CEFACT data (or at least the aliases which are not many, only 86 records!) and maps between the English name and the primary/native name.\r\n\r\nAnd for London we have this:\r\n\r\n1006886,\"London\",\"London,England,United Kingdom\",\"9041106,9047013,20339,2826\",\"\",\"GB\",\"City\"\r\n9041106,\"Greater London\",\"Greater London,England,United Kingdom\",\"20339,2826\",\"\",\"GB\",\"County\"\r\n9047013,\"London\",\"London TV Region,England,United Kingdom\",\"20339,2826\",\"\",\"GB\",\"TV Region\"\r\n20339,\"England\",\"England,United Kingdom\",\"2826\",\"GB-ENG\",\"GB\",\"Province\"\r\n2826,\"United Kingdom\",\"United Kingdom\",\"\",\"\",\"GB\",\"Country\"\r\n\r\nThis shows another reason to not use the Parent Criteria IDs: some records have multiple sibling parents, it's not a simple tree. But if you follow only the Canonical Names, the secondary path through the \"London TV Region\" above disappears, you have London > Greater London > England (GB-ENG) > United Kingdom (GB). Now on the different region codes: GB-LON is indeed the code for London, but GB-ENG is England. But the latter is not found in the latest ISO files, my reference is https://en.wikipedia.org/wiki/ISO_3166-2:GB which points to http://www.iso.org/iso/iso_3166-2_newsletter_ii-3_2011-12-13.pdf. Actually, the ISO files have a GB-ENG but it's the Englefield Green city in Surrey, England. it seems there's something wrong or obsolete, perhaps both the DoubleClick dataset and the Wikipedia article are outdated (the referred PDF is from 2011)? Not something that I know particularly well so this would need some further investigation..."
}
, {
  "url": "https://api.github.com/repos/google/openrtb-doubleclick/issues/comments/52136905",
  "html_url": "https://github.com/google/openrtb-doubleclick/issues/7#issuecomment-52136905",
  "issue_url": "https://api.github.com/repos/google/openrtb-doubleclick/issues/7",
  "id": 52136905,
  "user": {
    "login": "eugen-yakovets",
    "id": 1287790,
    "avatar_url": "https://avatars.githubusercontent.com/u/1287790?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/eugen-yakovets",
    "html_url": "https://github.com/eugen-yakovets",
    "followers_url": "https://api.github.com/users/eugen-yakovets/followers",
    "following_url": "https://api.github.com/users/eugen-yakovets/following{/other_user}",
    "gists_url": "https://api.github.com/users/eugen-yakovets/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/eugen-yakovets/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/eugen-yakovets/subscriptions",
    "organizations_url": "https://api.github.com/users/eugen-yakovets/orgs",
    "repos_url": "https://api.github.com/users/eugen-yakovets/repos",
    "events_url": "https://api.github.com/users/eugen-yakovets/events{/privacy}",
    "received_events_url": "https://api.github.com/users/eugen-yakovets/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2014-08-14T02:28:10Z",
  "updated_at": "2014-08-14T02:28:10Z",
  "body": "It is indeed not easy to map cities and region for GB between OpenRTB and doubleclick.\r\nOne geo point can have several subdivisions according to ISO_3166-2 (https://en.wikipedia.org/wiki/ISO_3166-2:GB). For instance 'Winchester' city is located in 'Hampshire' county and 'England' country. https://en.wikipedia.org/wiki/Winchester\r\nOpenRTB specification have only one field for region and this create a space for different interpretations and confusion.\r\n\r\nIn doubleclick data there is geo criterias for:\r\n - countries/province (England,Northern Ireland,Scotland,Wales)\r\n - two-tier county (subdivisions of England\t)\r\n\r\nThere is no subdivisions for Northern Ireland,Scotland and Wales. So if need to get ISO_3166-2 region from doubleclick data the only good chose is to use countries/province as regions. This is pretty big regions and not likely other RTB exchanges will same approach. So in pactice it appear that you can't do one-to-one matching for GB regions.\r\nThis makes city matching a somewhat tricky - there could be two cities with exact same name, and when you don't have regions you can mix them up."
}
, {
  "url": "https://api.github.com/repos/google/openrtb-doubleclick/issues/comments/52384975",
  "html_url": "https://github.com/google/openrtb-doubleclick/issues/7#issuecomment-52384975",
  "issue_url": "https://api.github.com/repos/google/openrtb-doubleclick/issues/7",
  "id": 52384975,
  "user": {
    "login": "aprotsenko",
    "id": 8168281,
    "avatar_url": "https://avatars.githubusercontent.com/u/8168281?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/aprotsenko",
    "html_url": "https://github.com/aprotsenko",
    "followers_url": "https://api.github.com/users/aprotsenko/followers",
    "following_url": "https://api.github.com/users/aprotsenko/following{/other_user}",
    "gists_url": "https://api.github.com/users/aprotsenko/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/aprotsenko/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/aprotsenko/subscriptions",
    "organizations_url": "https://api.github.com/users/aprotsenko/orgs",
    "repos_url": "https://api.github.com/users/aprotsenko/repos",
    "events_url": "https://api.github.com/users/aprotsenko/events{/privacy}",
    "received_events_url": "https://api.github.com/users/aprotsenko/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2014-08-16T05:55:56Z",
  "updated_at": "2014-08-16T05:55:56Z",
  "body": "@opinali I appreciate you for your investigation.\r\n@eugen-yakovets thank you for the comment.\r\n\r\nIt is all clear with Moscow, but still there are issues with some other Russian cities. One of they is Yoshkar-Ola which is recorded as Joshkar-Ola in the UN/LOCODE list. There is a bundle of such inconsistencies. Yes, they are minor and we might handle they. And probably the “GB Issue” might be solved. However we sure there are other inconsistencies, which we haven't caught yet.\r\n\r\nSo far we made a decision to separate DoubleClick geo targeting from geo targeting of other Exchanges in our product. At least this decision ensures accuracy of geo targeting."
}
, {
  "url": "https://api.github.com/repos/google/openrtb-doubleclick/issues/comments/52404286",
  "html_url": "https://github.com/google/openrtb-doubleclick/issues/7#issuecomment-52404286",
  "issue_url": "https://api.github.com/repos/google/openrtb-doubleclick/issues/7",
  "id": 52404286,
  "user": {
    "login": "opinali",
    "id": 3017776,
    "avatar_url": "https://avatars.githubusercontent.com/u/3017776?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/opinali",
    "html_url": "https://github.com/opinali",
    "followers_url": "https://api.github.com/users/opinali/followers",
    "following_url": "https://api.github.com/users/opinali/following{/other_user}",
    "gists_url": "https://api.github.com/users/opinali/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/opinali/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/opinali/subscriptions",
    "organizations_url": "https://api.github.com/users/opinali/orgs",
    "repos_url": "https://api.github.com/users/opinali/repos",
    "events_url": "https://api.github.com/users/opinali/events{/privacy}",
    "received_events_url": "https://api.github.com/users/opinali/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2014-08-16T20:01:43Z",
  "updated_at": "2014-08-16T20:01:43Z",
  "body": "@eugen-yakovets @aprotsenko You are correct that the DoubleClick geotargeting data is not as complete as it could be... even in the US where the map seems to be more \"organized\" you can find towns with the same name in the same state, but you won't find any of that in the geo-table.csv, we just put a single town in these cases. The solution for this is usually using the zipcode, but then the problem is that you can have many zipcodes per city. You can use the zipcode prefix (three first digits) which gives you regions that are bigger than cities, but not exactly the same as counties, so this may or may not be useful for targeting and it would be US-specific anyway. For real good per-city targeting you need to load in your bidder the full zipcode database and then it's simple to find cities precisely. You can find the data for all countries in GeoNames.org, would that be a good solution? (Unfortunately not planning to add this to the openrtb-doubleclick project, which scope is set to implementing OpenRTB & DoubleClick protocols and datasets but not external things like GeoNames.)"
}
, {
  "url": "https://api.github.com/repos/google/openrtb-doubleclick/issues/comments/52482777",
  "html_url": "https://github.com/google/openrtb-doubleclick/issues/7#issuecomment-52482777",
  "issue_url": "https://api.github.com/repos/google/openrtb-doubleclick/issues/7",
  "id": 52482777,
  "user": {
    "login": "aprotsenko",
    "id": 8168281,
    "avatar_url": "https://avatars.githubusercontent.com/u/8168281?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/aprotsenko",
    "html_url": "https://github.com/aprotsenko",
    "followers_url": "https://api.github.com/users/aprotsenko/followers",
    "following_url": "https://api.github.com/users/aprotsenko/following{/other_user}",
    "gists_url": "https://api.github.com/users/aprotsenko/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/aprotsenko/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/aprotsenko/subscriptions",
    "organizations_url": "https://api.github.com/users/aprotsenko/orgs",
    "repos_url": "https://api.github.com/users/aprotsenko/repos",
    "events_url": "https://api.github.com/users/aprotsenko/events{/privacy}",
    "received_events_url": "https://api.github.com/users/aprotsenko/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2014-08-18T11:59:58Z",
  "updated_at": "2014-08-18T11:59:58Z",
  "body": "Probably use of postal codes is a solution. I'll ask our account manager if field 'postal-code' of BidRequest is mostly filled for Russia and other countries.\r\n\r\nThank you, @opinali."
}
, {
  "url": "https://api.github.com/repos/google/openrtb-doubleclick/issues/comments/52968970",
  "html_url": "https://github.com/google/openrtb-doubleclick/issues/7#issuecomment-52968970",
  "issue_url": "https://api.github.com/repos/google/openrtb-doubleclick/issues/7",
  "id": 52968970,
  "user": {
    "login": "eugen-yakovets",
    "id": 1287790,
    "avatar_url": "https://avatars.githubusercontent.com/u/1287790?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/eugen-yakovets",
    "html_url": "https://github.com/eugen-yakovets",
    "followers_url": "https://api.github.com/users/eugen-yakovets/followers",
    "following_url": "https://api.github.com/users/eugen-yakovets/following{/other_user}",
    "gists_url": "https://api.github.com/users/eugen-yakovets/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/eugen-yakovets/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/eugen-yakovets/subscriptions",
    "organizations_url": "https://api.github.com/users/eugen-yakovets/orgs",
    "repos_url": "https://api.github.com/users/eugen-yakovets/repos",
    "events_url": "https://api.github.com/users/eugen-yakovets/events{/privacy}",
    "received_events_url": "https://api.github.com/users/eugen-yakovets/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2014-08-21T19:21:37Z",
  "updated_at": "2014-08-21T19:21:37Z",
  "body": "thanks for suggestions @aprotsenko @opinali \r\nYes, using postal codes for city targeting is an interesting solution. \r\n\r\nThis add additional level of indirection and I can imagine that such solution have own issue, for instance with postal codes that belong to two or more cities or even several states. \r\n\r\nLooks like precise targeting is going to be tough anyway. I'll check GeoNames.org data and come back if found any another solution. \r\n\r\nAny chances doubleclick will send ISO-3166 geo in bid requests?"
}
, {
  "url": "https://api.github.com/repos/google/openrtb-doubleclick/issues/comments/57333996",
  "html_url": "https://github.com/google/openrtb-doubleclick/issues/7#issuecomment-57333996",
  "issue_url": "https://api.github.com/repos/google/openrtb-doubleclick/issues/7",
  "id": 57333996,
  "user": {
    "login": "opinali",
    "id": 3017776,
    "avatar_url": "https://avatars.githubusercontent.com/u/3017776?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/opinali",
    "html_url": "https://github.com/opinali",
    "followers_url": "https://api.github.com/users/opinali/followers",
    "following_url": "https://api.github.com/users/opinali/following{/other_user}",
    "gists_url": "https://api.github.com/users/opinali/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/opinali/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/opinali/subscriptions",
    "organizations_url": "https://api.github.com/users/opinali/orgs",
    "repos_url": "https://api.github.com/users/opinali/repos",
    "events_url": "https://api.github.com/users/opinali/events{/privacy}",
    "received_events_url": "https://api.github.com/users/opinali/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2014-09-30T15:40:58Z",
  "updated_at": "2014-09-30T15:40:58Z",
  "body": "@eugen-yakovets (reviewing this after some time, noticed your question...) No plans for geo changes like you suggest, I'm afraid. We're adding improvements like Hyperlocals, but the existing fields are not likely to change."
}
]
