[
{
  "url": "https://api.github.com/repos/google/vroom/issues/comments/48078803",
  "html_url": "https://github.com/google/vroom/issues/38#issuecomment-48078803",
  "issue_url": "https://api.github.com/repos/google/vroom/issues/38",
  "id": 48078803,
  "user": {
    "login": "dbarnett",
    "id": 65244,
    "avatar_url": "https://avatars.githubusercontent.com/u/65244?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/dbarnett",
    "html_url": "https://github.com/dbarnett",
    "followers_url": "https://api.github.com/users/dbarnett/followers",
    "following_url": "https://api.github.com/users/dbarnett/following{/other_user}",
    "gists_url": "https://api.github.com/users/dbarnett/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/dbarnett/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/dbarnett/subscriptions",
    "organizations_url": "https://api.github.com/users/dbarnett/orgs",
    "repos_url": "https://api.github.com/users/dbarnett/repos",
    "events_url": "https://api.github.com/users/dbarnett/events{/privacy}",
    "received_events_url": "https://api.github.com/users/dbarnett/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2014-07-05T05:30:13Z",
  "updated_at": "2014-07-05T05:30:13Z",
  "body": "As far as portability, it's probably just as simple to use plain python and provide a common python vim interface instead of creating a DSL. You can even do something like this already with the `:python` command and the built-in vim module (although I noticed AssertionErrors aren't being handled properly: #39)."
}
, {
  "url": "https://api.github.com/repos/google/vroom/issues/comments/48086502",
  "html_url": "https://github.com/google/vroom/issues/38#issuecomment-48086502",
  "issue_url": "https://api.github.com/repos/google/vroom/issues/38",
  "id": 48086502,
  "user": {
    "login": "tarruda",
    "id": 842846,
    "avatar_url": "https://avatars.githubusercontent.com/u/842846?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/tarruda",
    "html_url": "https://github.com/tarruda",
    "followers_url": "https://api.github.com/users/tarruda/followers",
    "following_url": "https://api.github.com/users/tarruda/following{/other_user}",
    "gists_url": "https://api.github.com/users/tarruda/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/tarruda/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/tarruda/subscriptions",
    "organizations_url": "https://api.github.com/users/tarruda/orgs",
    "repos_url": "https://api.github.com/users/tarruda/repos",
    "events_url": "https://api.github.com/users/tarruda/events{/privacy}",
    "received_events_url": "https://api.github.com/users/tarruda/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2014-07-05T13:29:56Z",
  "updated_at": "2014-07-05T13:29:56Z",
  "body": "> As far as portability, it's probably just as simple to use plain python and provide a common python vim interface instead of creating a DSL. You can even do something like this already with the :python command and the built-in vim module (although I noticed AssertionErrors aren't being handled properly: #39).\r\n\r\nIf you don't mind, I'd like to give a shot at implementing this DSL for vroom. Even though embedding python is a simpler solution, it would be great to have a DSL that understands statements like `line2.foreground should be 'red' from 8 to 12 `. This also has the advantage of keeping vroom specs language-agnostic"
}
, {
  "url": "https://api.github.com/repos/google/vroom/issues/comments/48097364",
  "html_url": "https://github.com/google/vroom/issues/38#issuecomment-48097364",
  "issue_url": "https://api.github.com/repos/google/vroom/issues/38",
  "id": 48097364,
  "user": {
    "login": "dbarnett",
    "id": 65244,
    "avatar_url": "https://avatars.githubusercontent.com/u/65244?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/dbarnett",
    "html_url": "https://github.com/dbarnett",
    "followers_url": "https://api.github.com/users/dbarnett/followers",
    "following_url": "https://api.github.com/users/dbarnett/following{/other_user}",
    "gists_url": "https://api.github.com/users/dbarnett/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/dbarnett/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/dbarnett/subscriptions",
    "organizations_url": "https://api.github.com/users/dbarnett/orgs",
    "repos_url": "https://api.github.com/users/dbarnett/repos",
    "events_url": "https://api.github.com/users/dbarnett/events{/privacy}",
    "received_events_url": "https://api.github.com/users/dbarnett/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2014-07-05T21:51:11Z",
  "updated_at": "2014-07-05T21:51:11Z",
  "body": "Sure, let's get a prototype going and see how it feels.\r\n\r\nMy biggest concern is just the complexity of having too many different ways to do things in vroom. That would depend on the scope of the DSL, how much documentation it would want, and how much overlap it has with other approaches.\r\n\r\nStrong selling points might be if:\r\n * It's way more concise/expressive but conceptually simple\r\n * It unlocks completely new power or flexibility in vroom\r\n\r\nFor instance, we've discussed different approaches to making assertions (#18). If this just replaced existing approaches as the de facto way to write assertions (maybe with a single-line syntax instead of block syntax), that might work nicely. You might have more high-level information about what's being asserted and be able to give better failure messages.\r\n\r\nA few random suggestions:\r\n * I might lean towards not allowing vroom comments inside these blocks, as that makes it feel more like a heavyweight \"mode switch\" instead of a lightweight block syntax.\r\n * If there are lots of special variables besides \"screen\", I might suggest still attaching them to a single special namespace or having some mnemonic so the data flow is more obvious.\r\n * We have syntax highlighting implemented in https://github.com/google/vim-ft.vroom (not perfect, but pretty decent). This DSL sounds like it would entail changes there (if nothing more than disabling all highlighting within these blocks). Just be aware that could get ugly for people using the syntax highlighting."
}
]
