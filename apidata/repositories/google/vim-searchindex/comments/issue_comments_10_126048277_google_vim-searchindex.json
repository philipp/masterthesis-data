[
{
  "url": "https://api.github.com/repos/google/vim-searchindex/issues/comments/170724828",
  "html_url": "https://github.com/google/vim-searchindex/issues/10#issuecomment-170724828",
  "issue_url": "https://api.github.com/repos/google/vim-searchindex/issues/10",
  "id": 170724828,
  "user": {
    "login": "rburny",
    "id": 7561808,
    "avatar_url": "https://avatars.githubusercontent.com/u/7561808?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/rburny",
    "html_url": "https://github.com/rburny",
    "followers_url": "https://api.github.com/users/rburny/followers",
    "following_url": "https://api.github.com/users/rburny/following{/other_user}",
    "gists_url": "https://api.github.com/users/rburny/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/rburny/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/rburny/subscriptions",
    "organizations_url": "https://api.github.com/users/rburny/orgs",
    "repos_url": "https://api.github.com/users/rburny/repos",
    "events_url": "https://api.github.com/users/rburny/events{/privacy}",
    "received_events_url": "https://api.github.com/users/rburny/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2016-01-11T22:59:39Z",
  "updated_at": "2016-01-11T22:59:39Z",
  "body": "I've looked into that and it seems like there's no way to get `[4/4]` result. The problem is that `vim-searchindex` uses `:%s/pattern//n` Ex command internally, and this command does not provide appropriate option. But this is also the only search command that works efficiently on large line ranges, so it cannot be replaced.\r\n\r\nI can however implement a partial fix, which will at least make numbers self-consistent. I could make all match counts work as if `cpoptions` contained `c`. This means that search counts will be inconsistent with cursor jumps, displaying `[2/2]` in your case - which still makes more sense than `[4/2]`.\r\n\r\nWhat do you think?  Sorry that there's no better fix, but Vim's searching functions are a mess, and much of complexity in `vim-searchindex` is working around their quirks. "
}
, {
  "url": "https://api.github.com/repos/google/vim-searchindex/issues/comments/170927381",
  "html_url": "https://github.com/google/vim-searchindex/issues/10#issuecomment-170927381",
  "issue_url": "https://api.github.com/repos/google/vim-searchindex/issues/10",
  "id": 170927381,
  "user": {
    "login": "JoshMermel",
    "id": 1416467,
    "avatar_url": "https://avatars.githubusercontent.com/u/1416467?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/JoshMermel",
    "html_url": "https://github.com/JoshMermel",
    "followers_url": "https://api.github.com/users/JoshMermel/followers",
    "following_url": "https://api.github.com/users/JoshMermel/following{/other_user}",
    "gists_url": "https://api.github.com/users/JoshMermel/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/JoshMermel/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/JoshMermel/subscriptions",
    "organizations_url": "https://api.github.com/users/JoshMermel/orgs",
    "repos_url": "https://api.github.com/users/JoshMermel/repos",
    "events_url": "https://api.github.com/users/JoshMermel/events{/privacy}",
    "received_events_url": "https://api.github.com/users/JoshMermel/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2016-01-12T14:30:01Z",
  "updated_at": "2016-01-12T14:30:01Z",
  "body": "Hmm, would it be sensible to file an upstream feature request with Ex?\r\n\r\nI slightly prefer `[4/2]` to `[2/2]` as the result of the above instructions. If we used `nnn` instead of `N`, I think it is better to have the first number increment with each press.\r\n\r\nAlso in practice I don't think `cpo-c` has ever made a difference in my Vim usage. I suspect very few users will ever come across this bug unless they contrive a situation to check for it like I did."
}
, {
  "url": "https://api.github.com/repos/google/vim-searchindex/issues/comments/170965144",
  "html_url": "https://github.com/google/vim-searchindex/issues/10#issuecomment-170965144",
  "issue_url": "https://api.github.com/repos/google/vim-searchindex/issues/10",
  "id": 170965144,
  "user": {
    "login": "rburny",
    "id": 7561808,
    "avatar_url": "https://avatars.githubusercontent.com/u/7561808?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/rburny",
    "html_url": "https://github.com/rburny",
    "followers_url": "https://api.github.com/users/rburny/followers",
    "following_url": "https://api.github.com/users/rburny/following{/other_user}",
    "gists_url": "https://api.github.com/users/rburny/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/rburny/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/rburny/subscriptions",
    "organizations_url": "https://api.github.com/users/rburny/orgs",
    "repos_url": "https://api.github.com/users/rburny/repos",
    "events_url": "https://api.github.com/users/rburny/events{/privacy}",
    "received_events_url": "https://api.github.com/users/rburny/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2016-01-12T16:28:03Z",
  "updated_at": "2016-01-12T16:29:56Z",
  "body": "Vim is a fragile legacy codebase (still written in pre-ANSI C), and does not have maintainers to implement such feature requests. So, I don't consider changing it a realistic option.\r\n\r\nSince we can't solve this problem, I can show you another fun thing in exchange:\r\n\r\n```\r\nset shortmess+=s\r\n/some_non_existing_text<CR>\r\n```\r\n\r\nNow error message is two-line long, requiring you to press enter. Why does it happen, given that \"shortmess+=s\" is pretty much unrelated? I have no idea, but it's yet another reason not to touch this codebase :) "
}
]
