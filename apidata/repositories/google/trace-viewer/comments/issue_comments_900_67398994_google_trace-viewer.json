[
{
  "url": "https://api.github.com/repos/google/trace-viewer/issues/comments/91292916",
  "html_url": "https://github.com/google/trace-viewer/issues/900#issuecomment-91292916",
  "issue_url": "https://api.github.com/repos/google/trace-viewer/issues/900",
  "id": 91292916,
  "user": {
    "login": "petrcermak",
    "id": 2546601,
    "avatar_url": "https://avatars.githubusercontent.com/u/2546601?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/petrcermak",
    "html_url": "https://github.com/petrcermak",
    "followers_url": "https://api.github.com/users/petrcermak/followers",
    "following_url": "https://api.github.com/users/petrcermak/following{/other_user}",
    "gists_url": "https://api.github.com/users/petrcermak/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/petrcermak/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/petrcermak/subscriptions",
    "organizations_url": "https://api.github.com/users/petrcermak/orgs",
    "repos_url": "https://api.github.com/users/petrcermak/repos",
    "events_url": "https://api.github.com/users/petrcermak/events{/privacy}",
    "received_events_url": "https://api.github.com/users/petrcermak/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-04-09T17:03:55Z",
  "updated_at": "2015-04-09T17:10:50Z",
  "body": "We discussed this with @skyostil and identified two things that we think should be changed:\r\n\r\n1. The ***tracks* currently decide which filtering method to call on the model items** (e.g. <tt>filter.matchSlice(item)</tt>). This is not a good design because a track is essentially just a container that should have as little knowledge about the model items it's displaying as possible. Moreover, some tracks (namely <tt>RectTrack</tt>, <tt>LetterDotTrack</tt>, and <tt>ChartTrack</tt> in the near future) were refactored to display *proxy* items (e.g. a letter dot) instead of  *model* items (e.g. an alert) so they do not have any simple way to determine which matching method to call (because filtering is done on model items).\r\n2. Given that all filtering is done on *model* items, there is **no need to perform filtering by traversing tracks**. Instead, we could simply traverse the **model** itself instead. This could have a performance benefit because the model doesn't contain *duplicates* (there might be multiple tracks showing the same model item). On the other hand, this would have two potential drawbacks:\r\n    1. We wouldn't be able to filter on *non-model things* (e.g. track name) easily if we wanted.\r\n    2. There would be a larger \"gap\" between the search box and the new *console* (#901), which inherently depends on the tracks because of predicates like <tt>hasParent</tt>.\r\n\r\nThe first issue could be addressed as follows:\r\n\r\n- Rather than the track (e.g. <tt>RectTrack</tt>), the **model items** (e.g. <tt>*Slice*</tt>) **should be responsible for calling the correct filtering method** (e.g. <tt>match*Slice*</tt>) themselves. Together with proxy items, this would lead to the following call chain: <tt>*track*.addAllObjectsMatchingFilterToSelection</tt> → [<tt>*proxyItem*.addToSelectionIfMatchingFilter</tt>] → <tt>*modelItem*.addToSelectionIfMatchingFilter</tt> → <tt>filter.matchSlice</tt>. It does add at least one extra layer of indirection, which might have a small performance impact. However, this change (or a similar one) seems inevitable.\r\n\r\nArguably, model items (e.g. <tt>Slice</tt>) *should not be aware of filtering*. Therefore, a cleaner approach would be to define a **visitor pattern** over the model items (e.g. <tt>modelItem.visit(visitor,opt_ arg)</tt> → <tt>visitor.visitSlice(modelItem, opt_arg</tt>), which the filtering mechanism could hook into. The drawbacks of this approach are potential *over-engineering* and worse performance (due to yet another layer of indirection).\r\n\r\nNote that having a visitor pattern over the *whole* model (including containers) would allow us to implement the second change (filtering through model rather than tracks) easily.\r\n\r\n@natduca, @dj2: What are your thoughts on this?"
}
, {
  "url": "https://api.github.com/repos/google/trace-viewer/issues/comments/91403647",
  "html_url": "https://github.com/google/trace-viewer/issues/900#issuecomment-91403647",
  "issue_url": "https://api.github.com/repos/google/trace-viewer/issues/900",
  "id": 91403647,
  "user": {
    "login": "natduca",
    "id": 412396,
    "avatar_url": "https://avatars.githubusercontent.com/u/412396?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/natduca",
    "html_url": "https://github.com/natduca",
    "followers_url": "https://api.github.com/users/natduca/followers",
    "following_url": "https://api.github.com/users/natduca/following{/other_user}",
    "gists_url": "https://api.github.com/users/natduca/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/natduca/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/natduca/subscriptions",
    "organizations_url": "https://api.github.com/users/natduca/orgs",
    "repos_url": "https://api.github.com/users/natduca/repos",
    "events_url": "https://api.github.com/users/natduca/events{/privacy}",
    "received_events_url": "https://api.github.com/users/natduca/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-04-10T02:17:46Z",
  "updated_at": "2015-04-10T02:17:46Z",
  "body": "Suppose we implement a feature where some model items arent visible in the ui? Then you say \"find\".\r\n\r\nIt would be surprising to see the things that are hidden.\r\n\r\nTherefore, we need a way to make finding work for things that are visible.\r\n\r\nBUT!!!! The find feature predates the Viewports eventToContainerMap. If an event is not in the e2cmap then it isn't visible. So we could have the find functionality be on the view, but be driven primarily by an iteration of all model events, checked purely by the e2cmap"
}
, {
  "url": "https://api.github.com/repos/google/trace-viewer/issues/comments/91406284",
  "html_url": "https://github.com/google/trace-viewer/issues/900#issuecomment-91406284",
  "issue_url": "https://api.github.com/repos/google/trace-viewer/issues/900",
  "id": 91406284,
  "user": {
    "login": "natduca",
    "id": 412396,
    "avatar_url": "https://avatars.githubusercontent.com/u/412396?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/natduca",
    "html_url": "https://github.com/natduca",
    "followers_url": "https://api.github.com/users/natduca/followers",
    "following_url": "https://api.github.com/users/natduca/following{/other_user}",
    "gists_url": "https://api.github.com/users/natduca/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/natduca/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/natduca/subscriptions",
    "organizations_url": "https://api.github.com/users/natduca/orgs",
    "repos_url": "https://api.github.com/users/natduca/repos",
    "events_url": "https://api.github.com/users/natduca/events{/privacy}",
    "received_events_url": "https://api.github.com/users/natduca/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-04-10T02:33:30Z",
  "updated_at": "2015-04-10T02:33:30Z",
  "body": "one other note: not every model event has a category filter, so title&cat filter is busted too... mabye it should but category is not on the base event for some unknown reason."
}
, {
  "url": "https://api.github.com/repos/google/trace-viewer/issues/comments/91413107",
  "html_url": "https://github.com/google/trace-viewer/issues/900#issuecomment-91413107",
  "issue_url": "https://api.github.com/repos/google/trace-viewer/issues/900",
  "id": 91413107,
  "user": {
    "login": "natduca",
    "id": 412396,
    "avatar_url": "https://avatars.githubusercontent.com/u/412396?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/natduca",
    "html_url": "https://github.com/natduca",
    "followers_url": "https://api.github.com/users/natduca/followers",
    "following_url": "https://api.github.com/users/natduca/following{/other_user}",
    "gists_url": "https://api.github.com/users/natduca/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/natduca/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/natduca/subscriptions",
    "organizations_url": "https://api.github.com/users/natduca/orgs",
    "repos_url": "https://api.github.com/users/natduca/repos",
    "events_url": "https://api.github.com/users/natduca/events{/privacy}",
    "received_events_url": "https://api.github.com/users/natduca/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-04-10T03:03:32Z",
  "updated_at": "2015-04-10T03:03:32Z",
  "body": "We should totally clean this up here, but this was blocking rolling updated versions into Android and Chrome so Chris and I came up with a not horrifying short term fix that unblocks things. Lets keep chatting about the proper arch here and when we have it I'd be happy to help make it happen."
}
, {
  "url": "https://api.github.com/repos/google/trace-viewer/issues/comments/127689019",
  "html_url": "https://github.com/google/trace-viewer/issues/900#issuecomment-127689019",
  "issue_url": "https://api.github.com/repos/google/trace-viewer/issues/900",
  "id": 127689019,
  "user": {
    "login": "catapult-bot",
    "id": 13457154,
    "avatar_url": "https://avatars.githubusercontent.com/u/13457154?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/catapult-bot",
    "html_url": "https://github.com/catapult-bot",
    "followers_url": "https://api.github.com/users/catapult-bot/followers",
    "following_url": "https://api.github.com/users/catapult-bot/following{/other_user}",
    "gists_url": "https://api.github.com/users/catapult-bot/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/catapult-bot/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/catapult-bot/subscriptions",
    "organizations_url": "https://api.github.com/users/catapult-bot/orgs",
    "repos_url": "https://api.github.com/users/catapult-bot/repos",
    "events_url": "https://api.github.com/users/catapult-bot/events{/privacy}",
    "received_events_url": "https://api.github.com/users/catapult-bot/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-08-04T17:43:41Z",
  "updated_at": "2015-08-04T17:43:41Z",
  "body": "Migrated to https://github.com/catapult-project/catapult/issues/900"
}
]
