[
{
  "url": "https://api.github.com/repos/twitter/commons/issues/comments/63366836",
  "html_url": "https://github.com/twitter/commons/issues/329#issuecomment-63366836",
  "issue_url": "https://api.github.com/repos/twitter/commons/issues/329",
  "id": 63366836,
  "user": {
    "login": "thoward",
    "id": 478164,
    "avatar_url": "https://avatars.githubusercontent.com/u/478164?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/thoward",
    "html_url": "https://github.com/thoward",
    "followers_url": "https://api.github.com/users/thoward/followers",
    "following_url": "https://api.github.com/users/thoward/following{/other_user}",
    "gists_url": "https://api.github.com/users/thoward/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/thoward/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/thoward/subscriptions",
    "organizations_url": "https://api.github.com/users/thoward/orgs",
    "repos_url": "https://api.github.com/users/thoward/repos",
    "events_url": "https://api.github.com/users/thoward/events{/privacy}",
    "received_events_url": "https://api.github.com/users/thoward/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2014-11-17T20:07:09Z",
  "updated_at": "2014-11-17T20:07:09Z",
  "body": "This error continues to crop up more and more regularly. After looking over the code a bit, I think #294 is identifying a slightly different problem, which is more related to deadlocks than outright errors. \r\n\r\n## Fixing the problem for me\r\n\r\nIn my case, I think changing [this phrase in lock.py](https://github.com/twitter/commons/blob/master/src/python/twitter/common/dirutil/lock.py#L49):\r\n```\r\n    if not lock_fd:\r\n      blocking = True\r\n      with open(path, 'r') as fd:\r\n        pid = int(fd.read().strip())\r\n        if onwait:\r\n          blocking = onwait(pid)\r\n```\r\n\r\nto \r\n\r\n```\r\n    if not lock_fd:\r\n      blocking = True\r\n      if onwait:\r\n        with open(path, 'r') as fd:\r\n          pid = int(fd.read().strip())\r\n          blocking = onwait(pid)\r\n```\r\n\r\nwould resolve the error I'm seeing. Basically, the error can be described as:\r\n\r\n1. A acquires a lock, begins to write it's PID\r\n2. B attempts to acquire the lock, but fails.\r\n3. B tries to read PID, in order to call `onwait(pid)`\r\n4. A has not yet finished writing it's PID to the lock file, so B reads empty string, which cannot be converted to integer by `int` function on line 49. Exception is raised.\r\n\r\nMoving the read operation within the `if` block would fix *my* problem, because I'm not passing an `onwait` function in my usage, which means there's no need to read the PID. If you did pass `onwait` you'd still encounter this problem even with these proposed changes. \r\n\r\n## Doing it better\r\n\r\nA more correct implementation would not use `fcntl.flock` at all, but rather fall back to the more generic `fcntl.fcntl` which allows passing the flock struct as a 3rd argument. When called this way, if the lock is already held by another process, the return value is a lock structure that includes the PID of the process that holds the lock. This way, there would be no need to perform read/write ops on the lock file at all. \r\n\r\nThere's a implementation like this in `posixfile.lock` which unpacks the OS-dependent struct layout, and  returns the PID, but it's marked as deprecated in favor of `fcntl.lockf`. `lockf` gives you the return value as a string, which is a struct that you must manually unpack. I'm not sure I see the benefit of this over the implementation in `posixfile`, since that takes care of the unpacking for you anyway.\r\n\r\nSo, for our purposes here, I'd suggest using `posixfile.lock` and returning the PID from that call. It will have far fewer race condition issues (though may have more cross-platform issues). \r\n"
}
]
