[
{
  "url": "https://api.github.com/repos/twitter/twemproxy/issues/comments/21789832",
  "html_url": "https://github.com/twitter/twemproxy/issues/131#issuecomment-21789832",
  "issue_url": "https://api.github.com/repos/twitter/twemproxy/issues/131",
  "id": 21789832,
  "user": {
    "login": "rraptorr",
    "id": 62799,
    "avatar_url": "https://avatars.githubusercontent.com/u/62799?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/rraptorr",
    "html_url": "https://github.com/rraptorr",
    "followers_url": "https://api.github.com/users/rraptorr/followers",
    "following_url": "https://api.github.com/users/rraptorr/following{/other_user}",
    "gists_url": "https://api.github.com/users/rraptorr/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/rraptorr/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/rraptorr/subscriptions",
    "organizations_url": "https://api.github.com/users/rraptorr/orgs",
    "repos_url": "https://api.github.com/users/rraptorr/repos",
    "events_url": "https://api.github.com/users/rraptorr/events{/privacy}",
    "received_events_url": "https://api.github.com/users/rraptorr/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-07-30T13:17:31Z",
  "updated_at": "2013-07-30T13:17:31Z",
  "body": "From my point of view, the main problem that twemproxy solves is handling very large installations that have many servers trying to connect to redis. When your scale grows, let's say above 100 application servers, you quickly start to realize that your redis servers are very busy accepting new connections.\r\nTypical use case is then to launch single twemproxy instance per application server. Due to pipelining twemproxy needs only a single connection to redis, thus greatly reducing both number of connections and connection attempts per second.\r\nObviously the same can be achieved in your application code, but it tends to be not that easy (especially if you try to use pipelining) and sometimes even impossible (PHP being one of the examples, unless you code your own PHP extension in C).\r\n\r\nAdditionally twemproxy provides a way to to get partitioning and some sort of HA (if you use redis as a cache) for free. Again, this can be done in application code.\r\n\r\nAll of those features are for \"free\", you just start twemproxy, point your application to it and it works. But as all \"for free\" it has it's price. Some commands are not supported and probably never will be, for example:\r\n- it is impossible to support select as multiple redis instances can be hidden behind single twemproxy, which one should receive the command? even with a single instance pipelining would quickly cause you commands being sent to a wrong database\r\n- the same goes for most of the maintenance commands, you have multiple instances, which one should CONFIG refer to?\r\n- as for the blocking commands I believe that those are also impossible, due to the pipelining twemproxy needs only one connection per redis instance, but issuing a blocking command would obviously block that connection (you can always do multiple connections, but reducing number of connections is the point of using twemproxy in the first place)\r\n\r\nAs you can seen, you get some benefits, but at the price of some limits. If you cannot pay the price, then simply don't use twemproxy. Implement whatever you need in your application, you will get exactly what you need then."
}
, {
  "url": "https://api.github.com/repos/twitter/twemproxy/issues/comments/21791013",
  "html_url": "https://github.com/twitter/twemproxy/issues/131#issuecomment-21791013",
  "issue_url": "https://api.github.com/repos/twitter/twemproxy/issues/131",
  "id": 21791013,
  "user": {
    "login": "Dynom",
    "id": 39014,
    "avatar_url": "https://avatars.githubusercontent.com/u/39014?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/Dynom",
    "html_url": "https://github.com/Dynom",
    "followers_url": "https://api.github.com/users/Dynom/followers",
    "following_url": "https://api.github.com/users/Dynom/following{/other_user}",
    "gists_url": "https://api.github.com/users/Dynom/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/Dynom/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/Dynom/subscriptions",
    "organizations_url": "https://api.github.com/users/Dynom/orgs",
    "repos_url": "https://api.github.com/users/Dynom/repos",
    "events_url": "https://api.github.com/users/Dynom/events{/privacy}",
    "received_events_url": "https://api.github.com/users/Dynom/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-07-30T13:37:13Z",
  "updated_at": "2013-07-30T13:39:26Z",
  "body": "Hi @rraptorr,\r\n\r\nThanks for the quick response!\r\n\r\nAs I see it, the only problem with the select command, is that there is no way to shard the command to a specific instance. Perhaps a namespaced solution can give TwemProxy a solution in that area. It's just a quick thought.\r\n\r\n```\r\nnamespace 000fff # Sharding on this value\r\n  select 3\r\n  incr foo\r\n  sync\r\n  select 0\r\n  yield get bar # The return value\r\nend # Marking the end of the namespace/transaction (without atomicity guarantees).\r\n```\r\nThis would execute all commands on one instance. Obviously it won't be this trivial and there are probably many cases where this won't work, but this would certainly satisfy some use cases.\r\n\r\nAnyway, for now we need to solve this differently and rely on a purely-application specific implementation and figure out a way to do this properly on large volumes, or reduce complexity of commands and the capabilities Redis has to offer. We don't use Redis as a cache and perform complex sorting and filtering in e.g. Lua. Redis is a fantastic tool for us, due to it's rich feature set.\r\n\r\nThanks again for the quick response."
}
, {
  "url": "https://api.github.com/repos/twitter/twemproxy/issues/comments/27905828",
  "html_url": "https://github.com/twitter/twemproxy/issues/131#issuecomment-27905828",
  "issue_url": "https://api.github.com/repos/twitter/twemproxy/issues/131",
  "id": 27905828,
  "user": {
    "login": "manjuraj",
    "id": 349696,
    "avatar_url": "https://avatars.githubusercontent.com/u/349696?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/manjuraj",
    "html_url": "https://github.com/manjuraj",
    "followers_url": "https://api.github.com/users/manjuraj/followers",
    "following_url": "https://api.github.com/users/manjuraj/following{/other_user}",
    "gists_url": "https://api.github.com/users/manjuraj/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/manjuraj/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/manjuraj/subscriptions",
    "organizations_url": "https://api.github.com/users/manjuraj/orgs",
    "repos_url": "https://api.github.com/users/manjuraj/repos",
    "events_url": "https://api.github.com/users/manjuraj/events{/privacy}",
    "received_events_url": "https://api.github.com/users/manjuraj/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-11-06T19:37:08Z",
  "updated_at": "2013-11-06T19:37:08Z",
  "body": "Thanks @rraptorr for helping @Dynom "
}
]
