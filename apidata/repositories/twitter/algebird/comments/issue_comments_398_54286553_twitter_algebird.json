[
{
  "url": "https://api.github.com/repos/twitter/algebird/issues/comments/69886082",
  "html_url": "https://github.com/twitter/algebird/issues/398#issuecomment-69886082",
  "issue_url": "https://api.github.com/repos/twitter/algebird/issues/398",
  "id": 69886082,
  "user": {
    "login": "miguno",
    "id": 294849,
    "avatar_url": "https://avatars.githubusercontent.com/u/294849?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/miguno",
    "html_url": "https://github.com/miguno",
    "followers_url": "https://api.github.com/users/miguno/followers",
    "following_url": "https://api.github.com/users/miguno/following{/other_user}",
    "gists_url": "https://api.github.com/users/miguno/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/miguno/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/miguno/subscriptions",
    "organizations_url": "https://api.github.com/users/miguno/orgs",
    "repos_url": "https://api.github.com/users/miguno/repos",
    "events_url": "https://api.github.com/users/miguno/events{/privacy}",
    "received_events_url": "https://api.github.com/users/miguno/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-01-14T08:51:40Z",
  "updated_at": "2015-01-14T08:51:40Z",
  "body": "That's a good suggestion.  I have been thinking about the very same as we have been using `CMS[BigInt]` to effectively do `CMS[String]` (you can easily convert BigInt<->String with built-in methods). :-)\r\n\r\n> 1) hash using Murmur to Long, then use the Long universal hash functions.\r\n\r\nHmm.  Why would you want to hash twice, i.e. `hashLong(hashMurmur(s))` to get the desired `Int`?\r\n\r\nMurmur already gives us the desired `Int`, hence doing only `hashMurmur(s)` is faster.  Furthermore, I *think* the Murmur-only approach would provide us with a more uniform distribution of `Int`s then a double-hash approach;  at least in the Murmur-only case we *know* based on prior work (I don't mean #392 necessarily, I mean studies of Murmur3 in general) that the `Int` output is well-formed, whereas we'd have to do our own research/plotting/etc. to understand the impact of the proposed double-hashing.  For instance, `(ax + b) mod p` is said to sort-of [produce predictable patterns](http://stackoverflow.com/a/12120625/1743580) -- to me this sounds like it would lower the quality of the Murmur hashing, and we'd have to do the latter anyways.\r\n\r\nSimilarly, my experience with #392 makes me feel that we should rely on just Murmur3, as the `(ax + b) mod p` hashing resulted in counter-intuitive (read: wrong, IMHO) behavior when hashing `BigInt`s.  And as I said above, we have been using `CMS[BigInt]` to effectively do `CMS[String]`, so I can claim limited practical experience with it. :-)\r\n\r\n> 2) append the a and b we get for the family of hashes and use Murmur on the whole block.\r\n\r\nNot sure I understand -- why would you want to append `a` and/or `b` to the string before hashing?\r\n\r\n> We could also add Array[Byte] while we are at it since we will probably go through that anyway.\r\n\r\nThe `BigInt` hashing is effectively hashing `Array[Byte]` already, so that would be a starting point.  \r\n\r\nHere's a snippet for hashing `BigInt`, `String`, `Array[Byte]`:\r\n\r\n```scala\r\n  implicit object CMSHasherArrayByte extends CMSHasher[Array[Byte]] {\r\n    override def hash(a: Int, b: Int, width: Int)(x: Array[Byte]): Int = {\r\n      val hash: Int = scala.util.hashing.MurmurHash3.arrayHash(x, a)\r\n      val positiveHash = hash & Int.MaxValue\r\n      positiveHash % width\r\n    }\r\n  }\r\n\r\n  implicit object CMSHasherBigInt extends CMSHasher[BigInt] {\r\n    override def hash(a: Int, b: Int, width: Int)(x: BigInt): Int =\r\n        CMSHasherArrayByte.hash(a, b, width)(x.toByteArray)\r\n  }\r\n\r\n  implicit object CMSHasherString extends CMSHasher[String] {\r\n    override def hash(a: Int, b: Int, width: Int)(x: String): Int =\r\n        CMSHasherArrayByte.hash(a, b, width)(x.getBytes(\"UTF-8\"))\r\n  }\r\n```\r\n\r\nGiven my (limited) research into this hashing topic over the past few days I think the snippet above would address the `String` hashing in a simple as well as effective way.\r\n\r\nThe one open question I still have is what the memory and GC impacts of the Murmur-based setup are in practice.  I've only done limited testing so far, though this should change over the next couple of days.\r\n\r\nJust my two cents! :-)\r\n\r\nPS: There is also a section on [hashing strings](http://en.wikipedia.org/wiki/Universal_hashing#Hashing_strings) in the Wikipedia article on [Universal Hashing](http://en.wikipedia.org/wiki/Universal_hashing#Hashing_strings)."
}
, {
  "url": "https://api.github.com/repos/twitter/algebird/issues/comments/69990323",
  "html_url": "https://github.com/twitter/algebird/issues/398#issuecomment-69990323",
  "issue_url": "https://api.github.com/repos/twitter/algebird/issues/398",
  "id": 69990323,
  "user": {
    "login": "johnynek",
    "id": 67958,
    "avatar_url": "https://avatars.githubusercontent.com/u/67958?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/johnynek",
    "html_url": "https://github.com/johnynek",
    "followers_url": "https://api.github.com/users/johnynek/followers",
    "following_url": "https://api.github.com/users/johnynek/following{/other_user}",
    "gists_url": "https://api.github.com/users/johnynek/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/johnynek/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/johnynek/subscriptions",
    "organizations_url": "https://api.github.com/users/johnynek/orgs",
    "repos_url": "https://api.github.com/users/johnynek/repos",
    "events_url": "https://api.github.com/users/johnynek/events{/privacy}",
    "received_events_url": "https://api.github.com/users/johnynek/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-01-14T20:58:15Z",
  "updated_at": "2015-01-14T20:58:15Z",
  "body": "The ones you posted here seem good to me.\r\n\r\nI wonder if we could cleanly make a contramap method on the count-min-sketch monoid so that we could take a function from `L => K` on a `CMS[K]` monoid to produce `CMS[L]` monoid. This might make it easy to work with, say, the `CMS[String]` monoid and use `(_.toString)` as a cheap function to get a quick and dirty CMS."
}
, {
  "url": "https://api.github.com/repos/twitter/algebird/issues/comments/70127820",
  "html_url": "https://github.com/twitter/algebird/issues/398#issuecomment-70127820",
  "issue_url": "https://api.github.com/repos/twitter/algebird/issues/398",
  "id": 70127820,
  "user": {
    "login": "miguno",
    "id": 294849,
    "avatar_url": "https://avatars.githubusercontent.com/u/294849?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/miguno",
    "html_url": "https://github.com/miguno",
    "followers_url": "https://api.github.com/users/miguno/followers",
    "following_url": "https://api.github.com/users/miguno/following{/other_user}",
    "gists_url": "https://api.github.com/users/miguno/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/miguno/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/miguno/subscriptions",
    "organizations_url": "https://api.github.com/users/miguno/orgs",
    "repos_url": "https://api.github.com/users/miguno/repos",
    "events_url": "https://api.github.com/users/miguno/events{/privacy}",
    "received_events_url": "https://api.github.com/users/miguno/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-01-15T17:43:31Z",
  "updated_at": "2015-01-15T17:43:31Z",
  "body": "I like the contramap idea!  I sent a pull request with a first draft."
}
, {
  "url": "https://api.github.com/repos/twitter/algebird/issues/comments/78351371",
  "html_url": "https://github.com/twitter/algebird/issues/398#issuecomment-78351371",
  "issue_url": "https://api.github.com/repos/twitter/algebird/issues/398",
  "id": 78351371,
  "user": {
    "login": "miguno",
    "id": 294849,
    "avatar_url": "https://avatars.githubusercontent.com/u/294849?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/miguno",
    "html_url": "https://github.com/miguno",
    "followers_url": "https://api.github.com/users/miguno/followers",
    "following_url": "https://api.github.com/users/miguno/following{/other_user}",
    "gists_url": "https://api.github.com/users/miguno/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/miguno/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/miguno/subscriptions",
    "organizations_url": "https://api.github.com/users/miguno/orgs",
    "repos_url": "https://api.github.com/users/miguno/repos",
    "events_url": "https://api.github.com/users/miguno/events{/privacy}",
    "received_events_url": "https://api.github.com/users/miguno/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-03-11T19:26:18Z",
  "updated_at": "2015-03-11T19:26:18Z",
  "body": "This ticket can be closed as this functionality is covered in https://github.com/twitter/algebird/pull/399, which will be included in upcoming Algebird 0.10."
}
]
