[
{
  "url": "https://api.github.com/repos/twitter/torch-autograd/issues/comments/171059219",
  "html_url": "https://github.com/twitter/torch-autograd/issues/79#issuecomment-171059219",
  "issue_url": "https://api.github.com/repos/twitter/torch-autograd/issues/79",
  "id": 171059219,
  "user": {
    "login": "alexbw",
    "id": 161935,
    "avatar_url": "https://avatars.githubusercontent.com/u/161935?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/alexbw",
    "html_url": "https://github.com/alexbw",
    "followers_url": "https://api.github.com/users/alexbw/followers",
    "following_url": "https://api.github.com/users/alexbw/following{/other_user}",
    "gists_url": "https://api.github.com/users/alexbw/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/alexbw/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/alexbw/subscriptions",
    "organizations_url": "https://api.github.com/users/alexbw/orgs",
    "repos_url": "https://api.github.com/users/alexbw/repos",
    "events_url": "https://api.github.com/users/alexbw/events{/privacy}",
    "received_events_url": "https://api.github.com/users/alexbw/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2016-01-12T21:22:26Z",
  "updated_at": "2016-01-12T21:22:26Z",
  "body": "Good catch, this is because we don't have `__lt`metamethod defined internally. I'll add this."
}
, {
  "url": "https://api.github.com/repos/twitter/torch-autograd/issues/comments/171434108",
  "html_url": "https://github.com/twitter/torch-autograd/issues/79#issuecomment-171434108",
  "issue_url": "https://api.github.com/repos/twitter/torch-autograd/issues/79",
  "id": 171434108,
  "user": {
    "login": "alexbw",
    "id": 161935,
    "avatar_url": "https://avatars.githubusercontent.com/u/161935?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/alexbw",
    "html_url": "https://github.com/alexbw",
    "followers_url": "https://api.github.com/users/alexbw/followers",
    "following_url": "https://api.github.com/users/alexbw/following{/other_user}",
    "gists_url": "https://api.github.com/users/alexbw/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/alexbw/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/alexbw/subscriptions",
    "organizations_url": "https://api.github.com/users/alexbw/orgs",
    "repos_url": "https://api.github.com/users/alexbw/repos",
    "events_url": "https://api.github.com/users/alexbw/events{/privacy}",
    "received_events_url": "https://api.github.com/users/alexbw/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2016-01-13T21:12:20Z",
  "updated_at": "2016-01-13T22:04:50Z",
  "body": "An update — \r\nWe will not be able to _directly_ support `<,<=,>,>=,==` operations, because Lua requires that the variables on either side of the comparison have the same type. This will never be true when using autograd, because we wrap all values in objects to keep track of how they're used in the forward pass, to automatically infer the backward pass. \r\n\r\nHowever, we can use our own utilities. So, I added `util.le`, `util.lt`, `util.ge`, `util.gt`, `util.eq` so you can make comparisons safely in autograd. See the [new LessThan test](https://github.com/twitter/torch-autograd/blob/b798a5d5bb0a37a9a518923d957a0e51955aa546/test/test.lua#L1338-L1351).\r\n\r\nYou're also keeping track of values in a dynamically-allocated array. This is the next thing we'll have to tackle, because autograd doesn't support assignment to arrays (autograd requires all method calls be pure functions). I'll make a little utility that could help with that."
}
, {
  "url": "https://api.github.com/repos/twitter/torch-autograd/issues/comments/171797605",
  "html_url": "https://github.com/twitter/torch-autograd/issues/79#issuecomment-171797605",
  "issue_url": "https://api.github.com/repos/twitter/torch-autograd/issues/79",
  "id": 171797605,
  "user": {
    "login": "alexbw",
    "id": 161935,
    "avatar_url": "https://avatars.githubusercontent.com/u/161935?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/alexbw",
    "html_url": "https://github.com/alexbw",
    "followers_url": "https://api.github.com/users/alexbw/followers",
    "following_url": "https://api.github.com/users/alexbw/following{/other_user}",
    "gists_url": "https://api.github.com/users/alexbw/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/alexbw/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/alexbw/subscriptions",
    "organizations_url": "https://api.github.com/users/alexbw/orgs",
    "repos_url": "https://api.github.com/users/alexbw/repos",
    "events_url": "https://api.github.com/users/alexbw/events{/privacy}",
    "received_events_url": "https://api.github.com/users/alexbw/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2016-01-14T22:09:48Z",
  "updated_at": "2016-01-14T22:09:48Z",
  "body": "We added support for comparisons, as I mentioned above, and also now support concatenating numbers, using our own `autograd.util.cat` function. It currently only supports catting variables of the same type (e.g. all FloatTensors or all numbers), and will probably stay that way for awhile, unless specific requests come up.\r\n\r\nI modified your example only slightly, so please try this now:\r\n\r\n```lua\r\nt = require 'torch'\r\nlocal grad = require 'autograd'\r\nlocal util = require 'autograd.util'\r\n\r\nparams = {\r\n   a = t.randn(1,1)\r\n}\r\n\r\nf = function(params, x)\r\n   local result = t.sum(x * params.a)\r\n\r\n   -- sample from Bernoulli dist\r\n   local bernoulli = {}\r\n   if util.lt(torch.uniform(), t.sum(params.a)) then\r\n         bernoulli[1] = 1\r\n   end\r\n\r\n   return t.sum(util.cat(bernoulli) * result)\r\nend\r\n\r\ndf = grad(f)\r\nx = t.randn(1,1)\r\nprint(f(params, x))\r\nprint(df(params, x))\r\n```\r\n\r\nBy the way, if you want to use optimized mode (`autograd(f, {optimize=true})`), you will have to pre-generate the random numbers, and put them in the `params` table. Otherwise, the `if` statement will be optimized away entirely (we can't overload, and thus can't track, control flow in optimized mode)."
}
, {
  "url": "https://api.github.com/repos/twitter/torch-autograd/issues/comments/171802077",
  "html_url": "https://github.com/twitter/torch-autograd/issues/79#issuecomment-171802077",
  "issue_url": "https://api.github.com/repos/twitter/torch-autograd/issues/79",
  "id": 171802077,
  "user": {
    "login": "alexbw",
    "id": 161935,
    "avatar_url": "https://avatars.githubusercontent.com/u/161935?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/alexbw",
    "html_url": "https://github.com/alexbw",
    "followers_url": "https://api.github.com/users/alexbw/followers",
    "following_url": "https://api.github.com/users/alexbw/following{/other_user}",
    "gists_url": "https://api.github.com/users/alexbw/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/alexbw/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/alexbw/subscriptions",
    "organizations_url": "https://api.github.com/users/alexbw/orgs",
    "repos_url": "https://api.github.com/users/alexbw/repos",
    "events_url": "https://api.github.com/users/alexbw/events{/privacy}",
    "received_events_url": "https://api.github.com/users/alexbw/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2016-01-14T22:28:13Z",
  "updated_at": "2016-01-14T22:28:13Z",
  "body": "Closing. Reopen if something is wrong."
}
]
