[
{
  "url": "https://api.github.com/repos/twitter/torch-autograd/issues/comments/174864265",
  "html_url": "https://github.com/twitter/torch-autograd/pull/89#issuecomment-174864265",
  "issue_url": "https://api.github.com/repos/twitter/torch-autograd/issues/89",
  "id": 174864265,
  "user": {
    "login": "luketwitter",
    "id": 232884,
    "avatar_url": "https://avatars.githubusercontent.com/u/232884?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/luketwitter",
    "html_url": "https://github.com/luketwitter",
    "followers_url": "https://api.github.com/users/luketwitter/followers",
    "following_url": "https://api.github.com/users/luketwitter/following{/other_user}",
    "gists_url": "https://api.github.com/users/luketwitter/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/luketwitter/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/luketwitter/subscriptions",
    "organizations_url": "https://api.github.com/users/luketwitter/orgs",
    "repos_url": "https://api.github.com/users/luketwitter/repos",
    "events_url": "https://api.github.com/users/luketwitter/events{/privacy}",
    "received_events_url": "https://api.github.com/users/luketwitter/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2016-01-26T07:05:24Z",
  "updated_at": "2016-01-26T07:12:32Z",
  "body": "There were a few more system bugs with grads of grads that I've fixed. I've also had to add a temporary option to disable expression inlining in the code generator, because this example generates nested expressions so deep that it breaks Lua.\r\n\r\nAlso, the test has a few errors, specifically:\r\n\r\n- torch.Tensor() syntax doesn't work yet. Use torch.Tensor.new().\r\n- The training and data sets must be passed as (non-differentiable) params.\r\n- We don't yet support the 4-argument torch.add, or any of the torch.XXX functions that operate in-place. I've written it as: params.W = params.W + grads.W * -metaParams.learningRate\r\n\r\nWith all that fixed, it gets a lot further. Let me know if the values being produced are nonsense or not.\r\n\r\n```\r\n\r\n   ReversibleDynamics = function()\r\n      --This implements a very simple example of the concepts in:\r\n      --Maclaurin, Duvenaud, and Adams. \"Gradient-based Hyperparameter Optimization through Reversible Learning.\"\r\n      --We learn a linear regression model with SGD, and differentiate the test set performance of the model with respect to the learning rate used at train time\r\n      --Ideally this would be used to learn the learning rate using gradient descent. This test is more simple.\r\n      --It just evaluates the gradient of the loss with respect to the learningRate for a few different learning rate values.\r\n      --Also, Maclaurin et al. use SGD with momentum, but this test uses simple SGD.\r\n\r\n      -- define trainable parameters:\r\n      local numFeatures = 10\r\n      local numOutputs = 3\r\n      local numSamples = 250\r\n      local numEpochs = 1\r\n\r\n      --this sets up the true model from which the data is drawn\r\n      local trueParams = {\r\n         W = torch.randn(numFeatures,numOutputs),\r\n         b = torch.randn(numOutputs)\r\n      }\r\n\r\n      --this is used to generate train and test datasets\r\n      local function genDataset()\r\n         local dataset = {}\r\n         for tt = 1,numSamples do\r\n            local x = torch.randn(1,numFeatures)\r\n            local y = x*trueParams.W + trueParams.b\r\n            table.insert(dataset,{x = x,y = y})\r\n         end\r\n         return dataset\r\n      end\r\n      local trainingSet = genDataset()\r\n      local testSet = genDataset()\r\n\r\n\r\n      -- define loss for linear regression model\r\n      local linearRegressionLoss = function(params, x, y)\r\n         local yhat = x*params.W + params.b\r\n         local residual = y - yhat\r\n         local loss = 0.5*torch.sum(torch.cmul(residual,residual))\r\n         return loss\r\n      end\r\n\r\n      -- gradient of loss\r\n      local dLinearRegressionLoss = autograd(linearRegressionLoss)\r\n\r\n\r\n      --we learn the regression parameters using SGD with a fixed step size\r\n      function gradientUpdate(metaParams,params,grads)\r\n         params.W = params.W + grads.W * -metaParams.learningRate\r\n         params.b = params.b + grads.b * -metaParams.learningRate\r\n      end\r\n\r\n      --this initializes the params that are optimized by learning.\r\n      function getInitParams()\r\n         local p =  {\r\n            W = torch.zero(torch.Tensor.new(numFeatures,numOutputs)),\r\n            b = torch.zero(torch.Tensor.new(numOutputs))\r\n         }\r\n         return p\r\n      end\r\n\r\n      --this learns a regression model on the train set and evaluates its squared loss on the test set\r\n      --its behavior depends on the value of learningRate in metaParams\r\n      function learn(metaParams, trainingSet, testSet)\r\n         local params = getInitParams()\r\n         for epoch = 1,numEpochs do\r\n            for _,sample in ipairs(trainingSet) do\r\n               local grads, loss = dLinearRegressionLoss(params, sample.x, sample.y)\r\n               gradientUpdate(metaParams,params,grads)\r\n            end\r\n         end\r\n\r\n         local testLoss = torch.zero(torch.Tensor(1))\r\n         local numSamples = #testSet\r\n         for _,sample in ipairs(testSet) do\r\n            local g, tL = dLinearRegressionLoss(params,sample.x,sample.y)\r\n            testLoss = testLoss + tL\r\n         end\r\n         return testLoss[1]\r\n      end\r\n\r\n      --this is the gradient of the test loss with respect to the learning rate used on the train data\r\n      local dLearn = autograd(learn, { inline = false })\r\n\r\n      --this just evaluates dLearn for a few choices of the learning rate. Eventually, we would like to learn the learningRate with gradient descent.\r\n      for _,lr in ipairs({0.001,0.01,0.1}) do\r\n\r\n         --this just evaluates the 'learn' function directly, in order to demonstrate that this code path works\r\n         local testLoss1 = learn({learningRate = lr}, trainingSet, testSet)\r\n\r\n         --this evaluates the derivative of the 'learn function.' Currently it is failing.\r\n         local testLoss2, dMetaParams = dLearn({learningRate = lr}, trainingSet, testSet)\r\n         print(testLoss2)\r\n         tester:assert(testLoss1 == testLoss2,\"value computed using 'learn' function does not match value computed by 'dLearn'\")\r\n      end\r\n   end\r\n```"
}
, {
  "url": "https://api.github.com/repos/twitter/torch-autograd/issues/comments/175179191",
  "html_url": "https://github.com/twitter/torch-autograd/pull/89#issuecomment-175179191",
  "issue_url": "https://api.github.com/repos/twitter/torch-autograd/issues/89",
  "id": 175179191,
  "user": {
    "login": "davidBelanger",
    "id": 1179562,
    "avatar_url": "https://avatars.githubusercontent.com/u/1179562?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/davidBelanger",
    "html_url": "https://github.com/davidBelanger",
    "followers_url": "https://api.github.com/users/davidBelanger/followers",
    "following_url": "https://api.github.com/users/davidBelanger/following{/other_user}",
    "gists_url": "https://api.github.com/users/davidBelanger/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/davidBelanger/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/davidBelanger/subscriptions",
    "organizations_url": "https://api.github.com/users/davidBelanger/orgs",
    "repos_url": "https://api.github.com/users/davidBelanger/repos",
    "events_url": "https://api.github.com/users/davidBelanger/events{/privacy}",
    "received_events_url": "https://api.github.com/users/davidBelanger/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2016-01-26T18:59:31Z",
  "updated_at": "2016-01-26T19:00:22Z",
  "body": "Thanks for the help. I currently have it set up to closely mirror the paper\r\n<http://arxiv.org/pdf/1502.03492.pdf>. It is working. I can tune my\r\nhyperparameters by directly optimizing for dev set performance.\r\nUnfortunately,  I hit numerical overflow/underflow if I scale things up, as discussed in sections 2.2 and 2.3 of the paper.\r\n\r\nIf you'd like to use the code as an example, I'd be happy to make a PR for\r\nit in an examples directory or something. Unfortunately, it only works for\r\nsmall problems, though. Therefore, I'm closing this PR for now.\r\n\r\n\r\nOn Tue, Jan 26, 2016 at 2:05 AM luketwitter <notifications@github.com>\r\nwrote:\r\n\r\n> There were a few more system bugs with grads of grads that I've fixed.\r\n>\r\n> Also, the test has a few errors, specifically:\r\n>\r\n>    - torch.Tensor() syntax doesn't work yet. Use torch.Tensor.new().\r\n>    - The training and data sets must be passed as (non-differentiable)\r\n>    params.\r\n>    - We don't yet support the 4 argument torch.add, or any of the\r\n>    torch.XXX functions that operate in place. I've written it as: params.W =\r\n>    params.W + grads.W * -metaParams.learningRate\r\n>\r\n> With all that fixed, it gets a lot further. Let me know if the values\r\n> being produced are nonsense or not.\r\n>\r\n>\r\n>    ReversibleDynamics = function()\r\n>       --This implements a very simple example of the concepts in:\r\n>       --Maclaurin, Duvenaud, and Adams. \"Gradient-based Hyperparameter Optimization through Reversible Learning.\"\r\n>       --We learn a linear regression model with SGD, and differentiate the test set performance of the model with respect to the learning rate used at train time\r\n>       --Ideally this would be used to learn the learning rate using gradient descent. This test is more simple.\r\n>       --It just evaluates the gradient of the loss with respect to the learningRate for a few different learning rate values.\r\n>       --Also, Maclaurin et al. use SGD with momentum, but this test uses simple SGD.\r\n>\r\n>       -- define trainable parameters:\r\n>       local numFeatures = 10\r\n>       local numOutputs = 3\r\n>       local numSamples = 250\r\n>       local numEpochs = 1\r\n>\r\n>       --this sets up the true model from which the data is drawn\r\n>       local trueParams = {\r\n>          W = torch.randn(numFeatures,numOutputs),\r\n>          b = torch.randn(numOutputs)\r\n>       }\r\n>\r\n>       --this is used to generate train and test datasets\r\n>       local function genDataset()\r\n>          local dataset = {}\r\n>          for tt = 1,numSamples do\r\n>             local x = torch.randn(1,numFeatures)\r\n>             local y = x*trueParams.W + trueParams.b\r\n>             table.insert(dataset,{x = x,y = y})\r\n>          end\r\n>          return dataset\r\n>       end\r\n>       local trainingSet = genDataset()\r\n>       local testSet = genDataset()\r\n>\r\n>\r\n>       -- define loss for linear regression model\r\n>       local linearRegressionLoss = function(params, x, y)\r\n>          local yhat = x*params.W + params.b\r\n>          local residual = y - yhat\r\n>          local loss = 0.5*torch.sum(torch.cmul(residual,residual))\r\n>          return loss\r\n>       end\r\n>\r\n>       -- gradient of loss\r\n>       local dLinearRegressionLoss = autograd(linearRegressionLoss)\r\n>\r\n>\r\n>       --we learn the regression parameters using SGD with a fixed step size\r\n>       function gradientUpdate(metaParams,params,grads)\r\n>          params.W = params.W + grads.W * -metaParams.learningRate\r\n>          params.b = params.b + grads.b * -metaParams.learningRate\r\n>       end\r\n>\r\n>       --this initializes the params that are optimized by learning.\r\n>       function getInitParams()\r\n>          local p =  {\r\n>             W = torch.zero(torch.Tensor.new(numFeatures,numOutputs)),\r\n>             b = torch.zero(torch.Tensor.new(numOutputs))\r\n>          }\r\n>          return p\r\n>       end\r\n>\r\n>       --this learns a regression model on the train set and evaluates its squared loss on the test set\r\n>       --its behavior depends on the value of learningRate in metaParams\r\n>       function learn(metaParams, trainingSet, testSet)\r\n>          local params = getInitParams()\r\n>          for epoch = 1,numEpochs do\r\n>             for _,sample in ipairs(trainingSet) do\r\n>                local grads, loss = dLinearRegressionLoss(params, sample.x, sample.y)\r\n>                gradientUpdate(metaParams,params,grads)\r\n>             end\r\n>          end\r\n>\r\n>          local testLoss = torch.zero(torch.Tensor(1))\r\n>          local numSamples = #testSet\r\n>          for _,sample in ipairs(testSet) do\r\n>             local g, tL = dLinearRegressionLoss(params,sample.x,sample.y)\r\n>             testLoss = testLoss + tL\r\n>          end\r\n>          return testLoss[1]\r\n>       end\r\n>\r\n>       --this is the gradient of the test loss with respect to the learning rate used on the train data\r\n>       local dLearn = autograd(learn, { inline = false })\r\n>\r\n>       --this just evaluates dLearn for a few choices of the learning rate. Eventually, we would like to learn the learningRate with gradient descent.\r\n>       for _,lr in ipairs({0.001,0.01,0.1}) do\r\n>\r\n>          --this just evaluates the 'learn' function directly, in order to demonstrate that this code path works\r\n>          local testLoss1 = learn({learningRate = lr}, trainingSet, testSet)\r\n>\r\n>          --this evaluates the derivative of the 'learn function.' Currently it is failing.\r\n>          local testLoss2, dMetaParams = dLearn({learningRate = lr}, trainingSet, testSet)\r\n>          print(testLoss2)\r\n>          tester:assert(testLoss1 == testLoss2,\"value computed using 'learn' function does not match value computed by 'dLearn'\")\r\n>       end\r\n>    end\r\n>\r\n> —\r\n> Reply to this email directly or view it on GitHub\r\n> <https://github.com/twitter/torch-autograd/pull/89#issuecomment-174864265>\r\n> .\r\n>\r\n"
}
, {
  "url": "https://api.github.com/repos/twitter/torch-autograd/issues/comments/175187378",
  "html_url": "https://github.com/twitter/torch-autograd/pull/89#issuecomment-175187378",
  "issue_url": "https://api.github.com/repos/twitter/torch-autograd/issues/89",
  "id": 175187378,
  "user": {
    "login": "davidBelanger",
    "id": 1179562,
    "avatar_url": "https://avatars.githubusercontent.com/u/1179562?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/davidBelanger",
    "html_url": "https://github.com/davidBelanger",
    "followers_url": "https://api.github.com/users/davidBelanger/followers",
    "following_url": "https://api.github.com/users/davidBelanger/following{/other_user}",
    "gists_url": "https://api.github.com/users/davidBelanger/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/davidBelanger/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/davidBelanger/subscriptions",
    "organizations_url": "https://api.github.com/users/davidBelanger/orgs",
    "repos_url": "https://api.github.com/users/davidBelanger/repos",
    "events_url": "https://api.github.com/users/davidBelanger/events{/privacy}",
    "received_events_url": "https://api.github.com/users/davidBelanger/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2016-01-26T19:19:53Z",
  "updated_at": "2016-01-26T19:19:53Z",
  "body": "To clarify, my fork has an updated version of the test:\r\nhttps://github.com/twitter/torch-autograd/blob/master/test/test.lua\r\nIf you'd like me to open an new PR for  that, I'd be happy to. My concern is that it's hard to make a well-defined test for it since it's hard to check that values have overflowed (they aren't nan or inf; they're just very big values). \r\n\r\n"
}
]
