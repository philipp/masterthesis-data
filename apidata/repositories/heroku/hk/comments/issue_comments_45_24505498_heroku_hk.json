[
{
  "url": "https://api.github.com/repos/heroku/hk/issues/comments/30867543",
  "html_url": "https://github.com/heroku/hk/pull/45#issuecomment-30867543",
  "issue_url": "https://api.github.com/repos/heroku/hk/issues/45",
  "id": 30867543,
  "user": {
    "login": "kr",
    "id": 4178,
    "avatar_url": "https://avatars.githubusercontent.com/u/4178?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/kr",
    "html_url": "https://github.com/kr",
    "followers_url": "https://api.github.com/users/kr/followers",
    "following_url": "https://api.github.com/users/kr/following{/other_user}",
    "gists_url": "https://api.github.com/users/kr/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/kr/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/kr/subscriptions",
    "organizations_url": "https://api.github.com/users/kr/orgs",
    "repos_url": "https://api.github.com/users/kr/repos",
    "events_url": "https://api.github.com/users/kr/events{/privacy}",
    "received_events_url": "https://api.github.com/users/kr/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-12-18T18:34:02Z",
  "updated_at": "2013-12-18T18:34:02Z",
  "body": "I know you guys are intentionally moving away from unix\r\ndesign, but this still makes me a little sad."
}
, {
  "url": "https://api.github.com/repos/heroku/hk/issues/comments/30872598",
  "html_url": "https://github.com/heroku/hk/pull/45#issuecomment-30872598",
  "issue_url": "https://api.github.com/repos/heroku/hk/issues/45",
  "id": 30872598,
  "user": {
    "login": "bgentry",
    "id": 114033,
    "avatar_url": "https://avatars.githubusercontent.com/u/114033?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/bgentry",
    "html_url": "https://github.com/bgentry",
    "followers_url": "https://api.github.com/users/bgentry/followers",
    "following_url": "https://api.github.com/users/bgentry/following{/other_user}",
    "gists_url": "https://api.github.com/users/bgentry/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/bgentry/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/bgentry/subscriptions",
    "organizations_url": "https://api.github.com/users/bgentry/orgs",
    "repos_url": "https://api.github.com/users/bgentry/repos",
    "events_url": "https://api.github.com/users/bgentry/events{/privacy}",
    "received_events_url": "https://api.github.com/users/bgentry/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-12-18T19:31:35Z",
  "updated_at": "2013-12-18T19:31:35Z",
  "body": "@kr it's been a pretty common point of confusion so far that it's unclear whether some commands actually did anything. Tools like `git` have trained users to expect output from basically any command they run (though less common tools like `hg` have stayed truer to unix design here). And since ~100% of Heroku's users are git users, breaking that expectation would not be easy.\r\n\r\nI'm sympathetic to the notion that some commands shouldn't produce extraneous output. The `create` command is a clear example to me. But commands like `addon-add` have multiple pieces of relevant information to return, such as app name, addon name, and addon plan (which could have been defaulted to). See #46 for more on that. I'm not sure there's a consistent design principle that would satisfy all of our commands.\r\n\r\nIt's made worse in hk by the nature of the `-a` flag and the fact that, due to git remotes, it's not always clear what app the command ran on. That's especially true with commands that take free-form arguments like `run`. Maybe that brings us back to discussing whether that argument should be quoted ala `su -c \"command\"`.\r\n\r\nI just don't think we can ignore these issues and pretend that they won't confuse many (most?) of Heroku's users.\r\n\r\nI also do understand the concern from the perspective of advanced users who know what they're doing, and want the tool to behave in a more \"unix-y\" fashion. The best compromise I can think of is a settings file ala `.gitconfig` that lets you enable a \"unix mode\". That doesn't feel great to me though :neutral_face: I suspect you'll feel the same due to your distaste for options and configs.\r\n\r\nThoughts? /cc @max "
}
, {
  "url": "https://api.github.com/repos/heroku/hk/issues/comments/30874232",
  "html_url": "https://github.com/heroku/hk/pull/45#issuecomment-30874232",
  "issue_url": "https://api.github.com/repos/heroku/hk/issues/45",
  "id": 30874232,
  "user": {
    "login": "parkr",
    "id": 237985,
    "avatar_url": "https://avatars.githubusercontent.com/u/237985?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/parkr",
    "html_url": "https://github.com/parkr",
    "followers_url": "https://api.github.com/users/parkr/followers",
    "following_url": "https://api.github.com/users/parkr/following{/other_user}",
    "gists_url": "https://api.github.com/users/parkr/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/parkr/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/parkr/subscriptions",
    "organizations_url": "https://api.github.com/users/parkr/orgs",
    "repos_url": "https://api.github.com/users/parkr/repos",
    "events_url": "https://api.github.com/users/parkr/events{/privacy}",
    "received_events_url": "https://api.github.com/users/parkr/received_events",
    "type": "User",
    "site_admin": true
  },
  "created_at": "2013-12-18T19:49:52Z",
  "updated_at": "2013-12-18T19:49:52Z",
  "body": "@bgentry One thing you might consider is a `-q/--quiet` flag which will mute this output and can be set as a default via an `env` var (e.g. `HK_QUIET=true`). `git` offers a `-q` flag which only prints out ERROR and FATAL messages."
}
, {
  "url": "https://api.github.com/repos/heroku/hk/issues/comments/30894148",
  "html_url": "https://github.com/heroku/hk/pull/45#issuecomment-30894148",
  "issue_url": "https://api.github.com/repos/heroku/hk/issues/45",
  "id": 30894148,
  "user": {
    "login": "kr",
    "id": 4178,
    "avatar_url": "https://avatars.githubusercontent.com/u/4178?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/kr",
    "html_url": "https://github.com/kr",
    "followers_url": "https://api.github.com/users/kr/followers",
    "following_url": "https://api.github.com/users/kr/following{/other_user}",
    "gists_url": "https://api.github.com/users/kr/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/kr/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/kr/subscriptions",
    "organizations_url": "https://api.github.com/users/kr/orgs",
    "repos_url": "https://api.github.com/users/kr/repos",
    "events_url": "https://api.github.com/users/kr/events{/privacy}",
    "received_events_url": "https://api.github.com/users/kr/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-12-19T00:06:51Z",
  "updated_at": "2013-12-19T00:06:51Z",
  "body": "Yeah the basic unix principle is that a command's output should\r\nbe useful and parsimonious. That's all. So a command whose\r\nwhole purpose is to produce output (for example, ls) should print\r\nexactly the information the user asked for. A command that gets\r\nrun purely for side effects (for example, cp) should produce no\r\noutput at all. If there's an error, the error needs to be described\r\n(on stderr), but if it worked then no output is needed.\r\n\r\nA command that is mainly used for its side effects but also\r\nproduces new information needed by the user (for example,\r\nhk create produces an app name), it should print that info,\r\npreferably unadorned so it's easy to pipe into other tools, like\r\n`hk create | pbcopy`.\r\n\r\nGit is a pretty sketchy interface to imitate. It's inconsistent in\r\nmany areas, including whether to print superfluous output. For\r\nexample, git branch x prints nothing, but git checkout x prints\r\n`Switched to branch 'x'` (which provides no information).\r\nThere's no consistent rule there for hk to use.\r\n\r\nAs for the specific case of hk run, I still argue that `ssh` is the\r\nbest model to follow.\r\n\r\nI understand hk's output has become confusing, so I'm not really\r\nsaying you should change direction. Just wanted to register\r\nsome feedback that this design direction will hurt hk's usability\r\nfor me. I don't know how unusual I am in this respect. I must\r\nadmit I find it hard to understand that people are confused that\r\n\"it's unclear whether some commands actually did anything\".\r\nDo they feel the same way about cp or mv? I certainly understand\r\nthat people are accustomed to the heroku gem, and hk is\r\n*different*, and that's important to respect at least in the short\r\nterm. I'm just a little sad to see long term repeat usability being\r\nsacrificed to clear up short term confusion."
}
, {
  "url": "https://api.github.com/repos/heroku/hk/issues/comments/30897122",
  "html_url": "https://github.com/heroku/hk/pull/45#issuecomment-30897122",
  "issue_url": "https://api.github.com/repos/heroku/hk/issues/45",
  "id": 30897122,
  "user": {
    "login": "bgentry",
    "id": 114033,
    "avatar_url": "https://avatars.githubusercontent.com/u/114033?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/bgentry",
    "html_url": "https://github.com/bgentry",
    "followers_url": "https://api.github.com/users/bgentry/followers",
    "following_url": "https://api.github.com/users/bgentry/following{/other_user}",
    "gists_url": "https://api.github.com/users/bgentry/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/bgentry/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/bgentry/subscriptions",
    "organizations_url": "https://api.github.com/users/bgentry/orgs",
    "repos_url": "https://api.github.com/users/bgentry/repos",
    "events_url": "https://api.github.com/users/bgentry/events{/privacy}",
    "received_events_url": "https://api.github.com/users/bgentry/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-12-19T01:03:50Z",
  "updated_at": "2013-12-19T01:03:50Z",
  "body": "@kr I am certainly sympathetic to your argument, especially this part:\r\n\r\n> I certainly understand that people are accustomed to the heroku gem, and hk is different, and that's important to respect at least in the short term. I'm just a little sad to see long term repeat usability being sacrificed to clear up short term confusion.\r\n\r\nI just think the hurdle is too high with respect to our users' expectations, and especially with the ambiguity that `-a` and git remotes introduce.\r\n\r\nUltimately it may be the case that we can't satisfy all of Heroku's users with a single style. What do you think of @parkr's proposal of a \"quiet mode\" which may in effect be a \"unix mode\"?"
}
, {
  "url": "https://api.github.com/repos/heroku/hk/issues/comments/30901588",
  "html_url": "https://github.com/heroku/hk/pull/45#issuecomment-30901588",
  "issue_url": "https://api.github.com/repos/heroku/hk/issues/45",
  "id": 30901588,
  "user": {
    "login": "kr",
    "id": 4178,
    "avatar_url": "https://avatars.githubusercontent.com/u/4178?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/kr",
    "html_url": "https://github.com/kr",
    "followers_url": "https://api.github.com/users/kr/followers",
    "following_url": "https://api.github.com/users/kr/following{/other_user}",
    "gists_url": "https://api.github.com/users/kr/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/kr/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/kr/subscriptions",
    "organizations_url": "https://api.github.com/users/kr/orgs",
    "repos_url": "https://api.github.com/users/kr/repos",
    "events_url": "https://api.github.com/users/kr/events{/privacy}",
    "received_events_url": "https://api.github.com/users/kr/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-12-19T02:57:21Z",
  "updated_at": "2013-12-19T02:57:21Z",
  "body": "> What do you think of @parkr's proposal of a \"quiet mode\" which may in effect be a \"unix mode\"?\r\n\r\nI'd rather live with the output and have fewer flags to learn.\r\n\r\nIn cases where I *really* want no output (for example in an\r\nif test in a shell script), `hk whatever >/dev/null` works\r\nwell enough."
}
, {
  "url": "https://api.github.com/repos/heroku/hk/issues/comments/30903196",
  "html_url": "https://github.com/heroku/hk/pull/45#issuecomment-30903196",
  "issue_url": "https://api.github.com/repos/heroku/hk/issues/45",
  "id": 30903196,
  "user": {
    "login": "bgentry",
    "id": 114033,
    "avatar_url": "https://avatars.githubusercontent.com/u/114033?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/bgentry",
    "html_url": "https://github.com/bgentry",
    "followers_url": "https://api.github.com/users/bgentry/followers",
    "following_url": "https://api.github.com/users/bgentry/following{/other_user}",
    "gists_url": "https://api.github.com/users/bgentry/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/bgentry/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/bgentry/subscriptions",
    "organizations_url": "https://api.github.com/users/bgentry/orgs",
    "repos_url": "https://api.github.com/users/bgentry/repos",
    "events_url": "https://api.github.com/users/bgentry/events{/privacy}",
    "received_events_url": "https://api.github.com/users/bgentry/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-12-19T03:48:30Z",
  "updated_at": "2013-12-19T03:48:30Z",
  "body": "@kr I certainly wouldn't want it to be required for every command, but possibly as an env or config that can persist between runs."
}
, {
  "url": "https://api.github.com/repos/heroku/hk/issues/comments/30904374",
  "html_url": "https://github.com/heroku/hk/pull/45#issuecomment-30904374",
  "issue_url": "https://api.github.com/repos/heroku/hk/issues/45",
  "id": 30904374,
  "user": {
    "login": "kr",
    "id": 4178,
    "avatar_url": "https://avatars.githubusercontent.com/u/4178?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/kr",
    "html_url": "https://github.com/kr",
    "followers_url": "https://api.github.com/users/kr/followers",
    "following_url": "https://api.github.com/users/kr/following{/other_user}",
    "gists_url": "https://api.github.com/users/kr/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/kr/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/kr/subscriptions",
    "organizations_url": "https://api.github.com/users/kr/orgs",
    "repos_url": "https://api.github.com/users/kr/repos",
    "events_url": "https://api.github.com/users/kr/events{/privacy}",
    "received_events_url": "https://api.github.com/users/kr/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-12-19T04:27:04Z",
  "updated_at": "2013-12-19T04:27:04Z",
  "body": "Eh, it means more surface area, more branches in the code\r\n(and hence more chances for bugs) more documentation to\r\nread, more general background noise. A \"quiet\" flag rarely\r\ncarries its weight."
}
, {
  "url": "https://api.github.com/repos/heroku/hk/issues/comments/30905139",
  "html_url": "https://github.com/heroku/hk/pull/45#issuecomment-30905139",
  "issue_url": "https://api.github.com/repos/heroku/hk/issues/45",
  "id": 30905139,
  "user": {
    "login": "parkr",
    "id": 237985,
    "avatar_url": "https://avatars.githubusercontent.com/u/237985?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/parkr",
    "html_url": "https://github.com/parkr",
    "followers_url": "https://api.github.com/users/parkr/followers",
    "following_url": "https://api.github.com/users/parkr/following{/other_user}",
    "gists_url": "https://api.github.com/users/parkr/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/parkr/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/parkr/subscriptions",
    "organizations_url": "https://api.github.com/users/parkr/orgs",
    "repos_url": "https://api.github.com/users/parkr/repos",
    "events_url": "https://api.github.com/users/parkr/events{/privacy}",
    "received_events_url": "https://api.github.com/users/parkr/received_events",
    "type": "User",
    "site_admin": true
  },
  "created_at": "2013-12-19T04:52:15Z",
  "updated_at": "2013-12-19T04:52:15Z",
  "body": "Fair enough. As long as the output is very clear, it shouldn't make a difference. Any ambiguity in the output will lead to considerable confusion (the user may classify a positive outcome as a negative, or a negative outcome as a positive, as [Matt Staats, Shin Hong, Moonzoo Kim, and Gregg Rothermel found in their 2012 study of user perception of invariants in output](http://dl.acm.org/citation.cfm?doid=2338965.2336776))."
}
, {
  "url": "https://api.github.com/repos/heroku/hk/issues/comments/30911975",
  "html_url": "https://github.com/heroku/hk/pull/45#issuecomment-30911975",
  "issue_url": "https://api.github.com/repos/heroku/hk/issues/45",
  "id": 30911975,
  "user": {
    "login": "kr",
    "id": 4178,
    "avatar_url": "https://avatars.githubusercontent.com/u/4178?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/kr",
    "html_url": "https://github.com/kr",
    "followers_url": "https://api.github.com/users/kr/followers",
    "following_url": "https://api.github.com/users/kr/following{/other_user}",
    "gists_url": "https://api.github.com/users/kr/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/kr/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/kr/subscriptions",
    "organizations_url": "https://api.github.com/users/kr/orgs",
    "repos_url": "https://api.github.com/users/kr/repos",
    "events_url": "https://api.github.com/users/kr/events{/privacy}",
    "received_events_url": "https://api.github.com/users/kr/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-12-19T08:19:15Z",
  "updated_at": "2013-12-19T08:19:15Z",
  "body": "That paper does not appear to be publicly available, but the\r\nabstract seems to be about users in an extremely narrow\r\nand specialized domain. Is it really applicable here?"
}
, {
  "url": "https://api.github.com/repos/heroku/hk/issues/comments/30936583",
  "html_url": "https://github.com/heroku/hk/pull/45#issuecomment-30936583",
  "issue_url": "https://api.github.com/repos/heroku/hk/issues/45",
  "id": 30936583,
  "user": {
    "login": "bgentry",
    "id": 114033,
    "avatar_url": "https://avatars.githubusercontent.com/u/114033?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/bgentry",
    "html_url": "https://github.com/bgentry",
    "followers_url": "https://api.github.com/users/bgentry/followers",
    "following_url": "https://api.github.com/users/bgentry/following{/other_user}",
    "gists_url": "https://api.github.com/users/bgentry/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/bgentry/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/bgentry/subscriptions",
    "organizations_url": "https://api.github.com/users/bgentry/orgs",
    "repos_url": "https://api.github.com/users/bgentry/repos",
    "events_url": "https://api.github.com/users/bgentry/events{/privacy}",
    "received_events_url": "https://api.github.com/users/bgentry/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-12-19T15:21:32Z",
  "updated_at": "2013-12-19T15:21:32Z",
  "body": "If you google for the paper's title, you'll find some copies of it. I skimmed through it and had a hard time finding any concrete recommendations for our situation.\r\n\r\nOn Thu, Dec 19, 2013 at 12:19 AM, Keith Rarick <notifications@github.com>\r\nwrote:\r\n\r\n> That paper does not appear to be publicly available, but the\r\n> abstract seems to be about users in an extremely narrow\r\n> and specialized domain. Is it really applicable here?\r\n> ---\r\n> Reply to this email directly or view it on GitHub:\r\n> https://github.com/heroku/hk/pull/45#issuecomment-30911975"
}
]
