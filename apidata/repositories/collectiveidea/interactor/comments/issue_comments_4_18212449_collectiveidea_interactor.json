[
{
  "url": "https://api.github.com/repos/collectiveidea/interactor/issues/comments/22838651",
  "html_url": "https://github.com/collectiveidea/interactor/issues/4#issuecomment-22838651",
  "issue_url": "https://api.github.com/repos/collectiveidea/interactor/issues/4",
  "id": 22838651,
  "user": {
    "login": "jasonroelofs",
    "id": 3223,
    "avatar_url": "https://avatars.githubusercontent.com/u/3223?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/jasonroelofs",
    "html_url": "https://github.com/jasonroelofs",
    "followers_url": "https://api.github.com/users/jasonroelofs/followers",
    "following_url": "https://api.github.com/users/jasonroelofs/following{/other_user}",
    "gists_url": "https://api.github.com/users/jasonroelofs/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/jasonroelofs/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/jasonroelofs/subscriptions",
    "organizations_url": "https://api.github.com/users/jasonroelofs/orgs",
    "repos_url": "https://api.github.com/users/jasonroelofs/repos",
    "events_url": "https://api.github.com/users/jasonroelofs/events{/privacy}",
    "received_events_url": "https://api.github.com/users/jasonroelofs/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-08-18T20:31:05Z",
  "updated_at": "2013-08-18T20:31:05Z",
  "body": "The \"doing three things and the 2nd one fails\" situation is basically on the interactor writer to handle by writing `#rollback` to handle such cases. Of course the ideal situation is that an Interactor does one thing only, so rollback is also quick and easy."
}
, {
  "url": "https://api.github.com/repos/collectiveidea/interactor/issues/comments/22842496",
  "html_url": "https://github.com/collectiveidea/interactor/issues/4#issuecomment-22842496",
  "issue_url": "https://api.github.com/repos/collectiveidea/interactor/issues/4",
  "id": 22842496,
  "user": {
    "login": "laserlemon",
    "id": 34264,
    "avatar_url": "https://avatars.githubusercontent.com/u/34264?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/laserlemon",
    "html_url": "https://github.com/laserlemon",
    "followers_url": "https://api.github.com/users/laserlemon/followers",
    "following_url": "https://api.github.com/users/laserlemon/following{/other_user}",
    "gists_url": "https://api.github.com/users/laserlemon/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/laserlemon/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/laserlemon/subscriptions",
    "organizations_url": "https://api.github.com/users/laserlemon/orgs",
    "repos_url": "https://api.github.com/users/laserlemon/repos",
    "events_url": "https://api.github.com/users/laserlemon/events{/privacy}",
    "received_events_url": "https://api.github.com/users/laserlemon/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-08-18T22:44:52Z",
  "updated_at": "2013-08-19T13:12:32Z",
  "body": "@brianhempel, I think you've hit on something really important here. In my mind, you're absolutely right. An individual interactor X should behave (as far as the controller's concerned), just like an organizer Y that contains only X. I think that there are two ways to do this:\r\n\r\n1. Change interactors to automatically roll themselves back on failure\r\n2. Change organizers to roll back only the successfully performed interactors, but not the failed interactor\r\n\r\n## Option 1\r\n\r\nIf interactors were changed to roll themselves back on failure, I feel like we may be somewhat encouraging multiple-responsibility interactors. If a developer truly writes a single-purpose interactor, there should really be no need for cleanup on failure.\r\n\r\n## Option 2\r\n\r\nIf organizers were changed to skip rollback for the failed interactor (which we *have* discussed), I think we're encouraging single-purpose interactors. This changes the concept of rollback to only really apply to a successfully performed interactor. This may have the effect of simplifying `rollback` methods by eliminating some of the conditional logic. It may also simplify `perform` methods because the context wouldn't have to be manipulated for the sole purpose of tracking how far into `perform` a potential failure occurs.\r\n\r\nI'm leaning toward option 2, but I'd really like all of your opinions on the matter @brianhempel, @jasonroelofs, @ersatzryan, @emilford, @danielmorrison, @pichot, @tbugai, @albus522, @bryckbost, @gaffneyc and @jcarpenter88, especially since either option would necessitate a major version bump (one day after the initial release)."
}
, {
  "url": "https://api.github.com/repos/collectiveidea/interactor/issues/comments/22892696",
  "html_url": "https://github.com/collectiveidea/interactor/issues/4#issuecomment-22892696",
  "issue_url": "https://api.github.com/repos/collectiveidea/interactor/issues/4",
  "id": 22892696,
  "user": {
    "login": "laserlemon",
    "id": 34264,
    "avatar_url": "https://avatars.githubusercontent.com/u/34264?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/laserlemon",
    "html_url": "https://github.com/laserlemon",
    "followers_url": "https://api.github.com/users/laserlemon/followers",
    "following_url": "https://api.github.com/users/laserlemon/following{/other_user}",
    "gists_url": "https://api.github.com/users/laserlemon/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/laserlemon/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/laserlemon/subscriptions",
    "organizations_url": "https://api.github.com/users/laserlemon/orgs",
    "repos_url": "https://api.github.com/users/laserlemon/repos",
    "events_url": "https://api.github.com/users/laserlemon/events{/privacy}",
    "received_events_url": "https://api.github.com/users/laserlemon/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-08-19T18:18:34Z",
  "updated_at": "2013-08-19T18:18:34Z",
  "body": "Fixed with 7bbeea3cdb5ad1609e3808452746c40f6e83a778."
}
]
