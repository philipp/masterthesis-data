[
{
  "url": "https://api.github.com/repos/elastic/elasticsearch-net/issues/comments/134915579",
  "html_url": "https://github.com/elastic/elasticsearch-net/issues/1541#issuecomment-134915579",
  "issue_url": "https://api.github.com/repos/elastic/elasticsearch-net/issues/1541",
  "id": 134915579,
  "user": {
    "login": "KodrAus",
    "id": 6721458,
    "avatar_url": "https://avatars.githubusercontent.com/u/6721458?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/KodrAus",
    "html_url": "https://github.com/KodrAus",
    "followers_url": "https://api.github.com/users/KodrAus/followers",
    "following_url": "https://api.github.com/users/KodrAus/following{/other_user}",
    "gists_url": "https://api.github.com/users/KodrAus/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/KodrAus/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/KodrAus/subscriptions",
    "organizations_url": "https://api.github.com/users/KodrAus/orgs",
    "repos_url": "https://api.github.com/users/KodrAus/repos",
    "events_url": "https://api.github.com/users/KodrAus/events{/privacy}",
    "received_events_url": "https://api.github.com/users/KodrAus/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-08-26T09:28:13Z",
  "updated_at": "2015-08-26T22:20:27Z",
  "body": "By default, if you're using a DateTime, Elasticsearch will index your dates using the `date_optional_time` format, see details [here](https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping-date-format.html).\r\n\r\n`date_time_no_millis` is actually a better match for the C# date string. It will output results that look like this:\r\n\r\n`2014-01-01T10:00:00+10:00`\r\n\r\nTake the following example:\r\n\r\n```\r\nusing System;\r\nusing System.Collections.Generic;\r\nusing System.Linq;\r\nusing Nest;\r\n\r\nnamespace Test\r\n{\r\n\tpublic class TestWithDate\r\n\t{\r\n\t\tpublic DateTime CreateOn { get; set; }\r\n\t}\r\n\r\n\tpublic class Program\r\n\t{\r\n\t\tstatic void Main(string[] args)\r\n\t\t{\r\n\t\t\tvar client = new ElasticClient(new ConnectionSettings(new Uri(\"http://localhost:9200\")));\r\n\r\n\t\t\tvar doc = new TestWithDate\r\n\t\t\t{\r\n\t\t\t\tCreateOn = new DateTime(2014,1,1).ToLocalTime()\r\n\t\t\t};\r\n\r\n\t\t\t//Set the mapping for the TestWithDate to explicitely use date_time_no_millis\r\n\t\t\tclient.Map<TestWithDate>(m => m\r\n\t\t\t\t.Index(\"test_index_for_date\")\r\n\t\t\t\t.DynamicDateFormats(new[] { \"date_time_no_millis\" })\r\n\t\t\t\t.MapFromAttributes()\r\n\t\t\t);\r\n\r\n\t\t\tclient.Index<TestWithDate>(doc, idx => idx.Index(\"test_index_for_date\"));\r\n\r\n\t\t\tvar results = client.Search<TestWithDate>(body => body\r\n\t\t\t\t.MatchAll()\r\n\t\t\t    .FacetRange<DateTime>(t => t\r\n\t\t\t        .OnField(f => f.CreateOn)\r\n\t\t\t        .Ranges(\r\n\t\t\t            r=>r.From(new DateTime(2014,1,1).ToLocalTime())\r\n\t\t\t        )\r\n\t\t\t    )\r\n\t\t\t);\r\n\r\n\t\t\tConsole.WriteLine(results.Documents.Count() + \" query results\");\r\n\r\n\t\t\tvar facets = results.FacetItems<FacetItem>(p=>p.CreateOn);\r\n\r\n\t\t\tConsole.WriteLine(facets.Count() + \" facet buckets\");\r\n\r\n\t\t\tforeach (DateRange facet in facets)\r\n\t\t\t{\r\n\t\t\t\tConsole.WriteLine(facet.From);\r\n\t\t\t\tConsole.WriteLine(facet.Count + \" document in facet\");\r\n\t\t\t}\r\n\t\t}\r\n\t}\r\n}\r\n```\r\n\r\nWhich will return something like this:\r\n\r\n```\r\n3 query results\r\n1 facet buckets\r\n1/01/2014 10:00:00 AM\r\n3 document in facet\r\n```"
}
, {
  "url": "https://api.github.com/repos/elastic/elasticsearch-net/issues/comments/134917519",
  "html_url": "https://github.com/elastic/elasticsearch-net/issues/1541#issuecomment-134917519",
  "issue_url": "https://api.github.com/repos/elastic/elasticsearch-net/issues/1541",
  "id": 134917519,
  "user": {
    "login": "starckgates",
    "id": 12822162,
    "avatar_url": "https://avatars.githubusercontent.com/u/12822162?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/starckgates",
    "html_url": "https://github.com/starckgates",
    "followers_url": "https://api.github.com/users/starckgates/followers",
    "following_url": "https://api.github.com/users/starckgates/following{/other_user}",
    "gists_url": "https://api.github.com/users/starckgates/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/starckgates/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/starckgates/subscriptions",
    "organizations_url": "https://api.github.com/users/starckgates/orgs",
    "repos_url": "https://api.github.com/users/starckgates/repos",
    "events_url": "https://api.github.com/users/starckgates/events{/privacy}",
    "received_events_url": "https://api.github.com/users/starckgates/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-08-26T09:30:32Z",
  "updated_at": "2015-08-26T09:30:32Z",
  "body": "@KodrAus  OK,I will Try.\r\nThanks a Lot!\r\n"
}
, {
  "url": "https://api.github.com/repos/elastic/elasticsearch-net/issues/comments/134918727",
  "html_url": "https://github.com/elastic/elasticsearch-net/issues/1541#issuecomment-134918727",
  "issue_url": "https://api.github.com/repos/elastic/elasticsearch-net/issues/1541",
  "id": 134918727,
  "user": {
    "login": "KodrAus",
    "id": 6721458,
    "avatar_url": "https://avatars.githubusercontent.com/u/6721458?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/KodrAus",
    "html_url": "https://github.com/KodrAus",
    "followers_url": "https://api.github.com/users/KodrAus/followers",
    "following_url": "https://api.github.com/users/KodrAus/following{/other_user}",
    "gists_url": "https://api.github.com/users/KodrAus/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/KodrAus/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/KodrAus/subscriptions",
    "organizations_url": "https://api.github.com/users/KodrAus/orgs",
    "repos_url": "https://api.github.com/users/KodrAus/repos",
    "events_url": "https://api.github.com/users/KodrAus/events{/privacy}",
    "received_events_url": "https://api.github.com/users/KodrAus/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-08-26T09:32:14Z",
  "updated_at": "2015-08-26T09:32:14Z",
  "body": "I'm fairly sure you can set the date format using NEST's property attributes... But I don't have Intellisense so I can't check :)"
}
, {
  "url": "https://api.github.com/repos/elastic/elasticsearch-net/issues/comments/135114647",
  "html_url": "https://github.com/elastic/elasticsearch-net/issues/1541#issuecomment-135114647",
  "issue_url": "https://api.github.com/repos/elastic/elasticsearch-net/issues/1541",
  "id": 135114647,
  "user": {
    "login": "gmarz",
    "id": 1594777,
    "avatar_url": "https://avatars.githubusercontent.com/u/1594777?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/gmarz",
    "html_url": "https://github.com/gmarz",
    "followers_url": "https://api.github.com/users/gmarz/followers",
    "following_url": "https://api.github.com/users/gmarz/following{/other_user}",
    "gists_url": "https://api.github.com/users/gmarz/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/gmarz/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/gmarz/subscriptions",
    "organizations_url": "https://api.github.com/users/gmarz/orgs",
    "repos_url": "https://api.github.com/users/gmarz/repos",
    "events_url": "https://api.github.com/users/gmarz/events{/privacy}",
    "received_events_url": "https://api.github.com/users/gmarz/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2015-08-26T17:24:26Z",
  "updated_at": "2015-08-26T17:24:26Z",
  "body": "That's correct.  `ElasticProperty` contains a [DateFormat](https://github.com/elastic/elasticsearch-net/blob/develop/src/Nest/Domain/Mapping/Attributes/ElasticPropertyAttribute.cs#L45) field which is used for setting the format.  Also, FYI the default date format is [date_optional_time](https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping-date-format.html#mapping-date-format).\r\n\r\nThanks for the thorough answer @KodrAus !\r\n\r\nClosing this for now, feel free to re-open if necessary @starckgates."
}
]
