[
{
  "url": "https://api.github.com/repos/elastic/elasticsearch/issues/comments/18099013",
  "html_url": "https://github.com/elastic/elasticsearch/issues/3056#issuecomment-18099013",
  "issue_url": "https://api.github.com/repos/elastic/elasticsearch/issues/3056",
  "id": 18099013,
  "user": {
    "login": "clintongormley",
    "id": 56599,
    "avatar_url": "https://avatars.githubusercontent.com/u/56599?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/clintongormley",
    "html_url": "https://github.com/clintongormley",
    "followers_url": "https://api.github.com/users/clintongormley/followers",
    "following_url": "https://api.github.com/users/clintongormley/following{/other_user}",
    "gists_url": "https://api.github.com/users/clintongormley/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/clintongormley/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/clintongormley/subscriptions",
    "organizations_url": "https://api.github.com/users/clintongormley/orgs",
    "repos_url": "https://api.github.com/users/clintongormley/repos",
    "events_url": "https://api.github.com/users/clintongormley/events{/privacy}",
    "received_events_url": "https://api.github.com/users/clintongormley/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-05-18T11:06:05Z",
  "updated_at": "2013-05-18T11:06:05Z",
  "body": "Hiya\r\n\r\nThis is a really interesting question (and thanks for the runnable gist!)\r\n\r\nThe issue is that the nested match_all is only matching docs which have\r\nnested docs. Really, you just care about whether a doc has any review by a\r\nregistered member, because all other products get the score of 3. In order\r\nto do that, we need to expose member.id in the parent doc as well, which we\r\ncan do by adding `include_in_parent: true` to the nested mapping.\r\n\r\nThen we write the query to match docs with member reviews, and calculate\r\nthe score using your script, and docs without member reviews, which get a\r\nscore of 3. For this we use a `bool` query with two clauses. Also, set\r\n`disable_coord` to true, so that the \"pure\" score from each clause comes\r\nthrough. Otherwise the bool query would divide the score by the number of\r\nclauses (ie 2).\r\n\r\n      \"bool\" : {\r\n         \"disable_coord\" : 1,\r\n         \"should\" : [\r\n            { clause to match docs without member reviews },\r\n            { clause to match docs with member reviews }\r\n         ]\r\n      }\r\n\r\nSo the clause WITH member reviews looks like the following:\r\n\r\n{\r\n   \"filtered\" : {\r\n      \"filter\" : {\r\n         \"exists\" : {\r\n            \"field\" : \"review.user.member_id\"\r\n         }\r\n      },\r\n      \"query\" : {\r\n         \"nested\" : {\r\n            \"query\" : {\r\n               \"custom_score\" : {\r\n                  \"script\" : \"doc[\\u0027review.user.member_id\\u0027].empty\r\n? 3 : doc[\\u0027review.rate\\u0027].value\",\r\n                  \"query\" : {\r\n                     \"match_all\" : {}\r\n                  }\r\n               }\r\n            },\r\n            \"score_mode\" : \"avg\",\r\n            \"path\" : \"review\"\r\n         }\r\n      }\r\n   }\r\n}\r\n\r\n\r\nThen, the clause to match docs WITHOUT reviews.  Initially, I wrote this:\r\n\r\n{\r\n   \"constant_score\" : {\r\n      \"boost\" : 3,\r\n      \"filter\" : {\r\n         \"missing\" : {\r\n            \"field\" : \"review.user.member_id\"\r\n         }\r\n      }\r\n   }\r\n}\r\n\r\nBut the score of `3` was being combined with the query norm, and so\r\nreturning values like 0.9xxxx. Weirdly, the custom_score doesn't get\r\ncombined with the query norm.  I'm not sure if that is intentional or not,\r\nbut that's the way it is.  So the way to get a pure score of `3` from the\r\nabove is to wrap it in a custom_score query, and have the script just\r\nreturn `3`:\r\n\r\n{\r\n   \"custom_score\" : {\r\n      \"script\" : \"3\",\r\n      \"query\" : {\r\n         \"constant_score\" : {\r\n            \"filter\" : {\r\n               \"missing\" : {\r\n                  \"field\" : \"review.user.member_id\"\r\n               }\r\n            }\r\n         }\r\n      }\r\n   }\r\n},\r\n\r\nThe full query is here: https://gist.github.com/clintongormley/5604037\r\n\r\nIMPORTANT: you're paying the cost of this calculation at query time, but\r\nall the information is known at index time.  A much better approach would\r\nbe to just calculate the avg rating when you index a document and store it\r\nas a field: `avg_rating`.  Then you can sort by that field.\r\n\r\nclint\r\n\r\n\r\n\r\n\r\nOn 17 May 2013 21:12, Junjun Zhang <notifications@github.com> wrote:\r\n\r\n> I recently ran into a problem of missing documents in query result when\r\n> custom score script is used. After some testing, I found that the problem\r\n> seems occur when the script tries to access a field in a nested doc where a\r\n> particular root document does not contain any such nested doc.\r\n>\r\n> To reproduce the problem, test data and queries can be found here:\r\n> http://goo.gl/iHOc5. The example may not make much sense in real world,\r\n> but the idea is to sort products by average rate from users' review. One\r\n> particular requirement is to always treat anonymous user's rate as 3 and\r\n> assign rate as 3 for products with no reviews.\r\n>\r\n> We can determine whether a user is anonymous or not by checking\r\n> review.user.member_id field is empty or not:\r\n> doc['review.user.member_id'].empty, this seems work fine except that\r\n> products with no reviews are dropped out in the result as the first query\r\n> example shows. Is this a bug? As there is no query/filter that excludes\r\n> documents, shouldn't all documents be returned?\r\n>\r\n> Also, there seems no way to determine whether a review exists or not. The\r\n> second query example shows doc['review'].empty does not work, this makes\r\n> sense because indeed, there is not such field as 'review' under the\r\n> 'product' index, 'review' is a nested document. However, the question\r\n> remains: is there a way to determine the existence of a nested doc?\r\n>\r\n> —\r\n> Reply to this email directly or view it on GitHub<https://github.com/elasticsearch/elasticsearch/issues/3056>\r\n> .\r\n>"
}
, {
  "url": "https://api.github.com/repos/elastic/elasticsearch/issues/comments/22767008",
  "html_url": "https://github.com/elastic/elasticsearch/issues/3056#issuecomment-22767008",
  "issue_url": "https://api.github.com/repos/elastic/elasticsearch/issues/3056",
  "id": 22767008,
  "user": {
    "login": "btiernay",
    "id": 2717578,
    "avatar_url": "https://avatars.githubusercontent.com/u/2717578?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/btiernay",
    "html_url": "https://github.com/btiernay",
    "followers_url": "https://api.github.com/users/btiernay/followers",
    "following_url": "https://api.github.com/users/btiernay/following{/other_user}",
    "gists_url": "https://api.github.com/users/btiernay/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/btiernay/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/btiernay/subscriptions",
    "organizations_url": "https://api.github.com/users/btiernay/orgs",
    "repos_url": "https://api.github.com/users/btiernay/repos",
    "events_url": "https://api.github.com/users/btiernay/events{/privacy}",
    "received_events_url": "https://api.github.com/users/btiernay/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-08-16T13:49:06Z",
  "updated_at": "2013-08-16T13:49:49Z",
  "body": "For the future reader, https://github.com/elasticsearch/elasticsearch/issues/3058 was created to address:\r\n\r\n> Weirdly, the custom_score doesn't get combined with the query norm.  I'm not sure if that is intentional or not, but that's the way it is. "
}
, {
  "url": "https://api.github.com/repos/elastic/elasticsearch/issues/comments/51593095",
  "html_url": "https://github.com/elastic/elasticsearch/issues/3056#issuecomment-51593095",
  "issue_url": "https://api.github.com/repos/elastic/elasticsearch/issues/3056",
  "id": 51593095,
  "user": {
    "login": "clintongormley",
    "id": 56599,
    "avatar_url": "https://avatars.githubusercontent.com/u/56599?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/clintongormley",
    "html_url": "https://github.com/clintongormley",
    "followers_url": "https://api.github.com/users/clintongormley/followers",
    "following_url": "https://api.github.com/users/clintongormley/following{/other_user}",
    "gists_url": "https://api.github.com/users/clintongormley/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/clintongormley/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/clintongormley/subscriptions",
    "organizations_url": "https://api.github.com/users/clintongormley/orgs",
    "repos_url": "https://api.github.com/users/clintongormley/repos",
    "events_url": "https://api.github.com/users/clintongormley/events{/privacy}",
    "received_events_url": "https://api.github.com/users/clintongormley/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2014-08-08T12:11:39Z",
  "updated_at": "2014-08-08T12:11:39Z",
  "body": "Closed in favour of #3495"
}
]
