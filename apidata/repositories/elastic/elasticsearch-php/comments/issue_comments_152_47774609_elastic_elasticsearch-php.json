[
{
  "url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/comments/61740117",
  "html_url": "https://github.com/elastic/elasticsearch-php/issues/152#issuecomment-61740117",
  "issue_url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/152",
  "id": 61740117,
  "user": {
    "login": "polyfractal",
    "id": 1224228,
    "avatar_url": "https://avatars.githubusercontent.com/u/1224228?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/polyfractal",
    "html_url": "https://github.com/polyfractal",
    "followers_url": "https://api.github.com/users/polyfractal/followers",
    "following_url": "https://api.github.com/users/polyfractal/following{/other_user}",
    "gists_url": "https://api.github.com/users/polyfractal/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/polyfractal/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/polyfractal/subscriptions",
    "organizations_url": "https://api.github.com/users/polyfractal/orgs",
    "repos_url": "https://api.github.com/users/polyfractal/repos",
    "events_url": "https://api.github.com/users/polyfractal/events{/privacy}",
    "received_events_url": "https://api.github.com/users/polyfractal/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2014-11-05T00:23:23Z",
  "updated_at": "2014-11-05T00:23:23Z",
  "body": "The reason you are getting these exceptions is because Elasticsearch recognizes that the update would have modified a document that changed since you issued the update, potentially losing data.  It rejects this update for safety reasons and lets your application decide what to do next.\r\n\r\nSoo...it depends on the nature of the updates.  If these are idempotent updates (e.g. it doesn't matter what order they are applied, such as incrementing a counter), you need to use the versioning support in Elasticsearch.  \r\n\r\nYou can read more about versioning here: [\"Optimistic Concurrency Control\"](http://www.elasticsearch.org/guide/en/elasticsearch/guide/current/optimistic-concurrency-control.html) and here: [\"Partial Updates\"](http://www.elasticsearch.org/guide/en/elasticsearch/guide/current/partial-updates.html) (in particular [\"Dealing with Conflicts\"](http://www.elasticsearch.org/guide/en/elasticsearch/guide/current/partial-updates.html#_updates_and_conflicts))\r\n\r\nThe short answer is that if the updates are idempotent, you should to tell Elasticsearch to retry those updates server-side in case of conflict:\r\n\r\n```php\r\n$params['index'] = 'my_index';\r\n$params['type']  = 'my_type';\r\n$params['id']    = 'my_id';\r\n\r\n// This needs to be set >= number of threads, since it might compete against all of them\r\n// in worst case\r\n$params['retry_on_conflict'] = 10;\r\n\r\n$params['body']  = [\r\n  'script' => 'ctx._source.views+=1',\r\n  'upsert' => [\r\n    'views' => 0\r\n  ]\r\n];\r\n$ret = $client->update($params);\r\n```\r\n\r\nIf your update is not idempotent (e.g. changing a name from 'fred' to 'tony'), that's where you will have to let your application catch the exception and deal with it manually, since there is no good automated way to deal with it.  Elasticsearch won't know if 'fred' or 'tony' is the correct value, so the only automated solution is to clobber the old value.\r\n"
}
, {
  "url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/comments/61775108",
  "html_url": "https://github.com/elastic/elasticsearch-php/issues/152#issuecomment-61775108",
  "issue_url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/152",
  "id": 61775108,
  "user": {
    "login": "r0mdau",
    "id": 3662999,
    "avatar_url": "https://avatars.githubusercontent.com/u/3662999?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/r0mdau",
    "html_url": "https://github.com/r0mdau",
    "followers_url": "https://api.github.com/users/r0mdau/followers",
    "following_url": "https://api.github.com/users/r0mdau/following{/other_user}",
    "gists_url": "https://api.github.com/users/r0mdau/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/r0mdau/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/r0mdau/subscriptions",
    "organizations_url": "https://api.github.com/users/r0mdau/orgs",
    "repos_url": "https://api.github.com/users/r0mdau/repos",
    "events_url": "https://api.github.com/users/r0mdau/events{/privacy}",
    "received_events_url": "https://api.github.com/users/r0mdau/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2014-11-05T08:37:35Z",
  "updated_at": "2014-11-05T08:37:35Z",
  "body": "Thank you for your answer.\r\n\r\nThe updates are idempotent.\r\n I will try with the parameter `retry_on_conflict` and I will give you a feedback."
}
, {
  "url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/comments/61855337",
  "html_url": "https://github.com/elastic/elasticsearch-php/issues/152#issuecomment-61855337",
  "issue_url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/152",
  "id": 61855337,
  "user": {
    "login": "r0mdau",
    "id": 3662999,
    "avatar_url": "https://avatars.githubusercontent.com/u/3662999?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/r0mdau",
    "html_url": "https://github.com/r0mdau",
    "followers_url": "https://api.github.com/users/r0mdau/followers",
    "following_url": "https://api.github.com/users/r0mdau/following{/other_user}",
    "gists_url": "https://api.github.com/users/r0mdau/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/r0mdau/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/r0mdau/subscriptions",
    "organizations_url": "https://api.github.com/users/r0mdau/orgs",
    "repos_url": "https://api.github.com/users/r0mdau/repos",
    "events_url": "https://api.github.com/users/r0mdau/events{/privacy}",
    "received_events_url": "https://api.github.com/users/r0mdau/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2014-11-05T18:19:37Z",
  "updated_at": "2014-11-05T18:19:37Z",
  "body": "`retry_on_conflict` is a good option to solve my problem.\r\n\r\nBut take care, we should determine an acceptance level, maybe we have to use `$params['retry_on_conflict'] >= number of threads * 2;` to be quiet."
}
]
