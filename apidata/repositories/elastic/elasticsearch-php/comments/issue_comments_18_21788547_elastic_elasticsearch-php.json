[
{
  "url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/comments/27358155",
  "html_url": "https://github.com/elastic/elasticsearch-php/issues/18#issuecomment-27358155",
  "issue_url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/18",
  "id": 27358155,
  "user": {
    "login": "polyfractal",
    "id": 1224228,
    "avatar_url": "https://avatars.githubusercontent.com/u/1224228?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/polyfractal",
    "html_url": "https://github.com/polyfractal",
    "followers_url": "https://api.github.com/users/polyfractal/followers",
    "following_url": "https://api.github.com/users/polyfractal/following{/other_user}",
    "gists_url": "https://api.github.com/users/polyfractal/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/polyfractal/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/polyfractal/subscriptions",
    "organizations_url": "https://api.github.com/users/polyfractal/orgs",
    "repos_url": "https://api.github.com/users/polyfractal/repos",
    "events_url": "https://api.github.com/users/polyfractal/events{/privacy}",
    "received_events_url": "https://api.github.com/users/polyfractal/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-10-30T01:15:26Z",
  "updated_at": "2013-10-30T11:53:50Z",
  "body": "There are a couple things at play here, and I'll say upfront that I don't have a good answer.  First is a simple rounding problem.  If you change the code to something like this:\r\n\r\n```php\r\n$start = microtime(true);\r\n$test = $this->params['transport'];\r\n$end = microtime(true) - $start;\r\n\r\nprint_r(\"Raw Timing: $end sec\\n\");\r\nprint_r('Rounded: '.round($end, 2).\"\\n\");\r\nprint_r('Rounded then multiplied: '.(round($end, 2) * 1000).\" ms\\n\");\r\nprint_r('True timing: '.($end * 1000).\" ms\\n\");\r\n```\r\n\r\nYou'll see there is a discrepancy between the reported and actual time (Edit: c/p the wrong example, fixed):\r\n\r\n```\r\nRaw Timing: 0.01535597038269 sec\r\nRounded: 0.02\r\nRounded then multiplied: 20 ms\r\nTrue timing: 15.35597038269 ms\r\n```\r\n\r\nSo that is at least part of the problem, although admittedly at most 5ms in either direction :)  \r\n\r\nNext, profiling shows that a good portion of the time is spent loading classes through the autoloader.  If you dump a classmap with Composer, you can shave a few more ms off:\r\n\r\n```\r\n$ composer dump-autoload --optimize\r\nGenerating autoload files\r\n\r\n$ php main.php\r\nRaw Timing: 0.012829065322876 sec\r\nRounded: 0.01\r\nRounded then multiplied: 10 ms\r\nTrue timing: 12.829065322876 ms\r\n```\r\n\r\nLastly, a good 10ms is spent due to the initial [host ping process in StaticConnectionPool](https://github.com/elasticsearch/elasticsearch-php/blob/master/src/Elasticsearch/ConnectionPool/StaticConnectionPool.php#L21).  The key here is that the time is not spent on network transfer/curl as one might expect, but mostly time in the autoloader pulling PHP files off disk:\r\n\r\n![selection_056](https://f.cloud.github.com/assets/1224228/1433785/31b98010-40ff-11e3-8990-a8bc3abe881e.png)\r\n\r\nIf you comment out the `scheduleCheck()` line, the number improve significantly:\r\n\r\n```\r\nRaw Timing: 0.0039072036743164 sec\r\nRounded: 0\r\nRounded then multiplied: 0 ms\r\nTrue timing: 3.9072036743164 ms\r\n```\r\n\r\nBut this number is deceptive, since it is basically avoiding the overhead associated with loading Guzzle and all the associated classes.  If you measure the speed of these two snippets:\r\n\r\n```php\r\n$start = microtime(true);\r\n$client = new Client();     // Includes scheduleCheck()\r\n$end = microtime(true);\r\nprint_r($end - $start);\r\n```\r\n\r\nAnd:\r\n\r\n```php\r\n$start = microtime(true);\r\n$client = new Client();     // Does not include scheduleCheck()\r\n$client->ping();            // But includes a network call after instantiation\r\n$end = microtime(true);\r\nprint_r($end - $start);\r\n```\r\n\r\nYou get identical times:\r\n\r\n```\r\n0.020578145980835\r\n0.020431041717529\r\n```\r\n\r\nWhich basically shows that the cost is simply deferred until the first network option.  So, what to be done?  We could rip Guzzle out and replace it with a \"lighter\" component like my CurlMultiConnection class...but in my benchmarks this is identical to Guzzle for steady-state indexing/search but lacks all the knowledge/bugfixes that Guzzle has baked in.\r\n\r\nWe can remove the scheduleCheck() pinging process that happens when the client is instantiated, but this ultimately just delays the cost until your first network operation.  I'm unsure if that is useful.\r\n\r\nIn practice, a opcode cache like APC or Zend should mitigate most of this performance problem.  The vast majority of the overhead is loading the PHP files themselves, so once the opcode is cached the autoloader no longer plays a big part.  I dont, however, have numbers on this...I can start working on benchmarks to see if my theory is correct.\r\n\r\nI'm definitely open to opinions/suggestions/thoughts.  \r\n\r\nWhat kind of hardware are you running?  Xdebug or APC/Zend enabled?  PHP Version?"
}
, {
  "url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/comments/27453647",
  "html_url": "https://github.com/elastic/elasticsearch-php/issues/18#issuecomment-27453647",
  "issue_url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/18",
  "id": 27453647,
  "user": {
    "login": "andy3rdworld",
    "id": 4616502,
    "avatar_url": "https://avatars.githubusercontent.com/u/4616502?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/andy3rdworld",
    "html_url": "https://github.com/andy3rdworld",
    "followers_url": "https://api.github.com/users/andy3rdworld/followers",
    "following_url": "https://api.github.com/users/andy3rdworld/following{/other_user}",
    "gists_url": "https://api.github.com/users/andy3rdworld/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/andy3rdworld/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/andy3rdworld/subscriptions",
    "organizations_url": "https://api.github.com/users/andy3rdworld/orgs",
    "repos_url": "https://api.github.com/users/andy3rdworld/repos",
    "events_url": "https://api.github.com/users/andy3rdworld/events{/privacy}",
    "received_events_url": "https://api.github.com/users/andy3rdworld/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-10-31T01:01:27Z",
  "updated_at": "2013-10-31T01:01:27Z",
  "body": "Thanks for the detailed response - much appreciated.\r\n\r\nI cited the 40ms tested on a Windows box without APC enabled but with composer autoload already dumped (and xdebug on), and running php 5.4.3.\r\n\r\nMy question was prompted mostly by the difference in time between running something like the following:\r\n\r\n$search_host = 'localhost';\r\n$search_port = '9200';\r\n$baseUri = $search_host.':'.$search_port.'/_stats';\r\n$ch = curl_init();\r\ncurl_setopt($ch, CURLOPT_URL, $baseUri);\r\ncurl_setopt($ch, CURLOPT_PORT, $search_port);\r\n$response = curl_exec($ch);\r\n\r\nvs simpling instantiating the client. This minimal curl example took 10ms (which fetches stats as well) whereas only the client api instantiation took 40ms. On my linux box with APC enabled (sorry about jumping around OS/settings) and xdebug disabled, the curl example ran in 1-2ms whereas the api client instantiation took 7-8ms. Admittedly these are now both very small times, but my use case is auto-complete (using a completion suggestion) and I've been trying to shave off as many milliseconds as possible."
}
, {
  "url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/comments/27480571",
  "html_url": "https://github.com/elastic/elasticsearch-php/issues/18#issuecomment-27480571",
  "issue_url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/18",
  "id": 27480571,
  "user": {
    "login": "polyfractal",
    "id": 1224228,
    "avatar_url": "https://avatars.githubusercontent.com/u/1224228?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/polyfractal",
    "html_url": "https://github.com/polyfractal",
    "followers_url": "https://api.github.com/users/polyfractal/followers",
    "following_url": "https://api.github.com/users/polyfractal/following{/other_user}",
    "gists_url": "https://api.github.com/users/polyfractal/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/polyfractal/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/polyfractal/subscriptions",
    "organizations_url": "https://api.github.com/users/polyfractal/orgs",
    "repos_url": "https://api.github.com/users/polyfractal/repos",
    "events_url": "https://api.github.com/users/polyfractal/events{/privacy}",
    "received_events_url": "https://api.github.com/users/polyfractal/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-10-31T12:09:02Z",
  "updated_at": "2013-10-31T12:09:02Z",
  "body": "Ah, I see.  I agree with you...auto-complete needs to be as fast as possible, so even a few ms make a big difference.\r\n\r\nI'll run some benchmarks on my CurlMultiConnection class and see how much faster startup is compared to Guzzle.  I think I also have a CurlSingleConnection class laying around uncommitted too.  They both need some fixup since I stopped maintaining them a long time ago, but otherwise should work fine.  \r\n\r\nThe client will still be slower than a simple curl, simply because there is _some_ overhead associated with autoloading various client classes, etc.  But with a lighter HTTP connection class I'm hoping we can shave a few more ms off and get it closer to a single curl instantiation.\r\n\r\nI think it makes sense to create a StaticConnectionPool derivative that doesn't ping the initial host list too, for cases where you need absolute speed.  It won't save on autoloading time, but it will save an extra network roundtrip."
}
, {
  "url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/comments/27657838",
  "html_url": "https://github.com/elastic/elasticsearch-php/issues/18#issuecomment-27657838",
  "issue_url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/18",
  "id": 27657838,
  "user": {
    "login": "bennimmo",
    "id": 1629121,
    "avatar_url": "https://avatars.githubusercontent.com/u/1629121?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/bennimmo",
    "html_url": "https://github.com/bennimmo",
    "followers_url": "https://api.github.com/users/bennimmo/followers",
    "following_url": "https://api.github.com/users/bennimmo/following{/other_user}",
    "gists_url": "https://api.github.com/users/bennimmo/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/bennimmo/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/bennimmo/subscriptions",
    "organizations_url": "https://api.github.com/users/bennimmo/orgs",
    "repos_url": "https://api.github.com/users/bennimmo/repos",
    "events_url": "https://api.github.com/users/bennimmo/events{/privacy}",
    "received_events_url": "https://api.github.com/users/bennimmo/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-11-03T23:31:11Z",
  "updated_at": "2013-11-03T23:31:11Z",
  "body": "Just following on from this comment by @polyfractal :\r\n\r\n> \"I think it makes sense to create a StaticConnectionPool derivative that doesn't ping the initial host list too, for cases where you need absolute speed. It won't save on autoloading time, but it will save an extra network roundtrip\"\r\n\r\nI think this is a great idea... Also currently in testing if I were to take a node down the script pauses for a second (time for timeout to occur) which is caused by the scheduleCheck() function. Should the check not only be run when a node is chosen by the selector and If that node is down then move on to another.\r\n\r\nAn example where this improves speed. If there were 3 nodes (and one was down) then only 1 in 3 connections would get the timeout occurring and a stall of the 1 second. Would this not be much better?"
}
, {
  "url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/comments/27705557",
  "html_url": "https://github.com/elastic/elasticsearch-php/issues/18#issuecomment-27705557",
  "issue_url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/18",
  "id": 27705557,
  "user": {
    "login": "polyfractal",
    "id": 1224228,
    "avatar_url": "https://avatars.githubusercontent.com/u/1224228?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/polyfractal",
    "html_url": "https://github.com/polyfractal",
    "followers_url": "https://api.github.com/users/polyfractal/followers",
    "following_url": "https://api.github.com/users/polyfractal/following{/other_user}",
    "gists_url": "https://api.github.com/users/polyfractal/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/polyfractal/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/polyfractal/subscriptions",
    "organizations_url": "https://api.github.com/users/polyfractal/orgs",
    "repos_url": "https://api.github.com/users/polyfractal/repos",
    "events_url": "https://api.github.com/users/polyfractal/events{/privacy}",
    "received_events_url": "https://api.github.com/users/polyfractal/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-11-04T17:40:12Z",
  "updated_at": "2013-11-04T17:40:12Z",
  "body": "@bennimmo Fixed the `StaticConnectionPool` logic.  The previous logic was very poor indeed...it now pings only on the first time a node is used (and some other logic depending on if it has failed previous pings or not)\r\n\r\nAlso opened a new ticket to implement a \"no ping\" connection pool, for cases where you don't want pinging at all.  I'll implement and add tests for that class soon."
}
, {
  "url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/comments/27708298",
  "html_url": "https://github.com/elastic/elasticsearch-php/issues/18#issuecomment-27708298",
  "issue_url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/18",
  "id": 27708298,
  "user": {
    "login": "bennimmo",
    "id": 1629121,
    "avatar_url": "https://avatars.githubusercontent.com/u/1629121?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/bennimmo",
    "html_url": "https://github.com/bennimmo",
    "followers_url": "https://api.github.com/users/bennimmo/followers",
    "following_url": "https://api.github.com/users/bennimmo/following{/other_user}",
    "gists_url": "https://api.github.com/users/bennimmo/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/bennimmo/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/bennimmo/subscriptions",
    "organizations_url": "https://api.github.com/users/bennimmo/orgs",
    "repos_url": "https://api.github.com/users/bennimmo/repos",
    "events_url": "https://api.github.com/users/bennimmo/events{/privacy}",
    "received_events_url": "https://api.github.com/users/bennimmo/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-11-04T18:12:55Z",
  "updated_at": "2013-11-04T18:12:55Z",
  "body": "@polyfractal this looks really good... I will run through it in a little more detail this evening and do some tests. \r\n\r\nGreat turn around time though. Loving ElasticSearch right now!!"
}
, {
  "url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/comments/27709863",
  "html_url": "https://github.com/elastic/elasticsearch-php/issues/18#issuecomment-27709863",
  "issue_url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/18",
  "id": 27709863,
  "user": {
    "login": "polyfractal",
    "id": 1224228,
    "avatar_url": "https://avatars.githubusercontent.com/u/1224228?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/polyfractal",
    "html_url": "https://github.com/polyfractal",
    "followers_url": "https://api.github.com/users/polyfractal/followers",
    "following_url": "https://api.github.com/users/polyfractal/following{/other_user}",
    "gists_url": "https://api.github.com/users/polyfractal/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/polyfractal/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/polyfractal/subscriptions",
    "organizations_url": "https://api.github.com/users/polyfractal/orgs",
    "repos_url": "https://api.github.com/users/polyfractal/repos",
    "events_url": "https://api.github.com/users/polyfractal/events{/privacy}",
    "received_events_url": "https://api.github.com/users/polyfractal/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-11-04T18:31:21Z",
  "updated_at": "2013-11-04T18:31:21Z",
  "body": "Great!  Let me know if you run into any strange problems, or some edge case the tests don't cover!  \r\n\r\nBTW, the timeout values (60s, max of 1hr) match the other clients...but it may make sense to reduce them for PHP.  I doubt there are many PHP scripts running for 1+ hrs, so the ping timeouts may need to be set a bit more aggressively to compensate.  In practice I suspect that the timeouts will not get used often, since a script will either succeed and exit, or fail all nodes and die.  Curious to hear your thoughts.\r\n\r\n@andy3rdworld Still working on your original problem.  Will update this ticket when I have an update."
}
, {
  "url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/comments/27889212",
  "html_url": "https://github.com/elastic/elasticsearch-php/issues/18#issuecomment-27889212",
  "issue_url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/18",
  "id": 27889212,
  "user": {
    "login": "polyfractal",
    "id": 1224228,
    "avatar_url": "https://avatars.githubusercontent.com/u/1224228?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/polyfractal",
    "html_url": "https://github.com/polyfractal",
    "followers_url": "https://api.github.com/users/polyfractal/followers",
    "following_url": "https://api.github.com/users/polyfractal/following{/other_user}",
    "gists_url": "https://api.github.com/users/polyfractal/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/polyfractal/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/polyfractal/subscriptions",
    "organizations_url": "https://api.github.com/users/polyfractal/orgs",
    "repos_url": "https://api.github.com/users/polyfractal/repos",
    "events_url": "https://api.github.com/users/polyfractal/events{/privacy}",
    "received_events_url": "https://api.github.com/users/polyfractal/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-11-06T16:36:36Z",
  "updated_at": "2013-11-06T16:36:36Z",
  "body": "Just making a note:  in addition to implementing a \"lightweight\" connection class, I'm going to investigate if we can use some of the tricks that Symfony uses:  http://symfony.com/doc/current/book/performance.html\r\n\r\nIn particular, the dependency bootstrap file and caching the autoloader itself."
}
, {
  "url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/comments/28328402",
  "html_url": "https://github.com/elastic/elasticsearch-php/issues/18#issuecomment-28328402",
  "issue_url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/18",
  "id": 28328402,
  "user": {
    "login": "polyfractal",
    "id": 1224228,
    "avatar_url": "https://avatars.githubusercontent.com/u/1224228?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/polyfractal",
    "html_url": "https://github.com/polyfractal",
    "followers_url": "https://api.github.com/users/polyfractal/followers",
    "following_url": "https://api.github.com/users/polyfractal/following{/other_user}",
    "gists_url": "https://api.github.com/users/polyfractal/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/polyfractal/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/polyfractal/subscriptions",
    "organizations_url": "https://api.github.com/users/polyfractal/orgs",
    "repos_url": "https://api.github.com/users/polyfractal/repos",
    "events_url": "https://api.github.com/users/polyfractal/events{/privacy}",
    "received_events_url": "https://api.github.com/users/polyfractal/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-11-12T20:10:03Z",
  "updated_at": "2013-11-12T20:10:03Z",
  "body": "@andy3rdworld So I updated the self-contained connection class and have some promising results.  These timings include the recent changes to the StaticConnectionPool (not pinging hosts, etc).  All done on my laptop with xdebug turned off, no APC because it is command line:\r\n\r\n**Guzzle**\r\n```\r\nNo Network\r\n> 7.095 ms\r\n\r\nNetwork Ping\r\n> 15.856 ms\r\n```\r\n\r\n**CurlMultiConnection**\r\n```\r\nNo Network\r\n> 4.345 ms\r\n\r\nNetwork Ping\r\n> 7.122 ms\r\n```\r\n\r\nSo the self-contained curl class basically cuts initialization of both the client and the networking components in half.  Note: the steady-state query speed is unaffected (they both perform identically) since this latency is almost entirely caused by autoloader speed.\r\n\r\nI still would still recommend using the default Guzzle class for everything that you can, since the Guzzle author has had much more time finding strange edge-cases and bugs.  It is going to be more robust.  But if you absolutely need blistering speed for autocomplete, you may consider this class.  To enable it:\r\n\r\n```php\r\n$params['connectionClass'] = '\\Elasticsearch\\Connections\\CurlMultiConnection';\r\n$client = new Client($params);\r\n```"
}
, {
  "url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/comments/28357540",
  "html_url": "https://github.com/elastic/elasticsearch-php/issues/18#issuecomment-28357540",
  "issue_url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/18",
  "id": 28357540,
  "user": {
    "login": "polyfractal",
    "id": 1224228,
    "avatar_url": "https://avatars.githubusercontent.com/u/1224228?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/polyfractal",
    "html_url": "https://github.com/polyfractal",
    "followers_url": "https://api.github.com/users/polyfractal/followers",
    "following_url": "https://api.github.com/users/polyfractal/following{/other_user}",
    "gists_url": "https://api.github.com/users/polyfractal/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/polyfractal/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/polyfractal/subscriptions",
    "organizations_url": "https://api.github.com/users/polyfractal/orgs",
    "repos_url": "https://api.github.com/users/polyfractal/repos",
    "events_url": "https://api.github.com/users/polyfractal/events{/privacy}",
    "received_events_url": "https://api.github.com/users/polyfractal/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-11-13T02:32:48Z",
  "updated_at": "2013-11-13T02:49:27Z",
  "body": "As a final note, if you are really looking for bleeding-edge performance, consider using [HHVM](http://www.hhvm.com/blog/).  I just ran the test suite on HHVM and everything passes.  Once the JIT is warmed, performance is awesome:\r\n\r\n**Guzzle**\r\n```\r\nNo Network\r\n  > APC:  0.989 ms\r\n  > HHVM: 0.551 ms\r\n\r\nNetwork Ping\r\n  > APC:  3.782 ms\r\n  > HHVM: 2.559 ms\r\n```\r\n\r\n**CurlMultiConnection**\r\n```\r\nNo Network\r\n  > APC:  1.713 ms\r\n  > HHVM: 1.008 ms\r\n\r\nNetwork Ping\r\n  > APC:  1.986 ms\r\n  > HHVM: 1.461 ms\r\n```\r\n\r\nEdit:  Added APC timing for a truly fair comparison"
}
, {
  "url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/comments/28489630",
  "html_url": "https://github.com/elastic/elasticsearch-php/issues/18#issuecomment-28489630",
  "issue_url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/18",
  "id": 28489630,
  "user": {
    "login": "andy3rdworld",
    "id": 4616502,
    "avatar_url": "https://avatars.githubusercontent.com/u/4616502?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/andy3rdworld",
    "html_url": "https://github.com/andy3rdworld",
    "followers_url": "https://api.github.com/users/andy3rdworld/followers",
    "following_url": "https://api.github.com/users/andy3rdworld/following{/other_user}",
    "gists_url": "https://api.github.com/users/andy3rdworld/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/andy3rdworld/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/andy3rdworld/subscriptions",
    "organizations_url": "https://api.github.com/users/andy3rdworld/orgs",
    "repos_url": "https://api.github.com/users/andy3rdworld/repos",
    "events_url": "https://api.github.com/users/andy3rdworld/events{/privacy}",
    "received_events_url": "https://api.github.com/users/andy3rdworld/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-11-14T14:54:09Z",
  "updated_at": "2013-11-14T14:54:09Z",
  "body": "@polyfractal Your code changes are great, very nice improvements. I'll give HHVM a test run as well."
}
, {
  "url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/comments/29359107",
  "html_url": "https://github.com/elastic/elasticsearch-php/issues/18#issuecomment-29359107",
  "issue_url": "https://api.github.com/repos/elastic/elasticsearch-php/issues/18",
  "id": 29359107,
  "user": {
    "login": "polyfractal",
    "id": 1224228,
    "avatar_url": "https://avatars.githubusercontent.com/u/1224228?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/polyfractal",
    "html_url": "https://github.com/polyfractal",
    "followers_url": "https://api.github.com/users/polyfractal/followers",
    "following_url": "https://api.github.com/users/polyfractal/following{/other_user}",
    "gists_url": "https://api.github.com/users/polyfractal/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/polyfractal/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/polyfractal/subscriptions",
    "organizations_url": "https://api.github.com/users/polyfractal/orgs",
    "repos_url": "https://api.github.com/users/polyfractal/repos",
    "events_url": "https://api.github.com/users/polyfractal/events{/privacy}",
    "received_events_url": "https://api.github.com/users/polyfractal/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2013-11-27T04:16:32Z",
  "updated_at": "2013-11-27T04:16:32Z",
  "body": "Going to close this...feel free to comment if you have issues related to the ticket and I'll reopen, or make a new ticket for anything new/related!"
}
]
