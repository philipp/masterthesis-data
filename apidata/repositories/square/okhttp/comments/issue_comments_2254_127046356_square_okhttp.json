[
{
  "url": "https://api.github.com/repos/square/okhttp/issues/comments/172275990",
  "html_url": "https://github.com/square/okhttp/issues/2254#issuecomment-172275990",
  "issue_url": "https://api.github.com/repos/square/okhttp/issues/2254",
  "id": 172275990,
  "user": {
    "login": "swankjesse",
    "id": 133019,
    "avatar_url": "https://avatars.githubusercontent.com/u/133019?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/swankjesse",
    "html_url": "https://github.com/swankjesse",
    "followers_url": "https://api.github.com/users/swankjesse/followers",
    "following_url": "https://api.github.com/users/swankjesse/following{/other_user}",
    "gists_url": "https://api.github.com/users/swankjesse/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/swankjesse/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/swankjesse/subscriptions",
    "organizations_url": "https://api.github.com/users/swankjesse/orgs",
    "repos_url": "https://api.github.com/users/swankjesse/repos",
    "events_url": "https://api.github.com/users/swankjesse/events{/privacy}",
    "received_events_url": "https://api.github.com/users/swankjesse/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2016-01-16T23:51:51Z",
  "updated_at": "2016-01-16T23:51:51Z",
  "body": "We can’t do this generally. The specs call out that certain headers contain `,` characters (specifically `Set-Cookie`) and so combining those is not possible.\r\n\r\nI think the best solution here is for webapp authors to combine with `,` in their applications. In this case your app should return a single header line with multiple values separated by commas."
}
, {
  "url": "https://api.github.com/repos/square/okhttp/issues/comments/172276866",
  "html_url": "https://github.com/square/okhttp/issues/2254#issuecomment-172276866",
  "issue_url": "https://api.github.com/repos/square/okhttp/issues/2254",
  "id": 172276866,
  "user": {
    "login": "rfc2822",
    "id": 156167,
    "avatar_url": "https://avatars.githubusercontent.com/u/156167?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/rfc2822",
    "html_url": "https://github.com/rfc2822",
    "followers_url": "https://api.github.com/users/rfc2822/followers",
    "following_url": "https://api.github.com/users/rfc2822/following{/other_user}",
    "gists_url": "https://api.github.com/users/rfc2822/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/rfc2822/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/rfc2822/subscriptions",
    "organizations_url": "https://api.github.com/users/rfc2822/orgs",
    "repos_url": "https://api.github.com/users/rfc2822/repos",
    "events_url": "https://api.github.com/users/rfc2822/events{/privacy}",
    "received_events_url": "https://api.github.com/users/rfc2822/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2016-01-17T00:09:22Z",
  "updated_at": "2016-01-17T00:30:57Z",
  "body": "> We can’t do this generally. The specs call out that certain headers contain , characters (specifically Set-Cookie) and so combining those is not possible.\r\n\r\nYou're right, RFC 6265 says:\r\n\r\n> Origin servers SHOULD NOT fold multiple Set-Cookie header fields into a single header field.  The usual mechanism for folding HTTP headers fields (i.e., as defined in [RFC2616]) might change the semantics of the Set-Cookie header field because the %x2C (\",\") character is used by Set-Cookie in a way that conflicts with such folding.\r\n\r\nSorry for the noise, caused by believing that HTTP works like described in RFC 2616.\r\n\r\n> I think the best solution here is for webapp authors to combine with , in their applications. In this case your app should return a single header line with multiple values separated by commas.\r\n\r\nI guess I will make a `static HttpUtils.headerConcat()` method that concatenates the `headers()` , and always use that one (except when dealing with `Set-Cookie`).\r\n\r\nDon't you think such a method `response.headerConcat()` (or however you'd like to name it), directly available from okhttp's `Response`, would be very useful for others? Would you accept code for that?"
}
, {
  "url": "https://api.github.com/repos/square/okhttp/issues/comments/172281301",
  "html_url": "https://github.com/square/okhttp/issues/2254#issuecomment-172281301",
  "issue_url": "https://api.github.com/repos/square/okhttp/issues/2254",
  "id": 172281301,
  "user": {
    "login": "swankjesse",
    "id": 133019,
    "avatar_url": "https://avatars.githubusercontent.com/u/133019?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/swankjesse",
    "html_url": "https://github.com/swankjesse",
    "followers_url": "https://api.github.com/users/swankjesse/followers",
    "following_url": "https://api.github.com/users/swankjesse/following{/other_user}",
    "gists_url": "https://api.github.com/users/swankjesse/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/swankjesse/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/swankjesse/subscriptions",
    "organizations_url": "https://api.github.com/users/swankjesse/orgs",
    "repos_url": "https://api.github.com/users/swankjesse/repos",
    "events_url": "https://api.github.com/users/swankjesse/events{/privacy}",
    "received_events_url": "https://api.github.com/users/swankjesse/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2016-01-17T00:57:27Z",
  "updated_at": "2016-01-17T00:57:27Z",
  "body": "This is one of those awkward situations where we have a disagreement between the semantic and physical layers of the protocol. Ideally, application code doesn’t care whether it’s two headers with one value each, or one header with two values separated by commas.\r\n\r\nIn practice it usually doesn’t matter. Very few headers are ever repeated, and when they are there’s typically agreement between client & server on how they’re repeated. For example, the `Accept-Encoding` header almost universally uses commas.\r\n\r\nI’m reluctant to do anything to transform one form into another. It’d be great if the semantic & physical layers were identical, but they aren’t and it’s weird to play favorites."
}
, {
  "url": "https://api.github.com/repos/square/okhttp/issues/comments/172303328",
  "html_url": "https://github.com/square/okhttp/issues/2254#issuecomment-172303328",
  "issue_url": "https://api.github.com/repos/square/okhttp/issues/2254",
  "id": 172303328,
  "user": {
    "login": "rfc2822",
    "id": 156167,
    "avatar_url": "https://avatars.githubusercontent.com/u/156167?v=3",
    "gravatar_id": "",
    "url": "https://api.github.com/users/rfc2822",
    "html_url": "https://github.com/rfc2822",
    "followers_url": "https://api.github.com/users/rfc2822/followers",
    "following_url": "https://api.github.com/users/rfc2822/following{/other_user}",
    "gists_url": "https://api.github.com/users/rfc2822/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/rfc2822/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/rfc2822/subscriptions",
    "organizations_url": "https://api.github.com/users/rfc2822/orgs",
    "repos_url": "https://api.github.com/users/rfc2822/repos",
    "events_url": "https://api.github.com/users/rfc2822/events{/privacy}",
    "received_events_url": "https://api.github.com/users/rfc2822/received_events",
    "type": "User",
    "site_admin": false
  },
  "created_at": "2016-01-17T09:23:19Z",
  "updated_at": "2016-01-17T09:23:19Z",
  "body": "> Very few headers are ever repeated, and when they are there’s typically agreement between client & server on how they’re repeated.\r\n\r\nIn my case, there was a single server that decided to split the header into two DAV headers. I found it only by occasion. So, yes, very few headers are ever repeated, but when they are, there's a big chance that this will be mistreated.\r\n\r\n> I’m reluctant to do anything to transform one form into another.\r\n\r\nNo problem, I'll do it myself. Thanks for your time."
}
]
