{
  "success": true,
  "status": "OK",
  "jsessionid": "F836BBD01A8B7E6B8BAEBECBC89CC3EB",
  "response": {
    "attributionURL": "http://www.glassdoor.com/Reviews/facebook-reviews-SRCH_KE0,8.htm",
    "currentPageNumber": 1,
    "totalNumberOfPages": 1,
    "totalRecordCount": 1,
    "employers": [{
      "id": 40772,
      "name": "Facebook",
      "website": "www.facebook.com",
      "isEEP": true,
      "exactMatch": true,
      "industry": "Internet",
      "numberOfRatings": 1297,
      "squareLogo": "https://media.glassdoor.com/sqll/40772/facebook-squarelogo-1381810479272.png",
      "overallRating": 4.5,
      "ratingDescription": "Very Satisfied",
      "cultureAndValuesRating": "4.5",
      "seniorLeadershipRating": "4.2",
      "compensationAndBenefitsRating": "4.6",
      "careerOpportunitiesRating": "4.3",
      "workLifeBalanceRating": "3.7",
      "recommendToFriendRating": "0.9",
      "sectorId": 10013,
      "sectorName": "Information Technology",
      "industryId": 200063,
      "industryName": "Internet",
      "featuredReview": {
        "id": 3888834,
        "currentJob": false,
        "reviewDateTime": "2014-03-17 21:26:11.827",
        "jobTitle": "Software Development Engineer",
        "location": "Seattle, WA",
        "jobTitleFromDb": "Software Development Engineer",
        "headline": "Open, fast, no bs",
        "pros": "It might be easy to roll your eyes when people from Facebook say how open their culture is, but it's true; it's more open than any other place I've worked at. At a company wide-level, secret projects, public incidents, important non-public business metrics and the like are all openly discussed. You can ask questions about them directly to Zuckerburg at the weekly Q&A. I think the idea is that if everyone is on the same page or at least, differing views are heard, the company will be stronger, and solutions may be offered from a place you didn't expect. This is much different from previous companies I worked at, where discussions on internal email lists would be shut down by some lawyer saying that there's certain things that can't be discussed, and important data is divided up to groups and individuals on a \"need to know\" basis, etc. \r\n\r\nThis culture applies at a lower level too. You feel comfortable giving feedback to each other about each other, about product decisions, about management, etc. The flipside of this openness is that you of course, have to be willing to receive the feedback, you have to recognize that while openness and feedback is highly encouraged, decisions have to get made, and actions and data are more valuable than words. At the higher level, since the company trusts employees with access to so much information, keeping such info confidential from the outside world is taken seriously.\r\n\r\nIt's a great place to work as an engineer. You're given a lot of freedom, but it's also a responsibility to make sure you're doing things that are valuable. You don't get much credit for working hard or being smart if you don't produce valuable output.\r\n\r\nOne cool thing about Facebook, in contrast to other comparable companies (Google, Amazon, and Microsoft, though in truth, FB is much smaller), is how they have a pretty singular focus. Even with the differing areas (including advertising and such), they do a good job of keeping their eye on their mission of connecting as many people as possible. I also think they are way more empathetic to their users than most people give them credit for. It seems like popular opinion has it that FB is arrogant and only cares about its users insomuch as they represent $-signs. From within the company, it didn't feel this way at all. I saw a lot of empathy towards users, and a lot effort spent to improve or things or fix broken things with no direct financial benefit. The strategy is not complex. The thinking is that if they can make FB easier and more fun to use, then more people will use it for more time each day (which will also have a network effect of attracting even more people to use it), then the advertising dollars will follow. Of course, it is true that FB wants to make the audience more accessible to advertisers as well, but there are a lot of people at FB who care about privacy and security.\r\n\r\nThey have really good infrastructure and really great ways to share the infrastructure and code. They have a lot of cool internal tools, and what they've built is really impressive, and more importantly, it helps your team build products faster without having to solve problems that someone else already solved. Every software company tries to do this, but FB seems to have been more successful with it. Perhaps it's because they're still relatively small, but if anything, I can at least say it is very cool while it lasts.\r\n\r\nThe perks and work environment are great, unless you're one of those types that can't stand open office spaces. I've worked in both a private office and open offices in multiple companies. While I do think a private office has some benefits, I mostly think it's a personal luxury for the employee and a huge waste of money for the company. I'd much rather have the money go into other areas like salary, benefits, and other workplace improvements rather than the added real estate necessary to have offices.\r\n\r\nOf course, you've heard about the food and snacks. They have an amazing selection of great stuff, and what I like about it is that it sort of goes above and beyond expectations. Sure, some days, lunch is better than others, but I really can't complain, and the selection of drinks and snacks is amazing. It's not like you should work at FB just because of that, but it demonstrates FB's desire to make work as fun and convenient as possible.\r\n\r\nYou'll be surrounded by people who like being there. I can't think of a better environment to work in. If you have a giant ego, you may not like it as much. Respect is definitely given to those that have deep experience in the industry, and they are expected to lead others and mentor more junior employees. However, if for whatever reason, you can't perform at the level expected, no one is going to care if you did this and that at Google or shipped ten things at MS, etc.\r\n\r\nFB also has a lot of fun events, and I made a lot of friends working there, so going to the events was fun. Also, if you're older and worried that FB is just a bunch of 22-year-olds, and that you won't fit in, I wouldn't worry about that. FB does have a lot of young employees (who are really smart btw), and it does hire a lot of people straight out of college, but it also attracts a lot of experienced engineers from other top companies like Google, MS, Amazon, etc.\r\n\r\nWork-life-balance seemed totally normal to me. It may be different depending on your team, but I felt you could do 40-50 hours of work a week for the most part and you would be totally fine. It's about what you produce, not how hard you're working. Other team members who had children would work normal hours and go home at normal times. I didn't see any of these folks have a problem when they left early to take care of their child or things like that. Of course, there could be times that people are expected to work extra if something critical happens, but for the most part everyone wants to avoid this and this happens sparingly, from what I observed. Now, there were many times where I chose to work late myself, but I never felt any pressure to do so. The caveat is that there are on-call rotations, and in addition, even if you are not on-call, you are expected to be reasonably available if the on-call person needs your help. However, again, no one wants this, and your team will work on ways to avoid these situations.\r\n\r\nThe best thing I can say is that working at FB is about productivity. I didn't experience and political bs and it was a pleasure working with a group of people who were all concerned with producing a good product and making the best of the time spent while doing it.",
        "cons": "FB expects a lot out of engineers, and you can't slack off. Of course, you shouldn't slack off at any job, but since FB is pretty fast-paced, there is a risk that you'll have trouble adjusting at first.\r\n\r\nFB has a lot of custom infrastructure and tools, and prehaps more impressively, it works great. It makes doing your job really great, but on the other hand, you'll end up learning a lot of stuff that won't be applicable elsewhere.\r\n\r\nFB's code-base is very good in some ways, but in other ways, it's not as great as some of the existing engineers think it is. I don't think this is that big of a deal, but the important part is that as an engineer, you need to quickly learn FB's values and practices and \"get with the program\" so to speak. If you don't like some things, then you just have to deal with it, as it's not likely you're going to change people's minds at this point. The nice thing is that things are at least very consistent.",
        "overall": 5,
        "overallNumeric": 5
      },
      "ceo": {
        "name": "Mark Zuckerberg",
        "title": "CEO",
        "numberOfRatings": 1052,
        "pctApprove": 97,
        "pctDisapprove": 3,
        "image": {
          "src": "https://media.glassdoor.com/people/sqll/40772/facebook-mark-zuckerberg.png",
          "height": 200,
          "width": 200
        }
      }
    }]
  }
}