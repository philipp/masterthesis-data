# API and processed CSV data

As part of my theses I'll provide all collected GitHub api data and processed data in this repository. The is a work in progress, expected to be finished end of march 2016.

	* api data from
		* GitHub
		* Glassdoor
	* csv files
		* processed repository and organization data
		* git logs (repositories)

## Dates of receiving data

	* Search Results for most popular projects, GitHub API [2016-01-21]
		- apidata/top_repos_by_language/
	* Issues of repositories (of commercial organizations), GitHub API [2016-01-30]
		- apidata/repositories/*/repositories_:org.json
		- apidata/repositories/*/*/issues_:full_name.json
	* Git Log files in csv files (of commercial organizations, which are none-forks) [2016-01-25 - 2016-01-27]
		- csv/repositories/logs/logs_:full_name.csv

Processing and analyzing scripts will be provided in separate repository.

(P) 2015 - 2016 by Philipp Staender <philipp.staender@rwth-aachen.de>
